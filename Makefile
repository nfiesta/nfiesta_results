#nfiesta_results/Makefile

EXTENSION = nfiesta_results # name of extension
MODULEDIR = nfiesta_results_dir
DATA = nfiesta_results--1.0.0.sql \
	nfiesta_results--1.0.0--1.0.1.sql \
	nfiesta_results--1.0.1--1.0.2.sql \
	nfiesta_results--1.0.2--1.0.3.sql \
	nfiesta_results--1.0.3--1.0.4.sql \
	nfiesta_results--1.0.4--1.0.5.sql \
	nfiesta_results--1.0.5--1.0.6.sql \
	nfiesta_results--1.0.6--1.0.7.sql \
	nfiesta_results--1.0.7--1.0.8.sql \
	nfiesta_results--1.0.8--1.0.9.sql \
	nfiesta_results--1.0.9--1.0.10.sql \
	nfiesta_results--1.0.10--1.0.11.sql \
	nfiesta_results--1.0.11--1.0.12.sql \
	nfiesta_results--1.0.12--1.0.13.sql \
	nfiesta_results--1.0.13--1.0.14.sql \
	nfiesta_results--1.0.14--1.0.15.sql

REGRESS = install

export SRC_DIR := $(shell pwd)

installcheck-all:
	make installcheck REGRESS_OPTS="--dbname=contrib_regression_web"
	make installcheck-data
    
installcheck-data:
	psql -d postgres -c "drop database if exists contrib_regression_web_data;" -c "create database contrib_regression_web_data template contrib_regression_web;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_web_data" REGRESS="import_data api_fce"    

purge:
	rm -rf /usr/share/postgresql/12/nfiesta_results_dir /usr/share/postgresql/12/extension/nfiesta_results.control

# postgres build
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

# Databázové schéma pro uložení dat výstupů Národní Inventarizace Lesů ČR pro webovou prezentaci

# Závisí na extenzi:
* PostGIS, využívá datový typ geom.

# Závisí na skupinových rolích:
* adm_nfi_data, usr_nfi_data, které musí na serveru existovat před instalací extenze.
/*
	CREATE ROLE adm_nfi_data NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
	CREATE ROLE usr_nfi_data NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
*/

# Při instalaci extenze dojde k nastavení oprávnění:
* skupinové roli adm_nfi_data: všechna oprávnění ke všem objektům extenze,
* skupinové roli usr_nfi_data: oprávnění číst data všech objektů extenze (SELECT),
* uvedené skupinové role mají oprávnění vypnout/zapnout triggery (TRIGGER), a oprávnění odkazovat se na data extenze v cizích klíčích (REFERENCES),
* uvedené skupinové role mají oprávnění USAGE pro objekt celého databázového schématu nfi_results4web.

# Instalace extenze (Linux):
* Spustit Makefile (sudo make install)

# Instalace extenze (Windows):
* Zkopírovat soubor nfiesta_results.control a všechny *.sql soubory do složky
extension (např. C:\Program Files\PostgreSQL\9.6\share\extension)

# Zavedení extenze a update na aktuální verzi:
/*
	DROP EXTENSION IF EXISTS nfiesta_results;
	CREATE EXTENSION nfiesta_results;
	ALTER EXTENSION nfiesta_results UPDATE TO '1.0.0';
*/
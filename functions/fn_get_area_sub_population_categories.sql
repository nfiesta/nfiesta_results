--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta_results.fn_get_area_sub_population_categories(integer, integer, integer, integer)
--DROP FUNCTION nfiesta_results.fn_get_area_sub_population_categories(integer, integer, integer, integer);
CREATE OR REPLACE FUNCTION nfiesta_results.fn_get_area_sub_population_categories(
		_target_variable integer, _area_or_sub_pop integer, _id integer, _lang integer)
RETURNS TABLE (
variable 	integer,
attype 		integer,
category 	integer,
label		varchar,
description	text,
label_en	varchar,
description_en	text
)
AS
$function$
DECLARE
	_lang_label					varchar;
	_lang_column_label			text;
	_lang_column_description	text;
BEGIN

IF _target_variable IS NULL THEN
	RAISE WARNING 'Target variable is NULL.';
	RETURN QUERY SELECT NULL::int, NULL::int, NULL::int, NULL::varchar, NULL::text, NULL::varchar, NULL::text;
END IF;

IF _area_or_sub_pop IS NULL
THEN
	RAISE EXCEPTION 'Parameter area domain or sub_population is not choosen!';
END IF; 

IF _area_or_sub_pop NOT IN (100,200)
THEN
	RAISE EXCEPTION 'Parameter area domain or sub_population has to be either 100 (area_domain) or 200 (sub_population)!';
END IF; 

IF _target_variable IS NOT NULL AND NOT EXISTS (SELECT id FROM nfiesta_results.c_target_variable WHERE id = _target_variable)
THEN
	RAISE EXCEPTION 'Specified target_variable (%) does not exist in table c_target_variable!', _target_variable;
END IF;

IF _target_variable IS NOT NULL AND NOT EXISTS (
	SELECT t1.id
	FROM nfiesta_results.t_variable AS t1
	WHERE t1.target_variable = _target_variable)
THEN 
	RAISE EXCEPTION 'Specified target_variable(%) does not exist in the data (t_variable)!', _target_variable;
END IF;

---------------------------------------
_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);

if _lang_label = 'en'::varchar(2)
then
	_lang_column_label = 't2.label_en';
	_lang_column_description = 't2.description_en';
else
	_lang_column_label = 't4.label';
	_lang_column_description = 't4.description';
end if;
---------------------------------------

CASE WHEN _area_or_sub_pop = 100
THEN
	IF _id IS NOT NULL AND _id != 0 AND NOT EXISTS (SELECT id FROM nfiesta_results.c_area_domain WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Specified area_domain (%) does not exist in table c_area_domain!', _id;
	END IF;

	IF _id IS NOT NULL AND NOT EXISTS (
		SELECT t1.id
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.c_area_domain_category AS t2
		ON t1.area_domain_category = t2.id
		WHERE t1.target_variable = _target_variable AND
			CASE WHEN _id != 0 THEN t2.area_domain = _id
			WHEN _id = 0 THEN t2.area_domain IS NULL	
			ELSE true END)
	THEN 
		RAISE EXCEPTION 'Specified combination of target_variable(%) and area_domain (%) does not exist in the data (t_variable)!', _target_variable, _id;
	END IF;

	RETURN QUERY EXECUTE
	'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
		SELECT
			t1.target_variable,
			coalesce(t2.variable_superior,t1.id) AS variable_superior,
			coalesce(t2.variable, t1.id) AS variable
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t2.variable = t1.id
		LEFT JOIN nfiesta_results.c_area_domain_category AS t3
		ON t1.area_domain_category = t3.id
		WHERE 	t1.target_variable = $1 AND
			CASE WHEN $2 IS NOT NULL AND $2 != 0 THEN t3.area_domain = $2 ELSE t1.area_domain_category IS NULL END
		UNION ALL
		SELECT
			t1.target_variable,
			t2.variable_superior,
			t2.variable
		FROM w_variables AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t1.variable_superior = t2.variable
		WHERE t2.variable IS NOT NULL
	)
	, w_attr_types AS (
		SELECT
			t1.target_variable,
			t1.variable_superior,
			t1.variable, 
			t3.area_domain AS area_domain_sup,
			t5.area_domain, 
			t3.id AS id_cat_sup,
			--t3.label AS category_sup,
			t5.id AS id_cat
			--t5.label AS category
		FROM w_variables AS t1
		INNER JOIN
			nfiesta_results.t_variable AS t2
		ON 
			t1.variable_superior = t2.id
		LEFT JOIN
			nfiesta_results.c_area_domain_category AS t3
		ON t2.area_domain_category = t3.id
		INNER JOIN
			nfiesta_results.t_variable AS t4
		ON 
			t1.variable = t4.id
		LEFT JOIN
			nfiesta_results.c_area_domain_category AS t5
		ON t4.area_domain_category = t5.id
	),
	w_union AS (
		SELECT	target_variable, variable_superior AS variable,  area_domain_sup AS area_domain
		FROM	w_attr_types
		UNION ALL
		SELECT	target_variable, variable,  area_domain
		FROM	w_attr_types
	)
	,w_distinct AS (
		SELECT DISTINCT target_variable, area_domain
		FROM w_union
	)
	SELECT
		t3.id AS variable,
		t2.area_domain AS type,
		t2.id AS category,
		'||_lang_column_label||' as label,
		'||_lang_column_description||' as description,
		t2.label_en,
		t2.description_en
	FROM
		w_distinct AS t1
	LEFT JOIN
		nfiesta_results.c_area_domain_category AS t2
	ON 	t1.area_domain = t2.area_domain
	-----------------------------------
	left join	(
				select cadcl.* from nfiesta_results.c_area_domain_category_language as cadcl
				where cadcl.language = $3
				and cadcl.area_domain_category in	(
													select cadc.id from nfiesta_results.c_area_domain_category as cadc
													where cadc.area_domain in (select w_distinct.area_domain from w_distinct)
													)
				) as t4
	on t2.id = t4.area_domain_category
	-----------------------------------
	INNER JOIN
		nfiesta_results.t_variable AS t3
	ON t1.target_variable = t3.target_variable AND
	CASE WHEN t2.id IS NOT NULL THEN t3.area_domain_category = t2.id
	ELSE t3.area_domain_category IS NULL END
	ORDER BY t3.id
	'
	USING _target_variable, _id, _lang;

WHEN _area_or_sub_pop = 200
THEN
	IF _id IS NOT NULL AND _id != 0 AND NOT EXISTS (SELECT id FROM nfiesta_results.c_sub_population WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Specified sub_population (%) does not exist in table c_sub_population!', _id;
	END IF;

	IF _id IS NOT NULL AND NOT EXISTS (
		SELECT t1.id
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.c_sub_population_category AS t2
		ON t1.sub_population_category = t2.id
		WHERE t1.target_variable = _target_variable AND
			CASE WHEN _id != 0 THEN t2.sub_population = _id
			WHEN _id = 0 THEN t2.sub_population IS NULL	
			ELSE true END)
	THEN 
		RAISE EXCEPTION 'Specified combination of target_variable(%) and sub_population (%) does not exist in the data (t_variable)!', _target_variable, _id;
	END IF;

	RETURN QUERY EXECUTE
	'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
		SELECT
			t1.target_variable,
			coalesce(t2.variable_superior,t1.id) AS variable_superior,
			coalesce(t2.variable, t1.id) AS variable
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t2.variable = t1.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t3
		ON t1.sub_population_category = t3.id
		WHERE 	t1.target_variable = $1 AND
			CASE WHEN $2 IS NOT NULL AND $2 != 0 THEN t3.sub_population = $2 ELSE t1.sub_population_category IS NULL END
		UNION ALL
		SELECT
			t1.target_variable,
			t2.variable_superior,
			t2.variable
		FROM w_variables AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t1.variable_superior = t2.variable
		WHERE t2.variable IS NOT NULL
	)
	, w_attr_types AS (
		SELECT
			t1.target_variable,
			t1.variable_superior,
			t1.variable, 
			t3.sub_population AS sub_population_sup,
			t5.sub_population, 
			t3.id AS id_cat_sup,
			--t3.label AS category_sup,
			t5.id AS id_cat
			--t5.label AS category
		FROM w_variables AS t1
		INNER JOIN
			nfiesta_results.t_variable AS t2
		ON 
			t1.variable_superior = t2.id
		LEFT JOIN
			nfiesta_results.c_sub_population_category AS t3
		ON t2.sub_population_category = t3.id
		INNER JOIN
			nfiesta_results.t_variable AS t4
		ON 
			t1.variable = t4.id
		LEFT JOIN
			nfiesta_results.c_sub_population_category AS t5
		ON t4.sub_population_category = t5.id
	),
	w_union AS (
		SELECT	target_variable, variable_superior AS variable,  sub_population_sup AS sub_population
		FROM	w_attr_types
		UNION ALL
		SELECT	target_variable, variable,  sub_population
		FROM	w_attr_types
	)
	,w_distinct AS (
		SELECT DISTINCT target_variable, sub_population
		FROM w_union
	)
	SELECT
		t3.id AS variable,
		t2.sub_population AS type,
		t2.id AS category,
		'||_lang_column_label||' as label,
		'||_lang_column_description||' as description,
		t2.label_en,
		t2.description_en
	FROM
		w_distinct AS t1
	LEFT JOIN
		nfiesta_results.c_sub_population_category AS t2
	ON 	t1.sub_population = t2.sub_population
	-----------------------------------
	left join	(
				select cspcl.* from nfiesta_results.c_sub_population_category_language as cspcl
				where cspcl.language = $3
				and cspcl.sub_population_category in	(
														select cspc.id from nfiesta_results.c_sub_population_category as cspc
														where cspc.sub_population in (select w_distinct.sub_population from w_distinct)
														)
				) as t4
	on t2.id = t4.sub_population_category
	-----------------------------------
	INNER JOIN
		nfiesta_results.t_variable AS t3
	ON t1.target_variable = t3.target_variable AND
	CASE WHEN t2.id IS NOT NULL THEN t3.sub_population_category = t2.id
	ELSE t3.sub_population_category IS NULL END
	ORDER BY t3.id'
	USING _target_variable, _id, _lang;
ELSE
	RAISE EXCEPTION 'Uknown attribute type!';
END CASE;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta_results.fn_get_area_sub_population_categories(integer,integer,integer,integer) IS 'Function returns table with all hierarchically superior variables and its complementary categories within area domain or sub_population. Input parameters are target_variable (id from table c_target_variable), area_or_sub_pop (100 for area_domain, 200 for sub_population) and id (id from table c_area_domain or id from table c_sub_population)';

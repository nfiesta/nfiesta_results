--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_gui_header
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_gui_header(character varying, character varying);

create or replace function nfiesta_results.fn_get_gui_header
(
	_jlang character varying,
	_header character varying
)
  returns varchar as
$$
declare
	_lang			integer;
	_lang_label		varchar;
	_res			varchar;
begin
	----------------------------------
	if _jlang is null
	then
		raise exception 'Error 01: fn_get_gui_header: Input argument _jlang must not be NULL !';
	end if;
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	-----------------------------------------------------------------
	if _lang_label = 'en'::varchar(2)
	then
		select cgh.label_en from nfiesta_results.c_gui_header as cgh
		where cgh.header = _header
		into _res;
	else
		select cghl.label from nfiesta_results.c_gui_header_language as cghl
		where cghl.gui_header = (select cgh.id from nfiesta_results.c_gui_header as cgh where cgh.header = _header)
		and cghl.language = _lang
		into _res;
	end if;
	----------------------------------
	if _res is null
	then
		raise exception 'Error 02: fn_get_gui_header: Output argument _res is NULL! Not exists any gui header label for given jlang = % and header = %!',_jlang, _header;
	end if;	
	----------------------------------
	return _res;
end;
$$
  language plpgsql stable
  cost 100;

comment on function nfiesta_results.fn_get_gui_header(character varying, character varying) is
'The function returns gui header label for given input language and header.';

grant execute on function nfiesta_results.fn_get_gui_header(character varying, character varying) to public;


--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_gui_headers
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_gui_headers(character varying);

create or replace function nfiesta_results.fn_get_gui_headers(_jlang character varying)
  returns json as
$$
declare
	_lang			integer;
	_lang_label		varchar;
	_lang_column	text;
	_res			json;
begin
	----------------------------------
	if _jlang is null
	then
		raise exception 'Error 01: fn_get_gui_headers: Input argument _jlang must not be NULL !';
	end if;
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	if _lang_label = 'en'
	then
		with
		w1 as	(
				select id, "header", label_en from nfiesta_results.c_gui_header
				)
		select json_agg(json_build_object('gui_header',w1.header,'label',w1.label_en)) from w1
		into _res;				
	else
		with
		w1 as	(
				select id, "header", label_en from nfiesta_results.c_gui_header 
				)
		,w2 as	(
				select * from nfiesta_results.c_gui_header_language
				where language = _lang
				)
		,w3 as	(
				select w1.id, w1.header, w1.label_en, w2.label from w1 inner join w2
				on w1.id = w2.gui_header
				)
		,w4 as	(
				select w3.id, w3.header, w3.label as label from w3
				)
		select json_agg(json_build_object('gui_header',w4.header,'label',w4.label)) from w4
		into _res;
	end if;
	----------------------------------
	if _res is null
	then
		raise exception 'Error 02: fn_get_gui_headers: Output argument _res is NULL! Not exists any gui header!';
	end if;	
	----------------------------------
	
	return _res;

end;
$$
  language plpgsql stable
  cost 100;

comment on function nfiesta_results.fn_get_gui_headers(character varying) is
'The function returns gui headers for given input language.';

grant execute on function nfiesta_results.fn_get_gui_headers(character varying) to public;


--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_heighest_estimation_cell
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_heighest_estimation_cell(integer[]);

create or replace function nfiesta_results.fn_get_heighest_estimation_cell(_estimation_cells integer[])
returns integer[]
as
$$
declare
		_res_1	integer;
		_res	integer[];
begin
		if _estimation_cells is null
		then
			raise exception 'Error 01: fn_get_heighest_estimation_cell: The input argument _estimation_cells must not be NULL !';
		end if;	
	
		select cell_superior from nfiesta_results.t_estimation_cell_hierarchy where cell = _estimation_cells[1]
		into _res_1;	
	
		if _res_1 is null
		then
			_res := _estimation_cells;
		else
			_res := nfiesta_results.fn_get_heighest_estimation_cell(array[_res_1] || _estimation_cells);
		end if;
	
	return _res;

end;
$$
language plpgsql
volatile
cost 100
security invoker;


comment on function nfiesta_results.fn_get_heighest_estimation_cell(integer[]) is
'The function returns the heighest estimation cell from t_estimation_cell_hierarchy table for given estimation_cell.';

grant execute on function nfiesta_results.fn_get_heighest_estimation_cell(integer[]) to public;


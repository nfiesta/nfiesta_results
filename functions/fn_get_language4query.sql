--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_language4query
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_language4query(character varying);

create or replace function nfiesta_results.fn_get_language4query(_jlang character varying)
  returns character varying as
$$
declare
	_clang	integer;
	_res	character varying(3);
begin
	----------------------------------
	if _jlang is null
	then
		raise exception 'Error 01: fn_get_language4query: Input argument _jlang must not be NULL !';
	end if;
	----------------------------------
	select count(id) from nfiesta_results.c_language where label = _jlang
	into _clang;
	----------------------------------
	if _clang = 0
	then
		raise exception 'Error 02: fn_get_language4query: For input argument _jlang = % not exist any record in nfi_results4web.c_language table !', _jlang;
	END IF;
	----------------------------------
	if _clang > 1
	then
		raise exception 'Error 03: fn_get_language4query: For input argument _jlang = % exists more records then one in nfi_results4web.c_language table !', _jlang;
	END IF;
	----------------------------------
	select
		case
			when _jlang = 'cs-CZ' then ('')::character varying
			else ('_' || substring(_jlang,1,2))::character varying
		end
	into
		_res;
	----------------------------------
	if _res is null
	then
		raise exception 'Error 04: fn_get_language4query: Output argument _res is NULL!';
	END IF;	
	----------------------------------
	
	return _res;

end;
$$
  language plpgsql stable
  cost 100;

comment on function nfiesta_results.fn_get_language4query(character varying) is
'The function returns suffix from c_language table for given input language.';

grant execute on function nfiesta_results.fn_get_language4query(character varying) to public;


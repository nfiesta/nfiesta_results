--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_language_id
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_language_id(character varying);

create or replace function nfiesta_results.fn_get_language_id
(
	_jlang character varying -- string value from web joomla
)
  returns integer as
$$
declare
	_lang	integer;
begin
	----------------------------------
	if _jlang is null
	then
		raise exception 'Error 01: fn_get_language_id: Input argument _jlang must not be NULL!';
	end if;
	----------------------------------
	select cl.id from nfiesta_results.c_language as cl where cl.label = substring(_jlang,1,2)
	into _lang;
	----------------------------------
	if _lang is null
	then
		raise exception 'Error 02: fn_get_language_id: For input argument _jlang = % not exist any record in c_language table!', _jlang;
	end if;
	----------------------------------
	return _lang;
end;
$$
  language plpgsql stable
  cost 100;

comment on function nfiesta_results.fn_get_language_id(character varying) is
'The function returns ID from c_language table for given input language.';

grant execute on function nfiesta_results.fn_get_language_id(character varying) to public;


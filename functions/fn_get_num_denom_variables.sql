--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta_results.fn_get_num_denom_variables.sql(integer, integer)
--DROP FUNCTION nfiesta_results.fn_get_num_denom_variables.sql(integer, integer);
CREATE OR REPLACE FUNCTION nfiesta_results.fn_get_num_denom_variables(
		_num_var integer[], _denom_var integer[] DEFAULT NULL::int[])
RETURNS TABLE (
variable_numerator 	integer,
variable_denominator 	integer
)
AS
$function$
DECLARE
_denom_var4query	integer[];
BEGIN
	IF _num_var IS NULL
	THEN
		RAISE EXCEPTION 'Given array of numerator variables is NULL!';
	END IF;

	IF (SELECT count(*) FROM nfiesta_results.t_variable WHERE array[id] <@ _num_var) != array_length(_num_var,1)
	THEN
		RAISE EXCEPTION 'Not all of numerator variables are present in table t_variable.';
	END IF;

	IF (SELECT count(*) FROM nfiesta_results.t_variable WHERE array[id] <@ _denom_var) != array_length(_denom_var,1)
	THEN
		RAISE EXCEPTION 'Not all of denominator variables are present in table t_variable.';
	END IF;

	IF array_length(_num_var,1) < array_length(_denom_var,1)
	THEN
		RAISE EXCEPTION 'Given array of denominator variables must be shorter (or equal) then given array of numerator variables.';
	END IF;

	IF _denom_var IS NULL
	THEN _denom_var4query := _num_var;
	ELSE _denom_var4query := _denom_var;
	END IF;

	RETURN QUERY EXECUTE
	'
	WITH RECURSIVE w_var AS (
		SELECT 1 AS iter, variable_superior AS variable_start, variable_superior, variable
		FROM nfiesta_results.t_variable_hierarchy
		WHERE variable_superior = ANY($1)
		UNION ALL
		SELECT iter + 1 AS iter, t1.variable_start, t2.variable_superior, t2.variable
		FROM w_var AS t1
		INNER JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t1.variable = t2.variable_superior
		WHERE NOT ARRAY[t1.variable] <@ $1
	), w_all AS (
		SELECT distinct iter, t1.variable_start, t1.variable_superior, t1.variable, t3.label_en AS lab_start, t7.label_en AS lab_sup, t5.label_en AS lab_var
		FROM w_var AS t1
		INNER JOIN nfiesta_results.t_variable AS t2
		ON t1.variable_start = t2.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t3
		ON t2.sub_population_category = t3.id
		INNER JOIN nfiesta_results.t_variable AS t4
		ON t1.variable = t4.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t5
		ON t4.sub_population_category = t5.id
		INNER JOIN nfiesta_results.t_variable AS t6
		ON t1.variable_superior = t6.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t7
		ON t6.sub_population_category = t7.id
		WHERE variable = ANY($2)
		ORDER BY variable_start, variable
	), w_iter AS(
		SELECT 	variable_start, variable_superior, variable, lab_start, --lab_sup, 
			lab_var,
			iter, min(iter) OVER (partition by variable) AS min_iter
		FROM w_all
	), w_final AS (
		SELECT variable_start AS denom, variable AS num
		FROM w_iter WHERE iter = min_iter AND
			NOT ARRAY[variable] <@ $1
		UNION ALL
		SELECT t1.var AS denom, t1.var AS num
		FROM unnest($1) AS t1(var)
	)
	SELECT DISTINCT t1.num, CASE WHEN $3 IS NOT NULL THEN t1.denom ELSE NULL::int END AS denom --, t3.label AS lab_denom, t5.label AS lab_num
	FROM w_final AS t1
	INNER JOIN nfiesta_results.t_variable AS t2
	ON t1.denom = t2.id
	LEFT JOIN nfiesta_results.c_sub_population_category AS t3
	ON t2.sub_population_category = t3.id
	INNER JOIN nfiesta_results.t_variable AS t4
	ON t1.num = t4.id
	LEFT JOIN nfiesta_results.c_sub_population_category AS t5
	ON t4.sub_population_category = t5.id
	ORDER BY denom, t1.num;
	'
	USING _denom_var4query, _num_var, _denom_var;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta_results.fn_get_num_denom_variables(integer[],integer[]) IS 'Function returns table with assigned denominator variables to numerator variables.';

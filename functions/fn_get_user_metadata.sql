--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang										integer;
	_lang_label									varchar;
	_estimate_type								integer;
	_target_variable							integer;
	_target_variable_denom						integer;
	_estimation_period							integer;
	_panel_refyearset_group						integer;
	_phase_estimate_type						integer;
	_phase_estimate_type_denom					integer;
	_estimation_cell_collection					integer;
	_area_domain								integer;
	_area_domain_denom							integer;
	_sub_population								integer;
	_sub_population_denom						integer;
	--_target_variable_num						integer[];
	--_target_variable_denom_check				integer;
	--_target_variable_denom					integer;
	_cond4topic_num								text;
	_cond4topic_denom							text;
	_res_topic_label							text;
	_res_topic_description						text;
	_res_estimation_period_label				text;
	_res_estimation_period_description			text;
	_res_estimation_cell_collection_label		text;
	_res_estimation_cell_collection_description	text;
	_res_estimate_type_label					text;
	_res_estimate_type_description				text;
	_res_unit_num_label							text;
	_res_unit_num_description					text;
	_res_unit_of_measurement_label				text;
	_res_unit_of_measurement_description		text;
	_res										json;

	_estimation_cell_from_hierarchy_for_ecc_select_by_user			integer;
	_estimation_cell_from_hierarchy_for_ecc_select_by_user_superior	integer;
	_heighest_estimation_cell_select_by_user						integer;
	_heighest_estimation_cell_collection_select_by_user				integer;
	_estimation_cell_collection_array								integer[];
	_test_occurence_result_group									integer;	
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_query: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select
			trg.estimate_type,
			trg.target_variable,
			trg.target_variable_denom,
			trg.estimation_period,
			trg.panel_refyearset_group,
			trg.phase_estimate_type,
			trg.phase_estimate_type_denom,
			trg.estimation_cell_collection,
			trg.area_domain,
			trg.area_domain_denom,
			trg.sub_population,
			trg.sub_population_denom
	from
			nfiesta_results.t_result_group as trg
	where
			trg.id = _id_group[1]
	and
			trg.web = true
	into
			_estimate_type,
			_target_variable,
			_target_variable_denom,
			_estimation_period,
			_panel_refyearset_group,
			_phase_estimate_type,
			_phase_estimate_type_denom,
			_estimation_cell_collection,
			_area_domain,
			_area_domain_denom,
			_sub_population,
			_sub_population_denom;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	select count(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom_check;

	if _target_variable_denom_check is distinct from 0 -- target variable denom exists
	then
		if _target_variable_denom_check > 1
		then
			raise exception 'Error: 03: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
		end if;

		select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
		where trg.id in (select unnest(_id_group))
		and trg.web = true
		into _target_variable_denom;		
	else
		_target_variable_denom := null::integer;
	end if;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	if _lang_label = 'en'::varchar(2)
	then
		execute
		'
		with
		w1 as	(
				select crt.topic from nfiesta_results.cm_result2topic as crt
				where ' || _cond4topic_num ||'
				and ' || _cond4topic_denom ||'
				)
		,w2 as	(
				select
						id,
						label_en as label,
						description_en as description
				from
						nfiesta_results.c_topic
				where
						id in (select distinct w1.topic from w1)
				)
		,w3 as	(
				select
						array_agg(w2.label order by w2.description) as label,
						array_agg(w2.description order by w2.description) as description
				from
						w2
				)
		select
				array_to_string(w3.label, ''; ''),
				array_to_string(w3.description, ''; '')
		from
				w3
		'
		using _target_variable, _target_variable_denom
		into _res_topic_label, _res_topic_description;
	else
		execute
		'
		with
		w1 as	(
				select crt.topic from nfiesta_results.cm_result2topic as crt
				where ' || _cond4topic_num ||'
				and ' || _cond4topic_denom ||'
				)
		,w2 as	(
				select
						topic as id,
						label,
						description
				from
						nfiesta_results.c_topic_language
				where
						topic in (select distinct w1.topic from w1)
				and
						language = $3
				)
		,w3 as	(
				select
						array_agg(w2.label order by w2.description) as label,
						array_agg(w2.description order by w2.description) as description
				from
						w2
				)
		select
				array_to_string(w3.label, ''; ''),
				array_to_string(w3.description, ''; '')
		from
				w3
		'
		using _target_variable, _target_variable_denom, _lang
		into _res_topic_label, _res_topic_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	if _lang_label = 'en'::varchar(2)
	then
		execute
		'
		select
				cep.label_en,
				cep.description_en
		from
				nfiesta_results.c_estimation_period as cep
		where
				cep.id =	(
							select distinct trg.estimation_period
							from nfiesta_results.t_result_group trg
							where trg.id in (select unnest($1))
							and trg.web = true
							)
		'
		using _id_group
		into _res_estimation_period_label, _res_estimation_period_description;
	else
		execute
		'
		select
				cepl.label,
				cepl.description
		from
				nfiesta_results.c_estimation_period_language as cepl
		where
				cepl.estimation_period =	(
											select distinct trg.estimation_period
											from nfiesta_results.t_result_group trg
											where trg.id in (select unnest($1))
											and trg.web = true
											)
		and		cepl.language = $2
		'
		using _id_group, _lang
		into _res_estimation_period_label, _res_estimation_period_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	select tech.cell from nfiesta_results.t_estimation_cell_hierarchy as tech
	where tech.cell in	(
						select cec.id from nfiesta_results.c_estimation_cell as cec
						where cec.estimation_cell_collection =	_estimation_cell_collection
						)
	order by cell limit 1
	into _estimation_cell_from_hierarchy_for_ecc_select_by_user;

	if _estimation_cell_from_hierarchy_for_ecc_select_by_user is null
	then
		-- selected estimation cell collection is maybe the heighest
		select count(tech.cell_superior) from nfiesta_results.t_estimation_cell_hierarchy as tech
		where tech.cell_superior in	(
									select cec.id from nfiesta_results.c_estimation_cell as cec
									where cec.estimation_cell_collection = _estimation_cell_collection
									)
		into _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior;

		if _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior = 0
		then
			raise exception 'Error: 04: fn_get_user_metadata: For user selected estimation_cell_collection = % not exists any estimation cell in t_estimation_cell_hierarchy table!',_estimation_cell_collection;
		end if;

		-- user selected the heighest estimation cell collection => test for occurrence in t_result_group table is not needed
		_estimation_cell_collection_array := array[_estimation_cell_collection];		
	else
		with
		w1 as	(
				select * from nfiesta_results.fn_get_heighest_estimation_cell(array[_estimation_cell_from_hierarchy_for_ecc_select_by_user]) as res
				)
		select w1.res[1] from w1
		into _heighest_estimation_cell_select_by_user; -- the heighest estimation cell from hierarchy

		select cec.estimation_cell_collection from nfiesta_results.c_estimation_cell as cec
		where cec.id = _heighest_estimation_cell_select_by_user
		into _heighest_estimation_cell_collection_select_by_user; -- the heighest estimation cell collection from hierarchy

		-- test for occurrence in t_result_group
		select id from nfiesta_results.t_result_group
		where estimate_type = _estimate_type
		and target_variable = _target_variable
		and coalesce(target_variable_denom,0) = coalesce(_target_variable_denom,0)
		and estimation_period = _estimation_period
		and phase_estimate_type = _phase_estimate_type
		and coalesce(phase_estimate_type_denom,0) = coalesce(_phase_estimate_type_denom,0)
		and estimation_cell_collection = _heighest_estimation_cell_collection_select_by_user
		and coalesce(area_domain,0) = coalesce(_area_domain,0)
		and coalesce(area_domain_denom,0) = coalesce(_area_domain_denom,0)
		and coalesce(sub_population,0) = coalesce(_sub_population,0)
		and coalesce(sub_population_denom,0) = coalesce(_sub_population_denom,0)
		and web = true
		into _test_occurence_result_group;

		if _test_occurence_result_group is null
		then
			_estimation_cell_collection_array := array[_estimation_cell_collection];
		else
			_estimation_cell_collection_array := array[_estimation_cell_collection] || array[_heighest_estimation_cell_collection_select_by_user];
		end if;
	end if;	

	if _lang_label = 'en'::varchar(2)
	then
		execute
		'
		with
		w1 as	(
				select
						cecc.id,
						cecc.label_en as label,
						cecc.description_en as description
				from
						nfiesta_results.c_estimation_cell_collection as cecc
				where
						cecc.id in (select unnest($1))
				)
		,w2 as	(
				select
						array_agg(w1.label order by w1.id) as label,
						array_agg(w1.description order by w1.id) as description
				from
						w1
				)
		select
				array_to_string(w2.label,''; '') as label,
				array_to_string(w2.description,''; '') as description
		from
			w2
		'
		using _estimation_cell_collection_array
		into _res_estimation_cell_collection_label, _res_estimation_cell_collection_description;
	else
		execute
		'
		with
		w1 as	(
				select
						ceccl.estimation_cell_collection as id,
						ceccl.label,
						ceccl.description
				from
						nfiesta_results.c_estimation_cell_collection_language as ceccl
				where
						ceccl.estimation_cell_collection in (select unnest($1))
				and
						ceccl.language = $2
				)
		,w2 as	(
				select
						array_agg(w1.label order by w1.id) as label,
						array_agg(w1.description order by w1.id) as description
				from
						w1
				)
		select
				array_to_string(w2.label,''; '') as label,
				array_to_string(w2.description,''; '') as description
		from
			w2
		'
		using _estimation_cell_collection_array, _lang
		into _res_estimation_cell_collection_label, _res_estimation_cell_collection_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATE TYPE
	if _target_variable_denom is null
	then
		execute
		'
		select
				cet.label,
				cet.description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 1
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;
	else
		execute
		'
		select
				cet.label,
				cet.description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 2
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	select
			((ctv.metadata->'''||_lang_label||''')->''unit'')->>''label'' as label,		
			((ctv.metadata->'''||_lang_label||''')->''unit'')->>''description'' as description
	from
			nfiesta_results.c_target_variable as ctv
	where
			ctv.id = $1
	'
	using _target_variable
	into _res_unit_num_label, _res_unit_num_description;		
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASUREMENT
	if _target_variable_denom is null
	then
		_res_unit_of_measurement_label := _res_unit_num_label;
		_res_unit_of_measurement_description := _res_unit_num_description;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_label_num,
						((w2.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_label_denom,					
						((w1.metadata->'''||_lang_label||''')->''unit'')->>''description'' as unit_description_num,
						((w2.metadata->'''||_lang_label||''')->''unit'')->>''description'' as unit_description_denom
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.*,
						case
							when (w3.target_variable_num = w3.target_variable_denom) or (w3.unit_label_num = w3.unit_label_denom) then ''%''
							else concat(w3.unit_label_num,'' / '',w3.unit_label_denom)
						end as label4user,						
						case
							when (w3.target_variable_num = w3.target_variable_denom) or (w3.unit_description_num = w3.unit_description_denom) then ''%''
							else concat(w3.unit_description_num,'' / '',w3.unit_description_denom)
						end as description4user
				from
						w3
				)
		select
				w4.label4user,
				w4.description4user
		from
				w4
		'
		using _target_variable, _target_variable_denom
		into _res_unit_of_measurement_label, _res_unit_of_measurement_description;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _target_variable_denom is null
	then
		-- basic metadata + numerator metadata
		select json_build_object
		(
			'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
			'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
			'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
			'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
			'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
			'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator(_jlang,_id_group))
		)
		into _res;
	else
		-- basic metadata + numerator metadata + denominator metadata
		select json_build_object
		(
			'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
			'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
			'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
			'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
			'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
			'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator(_jlang,_id_group)),
			'denominator',(select * from nfiesta_results.fn_get_user_metadata_denominator(_jlang,_id_group))
		)
		into _res;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) is
'The function returns metadata for given input ID (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) to public;


--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata_denominator
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang									integer;
	_lang_label								varchar;
	_not_specified_label					text;
	_not_specified_description				text;
	_without_distinction_label				text;
	_without_distinction_description		text;	
	_not_specified_definition_variant		json;
	_not_specified_adr_spr					json;
	_target_variable						integer[];
	_res_indicator_label					text;
	_res_indicator_description				text;
	_res_state_or_change_label				text;
	_res_state_or_change_description		text;
	_res_unit_label							text;
	_res_unit_description					text;
	_check_area_domain						integer[];
	_res_area_domain_attribute_json			json;
	_check_sub_population					integer[];
	_res_sub_population_attribute_json		json;
	--_check_core_ldsities					integer;
	_check_division_ldsities				integer;
	_res_core_json							json;
	_res_division_json						json;	
	_res									json;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata_denominator: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata_denominator: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom) -- column for DENOMINATOR
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable;

	if array_length(_target_variable,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	_not_specified_label := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_label'::character varying))::text;
	_not_specified_description := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_description'::character varying))::text;
	_without_distinction_label := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_label'::character varying))::text;
	_without_distinction_description := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_description'::character varying))::text;
	-----------------------------------------------------------------
	_not_specified_definition_variant := json_agg(json_build_object('label',_not_specified_label,'description',_not_specified_description));
	_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label',_not_specified_label,'description',_not_specified_description),'restriction',json_build_object('label',_not_specified_label,'description',_not_specified_description)));
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - denominator
	execute
	'
	select
			((ctv.metadata->'''||_lang_label||''')->''indicator'')->>''label'' as label,		
			((ctv.metadata->'''||_lang_label||''')->''indicator'')->>''description'' as description
	from
			nfiesta_results.c_target_variable as ctv
	where
			ctv.id = $1
	'
	using _target_variable[1]
	into _res_indicator_label, _res_indicator_description;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - denominator
	execute
	'
	select
			((ctv.metadata->'''||_lang_label||''')->''state_or_change'')->>''label'' as label,		
			((ctv.metadata->'''||_lang_label||''')->''state_or_change'')->>''description'' as description
	from
			nfiesta_results.c_target_variable as ctv
	where
			ctv.id = $1
	'
	using _target_variable[1]
	into _res_state_or_change_label, _res_state_or_change_description;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - denominator
	execute
	'
	select
			((ctv.metadata->'''||_lang_label||''')->''unit'')->>''label'' as label,		
			((ctv.metadata->'''||_lang_label||''')->''unit'')->>''description'' as description
	from
			nfiesta_results.c_target_variable as ctv
	where
			ctv.id = $1
	'
	using _target_variable[1]
	into _res_unit_label, _res_unit_description;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - denominator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain_denom is null then 0 else trg.area_domain_denom end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 04: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		_res_area_domain_attribute_json := json_agg(json_build_object('label',_without_distinction_label,'description',_without_distinction_description));
	else
		if _lang_label = 'en'::varchar(2)
		then
			execute
			'
			with
			w1 as	(
					select
							string_to_array(cad.label_en,'';'') as label,
							string_to_array(cad.description_en,'';'') as description
					from
							nfiesta_results.c_area_domain as cad
					where
							cad.id = $1
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_area_domain[1]
			into _res_area_domain_attribute_json;
		else
			execute
			'
			with
			w1 as	(
					select
							string_to_array(cadl.label,'';'') as label,
							string_to_array(cadl.description,'';'') as description
					from
							nfiesta_results.c_area_domain_language as cadl
					where
							cadl.area_domain = $1
					and
							cadl.language = $2
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_area_domain[1], _lang
			into _res_area_domain_attribute_json;		
		end if;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - denominator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 05: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		_res_sub_population_attribute_json := json_agg(json_build_object('label',_without_distinction_label,'description',_without_distinction_description));
	else
		if _lang_label = 'en'::varchar(2)
		then
			execute
			'
			with
			w1 as	(
					select
							string_to_array(csp.label_en,'';'') as label,
							string_to_array(csp.description_en,'';'') as description
					from
							nfiesta_results.c_sub_population as csp
					where
							csp.id = $1
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_sub_population[1]
			into _res_sub_population_attribute_json;
		else
			execute
			'
			with
			w1 as	(
					select
							string_to_array(cspl.label,'';'') as label,
							string_to_array(cspl.description,'';'') as description
					from
							nfiesta_results.c_sub_population_language as cspl
					where
							cspl.sub_population = $1
					and
							cspl.language = $2
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_sub_population[1], _lang
			into _res_sub_population_attribute_json;		
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local_densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'core'
	into _check_core_ldsities;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- CORE JSON
	execute
	'
	with
	w1 as	(
			select
					(ctv.metadata->'''||_lang_label||''')->''local_densities'' as local_densities
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)			
	,w3 as	(
			select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''core''
			)
	,w4 as	(
			select
					row_number() over() as new_id,
					(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
					(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
					(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
					(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
					(w3.local_densities->''ldsity'')->>''label'' as label,
					(w3.local_densities->''ldsity'')->>''description'' as description,	
					w3.local_densities->>''use_negative'' as use_negative,
					(w3.local_densities->''version'')->>''label'' as version_label,
					(w3.local_densities->''version'')->>''description'' as version_description,
					w3.local_densities->''definition_variant'' as definition_variant,
					w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
					w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
			from
					w3
			)
	,w5 as	(
			select
					w4.new_id,
					w4.object_type_label,
					w4.object_type_description,
					w4.object_label,
					w4.object_description,
					w4.label,
					w4.description,
					w4.use_negative,
					case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
					case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
					case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
					case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
					case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
			from
					w4
			)
	,w6 as	(
			select
					w5.new_id,
					json_build_object
						(
						''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
						''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
						''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
						''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
						''definition_variant'',w5.definition_variant,
						''area_domain_restrictions'',w5.area_domain_restrictions,
						''sub_population_restrictions'',w5.sub_population_restrictions,
						''use_negative'',w5.use_negative
						) as  local_densities
			from
					w5
			)
	select
			json_agg(w6.local_densities order by w6.new_id) as local_densities
	from
			w6
	'
	using _target_variable[1], _not_specified_label, _not_specified_description, _not_specified_definition_variant, _not_specified_adr_spr
	into _res_core_json;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- DIVISION JSON
	if _check_division_ldsities = 0
	then
		_res_division_json := null::json;
	else
		execute
		'
		with
		w1 as	(
				select
						(ctv.metadata->'''||_lang_label||''')->''local_densities'' as local_densities
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		,w2 as	(
				select json_array_elements(w1.local_densities) as local_densities from w1 
				)				
		,w3 as	(
				select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w4 as	(
				select
						row_number() over() as new_id,
						(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w3.local_densities->''ldsity'')->>''label'' as label,
						(w3.local_densities->''ldsity'')->>''description'' as description,	
						w3.local_densities->>''use_negative'' as use_negative,
						(w3.local_densities->''version'')->>''label'' as version_label,
						(w3.local_densities->''version'')->>''description'' as version_description,
						w3.local_densities->''definition_variant'' as definition_variant,
						w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w3
				)
		,w5 as	(
				select
						w4.new_id,
						w4.object_type_label,
						w4.object_type_description,
						w4.object_label,
						w4.object_description,
						w4.label,
						w4.description,
						w4.use_negative,
						case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
						case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
						case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
						case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
						case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
				from
						w4
				)
		,w6 as	(
				select
						w5.new_id,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
							''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
							''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
							''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
							''definition_variant'',w5.definition_variant,
							''area_domain_restrictions'',w5.area_domain_restrictions,
							''sub_population_restrictions'',w5.sub_population_restrictions,
							''use_negative'',w5.use_negative
							) as  local_densities
				from
						w5
				)
		select
				json_agg(w6.local_densities order by w6.new_id) as local_densities
		from
				w6
		'
		using _target_variable[1], _not_specified_label, _not_specified_description, _not_specified_definition_variant, _not_specified_adr_spr
		into _res_division_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities = 0
	then
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json
		))
		into _res;
	else
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json,
			'division_contributions',_res_division_json
		))
		into _res;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) is
'The function returns metadata (description) for given input ID (that represents target variable for denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) to public;


--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	res_metadata_type			text,
	res_metadata_description	text
)
language plpgsql
stable
as
$$
declare
	_lang_suffix					character varying(3);
	_lang							text;
	_target_variable_num			integer[];
	_target_variable_denom_check	integer;
	_target_variable_denom			integer;

	_cond4topic_num					text;
	_cond4topic_denom				text;

	_res_topic						text;
	_res_estimation_period			text;
	_res_estimation_cell_collection	text;
	_res_indicator_num				text;
	_res_indicator_denom			text;
	_res_state_or_change_num		text;
	_res_state_or_change_denom		text;
	_res_unit_num					text;
	_res_unit_denom					text;
	_res_unit_of_measure			text;

	_check_ldsity_contributions_num		integer;
	_check_ldsity_contributions_denom	integer;
	_query_ldsity_attributes_num_1		text;
	_query_ldsity_attributes_num_2		text;
	_query_ldsity_attributes_num		text;
	_query_ldsity_attributes_denom_1	text;
	_query_ldsity_attributes_denom_2	text;
	_query_ldsity_attributes_denom		text;
	_query_ldsity_attributes			text;

	_res								text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	select count(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom_check;

	if _target_variable_denom_check is distinct from 0 -- target variable denom exists
	then
		if _target_variable_denom_check > 1
		then
			raise exception 'Error: 03: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
		end if;

		select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
		where trg.id in (select unnest(_id_group))
		and trg.web = true
		into _target_variable_denom;		
	else
		_target_variable_denom := null::integer;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	execute
	'
	with
	w1 as	(
			select crt.topic from nfiesta_results.cm_result2topic as crt
			where ' || _cond4topic_num ||'
			and ' || _cond4topic_denom ||'
			)
	,w2 as	(
			select
					description'||_lang_suffix||' AS description
			from
					nfiesta_results.c_topic
			where
					id in (select distinct w1.topic from w1)
			)
	,w3 as	(
			select array_agg(w2.description order by w2.description) as description
			from w2
			)
			select array_to_string(w3.description, ''; '') from w3
	'
	using _target_variable_num[1], _target_variable_denom
	into _res_topic;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	execute
	'
	select
			cep.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_period as cep
	where
			cep.id =	(
						select distinct trg.estimation_period
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_period;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	execute
	'
	select
			cecc.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_cell_collection as cecc
	where
			cecc.id =	(
						select distinct trg.estimation_cell_collection
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_cell_collection;
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
					((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num[1]
	into _res_indicator_num;
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - denominator
	if _target_variable_denom is null
	then
		_res_indicator_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
						((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.description'||_lang_suffix||' AS description
		from
				w1
		'
		using _target_variable_denom
		into _res_indicator_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state or change'')->>''description'' as description,
					((ctv.metadata->''en'')->''state or change'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num[1]
	into _res_state_or_change_num;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - denominator
	if _target_variable_denom is null
	then
		_res_state_or_change_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''state or change'')->>''description'' as description,
						((ctv.metadata->''en'')->''state or change'')->>''description'' as description_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.description'||_lang_suffix||' AS description
		from
				w1
		'
		using _target_variable_denom
		into _res_state_or_change_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num[1]
	into _res_unit_num;
	-----------------------------------------------------------------
	-- METADATA OF UNIT - denominator
	if _target_variable_denom is null
	then
		_res_unit_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
						((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.description'||_lang_suffix||' AS description
		from
				w1
		'
		using _target_variable_denom
		into _res_unit_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA LDSITY - numerator
	-- check => how many ldsity contributions?
	execute
	'
	with			
	w1 as	(
			select
					t.id as target_variable,
					t.description'||_lang_suffix||' AS description
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as description,
							((ctv.metadata->''en'')->''local densities'') as description_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = $1
					) as t
			)									
	,w2 as	(
			select
					w1.target_variable,
					json_array_elements(w1.description) as description
			from
					w1
			)
	select count(w2.*) from w2
	'
	using _target_variable_num[1]
	into _check_ldsity_contributions_num;

	--raise notice '_check_ldsity_contributions_num: %',_check_ldsity_contributions_num;
			
	if _check_ldsity_contributions_num = 0
	then
		raise exception 'Error: 04: fn_get_user_options_metadata: For target variable [ID = %] numerator not exists any ldsity contribution!',_target_variable_num[1];
	end if;

	_query_ldsity_attributes_num_1 := concat(
	'
	with			
	w1a as	(
			select
					t.id as target_variable,
					t.description',_lang_suffix,' AS description
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as description,
							((ctv.metadata->''en'')->''local densities'') as description_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = ',_target_variable_num[1],'
					) as t
			)									
	,w2a as	(
			select
					w1a.target_variable,
					json_array_elements(w1a.description) as description
			from
					w1a
			)
	,w3a as	(
			select
					row_number() over () as new_id,
					w2a.target_variable,
					w2a.description->>''object type label'' as object_type_label,
					w2a.description->>''object type description'' as object_type_description,
					w2a.description->>''description'' as ldsity,
					replace(
					replace(
					replace(
					replace(
					coalesce((w2a.description->''definition variant'')->>''description'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as variant,					
					replace(
					replace(
					replace(
					replace(
					coalesce((w2a.description->''area domain'')->>''description'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as area_domain,
					replace(
					replace(
					replace(
					replace(
					coalesce((w2a.description->''population'')->>''description'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as population
			from
					w2a
			)
	');

	for i in 1.._check_ldsity_contributions_num
	loop
		if _lang = 'cs'
		then
			if i = 1
			then
				_query_ldsity_attributes_num_2 :=
				concat(
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Varianta:'' as res_metadata_type,
						w3a.variant as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Plošná doména:'' as res_metadata_type,
						w3a.area_domain as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Populace:'' as res_metadata_type,
						w3a.population as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			else
				_query_ldsity_attributes_num_2 := 
				concat(_query_ldsity_attributes_num_2,' union all ',
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Varianta:'' as res_metadata_type,
						w3a.variant as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Plošná doména:'' as res_metadata_type,
						w3a.area_domain as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Populace:'' as res_metadata_type,
						w3a.population as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			end if;
		else
			if i = 1
			then
				_query_ldsity_attributes_num_2 :=
				concat(
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Variant:'' as res_metadata_type,
						w3a.variant as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Area domain:'' as res_metadata_type,
						w3a.area_domain as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Population:'' as res_metadata_type,
						w3a.population as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			else
				_query_ldsity_attributes_num_2 := 
				concat(_query_ldsity_attributes_num_2,' union all ',
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Variant:'' as res_metadata_type,
						w3a.variant as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Area domain:'' as res_metadata_type,
						w3a.area_domain as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Population:'' as res_metadata_type,
						w3a.population as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			end if;
		end if;
	end loop;

	_query_ldsity_attributes_num := concat(_query_ldsity_attributes_num_1,' ',_query_ldsity_attributes_num_2);
	-----------------------------------------------------------------
	-- METADATA LDSITY - denominator
	if _target_variable_denom is null
	then
		if _lang = 'cs'
		then
			_query_ldsity_attributes_denom :=
			'
			select
					0 as id_ldsity,
					0 as id_attribute,
					null::text as res_metadata_type,
					null::text as res_metadata_description
			';
		else
		end if;
	else
		-- check => how many ldsity contributions?
		execute
		'
		with			
		w1 as	(
				select
						t.id as target_variable,
						t.description'||_lang_suffix||' AS description
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as description,
								((ctv.metadata->''en'')->''local densities'') as description_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = $1
						) as t
				)									
		,w2 as	(
				select
						w1.target_variable,
						json_array_elements(w1.description) as description
				from
						w1
				)
		select count(w2.*) from w2
		'
		using _target_variable_denom
		into _check_ldsity_contributions_denom;
				
		if _check_ldsity_contributions_denom = 0
		then
			raise exception 'Error: 05: fn_get_user_options_metadata: For target variable [ID = %] denominator not exists any ldsity contribution!',_target_variable_denom;
		end if;

		_query_ldsity_attributes_denom_1 := concat(
		'
		with			
		w1b as	(
				select
						t.id as target_variable,
						t.description',_lang_suffix,' AS description
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as description,
								((ctv.metadata->''en'')->''local densities'') as description_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = ',_target_variable_denom,'
						) as t
				)									
		,w2b as	(
				select
						w1b.target_variable,
						json_array_elements(w1b.description) as description
				from
						w1b
				)
		,w3b as	(
				select
						row_number() over () as new_id,
						w2b.target_variable,
						w2b.description->>''object type label'' as object_type_label,
						w2b.description->>''object type description'' as object_type_description,
						w2b.description->>''description'' as ldsity,
						replace(
						replace(
						replace(
						replace(
						coalesce((w2b.description->''definition variant'')->>''description'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as variant,					
						replace(
						replace(
						replace(
						replace(
						coalesce((w2b.description->''area domain'')->>''description'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as area_domain,
						replace(
						replace(
						replace(
						replace(
						coalesce((w2b.description->''population'')->>''description'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as population
				from
						w2b
				)
		');

		for i in 1.._check_ldsity_contributions_denom
		loop
			if _lang = 'cs'
			then
				if i = 1
				then
					_query_ldsity_attributes_denom_2 :=
					concat(
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Varianta:'' as res_metadata_type,
							w3b.variant as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Plošná doména:'' as res_metadata_type,
							w3b.area_domain as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Populace:'' as res_metadata_type,
							w3b.population as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				else
					_query_ldsity_attributes_denom_2 := 
					concat(_query_ldsity_attributes_denom_2,' union all ',
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Varianta:'' as res_metadata_type,
							w3b.variant as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Plošná doména:'' as res_metadata_type,
							w3b.area_domain as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Populace:'' as res_metadata_type,
							w3b.population as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				end if;
			else
				if i = 1
				then
					_query_ldsity_attributes_denom_2 :=
					concat(
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Variant:'' as res_metadata_type,
							w3b.variant as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Area domain:'' as res_metadata_type,
							w3b.area_domain as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Population:'' as res_metadata_type,
							w3b.population as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				else
					_query_ldsity_attributes_denom_2 := 
					concat(_query_ldsity_attributes_denom_2,' union all ',
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Variant:'' as res_metadata_type,
							w3b.variant as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Area domain:'' as res_metadata_type,
							w3b.area_domain as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Population:'' as res_metadata_type,
							w3b.population as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				end if;
			end if;
		end loop;

		_query_ldsity_attributes_denom := concat(_query_ldsity_attributes_denom_1,' ',_query_ldsity_attributes_denom_2);

	end if;
	-----------------------------------------------------------------
	if _target_variable_denom is null
	then
		_query_ldsity_attributes := concat(
		',w4a as	(',_query_ldsity_attributes_num,')
		,w4ab as	(
					select
							w4a.res_metadata_type as res_metadata_type_num,
							w4a.res_metadata_description as res_metadata_description_num,
							null::text as res_metadata_type_denom,
							null::text as res_metadata_description_denom
					from
							w4a
					)
		');
	else
		if _check_ldsity_contributions_num >= _check_ldsity_contributions_denom
		then
			_query_ldsity_attributes := concat(
			',w4a as	(',_query_ldsity_attributes_num,')
			,w4b as	(',_query_ldsity_attributes_denom,')
			,w4ab as	(
						select
								w4a.res_metadata_type as res_metadata_type_num,
								w4a.res_metadata_description as res_metadata_description_num,
								w4b.res_metadata_type as res_metadata_type_denom,
								w4b.res_metadata_description as res_metadata_description_denom
						from
								w4a left join w4b
						on
								w4a.id_ldsity = w4b.id_ldsity and
								w4a.id_attribute = w4b.id_attribute
						)
			');
		else
			_query_ldsity_attributes := concat(
			',w4a as	(',_query_ldsity_attributes_num,')
			,w4b as	(',_query_ldsity_attributes_denom,')
			,w4ab as	(
						select
								w4a.res_metadata_type as res_metadata_type_num,
								w4a.res_metadata_description as res_metadata_description_num,
								w4b.res_metadata_type as res_metadata_type_denom,
								w4b.res_metadata_description as res_metadata_description_denom
						from
								w4a right join w4b
						on
								w4a.id_ldsity = w4b.id_ldsity and
								w4a.id_attribute = w4b.id_attribute
						)
			');
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASURE
	if _target_variable_denom is null
	then
		_res_unit_of_measure := _res_unit_num;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''description'' as unit_num,
						((w2.metadata->''cs'')->''unit'')->>''description'' as unit_denom,
						((w1.metadata->''en'')->''unit'')->>''description'' as unit_num_en,
						((w2.metadata->''en'')->''unit'')->>''description'' as unit_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_num'||_lang_suffix||' AS unit_num,
						w3.unit_denom'||_lang_suffix||' AS unit_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_num = w4.unit_denom) then ''%''
							else concat(w4.unit_num,'' / '',w4.unit_denom)
						end as description4user
				from
						w4
				)
		select w5.description4user from w5
		'
		using _target_variable_num[1], _target_variable_denom
		into _res_unit_of_measure;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _lang_suffix = ''
	then
		if _target_variable_denom is null
		then
			_res := concat(
			'
			with
			w01 as	(
					select
							''Tematický okruh:'' as res_metadata_type,
							''',_res_topic,''' as res_metadata_description
					union all
					select
							''Období:'' as res_metadata_type,
							''',_res_estimation_period,''' as res_metadata_description
					union all
					select
							''Geografické členění:'' as res_metadata_type,
							''',_res_estimation_cell_collection,''' as res_metadata_description
					union all
					select
							''Jednotka výstupu:'' as res_metadata_type,
							''',_res_unit_of_measure,''' as res_metadata_description
					union all
					select
							''Indikátor:'' as res_metadata_type,
							''',_res_indicator_num,''' as res_metadata_description
					union all
					select
							''Stav nebo změna:'' as res_metadata_type,
							''',_res_state_or_change_num,''' as res_metadata_description
					union all
					select
							''Jednotka indikátoru:'' as res_metadata_type,
							''',_res_unit_num,''' as res_metadata_description
					)
			',_query_ldsity_attributes,
			'select
					w01.res_metadata_type,
					w01.res_metadata_description
			from
					w01
			union all
			select
					''Příspěvky:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_num as res_metadata_type,
					case when w4ab.res_metadata_description_num = ''null'' then null::text else w4ab.res_metadata_description_num end as res_metadata_description
			from
					w4ab;
			');
		else
			_res := concat(
			'
			with
			w01 as	(
					select
							''Tematický okruh:'' as res_metadata_type,
							''',_res_topic,''' as res_metadata_description
					union all
					select
							''Období:'' as res_metadata_type,
							''',_res_estimation_period,''' as res_metadata_description
					union all
					select
							''Geografické členění:'' as res_metadata_type,
							''',_res_estimation_cell_collection,''' as res_metadata_description
					union all
					select
							''Jednotka výstupu:'' as res_metadata_type,
							''',_res_unit_of_measure,''' as res_metadata_description
					union all
					select
							null::text as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Informace čitatele:'' as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Indikátor:'' as res_metadata_type,
							''',_res_indicator_num,''' as res_metadata_description
					union all
					select
							''Stav nebo změna:'' as res_metadata_type,
							''',_res_state_or_change_num,''' as res_metadata_description
					union all
					select
							''Jednotka indikátoru:'' as res_metadata_type,
							''',_res_unit_num,''' as res_metadata_description
					)
			,w02 as	(
					select
							null::text as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Informace jmenovatele:'' as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Indikátor:'' as res_metadata_type,
							''',_res_indicator_denom,''' as res_metadata_description
					union all
					select
							''Stav nebo změna:'' as res_metadata_type,
							''',_res_state_or_change_denom,''' as res_metadata_description
					union all
					select
							''Jednotka indikátoru:'' as res_metadata_type,
							''',_res_unit_denom,''' as res_metadata_description
					)
			',_query_ldsity_attributes,
			'select
					w01.res_metadata_type,
					w01.res_metadata_description
			from
					w01
			union all
			select
					''Příspěvky:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_num as res_metadata_type,
					case when w4ab.res_metadata_description_num = ''null'' then null::text else w4ab.res_metadata_description_num end as res_metadata_description
			from
					w4ab where w4ab.res_metadata_type_num is not null
			union all
			select
					w02.res_metadata_type,
					w02.res_metadata_description
			from
					w02
			union all
			select
					''Příspěvky:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_denom as res_metadata_type,
					case when w4ab.res_metadata_description_denom = ''null'' then null::text else w4ab.res_metadata_description_denom end as res_metadata_description
			from
					w4ab where w4ab.res_metadata_type_denom is not null
			');		
		end if;			
	else
		if _target_variable_denom is null
		then
			_res := concat(
			'
			with
			w01 as	(
					select
							''Topic:'' as res_metadata_type,
							''',_res_topic,''' as res_metadata_description
					union all
					select
							''Period:'' as res_metadata_type,
							''',_res_estimation_period,''' as res_metadata_description
					union all
					select
							''Geographic region:'' as res_metadata_type,
							''',_res_estimation_cell_collection,''' as res_metadata_description
					union all
					select
							''Unit of measurement:'' as res_metadata_type,
							''',_res_unit_of_measure,''' as res_metadata_description
					union all
					select
							''Indicator:'' as res_metadata_type,
							''',_res_indicator_num,''' as res_metadata_description
					union all
					select
							''State or change:'' as res_metadata_type,
							''',_res_state_or_change_num,''' as res_metadata_description
					union all
					select
							''Unit of indicator:'' as res_metadata_type,
							''',_res_unit_num,''' as res_metadata_description
					)
			',_query_ldsity_attributes,
			'select
					w01.res_metadata_type,
					w01.res_metadata_description
			from
					w01
			union all
			select
					''Contributions:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_num as res_metadata_type,
					case when w4ab.res_metadata_description_num = ''null'' then null::text else w4ab.res_metadata_description_num end as res_metadata_description
			from
					w4ab;
			');
		else
			_res := concat(
			'
			with
			w01 as	(
					select
							''Topic:'' as res_metadata_type,
							''',_res_topic,''' as res_metadata_description
					union all
					select
							''Period:'' as res_metadata_type,
							''',_res_estimation_period,''' as res_metadata_description
					union all
					select
							''Geographic region:'' as res_metadata_type,
							''',_res_estimation_cell_collection,''' as res_metadata_description
					union all
					select
							''Unit of measurement:'' as res_metadata_type,
							''',_res_unit_of_measure,''' as res_metadata_description
					union all
					select
							null::text as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Numerator informations:'' as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Indicator:'' as res_metadata_type,
							''',_res_indicator_num,''' as res_metadata_description
					union all
					select
							''State or change:'' as res_metadata_type,
							''',_res_state_or_change_num,''' as res_metadata_description
					union all
					select
							''Unit of indicator:'' as res_metadata_type,
							''',_res_unit_num,''' as res_metadata_description
					)
			,w02 as	(
					select
							null::text as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Denominator informations:'' as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Indicator:'' as res_metadata_type,
							''',_res_indicator_denom,''' as res_metadata_description
					union all
					select
							''State or change:'' as res_metadata_type,
							''',_res_state_or_change_denom,''' as res_metadata_description
					union all
					select
							''Unit of indicator:'' as res_metadata_type,
							''',_res_unit_denom,''' as res_metadata_description
					)
			',_query_ldsity_attributes,
			'select
					w01.res_metadata_type,
					w01.res_metadata_description
			from
					w01
			union all
			select
					''Contributions:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_num as res_metadata_type,
					case when w4ab.res_metadata_description_num = ''null'' then null::text else w4ab.res_metadata_description_num end as res_metadata_description
			from
					w4ab where w4ab.res_metadata_type_num is not null
			union all
			select
					w02.res_metadata_type,
					w02.res_metadata_description
			from
					w02
			union all
			select
					''Contributions:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_denom as res_metadata_type,
					case when w4ab.res_metadata_description_denom = ''null'' then null::text else w4ab.res_metadata_description_denom end as res_metadata_description
			from
					w4ab where w4ab.res_metadata_type_denom is not null
			');		
		end if;
	end if;

	return query execute ''||_res||'';
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) is
'The function returns metadata for given input ID (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) to public;


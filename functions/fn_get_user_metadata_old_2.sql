--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	column_1	text,
	column_2	text,
	column_3	text
)
language plpgsql
stable
as
$$
declare
	_lang_suffix					character varying(3);
	_lang							text;
	_target_variable_num			integer[];
	_target_variable_denom_check	integer;
	_target_variable_denom			integer;
	_cond4topic_num					text;
	_cond4topic_denom				text;
	_res_topic						text;
	_res_estimation_period			text;
	_res_estimation_cell_collection	text;
	_res_unit_num					text;
	_res_unit_of_measurement		text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	select count(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom_check;

	if _target_variable_denom_check is distinct from 0 -- target variable denom exists
	then
		if _target_variable_denom_check > 1
		then
			raise exception 'Error: 03: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
		end if;

		select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
		where trg.id in (select unnest(_id_group))
		and trg.web = true
		into _target_variable_denom;		
	else
		_target_variable_denom := null::integer;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	execute
	'
	with
	w1 as	(
			select crt.topic from nfiesta_results.cm_result2topic as crt
			where ' || _cond4topic_num ||'
			and ' || _cond4topic_denom ||'
			)
	,w2 as	(
			select
					description'||_lang_suffix||' AS description
			from
					nfiesta_results.c_topic
			where
					id in (select distinct w1.topic from w1)
			)
	,w3 as	(
			select array_agg(w2.description order by w2.description) as description
			from w2
			)
			select array_to_string(w3.description, ''; '') from w3
	'
	using _target_variable_num[1], _target_variable_denom
	into _res_topic;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	execute
	'
	select
			cep.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_period as cep
	where
			cep.id =	(
						select distinct trg.estimation_period
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_period;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	execute
	'
	select
			cecc.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_cell_collection as cecc
	where
			cecc.id =	(
						select distinct trg.estimation_cell_collection
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_cell_collection;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num[1]
	into _res_unit_num;	
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASUREMENT
	if _target_variable_denom is null
	then
		_res_unit_of_measurement := _res_unit_num;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''description'' as unit_num,
						((w2.metadata->''cs'')->''unit'')->>''description'' as unit_denom,
						((w1.metadata->''en'')->''unit'')->>''description'' as unit_num_en,
						((w2.metadata->''en'')->''unit'')->>''description'' as unit_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_num'||_lang_suffix||' AS unit_num,
						w3.unit_denom'||_lang_suffix||' AS unit_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_num = w4.unit_denom) then ''%''
							else concat(w4.unit_num,'' / '',w4.unit_denom)
						end as description4user
				from
						w4
				)
		select w5.description4user from w5
		'
		using _target_variable_num[1], _target_variable_denom
		into _res_unit_of_measurement;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _target_variable_denom is null
	then
		-- basic metadata + numerator metadata
		if _lang_suffix = ''
		then
			return query
			select
					'Tematický okruh:' as column_1,
					_res_topic as column_2,
					null::text as column_3
			union all
			select
					'Období:' as column_1,
					_res_estimation_period as column_2,
					null::text as column_3
			union all
			select
					'Geografické členění:' as column_1,
					_res_estimation_cell_collection as column_2,
					null::text as column_3
			union all
			select
					'Jednotka výstupu:' as column_1,
					_res_unit_of_measurement as column_2,
					null::text as column_3
			union all
			select
					null::text as column_1,
					null::text as column_2,
					null::text as column_3
			union all			
			select
					'Informace čitatele:' as column_1,
					null::text as column_2,
					null::text as column_3
			union all
			select
					t1.column_1,
					t1.column_2,
					t1.column_3
			from
					nfiesta_results.fn_get_user_metadata_numerator('cs-CZ',_id_group) as t1;
		else
			return query
			select
					'Topic:' as column_1,
					_res_topic as column_2,
					null::text as column_3
			union all
			select
					'Period:' as column_1,
					_res_estimation_period as column_2,
					null::text as column_3
			union all
			select
					'Geographic region:' as column_1,
					_res_estimation_cell_collection as column_2,
					null::text as column_3
			union all
			select
					'Unit of measurement:' as column_1,
					_res_unit_of_measurement as column_2,
					null::text as column_3
			union all
			select
					null::text as column_1,
					null::text as column_2,
					null::text as column_3
			union all			
			select
					'Numerator informations:' as column_1,
					null::text as column_2,
					null::text as column_3
			union all
			select
					t1.column_1,
					t1.column_2,
					t1.column_3
			from
					nfiesta_results.fn_get_user_metadata_numerator('en-GB',_id_group) as t1;		
		end if;
	else
		-- basic metadata + numerator metadata + denominator metadata
		if _lang_suffix = ''
		then
			return query
			select
					'Tematický okruh:' as column_1,
					_res_topic as column_2,
					null::text as column_3
			union all
			select
					'Období:' as column_1,
					_res_estimation_period as column_2,
					null::text as column_3
			union all
			select
					'Geografické členění:' as column_1,
					_res_estimation_cell_collection as column_2,
					null::text as column_3
			union all
			select
					'Jednotka výstupu:' as column_1,
					_res_unit_of_measurement as column_2,
					null::text as column_3
			union all
			select
					null::text as column_1,
					null::text as column_2,
					null::text as column_3
			union all			
			select
					'Informace čitatele:' as column_1,
					null::text as column_2,
					null::text as column_3
			union all
			select
					t1.column_1,
					t1.column_2,
					t1.column_3
			from
					nfiesta_results.fn_get_user_metadata_numerator('cs-CZ',_id_group) as t1
			union all
			select
					null::text as column_1,
					null::text as column_2,
					null::text as column_3
			union all			
			select
					'Informace jmenovatele:' as column_1,
					null::text as column_2,
					null::text as column_3
			union all		
			select
					t2.column_1,
					t2.column_2,
					t2.column_3
			from
					nfiesta_results.fn_get_user_metadata_denominator('cs-CZ',_id_group) as t2;				
		else
			return query
			select
					'Topic:' as column_1,
					_res_topic as column_2,
					null::text as column_3
			union all
			select
					'Period:' as column_1,
					_res_estimation_period as column_2,
					null::text as column_3
			union all
			select
					'Geographic region:' as column_1,
					_res_estimation_cell_collection as column_2,
					null::text as column_3
			union all
			select
					'Unit of measurement:' as column_1,
					_res_unit_of_measurement as column_2,
					null::text as column_3
			union all
			select
					null::text as column_1,
					null::text as column_2,
					null::text as column_3
			union all			
			select
					'Numerator informations:' as column_1,
					null::text as column_2,
					null::text as column_3
			union all
			select
					t1.column_1,
					t1.column_2,
					t1.column_3
			from
					nfiesta_results.fn_get_user_metadata_numerator('en-GB',_id_group) as t1
			union all
			select
					null::text as column_1,
					null::text as column_2,
					null::text as column_3
			union all			
			select
					'Denominator informations:' as column_1,
					null::text as column_2,
					null::text as column_3
			union all		
			select
					t2.column_1,
					t2.column_2,
					t2.column_3
			from
					nfiesta_results.fn_get_user_metadata_denominator('en-GB',_id_group) as t2;			
		end if;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) is
'The function returns metadata for given input ID (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) to public;


--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean);

create or replace function nfiesta_results.fn_get_user_options_denominator
(
	_jlang character varying,
	_denom_id_group integer[],
	_denom_indicator boolean DEFAULT false,
	_denom_state boolean DEFAULT false
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[]
)
language plpgsql
stable
as
$$
declare
	_lang					integer;
	_lang_label				varchar;
	_lang_column			text;
	_lang_covariate			text;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	if _denom_id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_denominator: Input argument _denom_id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	-- DENOM_INDICATOR --
	-----------------------------------------------------------------
	if _denom_indicator = false
	then
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group
				where id in (select unnest($1))
				and web = true
				)
		,w2 as	(
				select
						w1.target_variable_denom,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						w2.*,
						((ctv.metadata->'''||_lang_label||''')->''indicator'')->>''label'' as label
				from
						w2
						inner join nfiesta_results.c_target_variable as ctv
						on w2.target_variable_denom = ctv.id
				)
		,w4 as	(
				select
						w3.label,
						unnest(w3.id) as id
				from
						w3
				)
		,w5 as	(
				select w4.label, array_agg(w4.id) as id from w4 group by w4.label
				)
		select
				1 as res_id,
				w5.label as res_label,
				w5.id as res_id_group
		from
				w5 order by w5.label;
		'
		using _denom_id_group;
	else
		-----------------------------------------------------------------
		-- DENOM_STATE --
		-----------------------------------------------------------------
		if _denom_state = false
		then
			return query execute
			'
			with
			w1 as	(
					select * from nfiesta_results.t_result_group
					where id in (select unnest($1))
					and web = true
					)
			,w2 as	(
					select
							w1.target_variable_denom,
							array_agg(w1.id order by w1.id) as id
					from
							w1 group by target_variable_denom
					)
			,w3 as	(
					select
							w2.*,
							((ctv.metadata->'''||_lang_label||''')->''state_or_change'')->>''label'' as label
					from
							w2
							inner join nfiesta_results.c_target_variable as ctv
							on w2.target_variable_denom = ctv.id
					)
			,w4 as	(
					select
							w3.label,
							unnest(w3.id) as id
					from
							w3
					)
			,w5 as	(
					select w4.label, array_agg(w4.id) as id from w4 group by w4.label
					)
			select
					1 as res_id,
					w5.label as res_label,
					w5.id as res_id_group
			from
					w5 order by w5.label;
			'
			using _denom_id_group;
		else
			raise exception 'Error 02: fn_get_user_options_denominator: This variant of function is not implemented !';
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean) is
'The function returns list of available IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean) to public;


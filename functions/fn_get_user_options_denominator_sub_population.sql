--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_sub_population
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_sub_population
(
	_jlang character varying,
	_id_group integer[],
	_sub_population integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_sub_population integer[],
	res_division boolean
)
as
$$
declare
	_lang									integer;
	_lang_label								varchar;
	_lang_column							text;
	_without_distinction_label				text;
	_target_variable_denom					integer[];
	_check_records							integer;
	_count_target_variable					integer;
	_count_target_variable_with_core		integer;
	_count_target_variable_with_division	integer;
	_res_division							boolean;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_denominator_sub_population: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom;

	if array_length(_target_variable_denom,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_denominator_sub_population: The values in input argument _id_group = % must only be for the same target variable denominator!',_id_group;
	end if;
	-----------------------------------------------------------------
	_without_distinction_label := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_label'::character varying))::text;
	-----------------------------------------------------------------
	if _sub_population is null -- first calling of function
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)	
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when csp.label_en is null then $2 else csp.label_en end as label_en,
						case when t.label is null then $2 else t.label end as label
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
						left join	(
									select cspl.* from nfiesta_results.c_sub_population_language as cspl
									where cspl.language = $3
									and cspl.sub_population in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.sub_population
				)
		---------------------------------------------------
		,w17a as	(
					select
							w16.atomic_type as res_id,
							(w16.'||_lang_column||')::text AS res_label,
							w16.id as res_id_group,
							array[w16.atomic_type] as res_sub_population
					from
							w16
					)
		,w17 as	(
				select
						w17a.res_id,
						w17a.res_label,
						w17a.res_id_group,
						w17a.res_sub_population,
						null::boolean as res_division
				from
						w17a
				)
		-----------------------------------------
		,w18 as	(
				select
						a.*,
						b.target_variable
				from
						(
						select
								w17.res_id,
								w17.res_label,
								w17.res_sub_population,
								unnest(w17.res_id_group) as res_id_group
						from
								w17 where w17.res_id = 0
						) as a
				inner join (select * from nfiesta_results.t_result_group where web = true) as b on a.res_id_group = b.id
				)
		,w19 as	(
				select
						ctv.id,
						(ctv.metadata->''en'')->''local_densities'' as local_densities
				from
						nfiesta_results.c_target_variable as ctv where ctv.id in (select distinct w18.target_variable from w18)
				)
		,w20 as	(
				select
						w19.id,
						json_array_elements(w19.local_densities) as local_densities
				from
						w19
				)
		,w21 as	(
				select
						w20.id,
						(w20.local_densities->''ldsity_type'')->>''label'' as object_type_label
				from
						w20
				)
		,w22 as	(
				select w18.* from w18 where w18.target_variable in (select w21.id from w21 where w21.object_type_label = ''division'')
				)
		,w23 as	(
				select w18.* from w18 where w18.target_variable not in (select distinct w22.target_variable from w22) -- TARGET VARIABLE WITHOUT DIVISION
				)
		,w24a as	(
					select
							w23.res_id,
							w23.res_label,
							w23.res_sub_population,
							array_agg(res_id_group) as res_id_group
					from
							w23 group by w23.res_id, w23.res_label, w23.res_sub_population
					)
		,w24 as	(
				select
						w24a.res_id,
						w24a.res_label,
						w24a.res_sub_population,
						w24a.res_id_group,
						null::boolean as res_division
				from
						w24a
				)
				select w24.res_id, w24.res_label, w24.res_id_group, w24.res_sub_population, w24.res_division from w24 union all
				select w17.res_id, w17.res_label, w17.res_id_group, w17.res_sub_population, w17.res_division from w17 where w17.res_id is distinct from 0;
		'
		using _id_group, _without_distinction_label, _lang;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as sub_population,
								t.id
						from
								(
								select w11.sub_population, w11.id, w11.atomic_type from w11 union all
								select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when csp.label_en is null then $3 else csp.label_en end as label_en,
						case when t.label is null then $3 else t.label end as label
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
						left join	(
									select cspl.* from nfiesta_results.c_sub_population_language as cspl
									where cspl.language = $4
									and cspl.sub_population in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.sub_population
				)
		---------------------------------------------------
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.'||_lang_column||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_sub_population
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _sub_population, _without_distinction_label, _lang
		into _check_records;	
		
		if _check_records = 0
		then
			-------------------------------------
			-- input _id_group must be for the same group of target variables:
			-- variant 1 => target variables without division ldsity object(s)
			-- variant 2 => target variables with core ldsity object and with division ldsity object(s)
			-------------------------------------
			with
			w1 as	(
					select
							ctv.id,
							(ctv.metadata->'en')->'local_densities' as local_densities
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id in	(
										select trg.target_variable
										from nfiesta_results.t_result_group as trg
										where trg.id in (select unnest(_id_group))
										and trg.web = true
										)
					)
			,w2 as	(
					select
							w1.id,
							json_array_elements(w1.local_densities) as local_densities
					from
							w1
					)
			,w3 as	(
					select
							w2.id,
							(w2.local_densities->'ldsity_type')->>'label' as object_type_label
					from
							w2
					)
			-------------------------------
			,w4a as	(select distinct w3.id from w3)
			,w4b as	(select distinct w3.id from w3 where w3.object_type_label = 'core')
			,w4c as	(select distinct w3.id from w3 where w3.object_type_label = 'division')
			-------------------------------
			,w5a as	(select 1 as id, count(w4a.*) as count_target_variable from w4a)
			,w5b as	(select 1 as id, count(w4b.*) as count_target_variable_with_core from w4b)
			,w5c as	(select 1 as id, count(w4c.*) as count_target_variable_with_division from w4c)
			-------------------------------
			select
					w5a.count_target_variable,
					w5b.count_target_variable_with_core,
					w5c.count_target_variable_with_division
			from
					w5a
					inner join w5b on w5a.id = w5b.id
					inner join w5c on w5a.id = w5c.id
			into
					_count_target_variable,
					_count_target_variable_with_core,
					_count_target_variable_with_division;
			-------------------------------------
			-------------------------------------
			if _sub_population = array[0] -- first user selection was without distinction
			then
				if _count_target_variable_with_division is distinct from 0
				then
					raise exception 'Error: 03: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then the values in input argument _id_group = % must only be for target variables that are without any division local density!',_id_group;
				end if;

				-- variant 1
				if _count_target_variable is distinct from _count_target_variable_with_core
				then
					raise exception 'Error: 04: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						_sub_population as res_sub_population,
						false as res_division; 
			else
				-- second and more user selections

				if _count_target_variable_with_division = 0
				then
					-- variant 1 => not exists any target variable with division
					if _count_target_variable is distinct from _count_target_variable_with_core
					then
						raise exception 'Error: 05: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
					end if;

					_res_division := false;
				else
					-- variant 2 => exist target variable with any division
					if	(_count_target_variable is distinct from _count_target_variable_with_division)
						or
						(_count_target_variable_with_core is distinct from _count_target_variable_with_division)
					then
						raise exception 'Error: 06: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables must contains some division local density!',_id_group;
					end if;

					_res_division := true;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						array_remove(_sub_population,0) as res_sub_population,
						_res_division as res_division;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.sub_population] as sub_population,
							t.id
					from
							(
							select w1.sub_population, array_agg(w1.id order by w1.id) as id
							from w1 where w1.sub_population = 0
							group by w1.sub_population
							) as t
					)
			,w3 as	(
					select w1.sub_population, array_agg(w1.id order by w1.id) as id
					from w1 where w1.sub_population > 0
					group by w1.sub_population
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_sub_population as csp
							on w3.sub_population = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_sub_population_category as cmspc
					where cmspc.sub_population_category in
						(
						select cspc.id from nfiesta_results.c_sub_population_category as cspc
						where cspc.sub_population in (select w6.sub_population from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.sub_population as sub_population,
							cspc2.sub_population as atomic_type
					from
							w7
							inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
							inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.sub_population from w8
					)
			,w10 as (
					select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.sub_population
					)
			,w11 as (
					select
							w6.sub_population,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.sub_population = w10.sub_population
					)
			,w12 as	(
					select
							w11.sub_population,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.sub_population,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.sub_population,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
							from	(
									select distinct a1.atomic_type, a1.sub_population
									from
										(
										select w13.atomic_type, w13.sub_population from w13
										union all
										select w5.sub_population as atomic_type, w5.sub_population from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as sub_population,
									null::integer[] as sub_population,
									t.id
							from
									(
									select w11.sub_population, w11.id, w11.atomic_type from w11 union all
									select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			---------------------------------------------------
			,w16 as	(
					select
							w15.*,
							case when csp.label_en is null then $3 else csp.label_en end as label_en,
							case when t.label is null then $3 else t.label end as label
					from
							w15
							left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
							left join	(
										select cspl.* from nfiesta_results.c_sub_population_language as cspl
										where cspl.language = $4
										and cspl.sub_population in (select w15.atomic_type from w15)
										) as t
							on w15.atomic_type = t.sub_population
					)
			---------------------------------------------------
			select
					w16.atomic_type as res_id,
					(w16.'||_lang_column||')::text AS res_label,
					w16.id as res_id_group,
					$2 || array[w16.atomic_type] as res_sub_population,
					null::boolean as res_division
			from
					w16
					
					where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _sub_population, _without_distinction_label, _lang;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]) is
'The function returns list of available sub populations for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of sub populations.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]) to public;


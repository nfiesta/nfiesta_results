--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean);

create or replace function nfiesta_results.fn_get_user_options_numerator
(
	_jlang character varying,
	_topic integer default null::integer,
	_num_estimation_period integer default null::integer,
	_num_estimation_cell_collection integer default null::integer,
	_num_id_group integer[] default null::integer[],
	_num_indicator boolean default false,
	_num_state boolean default false,
	_num_unit_of_measure boolean default false,
	_unit_of_measurement boolean default false
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[]
)
language plpgsql
stable
as
$$
declare
	_lang					integer;
	_lang_label				varchar;
	_lang_column			text;
	_lang_covariate			text;
	_check_num_id_group		integer;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	-- TOPIC --
	-----------------------------------------------------------------
	if _topic is null
	then
		return query execute
		'
		with
		w1 as	(
				select
						distinct
						target_variable,
						case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom
				from
						nfiesta_results.t_result_group where web = true
				)
		,w2 as	(
				select
						target_variable,
						case when denominator is null then 0 else denominator end as denominator,
						topic
				from
						nfiesta_results.cm_result2topic
				)
		,w3 as	(
				select distinct w2.topic from w1 inner join w2
				on w1.target_variable = w2.target_variable
				and w1.target_variable_denom = w2.denominator
				)
		-----------------------------------------
		,w4a as	(
				select
						id,
						label_en
				from
						nfiesta_results.c_topic
				where
						id in (select w3.topic from w3)
				)
		,w4b as	(
				select ctl.* from nfiesta_results.c_topic_language as ctl
				where ctl.language = $1
				and ctl.topic in (select w4a.id from w4a)
				)
		,w4c as	(
				select w4a.id, w4a.label_en, w4b.label
				from w4a left join w4b
				on w4a.id = w4b.topic
				)
		,w4 as	(
				select
						w4c.id,
						w4c.'||_lang_column||' AS label
				from
						w4c where w4c.'||_lang_column||' is not null
				)
		-----------------------------------------
		select
				w4.id as res_id,
				w4.label::text as res_label,
				null::integer[] as res_id_group
		from
				w4 order by w4.label;
		'
		using _lang;
	else
		-----------------------------------------------------------------
		-- ESTIMATION_PERIOD --
		-----------------------------------------------------------------
		if _num_estimation_period is null
		then
			return query execute
			'
			with
			w1 as	(
					select
							target_variable,
							case when denominator is null then 0 else denominator end as denominator
					from
							nfiesta_results.cm_result2topic where topic = $1
					)
			,w2 as	(
					select
							target_variable,
							case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
							estimation_period
					from
							nfiesta_results.t_result_group where web = true
					)
			,w3 as	(
					select distinct w2.estimation_period from w2 inner join w1
					on w2.target_variable = w1.target_variable
					and w2.target_variable_denom = w1.denominator
					)
			-----------------------------------------
			,w4a as	(
					select
							id,
							label_en
					from
							nfiesta_results.c_estimation_period
					where
							id in (select w3.estimation_period from w3)
					)
			,w4b as	(
					select cepl.* from nfiesta_results.c_estimation_period_language as cepl
					where cepl.language = $2
					and cepl.estimation_period in (select w4a.id from w4a)
					)
			,w4c as	(
					select w4a.id, w4a.label_en, w4b.label
					from w4a left join w4b
					on w4a.id = w4b.estimation_period
					)
			,w4 as	(
					select
							w4c.id,
							w4c.'||_lang_column||' AS label
					from
							w4c where w4c.'||_lang_column||' is not null
					)
			-----------------------------------------
			select
					w4.id as res_id,
					w4.label::text as res_label,
					null::integer[] as res_id_group
			from
					w4 order by w4.label;				
			'
			using _topic, _lang;
		else
			-----------------------------------------------------------------
			-- ESTIMATION_CELL_COLLECTION --
			-----------------------------------------------------------------
			if _num_estimation_cell_collection is null
			then
				return query execute
				'			
				with
				w1 as	(
						select
								target_variable,
								case when denominator is null then 0 else denominator end as denominator
						from
								nfiesta_results.cm_result2topic where topic = $1
						)
				,w2 as	(
						select
								target_variable,
								case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
								estimation_period,
								estimation_cell_collection
						from
								nfiesta_results.t_result_group where web = true
						)
				,w3 as	(
						select * from w2 inner join w1
						on w2.target_variable = w1.target_variable
						and w2.target_variable_denom = w1.denominator
						)
				,w4 as	(
						select distinct w3.estimation_cell_collection from w3 where w3.estimation_period = $2
						)
				-----------------------------------------
				,w5a as	(
						select
								id,
								label_en
						from
								nfiesta_results.c_estimation_cell_collection
						where
								id in (select w4.estimation_cell_collection from w4)
						)
				,w5b as	(
						select cecc.* from nfiesta_results.c_estimation_cell_collection_language as cecc
						where cecc.language = $3
						and cecc.estimation_cell_collection in (select w5a.id from w5a)
						)
				,w5c as	(
						select w5a.id, w5a.label_en, w5b.label
						from w5a left join w5b
						on w5a.id = w5b.estimation_cell_collection
						)
				,w5 as	(
						select
								w5c.id,
								w5c.'||_lang_column||' AS label
						from
								w5c where w5c.'||_lang_column||' is not null
						)
				-----------------------------------------
				select
						w5.id as res_id,
						w5.label::text as res_label,
						null::integer[] as res_id_group
				from
						w5 order by w5.label;
				'
				using _topic, _num_estimation_period, _lang;
			else
				-----------------------------------------------------------------
				-- INDICATOR --
				-----------------------------------------------------------------
				if _num_indicator = false
				then
					return query execute
					'
					with
					w1 as	(
							select
									target_variable,
									case when denominator is null then 0 else denominator end as denominator
							from
									nfiesta_results.cm_result2topic where topic = $1
							)
					,w2 as	(
							select
									id,
									target_variable,
									case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
									estimation_period,
									estimation_cell_collection
							from
									nfiesta_results.t_result_group where web = true
							)
					,w3 as	(
							select w2.* from w2 inner join w1
							on w2.target_variable = w1.target_variable
							and w2.target_variable_denom = w1.denominator
							)
					,w4 as	(
							select w3.* from w3
							where w3.estimation_period = $2
							and w3.estimation_cell_collection = $3
							)
					,w5 as	(
							select
									target_variable,
									array_agg(w4.id order by w4.id) as id
							from
									w4 group by target_variable
							)
					,w6 as	(
							select
									w5.*,
									((ctv.metadata->'''||_lang_label||''')->''indicator'')->>''label'' as label
							from
									w5
									inner join nfiesta_results.c_target_variable as ctv
									on w5.target_variable = ctv.id
							)
					,w7 as	(
							select
									w6.label,
									unnest(w6.id) as id
							from
									w6
							)
					,w8 as	(
							select w7.label, array_agg(w7.id) as id from w7 group by w7.label
							)
					select
							null::integer as res_id,
							w8.label as res_label,
							w8.id as res_id_group
					from
							w8 order by w8.label;
					'
					using _topic, _num_estimation_period, _num_estimation_cell_collection;
				else
					-----------------------------------------------------------------
					-- NUM_STATE --
					-----------------------------------------------------------------
					if _num_state = false
					then
						return query execute
						'
						with
						w1 as	(
								select * from nfiesta_results.t_result_group as trg
								where trg.id in (select unnest($1))
								and trg.web = true
								)
						,w2 as	(
								select
										target_variable,
										array_agg(w1.id order by w1.id) as id
								from
										w1 group by target_variable
								)
						,w3 as	(
								select
										w2.*,
										((ctv.metadata->'''||_lang_label||''')->''state_or_change'')->>''label'' as label
								from
										w2
										inner join nfiesta_results.c_target_variable as ctv
										on w2.target_variable = ctv.id
								)
						,w4 as	(
								select
										w3.label,
										unnest(w3.id) as id
								from
										w3
								)
						,w5 as	(
								select w4.label, array_agg(w4.id) as id from w4 group by w4.label
								)
						select
								null::integer as res_id,
								w5.label as res_label,
								w5.id as res_id_group
						from
								w5 order by w5.label;
						'
						using _num_id_group;					
					else
						-----------------------------------------------------------------
						-- NUM_UNIT_OF_MEASURE --
						-----------------------------------------------------------------
						if _num_unit_of_measure = false
						then
							return query execute
							'
							with
							w1 as	(
									select * from nfiesta_results.t_result_group as trg
									where trg.id in (select unnest($1))
									and trg.web = true
									)
							,w2 as	(
									select
											target_variable,
											array_agg(w1.id order by w1.id) as id
									from
											w1 group by target_variable
									)
							,w3 as	(
									select
											w2.*,
											((ctv.metadata->'''||_lang_label||''')->''unit'')->>''label'' as label
									from
											w2
											inner join nfiesta_results.c_target_variable as ctv
											on w2.target_variable = ctv.id
									)
							,w4 as	(
									select
											w3.label,
											unnest(w3.id) as id
									from
											w3
									)
							,w5 as	(
									select w4.label, array_agg(w4.id) as id from w4 group by w4.label
									)
							select
									null::integer as res_id,
									w5.label as res_label,
									w5.id as res_id_group
							from
									w5 order by w5.label;
							'
							using _num_id_group;
						else
							-----------------------------------------------------------------
							-- UNIT_OF_MEASUREMENT --
							-----------------------------------------------------------------
							if _unit_of_measurement = false
							then			
								select
										count(t.target_variable)
								from
										(
										select distinct trg.target_variable from nfiesta_results.t_result_group as trg
										where trg.id in (select unnest(_num_id_group))
										and trg.web = true
										) as t
								into _check_num_id_group;

								if _check_num_id_group is distinct from 1
								then
									raise exception 'Error 01: fn_get_user_options_numerator: The values in input argument _num_id_group = % must only be for the same target variable!',_num_id_group;
								end if;

								return query execute
								'
								with
								w1 as	(
										select
												id,
												target_variable,
												coalesce(target_variable_denom,0) as target_variable_denom 
										from
												nfiesta_results.t_result_group
										where
												id in (select unnest($1))
										and
												web = true
										)
								,w2 as	(
										select
												w1.target_variable,
												w1.target_variable_denom,
												array_agg(w1.id order by w1.id) as id
										from
												w1 group by w1.target_variable, w1.target_variable_denom
										)
								,w3 as	(
										select
												w2.*,
												((ctv1.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_nom,
												((ctv2.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_denom
										from
												w2
												inner join nfiesta_results.c_target_variable as ctv1 on w2.target_variable = ctv1.id
												left join nfiesta_results.c_target_variable as ctv2 on w2.target_variable_denom = ctv2.id
										)
								,w4 as	(
										select
												w3.target_variable,
												w3.target_variable_denom,
												w3.id,
												w3.unit_nom,
												w3.unit_denom
										from
												w3
										)
								,w5 as	(
										select
												w4.*,
												case
													when (w4.target_variable = w4.target_variable_denom) or (w4.unit_nom = w4.unit_denom) then ''%''
													when w4.target_variable_denom = 0 then w4.unit_nom
													else concat(w4.unit_nom,'' / '',w4.unit_denom)
												end as label4user,
												------------------
												case
													when w4.target_variable_denom = 0 then 0
													else 1
												end as res_id
										from
												w4
										)
								,w6 as	(
										select
												w5.res_id,
												w5.label4user as label,
												unnest(w5.id) as id
										from
												w5
										)
								,w7 as	(
										select
												w6.res_id,
												w6.label,
												array_agg(w6.id) as id
										from
												w6 group by w6.res_id, w6.label
										)
								select
										w7.res_id,
										w7.label as res_label,
										w7.id as res_id_group
								from
										w7 order by w7.label;
								'
								using _num_id_group;																	
							else
								raise exception 'Error 02: fn_get_user_options_numerator: This variant of function is not implemented !';
							end if;
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean) is
'The function returns list of available IDs and labels of topic, estimation period, estimation cell collection, indicator, state or change, unit of indicator and unit of measurement from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean) to public;
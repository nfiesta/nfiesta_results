--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_area_domain
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_area_domain(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_area_domain
(
	_jlang character varying,
	_id_group integer[],
	_area_domain integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_area_domain integer[]
)
as
$$
declare
	_lang						integer;
	_lang_label					varchar;
	_lang_column				text;
	_without_distinction_label	text;
	_target_variable_num		integer[];
	_check_records				integer;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_numerator_area_domain: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_numerator_area_domain: The values in input argument _id_group = % must only be for the same target variable numerator!',_id_group;
	end if;
	-----------------------------------------------------------------
	_without_distinction_label := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_label'::character varying))::text;
	-----------------------------------------------------------------
	if _area_domain is null
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						cad.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as cad
						on w3.area_domain = cad.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmadc
				where cmadc.area_domain_category in
					(
					select cadc.id from nfiesta_results.c_area_domain_category as cadc
					where cadc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cadc1.area_domain as area_domain,
						cadc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cadc1 on w7.area_domain_category = cadc1.id
						inner join nfiesta_results.c_area_domain_category as cadc2 on w7.atomic_category = cadc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)	
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when cad.label_en is null then $2 else cad.label_en end as label_en,
						case when t.label is null then $2 else t.label end as label
				from
						w15
						left join nfiesta_results.c_area_domain as cad on w15.atomic_type = cad.id
						left join	(
									select cadl.* from nfiesta_results.c_area_domain_language as cadl
									where cadl.language = $3
									and cadl.area_domain in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.area_domain
				)
		---------------------------------------------------
		select
				w16.atomic_type as res_id,
				(w16.'||_lang_column||')::text AS res_label,
				w16.id as res_id_group,
				array[w16.atomic_type] as res_area_domain
		from
				w16;
		'
		using _id_group, _without_distinction_label, _lang;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						cad.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as cad
						on w3.area_domain = cad.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmadc
				where cmadc.area_domain_category in
					(
					select cadc.id from nfiesta_results.c_area_domain_category as cadc
					where cadc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cadc1.area_domain as area_domain,
						cadc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cadc1 on w7.area_domain_category = cadc1.id
						inner join nfiesta_results.c_area_domain_category as cadc2 on w7.atomic_category = cadc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as area_domain,
								t.id
						from
								(
								select w11.area_domain, w11.id, w11.atomic_type from w11 union all
								select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when cad.label_en is null then $3 else cad.label_en end as label_en,
						case when t.label is null then $3 else t.label end as label
				from
						w15
						left join nfiesta_results.c_area_domain as cad on w15.atomic_type = cad.id
						left join	(
									select cadl.* from nfiesta_results.c_area_domain_language as cadl
									where cadl.language = $4
									and cadl.area_domain in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.area_domain
				)
		---------------------------------------------------
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.'||_lang_column||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_area_domain
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _area_domain, _without_distinction_label, _lang
		into _check_records;	
		
		if _check_records = 0
		then
			if _area_domain = array[0]
			then
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						_area_domain as _res_area_domain;
			else
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						array_remove(_area_domain,0) as _res_area_domain;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.area_domain] as area_domain,
							t.id
					from
							(
							select w1.area_domain, array_agg(w1.id order by w1.id) as id
							from w1 where w1.area_domain = 0
							group by w1.area_domain
							) as t
					)
			,w3 as	(
					select w1.area_domain, array_agg(w1.id order by w1.id) as id
					from w1 where w1.area_domain > 0
					group by w1.area_domain
					)
			,w4 as	(
					select
							w3.*,
							cad.atomic
					from 
							w3
							inner join nfiesta_results.c_area_domain as cad
							on w3.area_domain = cad.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_area_domain_category as cmadc
					where cmadc.area_domain_category in
						(
						select cadc.id from nfiesta_results.c_area_domain_category as cadc
						where cadc.area_domain in (select w6.area_domain from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cadc1.area_domain as area_domain,
							cadc2.area_domain as atomic_type
					from
							w7
							inner join nfiesta_results.c_area_domain_category as cadc1 on w7.area_domain_category = cadc1.id
							inner join nfiesta_results.c_area_domain_category as cadc2 on w7.atomic_category = cadc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.area_domain from w8
					)
			,w10 as (
					select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.area_domain
					)
			,w11 as (
					select
							w6.area_domain,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.area_domain = w10.area_domain
					)
			,w12 as	(
					select
							w11.area_domain,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.area_domain,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.area_domain,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
							from	(
									select distinct a1.atomic_type, a1.area_domain
									from
										(
										select w13.atomic_type, w13.area_domain from w13
										union all
										select w5.area_domain as atomic_type, w5.area_domain from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as area_domain,
									null::integer[] as area_domain,
									t.id
							from
									(
									select w11.area_domain, w11.id, w11.atomic_type from w11 union all
									select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			---------------------------------------------------
			,w16 as	(
					select
							w15.*,
							case when cad.label_en is null then $3 else cad.label_en end as label_en,
							case when t.label is null then $3 else t.label end as label
					from
							w15
							left join nfiesta_results.c_area_domain as cad on w15.atomic_type = cad.id
							left join	(
										select cadl.* from nfiesta_results.c_area_domain_language as cadl
										where cadl.language = $4
										and cadl.area_domain in (select w15.atomic_type from w15)
										) as t
							on w15.atomic_type = t.area_domain
					)
			---------------------------------------------------
			select
					w16.atomic_type as res_id,
					(w16.'||_lang_column||')::text AS res_label,
					w16.id as res_id_group,
					$2 || array[w16.atomic_type] as res_area_domain
			from
					w16
					
					where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _area_domain, _without_distinction_label, _lang;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_numerator_area_domain(character varying, integer[], integer[]) is
'The function returns list of available area domains for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of area domains.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_area_domain(character varying, integer[], integer[]) to public;


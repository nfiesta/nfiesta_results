--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_ldsity_core
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_ldsity_core
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang								integer;
		_lang_label							varchar;
		_not_specified_label				text;
		_not_specified_description			text;
		_not_specified_definition_variant	json;
		_not_specified_adr_spr				json;
begin	
		-----------------------------------------------------------------
		_lang := nfiesta_results.fn_get_language_id(_jlang);
		_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_numerator_ldsity_core: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		_not_specified_label := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_label'::character varying))::text;
		_not_specified_description := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_description'::character varying))::text;
		-----------------------------------------------------------------
		_not_specified_definition_variant := json_agg(json_build_object('label',_not_specified_label,'description',_not_specified_description));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label',_not_specified_label,'description',_not_specified_description),'restriction',json_build_object('label',_not_specified_label,'description',_not_specified_description)));
		-----------------------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable
				)
		,w3 as	(
				select
						w2.target_variable,
						w2.id,
						((ctv.metadata->'''||_lang_label||''')->''local_densities'') as local_densities
				from
						w2
						inner join nfiesta_results.c_target_variable as ctv
						on w2.target_variable = ctv.id
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where (w4.local_densities->''ldsity_type'')->>''label'' = ''core''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						(w5.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w5.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w5.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w5.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w5.local_densities->''ldsity'')->>''label'' as label,
						(w5.local_densities->''ldsity'')->>''description'' as description,	
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition_variant'' as definition_variant,
						w5.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w5
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w7.object_type_label,''description'',w7.object_type_description),
							''ldsity_object'',json_build_object(''label'',w7.object_label,''description'',w7.object_description),
							''ldsity'',json_build_object(''label'',w7.label,''description'',w7.description),
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition_variant'',w7.definition_variant,
							''area_domain_restrictions'',w7.area_domain_restrictions,
							''sub_population_restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						--w8.new_id,
						json_agg(w8.local_densities order by w8.new_id) as local_densities
				from
						w8 group by w8.target_variable --, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_label, _not_specified_description, _not_specified_definition_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]) is
'The function returns list of available labels of standart local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]) to public;


--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_query
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_query(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_query
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	estimation_cell					text,
	variable_area_domain_num		text,
	variable_sub_population_num		text,
	variable_area_domain_denom		text,
	variable_sub_population_denom	text,	
	point_estimate					numeric,
	standard_deviation				numeric,
	variation_coeficient			numeric,
	--sample_size					integer,
	--min_sample_size				integer,
	interval_estimation				numeric
)
language plpgsql
stable
as
$$
declare
	_lang								integer;
	_lang_label							varchar;
	_lang_suffix						text;
	_without_distinction_label			text;
	_estimate_type						integer;
	_target_variable					integer;
	_target_variable_denom				integer;
	_estimation_period					integer;
	_panel_refyearset_group				integer;
	_phase_estimate_type				integer;
	_phase_estimate_type_denom			integer;
	_estimation_cell_collection			integer;
	_area_domain						integer;
	_area_domain_denom					integer;
	_sub_population						integer;
	_sub_population_denom				integer;
	_cond_area_domain					text;
	_cond_sub_population				text;
	_cond_area_domain_denom				text;
	_cond_sub_population_denom			text;
	_check4unit							integer;
	_coeficient4ratio					numeric;

	_estimation_cell_from_hierarchy_for_ecc_select_by_user			integer;
	_estimation_cell_from_hierarchy_for_ecc_select_by_user_superior	integer;
	_heighest_estimation_cell_select_by_user						integer;
	_heighest_estimation_cell_collection_select_by_user				integer;
	_estimation_cell_collection_array								integer[];
	_test_occurence_result_group									integer;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_suffix = '_en'; else _lang_suffix := ''; end if;
	-----------------------------------------------------------------
	_without_distinction_label := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_label'::character varying))::text;	
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_query: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_query: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select
			trg.estimate_type,
			trg.target_variable,
			trg.target_variable_denom,
			trg.estimation_period,
			trg.panel_refyearset_group,
			trg.phase_estimate_type,
			trg.phase_estimate_type_denom,
			trg.estimation_cell_collection,
			trg.area_domain,
			trg.area_domain_denom,
			trg.sub_population,
			trg.sub_population_denom
	from
			nfiesta_results.t_result_group as trg
	where
			trg.id = _id_group[1]
	into
			_estimate_type,
			_target_variable,
			_target_variable_denom,
			_estimation_period,
			_panel_refyearset_group,
			_phase_estimate_type,
			_phase_estimate_type_denom,
			_estimation_cell_collection,
			_area_domain,
			_area_domain_denom,
			_sub_population,
			_sub_population_denom;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	select tech.cell from nfiesta_results.t_estimation_cell_hierarchy as tech
	where tech.cell in	(
						select cec.id from nfiesta_results.c_estimation_cell as cec
						where cec.estimation_cell_collection = _estimation_cell_collection
						)
	order by cell limit 1
	into _estimation_cell_from_hierarchy_for_ecc_select_by_user;

	if _estimation_cell_from_hierarchy_for_ecc_select_by_user is null
	then
		-- selected estimation cell collection is maybe the heighest
		select count(tech.cell_superior) from nfiesta_results.t_estimation_cell_hierarchy as tech
		where tech.cell_superior in	(
									select cec.id from nfiesta_results.c_estimation_cell as cec
									where cec.estimation_cell_collection = _estimation_cell_collection
									)
		into _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior;

		if _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior = 0
		then
			raise exception 'Error: 03: fn_get_user_query: For user selected estimation_cell_collection = % not exists any estimation cell in t_estimation_cell_hierarchy table!',_estimation_cell_collection;
		end if;

		-- user selected the heighest estimation cell collection => test for occurrence in t_result_group table is not needed
		_estimation_cell_collection_array := array[_estimation_cell_collection];
	else
		with
		w1 as	(
				select * from nfiesta_results.fn_get_heighest_estimation_cell(array[_estimation_cell_from_hierarchy_for_ecc_select_by_user]) as res
				)
		select w1.res[1] from w1
		into _heighest_estimation_cell_select_by_user; -- the heighest estimation cell from hierarchy

		select cec.estimation_cell_collection from nfiesta_results.c_estimation_cell as cec
		where cec.id = _heighest_estimation_cell_select_by_user
		into _heighest_estimation_cell_collection_select_by_user; -- the heighest estimation cell collection from hierarchy

		-- test for occurrence in t_result_group
		select id from nfiesta_results.t_result_group
		where estimate_type = _estimate_type
		and target_variable = _target_variable
		and coalesce(target_variable_denom,0) = coalesce(_target_variable_denom,0)
		and estimation_period = _estimation_period
		and phase_estimate_type = _phase_estimate_type
		and coalesce(phase_estimate_type_denom,0) = coalesce(_phase_estimate_type_denom,0)
		and estimation_cell_collection = _heighest_estimation_cell_collection_select_by_user
		and coalesce(area_domain,0) = coalesce(_area_domain,0)
		and coalesce(area_domain_denom,0) = coalesce(_area_domain_denom,0)
		and coalesce(sub_population,0) = coalesce(_sub_population,0)
		and coalesce(sub_population_denom,0) = coalesce(_sub_population_denom,0)
		and web = true
		into _test_occurence_result_group;

		if _test_occurence_result_group is null
		then
			_estimation_cell_collection_array := array[_estimation_cell_collection];
		else
			_estimation_cell_collection_array := array[_estimation_cell_collection] || array[_heighest_estimation_cell_collection_select_by_user];
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _estimate_type = 1 -- TOTAL
	then
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$2';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$3';
		end if;
		---------------------------------------------------	
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$9,
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					null::integer,
					null::integer,
					null::integer
					)
				)
		,w2 as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $4
				and ttec.panel_refyearset_group = $5
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $6)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($6)))
				and ttec.phase_estimate_type = $7
				)
		,w3 as	(
				select
						w2.id as id_t_total_estimate_conf,
						w2.estimation_cell,
						w2.variable,
						w2.phase_estimate_type,
						w2.force_synthetic,
						w2.estimation_period,
						w2.panel_refyearset_group,
						w1.nominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,
						tv.area_domain_category,
						tv.sub_population_category
				from
						w2
						inner join w1 on w2.variable = w1.nominator_variable
						inner join nfiesta_results.t_variable as tv on w2.variable = tv.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf
				from
						nfiesta_results.t_estimate_conf as tec 
				where
						tec.total_estimate_conf in (select w3.id_t_total_estimate_conf from w3)
				and
						tec.estimate_type = $8
				)
		,w5 as	(
				select
						w4.*,
						w3.*
				from
						w4 inner join w3 on w4.total_estimate_conf = w3.id_t_total_estimate_conf
				)
		,w6 as	(
				select
						tr.*,
						w5.*
				from
						nfiesta_results.t_result as tr
						inner join w5 on tr.estimate_conf = w5.id_t_estimate_conf
				where
						tr.is_latest = true
				)
		,w7 as	(
				select
						cec.id as estimation_cell_id,
						--cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w6.area_domain_category is null then 0
							else cadc.id
						end
							as variable_area_domain_id,	
						---------------------------------
						--case
						--	when w6.area_domain_category is null then ''bez rozlišení''
						--	else replace(cadc.label,'';'',''; '')
						--end
						--	as variable_area_domain,
						---------------------------------
						case
							when w6.area_domain_category is null then ''without distinction''
							else replace(cadc.label_en,'';'',''; '')
						end
							as variable_area_domain_en,
						-----------------------------------------------------
						case
							when w6.sub_population_category is null then 0
							else cspc.id
						end
							as variable_sub_population_id,
						---------------------------------
						--case
						--	when w6.sub_population_category is null then ''bez rozlišení''
						--	else replace(cspc.label,'';'',''; '')
						--end
						--	as variable_sub_population,
						---------------------------------
						case
							when w6.sub_population_category is null then ''without distinction''
							else replace(cspc.label_en,'';'',''; '')
						end
							as variable_sub_population_en,
						-----------------------------------------------------
						w6.point as point_estimate,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else sqrt(w6.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null or w6.point = 0.0::numeric then null::numeric
							else (sqrt(w6.var) / w6.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						--w6.act_ssize as sample_size,
						--w6.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else (1.96 * sqrt(w6.var))
						end
							as interval_estimation
				from
						w6
						inner join nfiesta_results.c_estimation_cell as cec on w6.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc on w6.area_domain_category = cadc.id
						left join nfiesta_results.c_sub_population_category as cspc on w6.sub_population_category = cspc.id
				)
		,w8 as	(
				select
						w7.*,
						-----------------------------------------------------
						t1.description as estimation_cell,
						-----------------------------------------------------
						case
							when t2.area_domain_category is null then $10
							else replace(t2.label,'';'',''; '')
						end
							as variable_area_domain,
						-----------------------------------------------------
						case
							when t3.sub_population_category is null then $10
							else replace(t3.label,'';'',''; '')
						end
							as variable_sub_population			
				from
						w7
						left join	(
									select cecl.* from nfiesta_results.c_estimation_cell_language as cecl
									where cecl.language = $9
									and cecl.estimation_cell in (select w7.estimation_cell_id from w7)
									) as t1
						on w7.estimation_cell_id = t1.estimation_cell

						left join	(
									select cadcl.* from nfiesta_results.c_area_domain_category_language as cadcl
									where cadcl.language = $9
									and cadcl.area_domain_category in (select w7.variable_area_domain_id from w7)
									) as t2
						on w7.variable_area_domain_id = t2.area_domain_category

						left join	(
									select cspcl.* from nfiesta_results.c_sub_population_category_language as cspcl
									where cspcl.language = $9
									and cspcl.sub_population_category in (select w7.variable_sub_population_id from w7)
									) as t3
						on w7.variable_sub_population_id = t3.sub_population_category						
				)
		select
				w8.estimation_cell'||_lang_suffix||' as estimation_cell,
				w8.variable_area_domain'||_lang_suffix||' as variable_area_domain_num,
				w8.variable_sub_population'||_lang_suffix||' as variable_sub_population_num,
				null::text as variable_area_domain_denom,
				null::text as variable_sub_population_denom,
				round(w8.point_estimate::numeric,2) as point_estimate,
				round(w8.standard_deviation::numeric,2) as standard_deviation,
				round(w8.variation_coeficient::numeric,2) as variation_coeficient,
				--w8.sample_size::integer,
				--(ceil(w8.min_sample_size::numeric))::integer as min_sample_size,
				round(w8.interval_estimation::numeric,2) as interval_estimation
		from
				w8 order by w8.estimation_cell_id, w8.variable_area_domain_id, w8.variable_sub_population_id;
		'
		using
				_target_variable,
				_area_domain,
				_sub_population,
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection_array,
				_phase_estimate_type,
				_estimate_type,
				_lang,
				_without_distinction_label;
				
	else -- RATIO
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$3';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$4';
		end if;
		---------------------------------------------------
		if _area_domain_denom is null
		then
			_cond_area_domain_denom := 'null::integer';
		else
			_cond_area_domain_denom := '$5';
		end if;
		---------------------------------------------------
		if _sub_population_denom is null
		then
			_cond_sub_population_denom := 'null::integer';
		else
			_cond_sub_population_denom := '$6';
		end if;
		---------------------------------------------------
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_num,
						((w2.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_denom
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.*,
						case
							when (w3.target_variable_num = w3.target_variable_denom) or (w3.unit_num = w3.unit_denom) then 0
							else 1
						end as check4unit
				from
						w3
				)
		select w4.check4unit from w4
		'
		using _target_variable, _target_variable_denom
		into _check4unit;		
		---------------------------------------------------
		if _check4unit = 0
		then	
			_coeficient4ratio := 100.0::numeric;
		else
			_coeficient4ratio := 1.0::numeric;
		end if;
		---------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$13,
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					$2,
					'||_cond_area_domain_denom||',
					'||_cond_sub_population_denom||'
					)
				)
		,w2a as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($9)))
				and ttec.phase_estimate_type = $10
				)
		,w2b as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.denominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($9)))
				and ttec.phase_estimate_type = $10
				)
		,w3 as	(
				select
						w2a.id as id_t_total_estimate_conf_nom,
						w2b.id as id_t_total_estimate_conf_denom,
						w2a.estimation_cell,
						w2a.variable,
						w2a.phase_estimate_type,
						w2a.force_synthetic,
						w2a.estimation_period,
						w2a.panel_refyearset_group,
						w1.nominator_variable,
						w1.denominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,	
						tv1.area_domain_category as area_domain_category_nom,
						tv2.area_domain_category as area_domain_category_denom,
						tv1.sub_population_category as sub_population_category_nom,
						tv2.sub_population_category as sub_population_category_denom
				from
						w2a
						inner join w1 on w2a.variable = w1.nominator_variable
						inner join w2b on w2b.variable = w1.denominator_variable
						inner join nfiesta_results.t_variable as tv1 on w2a.variable = tv1.id
						inner join nfiesta_results.t_variable as tv2 on w2b.variable = tv2.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf,
						tec.denominator,
						w3.*
				from
						nfiesta_results.t_estimate_conf as tec
						
						inner join w3
						on tec.total_estimate_conf = w3.id_t_total_estimate_conf_nom
						and tec.denominator = w3.id_t_total_estimate_conf_denom
				where
						tec.estimate_type = $11
				)
		,w5 as	(
				select
						tr.id,
						tr.estimate_conf,
						tr.point * $12 as point,
						tr.var * $12 as var,
						--tr.min_ssize,
						--tr.act_ssize,
						w4.*
				from
						nfiesta_results.t_result as tr
						inner join w4 on tr.estimate_conf = w4.id_t_estimate_conf
				where
						tr.is_latest = true
				)			
		,w6 as	(
				select
						cec.id as estimation_cell_id,
						--cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w5.area_domain_category_nom is null then 0
							else cadc1.id
						end
							as variable_area_domain_id_nom,	
						---------------------------------
						case
							when w5.area_domain_category_denom is null then 0
							else cadc2.id
						end
							as variable_area_domain_id_denom,	
						---------------------------------
						--case
						--	when w5.area_domain_category_nom is null then ''bez rozlišení''
						--	else replace(cadc1.label,'';'',''; '')
						--end
						--	as variable_area_domain_nom,
						---------------------------------
						--case
						--	when w5.area_domain_category_denom is null then ''bez rozlišení''
						--	else replace(cadc2.label,'';'',''; '')
						--end
						--	as variable_area_domain_denom,
						---------------------------------
						case
							when w5.area_domain_category_nom is null then ''without distinction''
							else replace(cadc1.label_en,'';'',''; '')
						end
							as variable_area_domain_nom_en,
						---------------------------------
						case
							when w5.area_domain_category_denom is null then ''without distinction''
							else replace(cadc2.label_en,'';'',''; '')
						end
							as variable_area_domain_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						case
							when w5.sub_population_category_nom is null then 0
							else cspc1.id
						end
							as variable_sub_population_id_nom,	
						---------------------------------
						case
							when w5.sub_population_category_denom is null then 0
							else cspc2.id
						end
							as variable_sub_population_id_denom,	
						---------------------------------
						--case
						--	when w5.sub_population_category_nom is null then ''bez rozlišení''
						--	else replace(cspc1.label,'';'',''; '')
						--end
						--	as variable_sub_population_nom,
						---------------------------------
						--case
						--	when w5.sub_population_category_denom is null then ''bez rozlišení''
						--	else replace(cspc2.label,'';'',''; '')
						--end
						--	as variable_sub_population_denom,
						---------------------------------
						case
							when w5.sub_population_category_nom is null then ''without distinction''
							else replace(cspc1.label_en,'';'',''; '')
						end
							as variable_sub_population_nom_en,
						---------------------------------
						case
							when w5.sub_population_category_denom is null then ''without distinction''
							else replace(cspc2.label_en,'';'',''; '')
						end
							as variable_sub_population_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						w5.point as point_estimate,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else sqrt(w5.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null or w5.point = 0.0::numeric then null::numeric
							else (sqrt(w5.var) / w5.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						--w5.act_ssize as sample_size,
						--w5.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else (1.96 * sqrt(w5.var))
						end
							as interval_estimation
				from
						w5
						inner join nfiesta_results.c_estimation_cell as cec on w5.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc1 on w5.area_domain_category_nom = cadc1.id
						left join nfiesta_results.c_area_domain_category as cadc2 on w5.area_domain_category_denom = cadc2.id
						left join nfiesta_results.c_sub_population_category as cspc1 on w5.sub_population_category_nom = cspc1.id
						left join nfiesta_results.c_sub_population_category as cspc2 on w5.sub_population_category_denom = cspc2.id
				)
		,w7 as	(
				select
						w6.*,
						-----------------------------------------------------
						t1.description as estimation_cell,
						-----------------------------------------------------
						case
							when t2.area_domain_category is null then $14
							else replace(t2.label,'';'',''; '')
						end
							as variable_area_domain_nom,
						-----------------------------------------------------
						case
							when t3.area_domain_category is null then $14
							else replace(t3.label,'';'',''; '')
						end
							as variable_area_domain_denom,
						-----------------------------------------------------							
						-----------------------------------------------------
						case
							when t4.sub_population_category is null then $14
							else replace(t4.label,'';'',''; '')
						end
							as variable_sub_population_nom,
						-----------------------------------------------------
						case
							when t5.sub_population_category is null then $14
							else replace(t5.label,'';'',''; '')
						end
							as variable_sub_population_denom
				from
						w6
						left join	(
									select cecl.* from nfiesta_results.c_estimation_cell_language as cecl
									where cecl.language = $13
									and cecl.estimation_cell in (select w6.estimation_cell_id from w6)
									) as t1
						on w6.estimation_cell_id = t1.estimation_cell

						left join	(
									select cadcl.* from nfiesta_results.c_area_domain_category_language as cadcl
									where cadcl.language = $13
									and cadcl.area_domain_category in (select w6.variable_area_domain_id_nom from w6)
									) as t2
						on w6.variable_area_domain_id_nom = t2.area_domain_category

						left join	(
									select cadcl.* from nfiesta_results.c_area_domain_category_language as cadcl
									where cadcl.language = $13
									and cadcl.area_domain_category in (select w6.variable_area_domain_id_denom from w6)
									) as t3
						on w6.variable_area_domain_id_denom = t3.area_domain_category						

						left join	(
									select cspcl.* from nfiesta_results.c_sub_population_category_language as cspcl
									where cspcl.language = $13
									and cspcl.sub_population_category in (select w6.variable_sub_population_id_nom from w6)
									) as t4
						on w6.variable_sub_population_id_nom = t4.sub_population_category

						left join	(
									select cspcl.* from nfiesta_results.c_sub_population_category_language as cspcl
									where cspcl.language = $13
									and cspcl.sub_population_category in (select w6.variable_sub_population_id_denom from w6)
									) as t5
						on w6.variable_sub_population_id_denom = t5.sub_population_category
				)
		select
				w7.estimation_cell'||_lang_suffix||' as estimation_cell,
				--concat(w7.variable_area_domain_nom'||_lang_suffix||','' / '',w6.variable_area_domain_denom'||_lang_suffix||') as variable_area_domain,
				--concat(w7.variable_sub_population_nom'||_lang_suffix||','' / '',w6.variable_sub_population_denom'||_lang_suffix||') as variable_sub_population,
				w7.variable_area_domain_nom'||_lang_suffix||' as variable_area_domain_num,
				w7.variable_sub_population_nom'||_lang_suffix||' as variable_sub_population_num,
				w7.variable_area_domain_denom'||_lang_suffix||' as variable_area_domain_denom,				
				w7.variable_sub_population_denom'||_lang_suffix||' as variable_sub_population_denom,				
				round(w7.point_estimate::numeric,2) as point_estimate,
				round(w7.standard_deviation::numeric,2) as standard_deviation,
				round(w7.variation_coeficient::numeric,2) as variation_coeficient,
				--w7.sample_size::integer,
				--(ceil(w7.min_sample_size::numeric))::integer as min_sample_size,
				round(w7.interval_estimation::numeric,2) as interval_estimation
		from
				w7
		order
				by	w7.estimation_cell_id,
					w7.variable_area_domain_id_nom,
					w7.variable_area_domain_id_denom,
					w7.variable_sub_population_id_nom,
					w7.variable_sub_population_id_denom;
		'
		using
				_target_variable,
				_target_variable_denom,
				_area_domain,
				_sub_population,
				_area_domain_denom,
				_sub_population_denom,				
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection_array,
				_phase_estimate_type,
				_estimate_type,
				_coeficient4ratio,
				_lang,
				_without_distinction_label;	
	end if;

end;
$$
;

comment on function nfiesta_results.fn_get_user_query(character varying, integer[]) is
'The function returns result estimations for gived group of result and attribute variables from t_result table.';

grant execute on function nfiesta_results.fn_get_user_query(character varying, integer[]) to public;


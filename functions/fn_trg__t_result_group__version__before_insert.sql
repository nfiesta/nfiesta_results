--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg__t_result_group__version__before_insert
---------------------------------------------------------------------------------------------------

-- drop function target_data.fn_trg__t_result_group__version__before_insert();

create or replace function target_data.fn_trg__t_result_group__version__before_insert() 
returns trigger as 
$$
declare
begin
		select array_agg(trg.id order by trg.id) from nfi_results4web.t_result_group as trg
		where trg.estimate_type = new.estimate_type
		and trg.estimation_period = new.estimate_period
		and trg.panel_refyearset_group = new.panel_refyearset_group
		and trg.phase_estimate_type = new.phase_estimate_type
		and trg.estimation_cell_collection = new.estimation_cell_collection
		and coalesce(trg.target_variable_denom,0) = coalesce(new.target_variable_denom,0)
		and coalesce(trg.phase_estimate_type_denom,0) = coalesce(new.phase_estimate_type_denom,0)
		and coalesce(trg.area_domain,0) = coalesce(new.area_domain,0)
		and coalesce(trg.sub_population,0) = coalesce(new.sub_population,0)
		into _array_id;

		/*
		select array_agg(trg.id order by trg.id) from nfi_results4web.t_result_group as trg
		where trg.estimate_type = 1
		and trg.estimation_period = 1
		and trg.panel_refyearset_group = 15
		and trg.phase_estimate_type = 1
		and trg.estimation_cell_collection = 1
		and coalesce(trg.target_variable_denom,0) = coalesce(null::integer,0)
		and coalesce(trg.phase_estimate_type_denom,0) = coalesce(null::integer,0)
		and coalesce(trg.area_domain,0) = coalesce(null::integer,0)
		and coalesce(trg.sub_population,0) = coalesce(null::integer,0)
		*/

		-- kontrola indikatoru => pokud 0 zaznamu tak OK
		-- pokud nejaky zaznam tak postup pres dalsi metadata, ale muze existovat preklep,
		-- takze tato kontrola je neproveditelna v db triggerem

return new;
end;
$$
language plpgsql;

comment on function target_data.fn_trg__t_result_group__version__before_insert() is
'This trigger function controls the newly inserted records into t_result_group table.';

grant execute on function target_data.fn_trg__t_result_group__version__before_insert() to public;


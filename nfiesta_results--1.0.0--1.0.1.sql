--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_get_attribute_categories4target_variable" schema="nfiesta_results" src="functions/fn_get_attribute_categories4target_variable.sql">
--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta_results.fn_get_attribute_categories4target_variable(integer, integer, integer, integer, integer, integer)
--DROP FUNCTION nfiesta_results.fn_get_attribute_categories4target_variable(integer, integer, integer, integer, integer, integer);
CREATE OR REPLACE FUNCTION nfiesta_results.fn_get_attribute_categories4target_variable(
	_numerator_target_variable integer,
	_numerator_area_domain integer DEFAULT NULL::int,
	_numerator_sub_population integer DEFAULT NULL::int,
	_denominator_target_variable integer DEFAULT NULL::int,
	_denominator_area_domain integer DEFAULT NULL::int,
	_denominator_sub_population integer DEFAULT NULL::int)
RETURNS TABLE (
	nominator_variable integer,
	denominator_variable integer, 
	label varchar,
	description text,
	label_en varchar, 
	description_en text
)
AS
$function$
BEGIN
IF _numerator_target_variable IS NULL THEN
	RAISE EXCEPTION 'Target variable for numerator is NULL!';
END IF;

IF NOT EXISTS (SELECT id FROM nfiesta_results.c_target_variable WHERE id = _numerator_target_variable)
THEN
	RAISE EXCEPTION 'Specified numerator target variable (%) does not exist in table c_target_variable!', _numerator_target_variable;
END IF;

IF _numerator_area_domain IS NOT NULL AND _numerator_area_domain != 0
THEN
	IF NOT EXISTS (SELECT * FROM nfiesta_results.c_area_domain WHERE id = _numerator_area_domain)
	THEN RAISE EXCEPTION 'Given numerator area domain (%) does not exist in table c_area_domain!', _numerator_area_domain;
	END IF;
END IF;

IF _numerator_sub_population IS NOT NULL AND _numerator_sub_population != 0
THEN
	IF NOT EXISTS (SELECT * FROM nfiesta_results.c_sub_population WHERE id = _numerator_sub_population)
	THEN RAISE EXCEPTION 'Given numerator sub population (%) does not exist in table c_sub_population!', _numerator_sub_population;
	END IF;
END IF;

IF _denominator_target_variable IS NOT NULL
THEN
	IF NOT EXISTS (SELECT * FROM nfiesta_results.c_target_variable WHERE id = _denominator_target_variable)
	THEN RAISE EXCEPTION 'Given denominator target variable (%) does not exist in table c_target_variable!', _denominator_targer_variable;
	END IF;
END IF;

IF _denominator_area_domain IS NOT NULL AND _denominator_area_domain != 0
THEN
	IF NOT EXISTS (SELECT * FROM nfiesta_results.c_area_domain WHERE id = _denominator_area_domain)
	THEN RAISE EXCEPTION 'Given denominator area domain (%) does not exist in table c_area_domain!', _denominator_area_domain;
	END IF;
END IF;

IF _denominator_sub_population IS NOT NULL AND _denominator_sub_population != 0
THEN
	IF NOT EXISTS (SELECT * FROM nfiesta_results.c_sub_population WHERE id = _denominator_sub_population)
	THEN RAISE EXCEPTION 'Given denominator sub population (%) does not exist in table c_sub_population!', _denominator_sub_population;
	END IF;
END IF;

RETURN QUERY
	WITH w_ad_num AS(
		SELECT t1.variable, t1.category AS adc, t1.label AS adc_label, t1.description AS adc_description, t1.label_en AS adc_label_en, t1.description_en AS adc_description_en
		FROM nfiesta_results.fn_get_area_sub_population_categories(_numerator_target_variable,100,_numerator_area_domain) AS t1
	), w_sp_num AS (
		SELECT t1.variable, t1.category AS spc, t1.label AS spc_label, t1.description AS spc_description, t1.label_en AS spc_label_en, t1.description_en AS spc_description_en
		FROM nfiesta_results.fn_get_area_sub_population_categories(_numerator_target_variable,200,_numerator_sub_population) AS t1
	), w_numerator_variables AS (
		SELECT 	t1.variable AS id,
			t1.adc, adc_label, adc_description, adc_label_en, adc_description_en, 
			t2.spc, spc_label, spc_description, spc_label_en, spc_description_en
		FROM w_ad_num AS t1
		INNER JOIN w_sp_num AS t2
		ON t1.variable = t2.variable
	), w_numerator_variables_agg AS (
		SELECT array_agg(id ORDER BY id) AS num_variables
		FROM w_numerator_variables
	), w_ad_denom AS(
		SELECT t1.variable, t1.category AS adc, t1.label AS adc_label, t1.description AS adc_description, t1.label_en AS adc_label_en, t1.description_en AS adc_description_en
		FROM nfiesta_results.fn_get_area_sub_population_categories(_denominator_target_variable,100,_denominator_area_domain) AS t1
	), w_sp_denom AS (
		SELECT t1.variable, t1.category AS spc, t1.label AS spc_label, t1.description AS spc_description, t1.label_en AS spc_label_en, t1.description_en AS spc_description_en
		FROM nfiesta_results.fn_get_area_sub_population_categories(_denominator_target_variable,200,_denominator_sub_population) AS t1
	), w_denominator_variables AS (
		SELECT	t1.variable AS id, 
			t1.adc, adc_label, adc_description, adc_label_en, adc_description_en, 
			t2.spc, spc_label, spc_description, spc_label_en, spc_description_en
		FROM w_ad_denom AS t1
		INNER JOIN w_sp_denom AS t2
		ON t1.variable = t2.variable
	), w_denominator_variables_agg AS (
		SELECT array_agg(id ORDER BY id) AS denom_variables
		FROM w_denominator_variables
	),
	w_num_denom AS (
		SELECT t3.variable_numerator, t3.variable_denominator
		FROM 	w_numerator_variables_agg AS t1,
			w_denominator_variables_agg AS t2,
			nfiesta_results.fn_get_num_denom_variables(t1.num_variables, t2.denom_variables) AS t3
	), w_final AS (
		SELECT 
			t1.variable_numerator,
			t1.variable_denominator,
			coalesce(nullif(concat_ws(' - ', t2.adc_label, t2.spc_label),''),'bez rozlišení')::varchar AS label_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_label, t3.spc_label),''),'bez rozlišení')::varchar 
			ELSE NULL::varchar
			END AS label_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_description, t2.spc_description),''),'Bez rozlišení.')::text AS description_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_description, t3.spc_description),''),'Bez rozlišení.')::text
			ELSE NULL::text
			END AS description_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_label_en, t2.spc_label_en),''),'altogether')::varchar AS label_en_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_label_en, t3.spc_label_en),''),'altogether')::varchar 
			ELSE NULL::varchar
			END AS label_en_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_description_en, t2.spc_description_en),''),'Altogether.')::text AS description_en_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_description_en, t3.spc_description_en),''),'Altogether.')::text
			ELSE NULL::text
			END AS description_en_denom
		FROM
			w_num_denom AS t1
		INNER JOIN 
			w_numerator_variables AS t2
		ON t1.variable_numerator = t2.id
		LEFT JOIN
			w_denominator_variables AS t3
		ON t1.variable_denominator = t3.id
	) 
	SELECT
		variable_numerator,
		variable_denominator,
		concat_ws(' / ', label_num, label_denom)::varchar AS label, 
		concat_ws(' / ', description_num, description_denom)::text AS description, 
		concat_ws(' / ', label_en_num, label_en_denom)::varchar AS label_en, 
		concat_ws(' / ', description_en_num, description_en_denom)::text AS description_en 
	FROM w_final
	ORDER BY variable_numerator, variable_denominator;

/*		nfiesta_results.t_variable AS t2
	ON t1.variable_numerator = t2.id
	LEFT JOIN nfiesta_results.c_area_domain_category AS t3
	ON t2.area_domain_category = t3.id
	LEFT JOIN nfiesta_results.c_sub_population_category AS t4
	ON t2.sub_population_category = t4.id
	INNER JOIN nfiesta_results.t_variable AS t5
	ON t1.variable_denominator = t5.id;	
	LEFT JOIN nfiesta_results.c_area_domain_category AS t6
	ON t5.area_domain_category = t6.id
	LEFT JOIN nfiesta_results.c_sub_population_category AS t7
	ON t5.sub_population_category = t7.id;
*/

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta_results.fn_get_attribute_categories4target_variable(integer,integer,integer,integer,integer,integer) IS 'Function returns table with attribute categories (area domain and sub population categories) for given target variable and area domain/sub population. It also solves the right combination of numerator and denominator categories.';
-- </function>
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_get_userfn_get_area_sub_population_categories_query" schema="nfiesta_results" src="functions/fn_get_area_sub_population_categories.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta_results.fn_get_area_sub_population_categories(integer, integer, integer)
--DROP FUNCTION nfiesta_results.fn_get_area_sub_population_categories(integer, integer, integer);
CREATE OR REPLACE FUNCTION nfiesta_results.fn_get_area_sub_population_categories(
		_target_variable integer, _area_or_sub_pop integer, _id integer)
RETURNS TABLE (
variable 	integer,
attype 		integer,
category 	integer,
label		varchar,
description	text,
label_en	varchar,
description_en	text
)
AS
$function$
DECLARE
BEGIN

IF _target_variable IS NULL THEN
	RAISE WARNING 'Target variable is NULL.';
	RETURN QUERY SELECT NULL::int, NULL::int, NULL::int, NULL::varchar, NULL::text, NULL::varchar, NULL::text;
END IF;

IF _area_or_sub_pop IS NULL
THEN
	RAISE EXCEPTION 'Parameter area domain or sub_population is not choosen!';
END IF; 

IF _area_or_sub_pop NOT IN (100,200)
THEN
	RAISE EXCEPTION 'Parameter area domain or sub_population has to be either 100 (area_domain) or 200 (sub_population)!';
END IF; 

IF _target_variable IS NOT NULL AND NOT EXISTS (SELECT id FROM nfiesta_results.c_target_variable WHERE id = _target_variable)
THEN
	RAISE EXCEPTION 'Specified target_variable (%) does not exist in table c_target_variable!', _target_variable;
END IF;

IF _target_variable IS NOT NULL AND NOT EXISTS (
	SELECT t1.id
	FROM nfiesta_results.t_variable AS t1
	WHERE t1.target_variable = _target_variable)
THEN 
	RAISE EXCEPTION 'Specified target_variable(%) does not exist in the data (t_variable)!', _target_variable;
END IF;

CASE WHEN _area_or_sub_pop = 100
THEN
	IF _id IS NOT NULL AND _id != 0 AND NOT EXISTS (SELECT id FROM nfiesta_results.c_area_domain WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Specified area_domain (%) does not exist in table c_area_domain!', _id;
	END IF;

	IF _id IS NOT NULL AND NOT EXISTS (
		SELECT t1.id
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.c_area_domain_category AS t2
		ON t1.area_domain_category = t2.id
		WHERE t1.target_variable = _target_variable AND
			CASE WHEN _id != 0 THEN t2.area_domain = _id
			WHEN _id = 0 THEN t2.area_domain IS NULL	
			ELSE true END)
	THEN 
		RAISE EXCEPTION 'Specified combination of target_variable(%) and area_domain (%) does not exist in the data (t_variable)!', _target_variable, _id;
	END IF;

	RETURN QUERY EXECUTE
	'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
		SELECT
			t1.target_variable,
			coalesce(t2.variable_superior,t1.id) AS variable_superior,
			coalesce(t2.variable, t1.id) AS variable
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t2.variable = t1.id
		LEFT JOIN nfiesta_results.c_area_domain_category AS t3
		ON t1.area_domain_category = t3.id
		WHERE 	t1.target_variable = $1 AND
			CASE WHEN $2 IS NOT NULL AND $2 != 0 THEN t3.area_domain = $2 ELSE t1.area_domain_category IS NULL END
		UNION ALL
		SELECT
			t1.target_variable,
			t2.variable_superior,
			t2.variable
		FROM w_variables AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t1.variable_superior = t2.variable
		WHERE t2.variable IS NOT NULL
	)
	, w_attr_types AS (
		SELECT
			t1.target_variable,
			t1.variable_superior, t1.variable, 
			t3.area_domain AS area_domain_sup, t5.area_domain, 
			t3.id AS id_cat_sup, t3.label AS category_sup,
			t5.id AS id_cat, t5.label AS category
		FROM w_variables AS t1
		INNER JOIN
			nfiesta_results.t_variable AS t2
		ON 
			t1.variable_superior = t2.id
		LEFT JOIN
			nfiesta_results.c_area_domain_category AS t3
		ON t2.area_domain_category = t3.id
		INNER JOIN
			nfiesta_results.t_variable AS t4
		ON 
			t1.variable = t4.id
		LEFT JOIN
			nfiesta_results.c_area_domain_category AS t5
		ON t4.area_domain_category = t5.id
	),
	w_union AS (
		SELECT	target_variable, variable_superior AS variable,  area_domain_sup AS area_domain
		FROM	w_attr_types
		UNION ALL
		SELECT	target_variable, variable,  area_domain
		FROM	w_attr_types
	)
	,w_distinct AS (
		SELECT DISTINCT target_variable, area_domain
		FROM w_union
	)
	SELECT
		t3.id AS variable, t2.area_domain AS type, t2.id AS category,
		t2.label, t2.description, t2.label_en, t2.description_en
	FROM
		w_distinct AS t1
	LEFT JOIN
		nfiesta_results.c_area_domain_category AS t2
	ON 	t1.area_domain = t2.area_domain
	INNER JOIN
		nfiesta_results.t_variable AS t3
	ON t1.target_variable = t3.target_variable AND
	CASE WHEN t2.id IS NOT NULL THEN t3.area_domain_category = t2.id
	ELSE t3.area_domain_category IS NULL END
	ORDER BY t3.id
	'
	USING _target_variable, _id;

WHEN _area_or_sub_pop = 200
THEN
	IF _id IS NOT NULL AND _id != 0 AND NOT EXISTS (SELECT id FROM nfiesta_results.c_sub_population WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Specified sub_population (%) does not exist in table c_sub_population!', _id;
	END IF;

	IF _id IS NOT NULL AND NOT EXISTS (
		SELECT t1.id
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.c_sub_population_category AS t2
		ON t1.sub_population_category = t2.id
		WHERE t1.target_variable = _target_variable AND
			CASE WHEN _id != 0 THEN t2.sub_population = _id
			WHEN _id = 0 THEN t2.sub_population IS NULL	
			ELSE true END)
	THEN 
		RAISE EXCEPTION 'Specified combination of target_variable(%) and sub_population (%) does not exist in the data (t_variable)!', _target_variable, _id;
	END IF;

	RETURN QUERY EXECUTE
	'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
		SELECT
			t1.target_variable,
			coalesce(t2.variable_superior,t1.id) AS variable_superior,
			coalesce(t2.variable, t1.id) AS variable
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t2.variable = t1.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t3
		ON t1.sub_population_category = t3.id
		WHERE 	t1.target_variable = $1 AND
			CASE WHEN $2 IS NOT NULL AND $2 != 0 THEN t3.sub_population = $2 ELSE t1.sub_population_category IS NULL END
		UNION ALL
		SELECT
			t1.target_variable,
			t2.variable_superior,
			t2.variable
		FROM w_variables AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t1.variable_superior = t2.variable
		WHERE t2.variable IS NOT NULL
	)
	, w_attr_types AS (
		SELECT
			t1.target_variable,
			t1.variable_superior, t1.variable, 
			t3.sub_population AS sub_population_sup, t5.sub_population, 
			t3.id AS id_cat_sup, t3.label AS category_sup,
			t5.id AS id_cat, t5.label AS category
		FROM w_variables AS t1
		INNER JOIN
			nfiesta_results.t_variable AS t2
		ON 
			t1.variable_superior = t2.id
		LEFT JOIN
			nfiesta_results.c_sub_population_category AS t3
		ON t2.sub_population_category = t3.id
		INNER JOIN
			nfiesta_results.t_variable AS t4
		ON 
			t1.variable = t4.id
		LEFT JOIN
			nfiesta_results.c_sub_population_category AS t5
		ON t4.sub_population_category = t5.id
	),
	w_union AS (
		SELECT	target_variable, variable_superior AS variable,  sub_population_sup AS sub_population
		FROM	w_attr_types
		UNION ALL
		SELECT	target_variable, variable,  sub_population
		FROM	w_attr_types
	)
	,w_distinct AS (
		SELECT DISTINCT target_variable, sub_population
		FROM w_union
	)
	SELECT
		t3.id AS variable, t2.sub_population AS type, t2.id AS category,
		t2.label, t2.description, t2.label_en, t2.description_en
	FROM
		w_distinct AS t1
	LEFT JOIN
		nfiesta_results.c_sub_population_category AS t2
	ON 	t1.sub_population = t2.sub_population
	INNER JOIN
		nfiesta_results.t_variable AS t3
	ON t1.target_variable = t3.target_variable AND
	CASE WHEN t2.id IS NOT NULL THEN t3.sub_population_category = t2.id
	ELSE t3.sub_population_category IS NULL END
	ORDER BY t3.id'
	USING _target_variable, _id;
ELSE
	RAISE EXCEPTION 'Uknown attribute type!';
END CASE;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta_results.fn_get_area_sub_population_categories(integer,integer,integer) IS 'Function returns table with all hierarchically superior variables and its complementary categories within area domain or sub_population. Input parameters are target_variable (id from table c_target_variable), area_or_sub_pop (100 for area_domain, 200 for sub_population) and id (id from table c_area_domain or id from table c_sub_population)';
-- </function>
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_get_num_denom_variables" schema="nfiesta_results" src="functions/fn_get_num_denom_variables.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta_results.fn_get_num_denom_variables.sql(integer, integer)
--DROP FUNCTION nfiesta_results.fn_get_num_denom_variables.sql(integer, integer);
CREATE OR REPLACE FUNCTION nfiesta_results.fn_get_num_denom_variables(
		_num_var integer[], _denom_var integer[] DEFAULT NULL::int[])
RETURNS TABLE (
variable_numerator 	integer,
variable_denominator 	integer
)
AS
$function$
DECLARE
_denom_var4query	integer[];
BEGIN
	IF _num_var IS NULL
	THEN
		RAISE EXCEPTION 'Given array of numerator variables is NULL!';
	END IF;

	IF (SELECT count(*) FROM nfiesta_results.t_variable WHERE array[id] <@ _num_var) != array_length(_num_var,1)
	THEN
		RAISE EXCEPTION 'Not all of numerator variables are present in table t_variable.';
	END IF;

	IF (SELECT count(*) FROM nfiesta_results.t_variable WHERE array[id] <@ _denom_var) != array_length(_denom_var,1)
	THEN
		RAISE EXCEPTION 'Not all of denominator variables are present in table t_variable.';
	END IF;

	IF array_length(_num_var,1) < array_length(_denom_var,1)
	THEN
		RAISE EXCEPTION 'Given array of denominator variables must be shorter (or equal) then given array of numerator variables.';
	END IF;

	IF _denom_var IS NULL
	THEN _denom_var4query := _num_var;
	ELSE _denom_var4query := _denom_var;
	END IF;

	RETURN QUERY EXECUTE
	'
	WITH RECURSIVE w_var AS (
		SELECT 1 AS iter, variable_superior AS variable_start, variable_superior, variable
		FROM nfiesta_results.t_variable_hierarchy
		WHERE variable_superior = ANY($1)
		UNION ALL
		SELECT iter + 1 AS iter, t1.variable_start, t2.variable_superior, t2.variable
		FROM w_var AS t1
		INNER JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t1.variable = t2.variable_superior
		WHERE NOT ARRAY[t1.variable] <@ $1
	), w_all AS (
		SELECT distinct iter, t1.variable_start, t1.variable_superior, t1.variable, t3.label AS lab_start, t7.label AS lab_sup, t5.label AS lab_var
		FROM w_var AS t1
		INNER JOIN nfiesta_results.t_variable AS t2
		ON t1.variable_start = t2.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t3
		ON t2.sub_population_category = t3.id
		INNER JOIN nfiesta_results.t_variable AS t4
		ON t1.variable = t4.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t5
		ON t4.sub_population_category = t5.id
		INNER JOIN nfiesta_results.t_variable AS t6
		ON t1.variable_superior = t6.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t7
		ON t6.sub_population_category = t7.id
		WHERE variable = ANY($2)
		ORDER BY variable_start, variable
	), w_iter AS(
		SELECT 	variable_start, variable_superior, variable, lab_start, --lab_sup, 
			lab_var,
			iter, min(iter) OVER (partition by variable) AS min_iter
		FROM w_all
	), w_final AS (
		SELECT variable_start AS denom, variable AS num
		FROM w_iter WHERE iter = min_iter AND
			NOT ARRAY[variable] <@ $1
		UNION ALL
		SELECT t1.var AS denom, t1.var AS num
		FROM unnest($1) AS t1(var)
	)
	SELECT DISTINCT t1.num, CASE WHEN $3 IS NOT NULL THEN t1.denom ELSE NULL::int END AS denom --, t3.label AS lab_denom, t5.label AS lab_num
	FROM w_final AS t1
	INNER JOIN nfiesta_results.t_variable AS t2
	ON t1.denom = t2.id
	LEFT JOIN nfiesta_results.c_sub_population_category AS t3
	ON t2.sub_population_category = t3.id
	INNER JOIN nfiesta_results.t_variable AS t4
	ON t1.num = t4.id
	LEFT JOIN nfiesta_results.c_sub_population_category AS t5
	ON t4.sub_population_category = t5.id
	ORDER BY denom, t1.num;
	'
	USING _denom_var4query, _num_var, _denom_var;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta_results.fn_get_num_denom_variables(integer[],integer[]) IS 'Function returns table with assigned denominator variables to numerator variables.';
-- </function>
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_get_user_query" schema="nfiesta_results" src="functions/fn_get_user_query.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_query
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_query(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_query
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	estimation_cell					text,
	variable_area_domain			text,
	variable_sub_population			text,
	point_estimate					numeric,
	standard_deviation				numeric,
	variation_coeficient			numeric,
	sample_size						integer,
	min_sample_size					integer,
	interval_estimation				numeric
)
language plpgsql
stable
as
$$
declare
	_lang_suffix					character varying(3);
	_estimate_type					integer;
	_target_variable				integer;
	_target_variable_denom			integer;
	_estimation_period				integer;
	_panel_refyearset_group			integer;
	_phase_estimate_type			integer;
	_phase_estimate_type_denom		integer;
	_estimation_cell_collection		integer;
	_area_domain					integer;
	_area_domain_denom				integer;
	_sub_population					integer;
	_sub_population_denom			integer;
	_cond_area_domain				text;
	_cond_sub_population			text;
	_cond_area_domain_denom			text;
	_cond_sub_population_denom		text;
	_check4unit						integer;
	_coeficient4ratio				numeric;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_query: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_query: Count of elements in input argument _id_group must one!';
	end if;
	-----------------------------------------------------------------
	select
			trg.estimate_type,
			trg.target_variable,
			trg.target_variable_denom,
			trg.estimation_period,
			trg.panel_refyearset_group,
			trg.phase_estimate_type,
			trg.phase_estimate_type_denom,
			trg.estimation_cell_collection,
			trg.area_domain,
			trg.area_domain_denom,
			trg.sub_population,
			trg.sub_population_denom
	from
			nfiesta_results.t_result_group as trg
	where
			trg.id = _id_group[1]
	into
			_estimate_type,
			_target_variable,
			_target_variable_denom,
			_estimation_period,
			_panel_refyearset_group,
			_phase_estimate_type,
			_phase_estimate_type_denom,
			_estimation_cell_collection,
			_area_domain,
			_area_domain_denom,
			_sub_population,
			_sub_population_denom;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _estimate_type = 1 -- TOTAL
	then
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$2';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$3';
		end if;
		---------------------------------------------------	
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					null::integer,
					null::integer,
					null::integer
					)
				)
		,w2 as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $4
				and ttec.panel_refyearset_group = $5
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $6)
				and ttec.phase_estimate_type = $7
				)
		,w3 as	(
				select
						w2.id as id_t_total_estimate_conf,
						w2.estimation_cell,
						w2.variable,
						w2.phase_estimate_type,
						w2.force_synthetic,
						w2.estimation_period,
						w2.panel_refyearset_group,
						w1.nominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,
						tv.area_domain_category,
						tv.sub_population_category
				from
						w2
						inner join w1 on w2.variable = w1.nominator_variable
						inner join nfiesta_results.t_variable as tv on w2.variable = tv.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf
				from
						nfiesta_results.t_estimate_conf as tec 
				where
						tec.total_estimate_conf in (select w3.id_t_total_estimate_conf from w3)
				and
						tec.estimate_type = $8
				)
		,w5 as	(
				select
						w4.*,
						w3.*
				from
						w4 inner join w3 on w4.total_estimate_conf = w3.id_t_total_estimate_conf
				)
		,w6 as	(
				select
						tr.*,
						w5.*
				from
						nfiesta_results.t_result as tr
						inner join w5 on tr.estimate_conf = w5.id_t_estimate_conf
				where
						tr.is_latest = true
				)
		,w7 as	(
				select
						cec.id as estimation_cell_id,
						cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w6.area_domain_category is null then 0
							else cadc.id
						end
							as variable_area_domain_id,	
						---------------------------------
						case
							when w6.area_domain_category is null then ''bez rozliseni''
							else replace(cadc.label,'';'',''; '')
						end
							as variable_area_domain,
						---------------------------------
						case
							when w6.area_domain_category is null then ''without distinction''
							else replace(cadc.label_en,'';'',''; '')
						end
							as variable_area_domain_en,
						-----------------------------------------------------
						case
							when w6.sub_population_category is null then 0
							else cspc.id
						end
							as variable_sub_population_id,
						---------------------------------
						case
							when w6.sub_population_category is null then ''bez rozliseni''
							else replace(cspc.label,'';'',''; '')
						end
							as variable_sub_population,
						---------------------------------
						case
							when w6.sub_population_category is null then ''without distinction''
							else replace(cspc.label_en,'';'',''; '')
						end
							as variable_sub_population_en,
						-----------------------------------------------------
						w6.point as point_estimate,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else sqrt(w6.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null or w6.point = 0.0::numeric then null::numeric
							else (sqrt(w6.var) / w6.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						w6.act_ssize as sample_size,
						w6.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else (1.96 * sqrt(w6.var))
						end
							as interval_estimation
				from
						w6
						inner join nfiesta_results.c_estimation_cell as cec on w6.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc on w6.area_domain_category = cadc.id
						left join nfiesta_results.c_sub_population_category as cspc on w6.sub_population_category = cspc.id
				)
		select
				w7.estimation_cell'||_lang_suffix||' as estimation_cell,
				w7.variable_area_domain'||_lang_suffix||' as variable_area_domain,
				w7.variable_sub_population'||_lang_suffix||' as variable_sub_population,
				round(w7.point_estimate::numeric,2) as point_estimate,
				round(w7.standard_deviation::numeric,2) as standard_deviation,
				round(w7.variation_coeficient::numeric,2) as variation_coeficient,
				w7.sample_size::integer,
				(ceil(w7.min_sample_size::numeric))::integer as min_sample_size,
				round(w7.interval_estimation::numeric,2) as interval_estimation
		from
				w7 order by w7.estimation_cell_id, variable_area_domain_id, variable_sub_population_id;
		'
		using
				_target_variable,
				_area_domain,
				_sub_population,
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection,
				_phase_estimate_type,
				_estimate_type;
				
	else -- RATIO
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$3';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$4';
		end if;
		---------------------------------------------------
		if _area_domain_denom is null
		then
			_cond_area_domain_denom := 'null::integer';
		else
			_cond_area_domain_denom := '$5';
		end if;
		---------------------------------------------------
		if _sub_population_denom is null
		then
			_cond_sub_population_denom := 'null::integer';
		else
			_cond_sub_population_denom := '$6';
		end if;
		---------------------------------------------------
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''label'' as unit_num,
						((w2.metadata->''cs'')->''unit'')->>''label'' as unit_denom,
						((w1.metadata->''en'')->''unit'')->>''label'' as unit_num_en,
						((w2.metadata->''en'')->''unit'')->>''label'' as unit_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_num'||_lang_suffix||' AS unit_num,
						w3.unit_denom'||_lang_suffix||' AS unit_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_num = w4.unit_denom) then 0
							else 1
						end as check4unit
				from
						w4
				)
		select w5.check4unit from w5
		'
		using _target_variable, _target_variable_denom
		into _check4unit;		
		---------------------------------------------------
		if _check4unit = 0
		then	
			_coeficient4ratio := 100.0::numeric;
		else
			_coeficient4ratio := 1.0::numeric;
		end if;
		---------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					$2,
					'||_cond_area_domain_denom||',
					'||_cond_sub_population_denom||'
					)
				)
		,w2a as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.phase_estimate_type = $10
				)
		,w2b as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.denominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.phase_estimate_type = $10
				)
		,w3 as	(
				select
						w2a.id as id_t_total_estimate_conf_nom,
						w2b.id as id_t_total_estimate_conf_denom,
						w2a.estimation_cell,
						w2a.variable,
						w2a.phase_estimate_type,
						w2a.force_synthetic,
						w2a.estimation_period,
						w2a.panel_refyearset_group,
						w1.nominator_variable,
						w1.denominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,	
						tv1.area_domain_category as area_domain_category_nom,
						tv2.area_domain_category as area_domain_category_denom,
						tv1.sub_population_category as sub_population_category_nom,
						tv2.sub_population_category as sub_population_category_denom
				from
						w2a
						inner join w1 on w2a.variable = w1.nominator_variable
						inner join w2b on w2b.variable = w1.denominator_variable
						inner join nfiesta_results.t_variable as tv1 on w2a.variable = tv1.id
						inner join nfiesta_results.t_variable as tv2 on w2b.variable = tv2.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf,
						tec.denominator,
						w3.*
				from
						nfiesta_results.t_estimate_conf as tec
						
						inner join w3
						on tec.total_estimate_conf = w3.id_t_total_estimate_conf_nom
						and tec.denominator = w3.id_t_total_estimate_conf_denom
				where
						tec.estimate_type = $11
				)
		,w5 as	(
				select
						tr.id,
						tr.estimate_conf,
						tr.point * $12 as point,
						tr.var * $12 as var,
						tr.min_ssize,
						tr.act_ssize,
						w4.*
				from
						nfiesta_results.t_result as tr
						inner join w4 on tr.estimate_conf = w4.id_t_estimate_conf
				where
						tr.is_latest = true
				)			
		,w6 as	(
				select
						cec.id as estimation_cell_id,
						cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w5.area_domain_category_nom is null then 0
							else cadc1.id
						end
							as variable_area_domain_id_nom,	
						---------------------------------
						case
							when w5.area_domain_category_denom is null then 0
							else cadc2.id
						end
							as variable_area_domain_id_denom,	
						---------------------------------
						case
							when w5.area_domain_category_nom is null then ''bez rozliseni''
							else replace(cadc1.label,'';'',''; '')
						end
							as variable_area_domain_nom,
						---------------------------------
						case
							when w5.area_domain_category_denom is null then ''bez rozliseni''
							else replace(cadc2.label,'';'',''; '')
						end
							as variable_area_domain_denom,
						---------------------------------
						case
							when w5.area_domain_category_nom is null then ''without distinction''
							else replace(cadc1.label_en,'';'',''; '')
						end
							as variable_area_domain_nom_en,
						---------------------------------
						case
							when w5.area_domain_category_denom is null then ''without distinction''
							else replace(cadc2.label_en,'';'',''; '')
						end
							as variable_area_domain_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						case
							when w5.sub_population_category_nom is null then 0
							else cspc1.id
						end
							as variable_sub_population_id_nom,	
						---------------------------------
						case
							when w5.sub_population_category_denom is null then 0
							else cspc2.id
						end
							as variable_sub_population_id_denom,	
						---------------------------------
						case
							when w5.sub_population_category_nom is null then ''bez rozliseni''
							else replace(cspc1.label,'';'',''; '')
						end
							as variable_sub_population_nom,
						---------------------------------
						case
							when w5.sub_population_category_denom is null then ''bez rozliseni''
							else replace(cspc2.label,'';'',''; '')
						end
							as variable_sub_population_denom,
						---------------------------------
						case
							when w5.sub_population_category_nom is null then ''without distinction''
							else replace(cspc1.label_en,'';'',''; '')
						end
							as variable_sub_population_nom_en,
						---------------------------------
						case
							when w5.sub_population_category_denom is null then ''without distinction''
							else replace(cspc2.label_en,'';'',''; '')
						end
							as variable_sub_population_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						w5.point as point_estimate,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else sqrt(w5.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null or w5.point = 0.0::numeric then null::numeric
							else (sqrt(w5.var) / w5.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						w5.act_ssize as sample_size,
						w5.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else (1.96 * sqrt(w5.var))
						end
							as interval_estimation
				from
						w5
						inner join nfiesta_results.c_estimation_cell as cec on w5.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc1 on w5.area_domain_category_nom = cadc1.id
						left join nfiesta_results.c_area_domain_category as cadc2 on w5.area_domain_category_denom = cadc2.id
						left join nfiesta_results.c_sub_population_category as cspc1 on w5.sub_population_category_nom = cspc1.id
						left join nfiesta_results.c_sub_population_category as cspc2 on w5.sub_population_category_denom = cspc2.id
				)
		select
				w6.estimation_cell'||_lang_suffix||' as estimation_cell,
				concat(w6.variable_area_domain_nom'||_lang_suffix||','' / '',w6.variable_area_domain_denom'||_lang_suffix||') as variable_area_domain,
				concat(w6.variable_sub_population_nom'||_lang_suffix||','' / '',w6.variable_sub_population_denom'||_lang_suffix||') as variable_sub_population,
				round(w6.point_estimate::numeric,2) as point_estimate,
				round(w6.standard_deviation::numeric,2) as standard_deviation,
				round(w6.variation_coeficient::numeric,2) as variation_coeficient,
				w6.sample_size::integer,
				(ceil(w6.min_sample_size::numeric))::integer as min_sample_size,
				round(w6.interval_estimation::numeric,2) as interval_estimation
		from
				w6
		order
				by	w6.estimation_cell_id,
					w6.variable_area_domain_id_nom,
					w6.variable_area_domain_id_denom,
					w6.variable_sub_population_id_nom,
					w6.variable_sub_population_id_denom;
		'
		using
				_target_variable,
				_target_variable_denom,
				_area_domain,
				_sub_population,
				_area_domain_denom,
				_sub_population_denom,				
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection,
				_phase_estimate_type,
				_estimate_type,
				_coeficient4ratio;	
	end if;

end;
$$
;

comment on function nfiesta_results.fn_get_user_query(character varying, integer[]) is
'The function returns result estimations for gived group of result and attribute variables from t_result table.';

grant execute on function nfiesta_results.fn_get_user_query(character varying, integer[]) to public;
-- </function>
-------------------------------------------------------------------------------
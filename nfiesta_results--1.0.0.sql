---------------------------------------------------------------------------------------------------
--	SCHEMA --
---------------------------------------------------------------------------------------------------

	--DROP SCHEMA IF EXISTS nfiesta_results CASCADE;
	--CREATE SCHEMA nfiesta_results AUTHORIZATION adm_nfiesta;
	GRANT ALL ON SCHEMA nfiesta_results TO adm_nfiesta;
	COMMENT ON SCHEMA nfiesta_results IS 'Data vystupu NIL pro webovou prezentaci.';

	--drop schema if exists nfiesta_results CASCADE;
	--create schema nfiesta_results authorization adm_nfi_data;
	--grant all on schema nfiesta_results TO adm_nfi_data;
	--grant usage on schema nfiesta_results TO usr_nfi_data;
	--comment on schema nfiesta_results IS 'Data vystupu NIL pro webovou prezentaci.';
	
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
--	TABLES --
---------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--	c_language
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_language;

create table nfiesta_results.c_language
(
	id serial not null,
	"label" varchar(100) not null,
	description text not null,
	constraint pkey__c_language primary key (id)
);

comment on table nfiesta_results.c_language is 'Table of languages.';
comment on column nfiesta_results.c_language.id is 'Identifier of language, primary key.';
comment on column nfiesta_results.c_language.label is 'Label of language.';
comment on column nfiesta_results.c_language.description is 'Description of language.';

comment on constraint pkey__c_language on nfiesta_results.c_language is 'Database identifier, primary key.';

alter table nfiesta_results.c_language owner to adm_nfiesta;
grant all on table nfiesta_results.c_language to adm_nfiesta;
--grant select on table nfiesta_results.c_language to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	c_area_domain
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_area_domain;

create table nfiesta_results.c_area_domain
(
	id serial not null,
	"label" varchar(250) not null,
	description text not null,
	label_en varchar(250) not null,
	description_en text not null,
	atomic boolean not null,
	constraint pkey__c_area_domain primary key (id),
	constraint ukey__c_area_domain__label unique (label),
	constraint ukey__c_area_domain__label_en unique (label_en)
);

comment on table nfiesta_results.c_area_domain is 'Table of area domains.';
comment on column nfiesta_results.c_area_domain.id is 'Identifier of area domain, primary key.';
comment on column nfiesta_results.c_area_domain.label is 'Label of area domain.';
comment on column nfiesta_results.c_area_domain.description is 'Description of area domain.';
comment on column nfiesta_results.c_area_domain.label_en is 'English label of area domain.';
comment on column nfiesta_results.c_area_domain.description_en is 'English description of area domain.';
comment on column nfiesta_results.c_area_domain.atomic is 'Identifier if area_domain is (true) or is not (false) atomic type.';

comment on constraint pkey__c_area_domain on nfiesta_results.c_area_domain is 'Database identifier, primary key.';
comment on constraint ukey__c_area_domain__label on nfiesta_results.c_area_domain is 'Unique key on column label.';
comment on constraint ukey__c_area_domain__label_en on nfiesta_results.c_area_domain is 'Unique key on column label_en.';

alter table nfiesta_results.c_area_domain owner to adm_nfiesta;
grant all on table nfiesta_results.c_area_domain to adm_nfiesta;
--grant select on table nfiesta_results.c_area_domain to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	c_area_domain_category
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_area_domain_category;

create table nfiesta_results.c_area_domain_category
(
	id serial not null,
	area_domain integer not null,
	"label" varchar(250) not null,
	description text not null,
	label_en varchar(250) not null,
	description_en text not null,
	constraint pkey__c_area_domain_category primary key (id),
	constraint ukey__c_area_domain_category__label_area_domain unique (label, area_domain),
	constraint ukey__c_area_domain_category__label_en_area_domain unique (label_en, area_domain),
	constraint fkey__c_area_domain_category__c_area_domain foreign key (area_domain) references nfiesta_results.c_area_domain(id)	
);

comment on table nfiesta_results.c_area_domain_category is 'Table of area domain categories.';
comment on column nfiesta_results.c_area_domain_category.id is 'Identifier of area domain category, primary key.';
comment on column nfiesta_results.c_area_domain_category.area_domain is 'Identifier of area domain, foreign key to table c_area_domain.';
comment on column nfiesta_results.c_area_domain_category.label is 'Label of area domain category.';
comment on column nfiesta_results.c_area_domain_category.description is 'Description of area domain category.';
comment on column nfiesta_results.c_area_domain_category.label_en is 'English label of area domain category.';
comment on column nfiesta_results.c_area_domain_category.description_en is 'English description of area domain category.';

comment on constraint pkey__c_area_domain_category on nfiesta_results.c_area_domain_category is 'Database identifier, primary key.';
comment on constraint ukey__c_area_domain_category__label_area_domain on nfiesta_results.c_area_domain_category is 'Unique key on columns label and area_domain.';
comment on constraint ukey__c_area_domain_category__label_en_area_domain on nfiesta_results.c_area_domain_category is 'Unique key on columns label_en and area_domain.';
comment on constraint fkey__c_area_domain_category__c_area_domain on nfiesta_results.c_area_domain_category is 'Foreign key to table c_area_domain.';

alter table nfiesta_results.c_area_domain_category owner to adm_nfiesta;
grant all on table nfiesta_results.c_area_domain_category to adm_nfiesta;
--grant select on table nfiesta_results.c_area_domain_category to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	c_sub_population
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_sub_population;

create table nfiesta_results.c_sub_population
(
	id serial not null,
	"label" varchar(250) not null,
	description text not null,
	label_en varchar(250) not null,
	description_en text not null,
	atomic boolean not null,
	constraint pkey__c_sub_population primary key (id),
	constraint ukey__c_sub_population__label unique (label),
	constraint ukey__c_sub_population__label_en unique (label_en)
);

comment on table nfiesta_results.c_sub_population is 'Table of sub populations.';
comment on column nfiesta_results.c_sub_population.id is 'Identifier of sub population, primary key.';
comment on column nfiesta_results.c_sub_population.label is 'Label of sub population.';
comment on column nfiesta_results.c_sub_population.description is 'Description of sub population.';
comment on column nfiesta_results.c_sub_population.label_en is 'English label of sub population.';
comment on column nfiesta_results.c_sub_population.description_en is 'English description of sub population.';
comment on column nfiesta_results.c_sub_population.atomic is 'Identifier if sub population is (true) or is not (false) atomic type.';

comment on constraint pkey__c_sub_population on nfiesta_results.c_sub_population is 'Database identifier, primary key.';
comment on constraint ukey__c_sub_population__label on nfiesta_results.c_sub_population is 'Unique key on column label.';
comment on constraint ukey__c_sub_population__label_en on nfiesta_results.c_sub_population is 'Unique key on column label_en.';

alter table nfiesta_results.c_sub_population owner to adm_nfiesta;
grant all on table nfiesta_results.c_sub_population to adm_nfiesta;
--grant select on table nfiesta_results.c_sub_population to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	c_sub_population_category
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_sub_population_category;

create table nfiesta_results.c_sub_population_category
(
	id serial not null,
	sub_population integer not null,
	"label" varchar(250) not null,
	description text not null,
	label_en varchar(250) not null,
	description_en text not null,
	constraint pkey__c_sub_population_category primary key (id),
	constraint ukey__c_sub_population_category__label_sub_population unique (label, sub_population),
	constraint ukey__c_sub_population_category__label_en_sub_population unique (label_en, sub_population),
	constraint fkey__c_sub_population_category__c_sub_population foreign key (sub_population) references nfiesta_results.c_sub_population(id)	
);

comment on table nfiesta_results.c_sub_population_category is 'Table of sub population categories.';
comment on column nfiesta_results.c_sub_population_category.id is 'Identifier of sub population category, primary key.';
comment on column nfiesta_results.c_sub_population_category.sub_population is 'Identifier of sub population, foreign key to table c_sub_population.';
comment on column nfiesta_results.c_sub_population_category.label is 'Label of sub population category.';
comment on column nfiesta_results.c_sub_population_category.description is 'Description of sub population category.';
comment on column nfiesta_results.c_sub_population_category.label_en is 'English label of sub population category.';
comment on column nfiesta_results.c_sub_population_category.description_en is 'English description of sub population category.';

comment on constraint pkey__c_sub_population_category on nfiesta_results.c_sub_population_category is 'Database identifier, primary key.';
comment on constraint ukey__c_sub_population_category__label_sub_population on nfiesta_results.c_sub_population_category is 'Unique key on columns label and sub_population.';
comment on constraint ukey__c_sub_population_category__label_en_sub_population on nfiesta_results.c_sub_population_category is 'Unique key on columns label_en and sub_population.';
comment on constraint fkey__c_sub_population_category__c_sub_population ON nfiesta_results.c_sub_population_category is 'Foreign key to table c_sub_population.';

alter table nfiesta_results.c_sub_population_category owner to adm_nfiesta;
grant all on table nfiesta_results.c_sub_population_category to adm_nfiesta;
--grant select on table nfiesta_results.c_sub_population_category to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	cm_area_domain_category
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.cm_area_domain_category;

create table nfiesta_results.cm_area_domain_category
(
	id serial not null,
	area_domain_category integer not null,
	atomic_category integer not null,
	constraint pkey__cm_area_domain_category primary key (id),
	constraint ukey__cm_area_domain_category unique (area_domain_category, atomic_category),
	constraint fkey__cm_area_domain_category__c_area_domain_category_1 foreign key (area_domain_category) references nfiesta_results.c_area_domain_category(id),
	constraint fkey__cm_area_domain_category__c_area_domain_category_2 foreign key (atomic_category) references nfiesta_results.c_area_domain_category(id)
);

comment on table nfiesta_results.cm_area_domain_category is 'Mapping table of area domain categories.';
comment on column nfiesta_results.cm_area_domain_category.id is 'Identifier of record, primary key.';
comment on column nfiesta_results.cm_area_domain_category.area_domain_category is 'Identifier of non-atomic area_domain_category, foreign key to table c_area_domain_category.';
comment on column nfiesta_results.cm_area_domain_category.atomic_category is 'Identifier of atomic area_domain_category, foreign key to table c_area_domain_category.';

comment on constraint pkey__cm_area_domain_category on nfiesta_results.cm_area_domain_category is 'Database identifier, primary key.';
comment on constraint ukey__cm_area_domain_category on nfiesta_results.cm_area_domain_category is 'Unique key on columns area_domain_category and atomic_category.';
comment on constraint fkey__cm_area_domain_category__c_area_domain_category_1 ON nfiesta_results.cm_area_domain_category is 'Foreign key to table c_area_domain_category.';
comment on constraint fkey__cm_area_domain_category__c_area_domain_category_2 ON nfiesta_results.cm_area_domain_category is 'Foreign key to table c_area_domain_category.';

alter table nfiesta_results.cm_area_domain_category owner to adm_nfiesta;
grant all on table nfiesta_results.cm_area_domain_category to adm_nfiesta;
--grant select on table nfiesta_results.cm_area_domain_category to usr_nfi_data;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	cm_sub_population_category
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.cm_sub_population_category;

create table nfiesta_results.cm_sub_population_category
(
	id serial not null,
	sub_population_category integer not null,
	atomic_category integer not null,
	constraint pkey__cm_sub_population_category primary key (id),
	constraint ukey__cm_sub_population_category unique (sub_population_category, atomic_category),
	constraint fkey__cm_sub_population_category__c_sub_population_category_1 foreign key (sub_population_category) references nfiesta_results.c_sub_population_category(id),
	constraint fkey__cm_sub_population_category__c_sub_population_category_2 foreign key (atomic_category) references nfiesta_results.c_sub_population_category(id)
);

comment on table nfiesta_results.cm_sub_population_category is 'Mapping table of sub population categories.';
comment on column nfiesta_results.cm_sub_population_category.id is 'Identifier of record, primary key.';
comment on column nfiesta_results.cm_sub_population_category.sub_population_category is 'Identifier of non-atomic sub_population_category, foreign key to table c_sub_population_category.';
comment on column nfiesta_results.cm_sub_population_category.atomic_category is 'Identifier of atomic sub_population_category, foreign key to table c_sub_population_category.';

comment on constraint pkey__cm_sub_population_category on nfiesta_results.cm_sub_population_category is 'Database identifier, primary key.';
comment on constraint ukey__cm_sub_population_category on nfiesta_results.cm_sub_population_category is 'Unique key on columns sub_population_category and atomic_category.';
comment on constraint fkey__cm_sub_population_category__c_sub_population_category_1 ON nfiesta_results.cm_sub_population_category is 'Foreign key to table c_sub_population_category.';
comment on constraint fkey__cm_sub_population_category__c_sub_population_category_2 ON nfiesta_results.cm_sub_population_category is 'Foreign key to table c_sub_population_category.';

alter table nfiesta_results.cm_sub_population_category owner to adm_nfiesta;
grant all on table nfiesta_results.cm_sub_population_category to adm_nfiesta;
--grant select on table nfiesta_results.cm_sub_population_category to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	c_estimate_type
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_estimate_type;

create table nfiesta_results.c_estimate_type
(
	id serial not null,
	"label" varchar(120) not null,
	description text not null,
	constraint pkey__c_estimate_type primary key (id),
	constraint ukey__c_estimate_type__label unique (label)
);

comment on table nfiesta_results.c_estimate_type is 'Table of estimate types.';
comment on column nfiesta_results.c_estimate_type.id is 'Identifier of estimate type, primary key.';
comment on column nfiesta_results.c_estimate_type.label is 'Label of estimate type.';
comment on column nfiesta_results.c_estimate_type.description is 'Description of estimate type.';

comment on constraint pkey__c_estimate_type on nfiesta_results.c_estimate_type is 'Database identifier, primary key.';
comment on constraint ukey__c_estimate_type__label on nfiesta_results.c_estimate_type is 'Unique key on column label.';

alter table nfiesta_results.c_estimate_type owner to adm_nfiesta;
grant all on table nfiesta_results.c_estimate_type to adm_nfiesta;
--grant select on table nfiesta_results.c_estimate_type to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	c_estimation_cell_collection
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_estimation_cell_collection;

create table nfiesta_results.c_estimation_cell_collection
(
	id serial not null,
	"label" varchar(120) not null,
	description text not null,
	label_en varchar(120) not null,
	description_en text not null,
	constraint pkey__c_estimation_cell_collection primary key (id),
	constraint ukey__c_estimation_cell_collection__label unique (label),
	constraint ukey__c_estimation_cell_collection__label_en unique (label_en)
);

comment on table nfiesta_results.c_estimation_cell_collection is 'Table of estimate cell collections.';
comment on column nfiesta_results.c_estimation_cell_collection.id is 'Identifier of estimate cell collection, primary key.';
comment on column nfiesta_results.c_estimation_cell_collection.label is 'Label of estimation cell collection.';
comment on column nfiesta_results.c_estimation_cell_collection.description is 'Description of estimation cell collection.';
comment on column nfiesta_results.c_estimation_cell_collection.label_en is 'English label of estimation cell collection.';
comment on column nfiesta_results.c_estimation_cell_collection.description_en is 'English description of estimation cell collection.';

comment on constraint pkey__c_estimation_cell_collection on nfiesta_results.c_estimation_cell_collection is 'Database identifier, primary key.';
comment on constraint ukey__c_estimation_cell_collection__label on nfiesta_results.c_estimation_cell_collection is 'Unique key on column label.';
comment on constraint ukey__c_estimation_cell_collection__label_en on nfiesta_results.c_estimation_cell_collection is 'Unique key on column label_en.';

alter table nfiesta_results.c_estimation_cell_collection owner to adm_nfiesta;
grant all on table nfiesta_results.c_estimation_cell_collection to adm_nfiesta;
--grant select on table nfiesta_results.c_estimation_cell_collection to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	c_estimation_cell
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_estimation_cell;

create table nfiesta_results.c_estimation_cell
(
	id serial not null,
	estimation_cell_collection integer not null,
	"label" varchar(120) not null,
	description text not null,
	label_en varchar(120) not null,
	description_en text not null,
	constraint pkey__c_estimation_cell primary key (id),
	constraint ukey__c_estimation_cell__label_estimation_cell_collection unique (label, estimation_cell_collection),
	constraint ukey__c_estimation_cell__label_en_estimation_cell_collection unique (label_en, estimation_cell_collection),
	constraint fkey__c_estimation_cell__c_estimation_cell_collection foreign key (estimation_cell_collection) references nfiesta_results.c_estimation_cell_collection(id)	
);

comment on table nfiesta_results.c_estimation_cell is 'Table of estimation cells.';
comment on column nfiesta_results.c_estimation_cell.id is 'Identifier of estimation cell, primary key.';
comment on column nfiesta_results.c_estimation_cell.estimation_cell_collection is 'Identifier of estimation cell collection, foreign key to table c_estimation_cell_collection.';
comment on column nfiesta_results.c_estimation_cell.label is 'Label of estimation cell.';
comment on column nfiesta_results.c_estimation_cell.description is 'Description of estimation cell.';
comment on column nfiesta_results.c_estimation_cell.label_en is 'English label of estimation cell.';
comment on column nfiesta_results.c_estimation_cell.description_en is 'English description of estimation cell.';

comment on constraint pkey__c_estimation_cell on nfiesta_results.c_estimation_cell is 'Database identifier, primary key.';
comment on constraint ukey__c_estimation_cell__label_estimation_cell_collection on nfiesta_results.c_estimation_cell is 'Unique key on columns label and estimation_cell_collection.';
comment on constraint ukey__c_estimation_cell__label_en_estimation_cell_collection on nfiesta_results.c_estimation_cell is 'Unique key on columns label_en and estimation_cell_collection.';
comment on constraint fkey__c_estimation_cell__c_estimation_cell_collection ON nfiesta_results.c_estimation_cell is 'Foreign key to table c_estimation_cell_collection.';

alter table nfiesta_results.c_estimation_cell owner to adm_nfiesta;
grant all on table nfiesta_results.c_estimation_cell to adm_nfiesta;
--grant select on table nfiesta_results.c_estimation_cell to usr_nfi_data;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	f_a_cell
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.f_a_cell;

create table nfiesta_results.f_a_cell
(
	gid serial not null,
	geom geometry(multipolygon) not null,
	estimation_cell int4 not null,
	constraint pkey__f_a_cell primary key (gid),
	constraint fkey__f_a_cell__c_estimation_cell foreign key (estimation_cell) references nfiesta_results.c_estimation_cell(id)
);

comment on table nfiesta_results.f_a_cell is 'Table of areas, contains geometries.';
comment on column nfiesta_results.f_a_cell.gid is 'Identifier of the cell, primary key.';
comment on column nfiesta_results.f_a_cell.geom is 'MultiPolygon geometry of the cell.';
comment on column nfiesta_results.f_a_cell.estimation_cell is 'Estimation cell, foreign key to table c_estimation_cell.';

comment on constraint pkey__f_a_cell on nfiesta_results.f_a_cell is 'Database identifier, primary key.';
comment on constraint fkey__f_a_cell__c_estimation_cell on nfiesta_results.f_a_cell is 'Foreign key to table c_estimation_cell.';

create index fki__f_a_cell__c_estimation_cell on nfiesta_results.f_a_cell using btree (estimation_cell);
comment on index nfiesta_results.fki__f_a_cell__c_estimation_cell is 'Index over foreign key fkey__f_a_cell__c_estimation_cell';

alter table nfiesta_results.f_a_cell owner to adm_nfiesta;
grant all on table nfiesta_results.f_a_cell to adm_nfiesta;
--grant select on table nfiesta_results.f_a_cell to usr_nfi_data;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	c_estimation_period
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_estimation_period;

create table nfiesta_results.c_estimation_period
(
	id serial not null,
	estimate_date_begin date not null,
	estimate_date_end date not null,
	"label" varchar(200) not null,
	description text not null,
	label_en varchar(200) not null,
	description_en text not null,
	constraint pkey__c_estimation_period primary key (id),
	constraint ukey__c_estimation_period__label unique (label),
	constraint ukey__c_estimation_period__label_en unique (label_en),
	constraint ukey__c_estimation_period__estimate_date_begin_end unique (estimate_date_begin, estimate_date_end),
	constraint chck__c_estimation_period__estimate_date_begin_end check ((estimate_date_begin < estimate_date_end))
);

comment on table nfiesta_results.c_estimation_period is 'Lookup table with estimation periods specifically bounded with two dates (begin, end).';
comment on column nfiesta_results.c_estimation_period.id is 'Identificator of the category.';
comment on column nfiesta_results.c_estimation_period.estimate_date_begin is 'Begin of the estimation period.';
comment on column nfiesta_results.c_estimation_period.estimate_date_end is 'End of the estimation period.';
comment on column nfiesta_results.c_estimation_period.label is 'Label of the category.';
comment on column nfiesta_results.c_estimation_period.description is 'Detailed description of the category.';
comment on column nfiesta_results.c_estimation_period.label_en is 'Label of the category.';
comment on column nfiesta_results.c_estimation_period.description_en is 'Detailed description of the category.';

comment on constraint pkey__c_estimation_period on nfiesta_results.c_estimation_period is 'Database identifier, primary key.';
comment on constraint ukey__c_estimation_period__label on nfiesta_results.c_estimation_period is 'Unique key on column label.';
comment on constraint ukey__c_estimation_period__label_en on nfiesta_results.c_estimation_period is 'Unique key on column label_en.';
comment on constraint ukey__c_estimation_period__estimate_date_begin_end ON nfiesta_results.c_estimation_period is 'Unique key on columns estimate_date_begin and estimate_date_end.';
comment on constraint chck__c_estimation_period__estimate_date_begin_end ON nfiesta_results.c_estimation_period is 'Checks that estimate_date_begin < estimate_date_end.';

alter table nfiesta_results.c_estimation_period owner to adm_nfiesta;
grant all on table nfiesta_results.c_estimation_period to adm_nfiesta;
--grant select on table nfiesta_results.c_estimation_period to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	c_panel_refyearset_group
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_panel_refyearset_group;

create table nfiesta_results.c_panel_refyearset_group
(
	id serial not null,
	"label" varchar(200) not null,
	description text not null,
	label_en varchar(200) not null,
	description_en text not null,
	constraint pkey__c_panel_refyearset_group primary key (id),
	constraint ukey__c_panel_refyearset_group__label unique (label),
	constraint ukey__c_panel_refyearset_group__label_en unique (label_en)
);

comment on table nfiesta_results.c_panel_refyearset_group is 'Lookup table with named aggregated sets of panels and corresponding reference year sets.';
comment on column nfiesta_results.c_panel_refyearset_group.id is 'Identificator of the category.';
comment on column nfiesta_results.c_panel_refyearset_group.label is 'Label of the category.';
comment on column nfiesta_results.c_panel_refyearset_group.description is 'Detailed description of the category.';
comment on column nfiesta_results.c_panel_refyearset_group.label_en is 'Label of the category.';
comment on column nfiesta_results.c_panel_refyearset_group.description_en is 'Detailed description of the category.';

comment on constraint pkey__c_panel_refyearset_group on nfiesta_results.c_panel_refyearset_group is 'Database identifier, primary key.';
comment on constraint ukey__c_panel_refyearset_group__label on nfiesta_results.c_panel_refyearset_group is 'Unique key on column label.';
comment on constraint ukey__c_panel_refyearset_group__label_en on nfiesta_results.c_panel_refyearset_group is 'Unique key on column label_en.';

alter table nfiesta_results.c_panel_refyearset_group owner to adm_nfiesta;
grant all on table nfiesta_results.c_panel_refyearset_group to adm_nfiesta;
--grant select on table nfiesta_results.c_panel_refyearset_group to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	c_phase_estimate_type
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_phase_estimate_type;

create table nfiesta_results.c_phase_estimate_type
(
	id serial not null,
	"label" varchar(120) not null,
	description text not null,
	constraint pkey__c_phase_estimate_type primary key (id),
	constraint ukey__c_phase_estimate_type__label unique (label)
);

comment on table nfiesta_results.c_phase_estimate_type is 'Table of phase estimate types.';
comment on column nfiesta_results.c_phase_estimate_type.id is 'Identifier of phase estimate type, primary key.';
comment on column nfiesta_results.c_phase_estimate_type.label is 'Label of phase estimate type.';
comment on column nfiesta_results.c_phase_estimate_type.description is 'Description of phase estimate type.';

comment on constraint pkey__c_phase_estimate_type on nfiesta_results.c_phase_estimate_type is 'Database identifier, primary key.';
comment on constraint ukey__c_phase_estimate_type__label on nfiesta_results.c_phase_estimate_type is 'Unique key on column label.';

alter table nfiesta_results.c_phase_estimate_type owner to adm_nfiesta;
grant all on table nfiesta_results.c_phase_estimate_type to adm_nfiesta;
--grant select on table nfiesta_results.c_phase_estimate_type to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	c_target_variable
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_target_variable;

create table nfiesta_results.c_target_variable
(
	id serial not null,
	metadata json not null,
	constraint pkey__c_target_variable primary key (id)
);

comment on table nfiesta_results.c_target_variable is 'Table of target variables.';
comment on column nfiesta_results.c_target_variable.id is 'Identifier of target variable, primary key.';
comment on column nfiesta_results.c_target_variable.metadata is 'Metadata of the target variable.';

comment on constraint pkey__c_target_variable on nfiesta_results.c_target_variable is 'Database identifier, primary key.';

alter table nfiesta_results.c_target_variable owner to adm_nfiesta;
grant all on table nfiesta_results.c_target_variable to adm_nfiesta;
--grant select on table nfiesta_results.c_target_variable to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	c_topic
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_topic;

create table nfiesta_results.c_topic
(
	id serial not null,
	"label" varchar(200) not null,
	description text not null,
	label_en varchar(200) not null,
	description_en text not null,
	constraint pkey__c_topic primary key (id),
	constraint ukey__c_topic__label unique (label),
	constraint ukey__c_topic__label_en unique (label_en)
);

comment on table nfiesta_results.c_topic is 'Table of topics.';
comment on column nfiesta_results.c_topic.id is 'Identificator of the topic.';
comment on column nfiesta_results.c_topic.label is 'Label of the category.';
comment on column nfiesta_results.c_topic.description is 'Detailed description of the topic.';
comment on column nfiesta_results.c_topic.label_en is 'Label of the topic.';
comment on column nfiesta_results.c_topic.description_en is 'Detailed description of the topic.';

comment on constraint pkey__c_topic on nfiesta_results.c_topic is 'Database identifier, primary key.';
comment on constraint ukey__c_topic__label on nfiesta_results.c_topic is 'Unique key on column label.';
comment on constraint ukey__c_topic__label_en on nfiesta_results.c_topic is 'Unique key on column label_en.';

alter table nfiesta_results.c_topic owner to adm_nfiesta;
grant all on table nfiesta_results.c_topic to adm_nfiesta;
--grant select on table nfiesta_results.c_topic to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	cm_result2topic
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.cm_result2topic;

create table nfiesta_results.cm_result2topic
(
	id serial not null,
	target_variable int4 not null,
	denominator int4 null,
	topic int4 not null,
	constraint pkey__cm_result2topic primary key (id),
	constraint fkey__cm_result2topic__c_target_variable__target_variable foreign key (target_variable) references nfiesta_results.c_target_variable(id),
	constraint fkey__cm_result2topic__c_target_variable__denominator foreign key (denominator) references nfiesta_results.c_target_variable(id),
	constraint fkey__cm_result2topic__c_topic foreign key (topic) references nfiesta_results.c_topic(id)
);

comment on table nfiesta_results.cm_result2topic is 'Mapping between target variable (total, ratio) and topic.';
comment on column nfiesta_results.cm_result2topic.id is 'Identificator of the record, primary key.';
comment on column nfiesta_results.cm_result2topic.target_variable is 'Identifier of target variable, foreign key to table c_target_variable.';
comment on column nfiesta_results.cm_result2topic.denominator is 'Identifier of target variable, foreign key to table c_target_variable.';
comment on column nfiesta_results.cm_result2topic.topic is 'Identifier of topic, foreign key to table c_topic.';

comment on constraint pkey__cm_result2topic on nfiesta_results.cm_result2topic is 'Database identifier, primary key.';
comment on constraint fkey__cm_result2topic__c_target_variable__target_variable on nfiesta_results.cm_result2topic is 'Foreign key to table c_target_variable.';
comment on constraint fkey__cm_result2topic__c_target_variable__denominator on nfiesta_results.cm_result2topic is 'Foreign key to table c_target_variable.';
comment on constraint fkey__cm_result2topic__c_topic ON nfiesta_results.cm_result2topic is 'Foreign key to table c_topic.';

create index fki__cm_result2topic__c_target_variable on nfiesta_results.cm_result2topic using btree (target_variable);
comment on index nfiesta_results.fki__cm_result2topic__c_target_variable is 'Index over foreign key fkey__cm_result2topic__c_target_variable__target_variable';

create index fki__cm_result2topic__c_target_variable_2 on nfiesta_results.cm_result2topic using btree (denominator);
comment on index nfiesta_results.fki__cm_result2topic__c_target_variable_2 is 'Index over foreign key fkey__cm_result2topic__c_target_variable__denominator';

create index fki__cm_result2topic__c_topic on nfiesta_results.cm_result2topic using btree (topic);
comment on index nfiesta_results.fki__cm_result2topic__c_topic is 'Index over foreign key fkey__cm_result2topic__c_topic';

alter table nfiesta_results.cm_result2topic owner to adm_nfiesta;
grant all on table nfiesta_results.cm_result2topic to adm_nfiesta;
--grant select on table nfiesta_results.cm_result2topic to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	t_variable
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.t_variable;


create table nfiesta_results.t_variable
(
	id serial not null,
	target_variable int4 not null,
	sub_population_category int4 null,
	area_domain_category int4 null,
	constraint pkey__t_variable primary key (id),
	constraint fkey__t_variable__c_area_domain_category foreign key (area_domain_category) references nfiesta_results.c_area_domain_category(id),
	constraint fkey__t_variable__c_sub_population_category foreign key (sub_population_category) references nfiesta_results.c_sub_population_category(id),
	constraint fkey__t_variable__c_target_variable foreign key (target_variable) references nfiesta_results.c_target_variable(id)
);

create unique index uidx__t_variable__all_columns on nfiesta_results.t_variable using btree (coalesce(target_variable, 0), coalesce(area_domain_category, 0), coalesce(sub_population_category, 0));

comment on table nfiesta_results.t_variable is 'Table with available combinations of variables (target/aux) and sub-populations/area domains.';
comment on column nfiesta_results.t_variable.id is 'Identificator of the record, primary key.';
comment on column nfiesta_results.t_variable.target_variable is 'Identifier of target variable, foreign key to table c_target_variable.';
comment on column nfiesta_results.t_variable.sub_population_category is 'Identifier of sub population category, foreign key to table c_sub_population_category.';
comment on column nfiesta_results.t_variable.area_domain_category is 'Identifier of topic, foreign key to table c_area_domain_category.';

comment on constraint pkey__t_variable on nfiesta_results.t_variable is 'Database identifier, primary key.';
comment on constraint fkey__t_variable__c_target_variable ON nfiesta_results.t_variable is 'Foreign key to table c_target_variable.';
comment on constraint fkey__t_variable__c_sub_population_category on nfiesta_results.t_variable is 'Foreign key to table c_sub_population_category.';
comment on constraint fkey__t_variable__c_area_domain_category on nfiesta_results.t_variable is 'Foreign key to table c_area_domain_category.';

create index fki__t_variable__c_target_variable on nfiesta_results.t_variable using btree (target_variable);
comment on index nfiesta_results.fki__t_variable__c_target_variable is 'Index over foreign key fkey__t_variable__c_target_variable';

create index fki__t_variable__c_area_domain_category on nfiesta_results.t_variable using btree (area_domain_category);
comment on index nfiesta_results.fki__t_variable__c_area_domain_category is 'Index over foreign key fkey__t_variable__c_area_domain_category';

create index fki__t_variable__c_sub_population_category on nfiesta_results.t_variable using btree (sub_population_category);
comment on index nfiesta_results.fki__t_variable__c_sub_population_category is 'Index over foreign key fkey__t_variable__c_sub_population_category';

alter table nfiesta_results.t_variable owner to adm_nfiesta;
grant all on table nfiesta_results.t_variable to adm_nfiesta;
--grant select on table nfiesta_results.t_variable to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	t_total_estimate_conf
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.t_total_estimate_conf;

create table nfiesta_results.t_total_estimate_conf
(
	id serial not null,
	estimation_cell int4 not null,
	variable int4 not null,
	phase_estimate_type int4 not null,
	force_synthetic bool null,
	estimation_period int4 not null,
	panel_refyearset_group int4 not null,
	constraint pkey__t_total_estimate_conf primary key (id),
	constraint fkey__t_total_estimate_conf__c_estimation_cell foreign key (estimation_cell) references nfiesta_results.c_estimation_cell(id),
	constraint fkey__t_total_estimate_conf__t_variable foreign key (variable) references nfiesta_results.t_variable(id),
	constraint fkey__t_total_estimate_conf__c_phase_estimate_type foreign key (phase_estimate_type) references nfiesta_results.c_phase_estimate_type(id),	
	constraint fkey__t_total_estimate_conf__c_estimation_period foreign key (estimation_period) references nfiesta_results.c_estimation_period(id),
	constraint fkey__t_total_estimate_conf__c_panel_refyearset_group foreign key (panel_refyearset_group) references nfiesta_results.c_panel_refyearset_group(id)
);

create index fki__t_total_estimate_conf__t_variable on nfiesta_results.t_total_estimate_conf using btree (variable);
comment on index nfiesta_results.fki__t_total_estimate_conf__t_variable is 'Index for looking up total estimate configurations that belong to selected attribute category.';
create unique index uidx__t_total_estimate_conf__all_columns on nfiesta_results.t_total_estimate_conf using btree (estimation_cell, variable, phase_estimate_type, coalesce(force_synthetic, false), estimation_period, panel_refyearset_group);

comment on table nfiesta_results.t_total_estimate_conf is 'Table of total estimate configurations (only totals with defined estimation cell).';
comment on column nfiesta_results.t_total_estimate_conf.id is 'Identificator of the record, primary key.';
comment on column nfiesta_results.t_total_estimate_conf.estimation_cell is 'Identificator of estimation cell, foreign key to c_estimation_cell table.';
comment on column nfiesta_results.t_total_estimate_conf.variable is 'Identificator of variable, foreign key to t_variable table.';
comment on column nfiesta_results.t_total_estimate_conf.phase_estimate_type is 'Identificator of phase estimate type, foreign key to c_phase_estimate_type table.';
comment on column nfiesta_results.t_total_estimate_conf.force_synthetic is '';
comment on column nfiesta_results.t_total_estimate_conf.estimation_period is 'Identificator of estimation period, foreign key to c_estimation_period table.';
comment on column nfiesta_results.t_total_estimate_conf.panel_refyearset_group is 'Identificator of panel refyearset group, foreign key to c_panel_refyearset_group table.';

comment on constraint pkey__t_total_estimate_conf on nfiesta_results.t_total_estimate_conf is 'Database identifier, primary key.';
comment on constraint fkey__t_total_estimate_conf__c_estimation_cell on nfiesta_results.t_total_estimate_conf is 'Foreign key to table c_estimation_cell.';
comment on constraint fkey__t_total_estimate_conf__t_variable on nfiesta_results.t_total_estimate_conf is 'Foreign key to table t_variable.';
comment on constraint fkey__t_total_estimate_conf__c_phase_estimate_type on nfiesta_results.t_total_estimate_conf is 'Foreign key to table c_phase_estimate_type.';
comment on constraint fkey__t_total_estimate_conf__c_estimation_period on nfiesta_results.t_total_estimate_conf is 'Foreign key to table c_estimation_period.';
comment on constraint fkey__t_total_estimate_conf__c_panel_refyearset_group on nfiesta_results.t_total_estimate_conf is 'Foreign key to table c_panel_refyearset_group.';

alter table nfiesta_results.t_total_estimate_conf owner to adm_nfiesta;
grant all on table nfiesta_results.t_total_estimate_conf to adm_nfiesta;
--grant select on table nfiesta_results.t_total_estimate_conf to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	t_estimate_conf
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.t_estimate_conf;

create table nfiesta_results.t_estimate_conf
(
	id serial not null,
	estimate_type int4 not null,
	total_estimate_conf int4 not null,
	denominator int4 null,
	constraint check__t_estimate_conf__denominator check (case when (estimate_type = 1) then (denominator is null) else (denominator is not null) end),
	constraint pkey__t_estimate_conf primary key (id),
	constraint fkey__t_estimate_conf__c_estimate_type foreign key (estimate_type) references nfiesta_results.c_estimate_type(id),
	constraint fkey__t_estimate_conf__t_total_estimate_conf foreign key (total_estimate_conf) references nfiesta_results.t_total_estimate_conf(id),
	constraint fkey__t_estimate_conf__t_total_estimate_conf2 foreign key (denominator) references nfiesta_results.t_total_estimate_conf(id)	
);

create unique index ukey__t_estimate_conf__all_columns on nfiesta_results.t_estimate_conf using btree (total_estimate_conf, coalesce(denominator, 0));

comment on table nfiesta_results.t_estimate_conf is 'Table of estimate configurations (configuration of total or combination of two totals in case of ratio).';
comment on column nfiesta_results.t_estimate_conf.id is 'Identificator of the record, primary key.';
comment on column nfiesta_results.t_estimate_conf.estimate_type is 'Estimate type (total or ratio), foreign key to table c_estimate_type.';
comment on column nfiesta_results.t_estimate_conf.total_estimate_conf is 'Estimate of specific total, foreign key to table t_total_estimate_conf.';
comment on column nfiesta_results.t_estimate_conf.denominator is 'Optional field, in case of ratio the total used as a denominator, foreign key to table t_total_estimate_conf.';

comment on constraint check__t_estimate_conf__denominator on nfiesta_results.t_estimate_conf is 'Check: (case when (estimate_type = 1) then (denominator is null) else (denominator is not null) end).';
comment on constraint pkey__t_estimate_conf on nfiesta_results.t_estimate_conf is 'Database identifier, primary key.';
comment on constraint fkey__t_estimate_conf__c_estimate_type on nfiesta_results.t_estimate_conf is 'Foreign key to table c_estimate_type.';
comment on constraint fkey__t_estimate_conf__t_total_estimate_conf on nfiesta_results.t_estimate_conf is 'Foreign key to table t_total_estimate_conf.';
comment on constraint fkey__t_estimate_conf__t_total_estimate_conf2 on nfiesta_results.t_estimate_conf is 'Foreign key to table t_total_estimate_conf.';

alter table nfiesta_results.t_estimate_conf owner to adm_nfiesta;
grant all on table nfiesta_results.t_estimate_conf to adm_nfiesta;
--grant select on table nfiesta_results.t_estimate_conf to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	t_variable_hierarchy
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.t_variable_hierarchy;

create table nfiesta_results.t_variable_hierarchy
(
	id serial not null,
	variable int4 not null,
	variable_superior int4 not null,
	constraint pkey__t_variable_hierarchy primary key (id),
	constraint fkey__t_variable_hierarchy__variable foreign key (variable) references nfiesta_results.t_variable(id),
	constraint fkey__t_variable_hierarchy__variable_sup foreign key (variable_superior) references nfiesta_results.t_variable(id)
);

comment on table nfiesta_results.t_variable_hierarchy is 'Table of variable hierarchies.';
comment on column nfiesta_results.t_variable_hierarchy.id is 'Identificator of the record, primary key.';
comment on column nfiesta_results.t_variable_hierarchy.variable is 'Identifier of variable, foreign key to table t_variable.';
comment on column nfiesta_results.t_variable_hierarchy.variable_superior is 'Identifier of variable, foreign key to table t_variable.';

comment on constraint pkey__t_variable_hierarchy on nfiesta_results.t_variable_hierarchy is 'Database identifier, primary key.';
comment on constraint fkey__t_variable_hierarchy__variable on nfiesta_results.t_variable_hierarchy is 'Foreign key to table t_variable.';
comment on constraint fkey__t_variable_hierarchy__variable_sup on nfiesta_results.t_variable_hierarchy is 'Foreign key to table t_variable.';

create index fki__t_variable_hierarchy__t_variable on nfiesta_results.t_variable_hierarchy using btree (variable);
comment on index nfiesta_results.fki__t_variable_hierarchy__t_variable is 'Index over foreign key fkey__t_variable_hierarchy__variable';

create index fki__t_variable_hierarchy__t_variable_2 on nfiesta_results.t_variable_hierarchy using btree (variable_superior);
comment on index nfiesta_results.fki__t_variable_hierarchy__t_variable_2 is 'Index over foreign key fkey__t_variable_hierarchy__variable_sup';

alter table nfiesta_results.t_variable_hierarchy owner to adm_nfiesta;
grant all on table nfiesta_results.t_variable_hierarchy to adm_nfiesta;
--grant select on table nfiesta_results.t_variable_hierarchy to usr_nfi_data;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--	t_result
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.t_result;

create table nfiesta_results.t_result
(
	id serial not null,
	estimate_conf int4 not null,
	point float8 null,
	var float8 null,
	calc_started timestamptz not null,
	calc_duration interval(6) not null,
	min_ssize float8 null,
	act_ssize int8 null,
	is_latest bool not null default true,
	constraint pkey__t_result primary key (id),
	constraint fkey__t_result__t_estimate_conf foreign key (estimate_conf) references nfiesta_results.t_estimate_conf(id)
);

create index fki__t_result__t_estimate_conf on nfiesta_results.t_result using btree (estimate_conf);
comment on index nfiesta_results.fki__t_result__t_estimate_conf is 'Index for looking up final results that belong to selected estimate configuration.';
create unique index uidx__t_result__estimate_conf on nfiesta_results.t_result using btree (estimate_conf) where (is_latest = true);

comment on table nfiesta_results.t_result is 'Table with the final estimates.';
comment on column nfiesta_results.t_result.id is 'Identificator of the record, primary key.';
comment on column nfiesta_results.t_result.estimate_conf is 'Identifier of estimate configuration, foreign key to table t_estimate_conf.';
comment on column nfiesta_results.t_result.point is 'Point estimate.';
comment on column nfiesta_results.t_result.var is 'Estimate of variability.';
comment on column nfiesta_results.t_result.calc_started is 'Time when calculation started.';
comment on column nfiesta_results.t_result.calc_duration is 'Time necessary for calculation.';
comment on column nfiesta_results.t_result.min_ssize is '';
comment on column nfiesta_results.t_result.act_ssize is '';
comment on column nfiesta_results.t_result.is_latest is '';

comment on constraint pkey__t_result on nfiesta_results.t_result is 'Database identifier, primary key.';
comment on constraint fkey__t_result__t_estimate_conf on nfiesta_results.t_result is 'Foreign key to table t_estimate_conf.';

alter table nfiesta_results.t_result owner to adm_nfiesta;
grant all on table nfiesta_results.t_result to adm_nfiesta;
--grant select on table nfiesta_results.t_result to usr_nfi_data;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- <function name="fn_trg__t_result_group__version__before_insert" schema="nfiesta_results" src="functions/fn_trg__t_result_group__version__before_insert.sql">
-- </function>
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	t_result_group
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.t_result_group;

create table nfiesta_results.t_result_group
(
	id serial not null,
	estimate_type int4 not null,
	target_variable int4 not null,
	target_variable_denom int4 null,
	estimation_period int4 not null,
	panel_refyearset_group int4 not null,
	phase_estimate_type int4 not null,
	phase_estimate_type_denom int4 null,
	estimation_cell_collection int4 not null,
	area_domain int4 null,
	area_domain_denom int4 null,
	sub_population int4 null,
	sub_population_denom int4 null,
	web boolean not null default true,
	constraint pkey__t_result_group primary key (id),
	constraint fkey__t_result_group__c_estimate_type foreign key (estimate_type) references nfiesta_results.c_estimate_type(id),
	constraint fkey__t_result_group__c_target_variable foreign key (target_variable) references nfiesta_results.c_target_variable(id),
	constraint fkey__t_result_group__c_target_variable2 foreign key (target_variable_denom) references nfiesta_results.c_target_variable(id),
	constraint fkey__t_result_group__c_estimation_period foreign key (estimation_period) references nfiesta_results.c_estimation_period(id),
	constraint fkey__t_result_group__c_panel_refyearset_group foreign key (panel_refyearset_group) references nfiesta_results.c_panel_refyearset_group(id),
	constraint fkey__t_result_group__c_phase_estimate_type foreign key (phase_estimate_type) references nfiesta_results.c_phase_estimate_type(id),
	constraint fkey__t_result_group__c_phase_estimate_type2 foreign key (phase_estimate_type_denom) references nfiesta_results.c_phase_estimate_type(id),
	constraint fkey__t_result_group__c_estimation_cell_collection foreign key (estimation_cell_collection) references nfiesta_results.c_estimation_cell_collection(id),
	constraint fkey__t_result_group__c_area_domain_1 foreign key (area_domain) references nfiesta_results.c_area_domain(id),
	constraint fkey__t_result_group__c_area_domain_2 foreign key (area_domain_denom) references nfiesta_results.c_area_domain(id),
	constraint fkey__t_result_group__c_sub_population_1 foreign key (sub_population) references nfiesta_results.c_sub_population(id),
	constraint fkey__t_result_group__c_sub_population_2 foreign key (sub_population_denom) references nfiesta_results.c_sub_population(id)
);

create unique index ukey__t_result_group__all_columns on nfiesta_results.t_result_group using btree
(estimate_type, target_variable, coalesce(target_variable_denom, 0), estimation_period,
panel_refyearset_group, phase_estimate_type, coalesce(phase_estimate_type_denom, 0),
estimation_cell_collection, coalesce(area_domain, 0), coalesce(area_domain_denom, 0),
coalesce(sub_population, 0), coalesce(sub_population_denom, 0));

create unique index ukey__t_result_group__panel_refyearset_group on nfiesta_results.t_result_group using btree
(estimate_type, target_variable, coalesce(target_variable_denom, 0), estimation_period,
phase_estimate_type, coalesce(phase_estimate_type_denom, 0), estimation_cell_collection,
coalesce(area_domain, 0), coalesce(area_domain_denom, 0),
coalesce(sub_population, 0), coalesce(sub_population_denom, 0));

create unique index ukey__t_result_group__phase_estimate_type on nfiesta_results.t_result_group using btree
(estimate_type, target_variable, coalesce(target_variable_denom, 0), estimation_period,
panel_refyearset_group, estimation_cell_collection,
coalesce(area_domain, 0), coalesce(area_domain_denom, 0),
coalesce(sub_population, 0), coalesce(sub_population_denom, 0));

comment on table nfiesta_results.t_result_group is 'Table of result groups.';
comment on column nfiesta_results.t_result_group.id is 'Identificator of the record, primary key.';
comment on column nfiesta_results.t_result_group.estimate_type is 'Estimate type (total or ratio), foreign key to table c_estimate_type.';
comment on column nfiesta_results.t_result_group.target_variable is 'Identificator of target variable, foreign key to table c_target_variable.';
comment on column nfiesta_results.t_result_group.target_variable_denom is 'Identificator of target variable, foreign key to table c_target_variable.';
comment on column nfiesta_results.t_result_group.estimation_period is 'Identificator of estimation period, foreign key to table c_estimation_period.';
comment on column nfiesta_results.t_result_group.panel_refyearset_group is 'Identificator of panel refyearset group, foreign key to table c_panel_refyearset_group.';
comment on column nfiesta_results.t_result_group.phase_estimate_type is 'Identificator of phase estimate type, foreign key to table c_phase_estimate_type.';
comment on column nfiesta_results.t_result_group.phase_estimate_type_denom is 'Identificator of phase estimate type, foreign key to table c_phase_estimate_type.';
comment on column nfiesta_results.t_result_group.estimation_cell_collection is 'Identificator of estimation cell collection, foreign key to table c_estimation_cell_collection.';
comment on column nfiesta_results.t_result_group.area_domain is 'Identicator of area domain variables, foreign key to table c_area_domain. Null identificate without distinction.';
comment on column nfiesta_results.t_result_group.area_domain_denom is 'Identicator of area domain variables for denominator, foreign key to table c_area_domain. Null identificate without distinction.';
comment on column nfiesta_results.t_result_group.sub_population is 'Identicator of sub population variables, foreign key to table c_sub_population. Null identificate without distinction.';
comment on column nfiesta_results.t_result_group.sub_population is 'Identicator of sub population variables for denominator, foreign key to table c_sub_population. Null identificate without distinction.';
comment on column nfiesta_results.t_result_group.web is 'Identificator if result can be display on public website.';

comment on constraint pkey__t_result_group on nfiesta_results.t_result_group is 'Database identifier, primary key.';
comment on constraint fkey__t_result_group__c_estimate_type on nfiesta_results.t_result_group is 'Foreign key to table c_estimate_type.';
comment on constraint fkey__t_result_group__c_target_variable on nfiesta_results.t_result_group is 'Foreign key to table c_target_variable.';
comment on constraint fkey__t_result_group__c_target_variable2 on nfiesta_results.t_result_group is 'Foreign key to table c_target_variable.';
comment on constraint fkey__t_result_group__c_estimation_period on nfiesta_results.t_result_group is 'Foreign key to table c_estimation_period.';
comment on constraint fkey__t_result_group__c_panel_refyearset_group on nfiesta_results.t_result_group is 'Foreign key to table c_panel_refyearset_group.';
comment on constraint fkey__t_result_group__c_phase_estimate_type on nfiesta_results.t_result_group is 'Foreign key to table c_phase_estimate_type.';
comment on constraint fkey__t_result_group__c_phase_estimate_type2 on nfiesta_results.t_result_group is 'Foreign key to table c_phase_estimate_type.';
comment on constraint fkey__t_result_group__c_estimation_cell_collection on nfiesta_results.t_result_group is 'Foreign key to table c_estimation_cell_collection.';
comment on constraint fkey__t_result_group__c_area_domain_1 on nfiesta_results.t_result_group is 'Foreign key to table c_area_domain.';
comment on constraint fkey__t_result_group__c_area_domain_2 on nfiesta_results.t_result_group is 'Foreign key to table c_area_domain.';
comment on constraint fkey__t_result_group__c_sub_population_1 on nfiesta_results.t_result_group is 'Foreign key to table c_sub_population.';
comment on constraint fkey__t_result_group__c_sub_population_2 on nfiesta_results.t_result_group is 'Foreign key to table c_sub_population.';

create index fki__t_result_group__c_estimate_type on nfiesta_results.t_result_group using btree (estimate_type);
comment on index nfiesta_results.fki__t_result_group__c_estimate_type is 'Index over foreign key fkey__t_result_group__c_estimate_type';

create index fki__t_result_group__c_target_variable on nfiesta_results.t_result_group using btree (target_variable);
comment on index nfiesta_results.fki__t_result_group__c_target_variable is 'Index over foreign key fkey__t_result_group__c_target_variable';

create index fki__t_result_group__c_target_variable2 on nfiesta_results.t_result_group using btree (target_variable_denom);
comment on index nfiesta_results.fki__t_result_group__c_target_variable2 is 'Index over foreign key fkey__t_result_group__c_target_variable2';

create index fki__t_result_group__c_estimation_period on nfiesta_results.t_result_group using btree (estimation_period);
comment on index nfiesta_results.fki__t_result_group__c_estimation_period is 'Index over foreign key fkey__t_result_group__c_estimation_period';

create index fki__t_result_group__c_panel_refyearset_group on nfiesta_results.t_result_group using btree (panel_refyearset_group);
comment on index nfiesta_results.fki__t_result_group__c_panel_refyearset_group is 'Index over foreign key fkey__t_result_group__c_panel_refyearset_group';

create index fki__t_result_group__c_phase_estimate_type on nfiesta_results.t_result_group using btree (phase_estimate_type);
comment on index nfiesta_results.fki__t_result_group__c_phase_estimate_type is 'Index over foreign key fkey__t_result_group__c_phase_estimate_type';

create index fki__t_result_group__c_phase_estimate_type2 on nfiesta_results.t_result_group using btree (phase_estimate_type_denom);
comment on index nfiesta_results.fki__t_result_group__c_phase_estimate_type2 is 'Index over foreign key fkey__t_result_group__c_phase_estimate_type2';

create index fki__t_result_group__c_estimation_cell_collection on nfiesta_results.t_result_group using btree (estimation_cell_collection);
comment on index nfiesta_results.fki__t_result_group__c_estimation_cell_collection is 'Index over foreign key fkey__t_result_group__c_estimation_cell_collection';

create index fki__t_result_group__c_area_domain_1 on nfiesta_results.t_result_group using btree (area_domain);
comment on index nfiesta_results.fki__t_result_group__c_area_domain_1 is 'Index over foreign key fkey__t_result_group__c_area_domain_1';

create index fki__t_result_group__c_area_domain_2 on nfiesta_results.t_result_group using btree (area_domain_denom);
comment on index nfiesta_results.fki__t_result_group__c_area_domain_2 is 'Index over foreign key fkey__t_result_group__c_area_domain_2';

create index fki__t_result_group__c_sub_population_1 on nfiesta_results.t_result_group using btree (sub_population);
comment on index nfiesta_results.fki__t_result_group__c_sub_population_1 is 'Index over foreign key fkey__t_result_group__c_sub_population_1';

create index fki__t_result_group__c_sub_population_2 on nfiesta_results.t_result_group using btree (sub_population_denom);
comment on index nfiesta_results.fki__t_result_group__c_sub_population_2 is 'Index over foreign key fkey__t_result_group__c_sub_population_2';

alter table nfiesta_results.t_result_group owner to adm_nfiesta;
grant all on table nfiesta_results.t_result_group to adm_nfiesta;
--grant select on table nfiesta_results.t_result_group to usr_nfi_data;


-- v t_result_group nesmi u vystupu existovat vice variant pro atributy:
-- stav opravy dat => v JSONu, u citatele a jmenovatele se mohou lisit => resit before insert triggerem
-- panel_refyearset_group => ted se predpoklada, ze u citatele i jmenovatele to jsou stejne hodnoty => lze resit unique klicem
-- phase_estimate_type => u citatele a jmenovatele se mohou lisit => lze resit unique klicem
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------



-- OPTION FUNCTIONS --


-------------------------------------------------------------------------------
-- <function name="fn_get_user_options_numerator" schema="nfiesta_results" src="functions/fn_get_user_options_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean);

create or replace function nfiesta_results.fn_get_user_options_numerator
(
	_jlang character varying,
	_topic integer default null::integer,
	_nom_estimation_period integer default null::integer,
	_nom_estimation_cell_collection integer default null::integer,
	_nom_id_group integer[] default null::integer[],
	_nom_indicator boolean default false,
	_nom_state boolean default false,
	_nom_unit_of_measure boolean default false,
	_nom_ldsity boolean default false,
	_nom_definition_variant boolean default false,
	_nom_area_domain boolean default false,
	_nom_population boolean default false,
	_unit_of_measure boolean default false
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[]
)
language plpgsql
stable
as
$$
declare
	_lang_suffix			character varying(3);
	_lang_covariate			text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	-- TOPIC --
	-----------------------------------------------------------------
	if _topic is null
	then
		return query execute
		'
		with
		w1 as	(
				select
						distinct
						target_variable,
						case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom
				from
						nfiesta_results.t_result_group where web = true
				)
		,w2 as	(
				select
						target_variable,
						case when denominator is null then 0 else denominator end as denominator,
						topic
				from
						nfiesta_results.cm_result2topic
				)
		,w3 as	(
				select distinct w2.topic from w1 inner join w2
				on w1.target_variable = w2.target_variable
				and w1.target_variable_denom = w2.denominator
				)
		,w4 as	(
				select
						id,
						label'||_lang_suffix||' AS label
				from
						nfiesta_results.c_topic
				where
						id in (select w3.topic from w3)
				)
		select
				w4.id as res_id,
				w4.label::text as res_label,
				null::integer[] as res_id_group
		from
				w4 order by w4.label;
		';
	else
		-----------------------------------------------------------------
		-- ESTIMATION_PERIOD --
		-----------------------------------------------------------------
		if _nom_estimation_period is null
		then
			return query execute
			'
			with
			w1 as	(
					select
							target_variable,
							case when denominator is null then 0 else denominator end as denominator
					from
							nfiesta_results.cm_result2topic where topic = $1
					)
			,w2 as	(
					select
							target_variable,
							case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
							estimation_period
					from
							nfiesta_results.t_result_group where web = true
					)
			,w3 as	(
					select distinct w2.estimation_period from w2 inner join w1
					on w2.target_variable = w1.target_variable
					and w2.target_variable_denom = w1.denominator
					)
			,w4 as	(
					select
							id,
							label'||_lang_suffix||' AS label
					from
							nfiesta_results.c_estimation_period
					where
							id in (select w3.estimation_period from w3)
					)
			select
					w4.id as res_id,
					w4.label::text as res_label,
					null::integer[] as res_id_group
			from
					w4 order by w4.label;				
			'
			using _topic;
		else
			-----------------------------------------------------------------
			-- ESTIMATION_CELL_COLLECTION --
			-----------------------------------------------------------------
			if _nom_estimation_cell_collection is null
			then
				return query execute
				'			
				with
				w1 as	(
						select
								target_variable,
								case when denominator is null then 0 else denominator end as denominator
						from
								nfiesta_results.cm_result2topic where topic = $1
						)
				,w2 as	(
						select
								target_variable,
								case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
								estimation_period,
								estimation_cell_collection
						from
								nfiesta_results.t_result_group where web = true
						)
				,w3 as	(
						select * from w2 inner join w1
						on w2.target_variable = w1.target_variable
						and w2.target_variable_denom = w1.denominator
						)
				,w4 as	(
						select distinct w3.estimation_cell_collection from w3 where w3.estimation_period = $2
						)
				,w5 as	(
						select
								id,
								label'||_lang_suffix||' AS label
						from
								nfiesta_results.c_estimation_cell_collection
						where
								id in (select w4.estimation_cell_collection from w4)
						)
				select
						w5.id as res_id,
						w5.label::text as res_label,
						null::integer[] as res_id_group
				from
						w5 order by w5.label;
				'
				using _topic, _nom_estimation_period;
			else
				-----------------------------------------------------------------
				-- NOM_INDICATOR --
				-----------------------------------------------------------------
				if _nom_indicator = false
				then
					return query execute
					'
					with
					w1 as	(
							select
									target_variable,
									case when denominator is null then 0 else denominator end as denominator
							from
									nfiesta_results.cm_result2topic where topic = $1
							)
					,w2 as	(
							select
									id,
									target_variable,
									case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
									estimation_period,
									estimation_cell_collection
							from
									nfiesta_results.t_result_group where web = true
							)
					,w3 as	(
							select w2.* from w2 inner join w1
							on w2.target_variable = w1.target_variable
							and w2.target_variable_denom = w1.denominator
							)
					,w4 as	(
							select w3.* from w3
							where w3.estimation_period = $2
							and w3.estimation_cell_collection = $3
							)
					,w5 as	(
							select
									target_variable,
									array_agg(w4.id order by w4.id) as id
							from
									w4 group by target_variable
							)
					,w6 as	(
							select
									w5.*,
									((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
									((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
							from
									w5
									inner join nfiesta_results.c_target_variable as ctv
									on w5.target_variable = ctv.id
							)
					,w7 as	(
							select
									w6.id,
									label'||_lang_suffix||' AS label
							from
									w6
							)
					,w8 as	(
							select
									w7.label,
									unnest(w7.id) as id
							from
									w7
							)
					,w9 as	(
							select w8.label, array_agg(w8.id) as id from w8 group by w8.label
							)
					select
							null::integer as res_id,
							w9.label as res_label,
							w9.id as res_id_group
					from
							w9 order by w9.label;
					'
					using _topic, _nom_estimation_period, _nom_estimation_cell_collection;
				else
					-----------------------------------------------------------------
					-- NOM_STATE --
					-----------------------------------------------------------------
					if _nom_state = false
					then
						return query execute
						'
						with
						w1 as	(
								select * from nfiesta_results.t_result_group as trg
								where trg.id in (select unnest($1))
								and trg.web = true
								)
						,w2 as	(
								select
										target_variable,
										array_agg(w1.id order by w1.id) as id
								from
										w1 group by target_variable
								)
						,w3 as	(
								select
										w2.*,
										((ctv.metadata->''cs'')->''state or change'')->>''label'' as label,
										((ctv.metadata->''en'')->''state or change'')->>''label'' as label_en
								from
										w2
										inner join nfiesta_results.c_target_variable as ctv
										on w2.target_variable = ctv.id
								)
						,w4 as	(
								select
										w3.id,
										label'||_lang_suffix||' AS label
								from
										w3
								)
						,w5 as	(
								select
										w4.label,
										unnest(w4.id) as id
								from
										w4
								)
						,w6 as	(
								select w5.label, array_agg(w5.id) as id from w5 group by w5.label
								)
						select
								null::integer as res_id,
								w6.label as res_label,
								w6.id as res_id_group
						from
								w6 order by w6.label;
						'
						using _nom_id_group;					
					else
						-----------------------------------------------------------------
						-- NOM_UNIT_OF_MEASURE --
						-----------------------------------------------------------------
						if _nom_unit_of_measure = false
						then
							return query execute
							'
							with
							w1 as	(
									select * from nfiesta_results.t_result_group as trg
									where trg.id in (select unnest($1))
									and trg.web = true
									)
							,w2 as	(
									select
											target_variable,
											array_agg(w1.id order by w1.id) as id
									from
											w1 group by target_variable
									)
							,w3 as	(
									select
											w2.*,
											((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
											((ctv.metadata->''en'')->''unit'')->>''label'' as label_en
									from
											w2
											inner join nfiesta_results.c_target_variable as ctv
											on w2.target_variable = ctv.id
									)
							,w4 as	(
									select
											w3.id,
											label'||_lang_suffix||' AS label
									from
											w3
									)
							,w5 as	(
									select
											w4.label,
											unnest(w4.id) as id
									from
											w4
									)
							,w6 as	(
									select w5.label, array_agg(w5.id) as id from w5 group by w5.label
									)
							select
									null::integer as res_id,
									w6.label as res_label,
									w6.id as res_id_group
							from
									w6 order by w6.label;
							'
							using _nom_id_group;
						else
							-----------------------------------------------------------------
							-- NOM_LDSITY --
							-----------------------------------------------------------------
							if _nom_ldsity = false
							then
								return query execute
								'
								with
								w1 as	(
										select * from nfiesta_results.t_result_group as trg
										where trg.id in (select unnest($1))
										and trg.web = true
										)
								,w2 as	(
										select
												target_variable,
												array_agg(w1.id order by w1.id) as id
										from
												w1 group by target_variable
										)
								,w3 as	(
										select
												t.target_variable,
												t.id,
												t.label'||_lang_suffix||' AS label
										from
												(
												select
														w2.*,
														((ctv.metadata->''cs'')->''local densities'') as label,
														((ctv.metadata->''en'')->''local densities'') as label_en
												from
														w2
														inner join nfiesta_results.c_target_variable as ctv
														on w2.target_variable = ctv.id
												) as t
										)
								,w4 as	(
										select
												w3.target_variable,
												json_array_elements(w3.label) as label
										from
												w3
										)
								,w5 as	(
										select
												row_number() over () as new_id,
												w4.target_variable,
												w4.label->>''label'' as label
										from w4
										)
								,w6 as	(
										select
												w5.target_variable,
												array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
										from
												w5 group by w5.target_variable
										)
								,w7 as	(
										select w3.*, w6.label4user from w3 inner join w6
										on w3.target_variable = w6.target_variable
										)
								,w8 as	(
										select
												w7.label4user as label,
												unnest(w7.id) as id
										from
												w7
										)
								,w9 as	(
										select w8.label, array_agg(w8.id) as id from w8 group by w8.label
										)
								select
										null::integer as res_id,
										w9.label as res_label,
										w9.id as res_id_group
								from
										w9 order by w9.label;
								'
								using _nom_id_group;
							else
								-----------------------------------------------------------------
								-- NOM_DEFINITION_VARIANT --
								-----------------------------------------------------------------
								if _nom_definition_variant = false
								then
									return query execute
									'
									with
									w1 as	(
											select * from nfiesta_results.t_result_group as trg
											where trg.id in (select unnest($1))
											and trg.web = true
											)
									,w2 as	(
											select
													target_variable,
													array_agg(w1.id order by w1.id) as id
											from
													w1 group by target_variable
											)
									,w3 as	(
											select
													t.target_variable,
													t.id,
													t.label'||_lang_suffix||' AS label
											from
													(
													select
															w2.*,
															((ctv.metadata->''cs'')->''local densities'') as label,
															((ctv.metadata->''en'')->''local densities'') as label_en
													from
															w2
															inner join nfiesta_results.c_target_variable as ctv
															on w2.target_variable = ctv.id
													) as t
											)
									,w4 as	(
											select
													w3.target_variable,
													json_array_elements(w3.label) as label
											from
													w3
											)
									,w5 as	(
											select
													row_number() over () as new_id,
													w4.target_variable,
													coalesce((w4.label->''definition variant'')->>''label'',''null''::text) as label
											from w4
											)										
									,w6 as	(
										select
												w5.target_variable,
												array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
										from
												w5 group by w5.target_variable
											)
									,w7 as	(
											select w3.*, w6.label4user from w3 inner join w6
											on w3.target_variable = w6.target_variable
											)
									,w8 as	(
											select
													w7.label4user as label,
													unnest(w7.id) as id
											from
													w7
											)
									,w9 as	(
											select w8.label, array_agg(w8.id) as id from w8 group by w8.label
											)
									select
											null::integer as res_id,
											w9.label as res_label,
											w9.id as res_id_group
									from
											w9 order by w9.label;
									'
									using _nom_id_group;									
								else
									-----------------------------------------------------------------
									-- NOM_AREA_DOMAIN --
									-----------------------------------------------------------------
									if _nom_area_domain = false
									then
										return query execute
										'										
										with
										w1 as	(
												select * from nfiesta_results.t_result_group as trg
												where trg.id in (select unnest($1))
												and trg.web = true
												)
										,w2 as	(
												select
														target_variable,
														array_agg(w1.id order by w1.id) as id
												from
														w1 group by target_variable
												)
										,w3 as	(
												select
														t.target_variable,
														t.id,
														t.label'||_lang_suffix||' AS label
												from
														(
														select
																w2.*,
																((ctv.metadata->''cs'')->''local densities'') as label,
																((ctv.metadata->''en'')->''local densities'') as label_en
														from
																w2
																inner join nfiesta_results.c_target_variable as ctv
																on w2.target_variable = ctv.id
														) as t
												)
										,w4 as	(
												select
														w3.target_variable,
														json_array_elements(w3.label) as label
												from
														w3
												)
										,w5 as	(
												select
														row_number() over () as new_id,
														w4.target_variable,
														replace(
														replace(
														replace(
														replace(
														coalesce((w4.label->''area domain'')->>''label'',''null''::text)
														,''", "'',''; '')
														,''","'',''; '')
														,''["'','''')
														,''"]'','''')													
														as label
												from w4
												)
										,w6 as	(
											select
													w5.target_variable,
													array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
											from
													w5 group by w5.target_variable
												)
										,w7 as	(
												select w3.*, w6.label4user from w3 inner join w6
												on w3.target_variable = w6.target_variable
												)
										,w8 as	(
												select
														w7.label4user as label,
														unnest(w7.id) as id
												from
														w7
												)
										,w9 as	(
												select w8.label, array_agg(w8.id) as id from w8 group by w8.label
												)
										select
												null::integer as res_id,
												w9.label as res_label,
												w9.id as res_id_group
										from
												w9 order by w9.label;
										'
										using _nom_id_group;
									else
										-----------------------------------------------------------------
										-- NOM_POPULATION --
										-----------------------------------------------------------------
										if _nom_population = false
										then
											return query execute
											'	
											with
											w1 as	(
													select * from nfiesta_results.t_result_group as trg
													where trg.id in (select unnest($1))
													and trg.web = true
													)
											,w2 as	(
													select
															target_variable,
															array_agg(w1.id order by w1.id) as id
													from
															w1 group by target_variable
													)
											,w3 as	(
													select
															t.target_variable,
															t.id,
															t.label'||_lang_suffix||' AS label
													from
															(
															select
																	w2.*,
																	((ctv.metadata->''cs'')->''local densities'') as label,
																	((ctv.metadata->''en'')->''local densities'') as label_en
															from
																	w2
																	inner join nfiesta_results.c_target_variable as ctv
																	on w2.target_variable = ctv.id
															) as t
													)
											,w4 as	(
													select
															w3.target_variable,
															json_array_elements(w3.label) as label
													from
															w3
													)
											,w5 as	(
													select
															row_number() over () as new_id,
															w4.target_variable,
															replace(
															replace(
															replace(
															replace(
															coalesce((w4.label->''population'')->>''label'',''null''::text)
															,''", "'',''; '')
															,''","'',''; '')
															,''["'','''')
															,''"]'','''')													
															as label
													from w4
													)
											,w6 as	(
												select
														w5.target_variable,
														array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
												from
														w5 group by w5.target_variable
													)
											,w7 as	(
													select w3.*, w6.label4user from w3 inner join w6
													on w3.target_variable = w6.target_variable
													)
											,w8 as	(
													select
															w7.label4user as label,
															unnest(w7.id) as id
													from
															w7
													)
											,w9 as	(
													select w8.label, array_agg(w8.id) as id from w8 group by w8.label
													)
											select
													null::integer as res_id,
													w9.label as res_label,
													w9.id as res_id_group
											from
													w9 order by w9.label;
											'
											using _nom_id_group;
										else
											-----------------------------------------------------------------
											-- UNIT_OF_MEASURE --
											-----------------------------------------------------------------
											if _unit_of_measure = false
											then
												return query execute
												'
												with
												w1 as	(
														select
																id,
																target_variable,
																coalesce(target_variable_denom,0) as target_variable_denom 
														from
																nfiesta_results.t_result_group
														where
																id in (select unnest($1))
														and
																web = true
														)
												,w2 as	(
														select
																w1.target_variable,
																w1.target_variable_denom,
																array_agg(w1.id order by w1.id) as id
														from
																w1 group by w1.target_variable, w1.target_variable_denom
														)
												,w3 as	(
														select
																w2.*,
																((ctv1.metadata->''cs'')->''unit'')->>''label'' as unit_nom,
																((ctv2.metadata->''cs'')->''unit'')->>''label'' as unit_denom,
																((ctv1.metadata->''en'')->''unit'')->>''label'' as unit_nom_en,
																((ctv2.metadata->''en'')->''unit'')->>''label'' as unit_denom_en
														from
																w2
																inner join nfiesta_results.c_target_variable as ctv1 on w2.target_variable = ctv1.id
																left join nfiesta_results.c_target_variable as ctv2 on w2.target_variable_denom = ctv2.id
														)
												,w4 as	(
														select
																w3.target_variable,
																w3.target_variable_denom,
																w3.id,
																w3.unit_nom'||_lang_suffix||' AS unit_nom,
																w3.unit_denom'||_lang_suffix||' AS unit_denom
														from
																w3
														)
												,w5 as	(
														select
																w4.*,
																case
																	when (w4.target_variable = w4.target_variable_denom) or (w4.unit_nom = w4.unit_denom) then ''%''
																	when w4.target_variable_denom = 0 then w4.unit_nom
																	else concat(w4.unit_nom,'' / '',w4.unit_denom)
																end as label4user,
																------------------
																case
																	when w4.target_variable_denom = 0 then 0
																	else 1
																end as res_id
														from
																w4
														)
												,w6 as	(
														select
																w5.res_id,
																w5.label4user as label,
																unnest(w5.id) as id
														from
																w5
														)
												,w7 as	(
														select
																w6.res_id,
																w6.label,
																array_agg(w6.id) as id
														from
																w6 group by w6.res_id, w6.label
														)
												select
														w7.res_id,
														w7.label as res_label,
														w7.id as res_id_group
												from
														w7 order by w7.label;
												'
												using _nom_id_group;																	
											else
												raise exception 'Error 01: fn_get_user_options_numerator: This variant of function is not implemented !';
											end if;
										end if;
									end if;
								end if;
							end if;
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean) is
'The function returns list of available IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean) to public;
-- </function>
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_get_user_options_denominator" schema="nfiesta_results" src="functions/fn_get_user_options_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean, boolean, boolean, boolean, boolean);

create or replace function nfiesta_results.fn_get_user_options_denominator
(
	_jlang character varying,
	_denom_id_group integer[],
	_denom_indicator boolean DEFAULT false,
	_denom_state boolean DEFAULT false,
	_denom_ldsity boolean DEFAULT false,
	_denom_definition_variant boolean DEFAULT false,
	_denom_area_domain boolean DEFAULT false,
	_denom_population boolean DEFAULT false
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[]
)
language plpgsql
stable
as
$$
declare
	_lang_suffix			character varying(3);
	_lang_covariate			text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _denom_id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_denominator: Input argument _denom_id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	-- DENOM_INDICATOR --
	-----------------------------------------------------------------
	if _denom_indicator = false
	then
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group
				where id in (select unnest($1))
				and web = true
				)
		,w2 as	(
				select
						w1.target_variable_denom,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						w2.*,
						((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
						((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
				from
						w2
						inner join nfiesta_results.c_target_variable as ctv
						on w2.target_variable_denom = ctv.id
				)
		,w4 as	(
				select
						w3.id,
						label'||_lang_suffix||' AS label
				from
						w3
				)
		,w5 as	(
				select
						w4.label,
						unnest(w4.id) as id
				from
						w4
				)
		,w6 as	(
				select w5.label, array_agg(w5.id) as id from w5 group by w5.label
				)
		select
				1 as res_id,
				w6.label as res_label,
				w6.id as res_id_group
		from
				w6 order by w6.label;
		'
		using _denom_id_group;
	else
		-----------------------------------------------------------------
		-- DENOM_STATE --
		-----------------------------------------------------------------
		if _denom_state = false
		then
			return query execute
			'
			with
			w1 as	(
					select * from nfiesta_results.t_result_group
					where id in (select unnest($1))
					and web = true
					)
			,w2 as	(
					select
							w1.target_variable_denom,
							array_agg(w1.id order by w1.id) as id
					from
							w1 group by target_variable_denom
					)
			,w3 as	(
					select
							w2.*,
							((ctv.metadata->''cs'')->''state or change'')->>''label'' as label,
							((ctv.metadata->''en'')->''state or change'')->>''label'' as label_en
					from
							w2
							inner join nfiesta_results.c_target_variable as ctv
							on w2.target_variable_denom = ctv.id
					)
			,w4 as	(
					select
							w3.id,
							label'||_lang_suffix||' AS label
					from
							w3
					)
			,w5 as	(
					select
							w4.label,
							unnest(w4.id) as id
					from
							w4
					)
			,w6 as	(
					select w5.label, array_agg(w5.id) as id from w5 group by w5.label
					)
			select
					1 as res_id,
					w6.label as res_label,
					w6.id as res_id_group
			from
					w6 order by w6.label;
			'
			using _denom_id_group;
		else
			-----------------------------------------------------------------
			-- DENOM_LDSITY --
			-----------------------------------------------------------------
			if _denom_ldsity = false
			then
				return query execute
				'
				with
				w1 as	(
						select * from nfiesta_results.t_result_group
						where id in (select unnest($1))
						and web = true
						)
				,w2 as	(
						select
								w1.target_variable_denom,
								array_agg(w1.id order by w1.id) as id
						from
								w1 group by target_variable_denom
						)
				,w3 as	(
						select
								t.target_variable_denom,
								t.id,
								t.label'||_lang_suffix||' AS label
						from
								(
								select
										w2.*,
										((ctv.metadata->''cs'')->''local densities'') as label,
										((ctv.metadata->''en'')->''local densities'') as label_en
								from
										w2
										inner join nfiesta_results.c_target_variable as ctv
										on w2.target_variable_denom = ctv.id
								) as t
						)
				,w4 as	(
						select
								w3.target_variable_denom,
								json_array_elements(w3.label) as label
						from
								w3
						)
				,w5 as	(
						select
								row_number() over () as new_id,
								w4.target_variable_denom,
								w4.label->>''label'' as label
						from w4
						)
				,w6 as	(
						select
								w5.target_variable_denom,
								array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
						from
								w5 group by w5.target_variable_denom
						)
				,w7 as	(
						select w3.*, w6.label4user from w3 inner join w6
						on w3.target_variable_denom = w6.target_variable_denom
						)
				,w8 as	(
						select
								w7.label4user as label,
								unnest(w7.id) as id
						from
								w7
						)
				,w9 as	(
						select w8.label, array_agg(w8.id) as id from w8 group by w8.label
						)
				select
						1 as res_id,
						w9.label as res_label,
						w9.id as res_id_group
				from
						w9 order by w9.label;
				'
				using _denom_id_group;
			else
				-----------------------------------------------------------------
				-- DENOM_DEFINITION_VARIANT --
				-----------------------------------------------------------------
				if _denom_definition_variant = false
				then
					return query execute
					'
					with
					w1 as	(
							select * from nfiesta_results.t_result_group
							where id in (select unnest($1))
							and web = true
							)
					,w2 as	(
							select
									w1.target_variable_denom,
									array_agg(w1.id order by w1.id) as id
							from
									w1 group by target_variable_denom
							)
					,w3 as	(
							select
									t.target_variable_denom,
									t.id,
									t.label'||_lang_suffix||' AS label
							from
									(
									select
											w2.*,
											((ctv.metadata->''cs'')->''local densities'') as label,
											((ctv.metadata->''en'')->''local densities'') as label_en
									from
											w2
											inner join nfiesta_results.c_target_variable as ctv
											on w2.target_variable_denom = ctv.id
									) as t
							)
					,w4 as	(
							select
									w3.target_variable_denom,
									json_array_elements(w3.label) as label
							from
									w3
							)
					,w5 as	(
							select
									row_number() over () as new_id,
									w4.target_variable_denom,
									coalesce((w4.label->''definition variant'')->>''label'',''null''::text) as label
							from w4
							)										
					,w6 as	(
						select
								w5.target_variable_denom,
								array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
						from
								w5 group by w5.target_variable_denom
							)
					,w7 as	(
							select w3.*, w6.label4user from w3 inner join w6
							on w3.target_variable_denom = w6.target_variable_denom
							)
					,w8 as	(
							select
									w7.label4user as label,
									unnest(w7.id) as id
							from
									w7
							)
					,w9 as	(
							select w8.label, array_agg(w8.id) as id from w8 group by w8.label
							)
					select
							1 as res_id,
							w9.label as res_label,
							w9.id as res_id_group
					from
							w9 order by w9.label;
					'
					using _denom_id_group;
				else
					-----------------------------------------------------------------
					-- DENOM_AREA_DOMAIN --
					-----------------------------------------------------------------
					if _denom_area_domain = false
					then
						return query execute
						'
						with
						w1 as	(
								select * from nfiesta_results.t_result_group
								where id in (select unnest($1))
								and web = true
								)
						,w2 as	(
								select
										w1.target_variable_denom,
										array_agg(w1.id order by w1.id) as id
								from
										w1 group by target_variable_denom
								)
						,w3 as	(
								select
										t.target_variable_denom,
										t.id,
										t.label'||_lang_suffix||' AS label
								from
										(
										select
												w2.*,
												((ctv.metadata->''cs'')->''local densities'') as label,
												((ctv.metadata->''en'')->''local densities'') as label_en
										from
												w2
												inner join nfiesta_results.c_target_variable as ctv
												on w2.target_variable_denom = ctv.id
										) as t
								)
						,w4 as	(
								select
										w3.target_variable_denom,
										json_array_elements(w3.label) as label
								from
										w3
								)
						,w5 as	(
								select
										row_number() over () as new_id,
										w4.target_variable_denom,
										replace(
										replace(
										replace(
										replace(
										coalesce((w4.label->''area domain'')->>''label'',''null''::text)
										,''", "'',''; '')
										,''","'',''; '')
										,''["'','''')
										,''"]'','''')													
										as label
								from w4
								)
						,w6 as	(
							select
									w5.target_variable_denom,
									array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
							from
									w5 group by w5.target_variable_denom
								)
						,w7 as	(
								select w3.*, w6.label4user from w3 inner join w6
								on w3.target_variable_denom = w6.target_variable_denom
								)
						,w8 as	(
								select
										w7.label4user as label,
										unnest(w7.id) as id
								from
										w7
								)
						,w9 as	(
								select w8.label, array_agg(w8.id) as id from w8 group by w8.label
								)
						select
								1 as res_id,
								w9.label as res_label,
								w9.id as res_id_group
						from
								w9 order by w9.label;
						'
						using _denom_id_group;
					else
						-----------------------------------------------------------------
						-- DENOM_POPULATION --
						-----------------------------------------------------------------
						if _denom_population = false
						then
							return query execute
							'
							with
							w1 as	(
									select * from nfiesta_results.t_result_group
									where id in (select unnest($1))
									and web = true
									)
							,w2 as	(
									select
											w1.target_variable_denom,
											array_agg(w1.id order by w1.id) as id
									from
											w1 group by target_variable_denom
									)
							,w3 as	(
									select
											t.target_variable_denom,
											t.id,
											t.label'||_lang_suffix||' AS label
									from
											(
											select
													w2.*,
													((ctv.metadata->''cs'')->''local densities'') as label,
													((ctv.metadata->''en'')->''local densities'') as label_en
											from
													w2
													inner join nfiesta_results.c_target_variable as ctv
													on w2.target_variable_denom = ctv.id
											) as t
									)
							,w4 as	(
									select
											w3.target_variable_denom,
											json_array_elements(w3.label) as label
									from
											w3
									)
							,w5 as	(
									select
											row_number() over () as new_id,
											w4.target_variable_denom,
											replace(
											replace(
											replace(
											replace(
											coalesce((w4.label->''population'')->>''label'',''null''::text)
											,''", "'',''; '')
											,''","'',''; '')
											,''["'','''')
											,''"]'','''')													
											as label
									from w4
									)
							,w6 as	(
								select
										w5.target_variable_denom,
										array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
								from
										w5 group by w5.target_variable_denom
									)
							,w7 as	(
									select w3.*, w6.label4user from w3 inner join w6
									on w3.target_variable_denom = w6.target_variable_denom
									)
							,w8 as	(
									select
											w7.label4user as label,
											unnest(w7.id) as id
									from
											w7
									)
							,w9 as	(
									select w8.label, array_agg(w8.id) as id from w8 group by w8.label
									)
							select
									0 as res_id,
									w9.label as res_label,
									w9.id as res_id_group
							from
									w9 order by w9.label;
							'
							using _denom_id_group; 
						else
							raise exception 'Error 02: fn_get_user_options_denominator: This variant of function is not implemented !';
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean, boolean, boolean, boolean, boolean) is
'The function returns list of available IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean, boolean, boolean, boolean, boolean) to public;
-- </function>
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_get_user_options_area_domain_numerator" schema="nfiesta_results" src="functions/fn_get_user_options_area_domain_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_area_domain_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_area_domain_numerator(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_area_domain_numerator
(
	_jlang character varying,
	_id_group integer[],
	_area_domain integer[] default null::integer[]
)
returns table
(
	res_estimate_type integer,
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_area_domain integer[]
)
as
$$
declare
	_lang_suffix			character varying(3);
	_lang_covariate			text;
	_estimate_type			integer;
	_check_records			integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_area_domain_numerator: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select distinct estimate_type from nfiesta_results.t_result_group
	where id in (select unnest(_id_group))
	and web = true
	into _estimate_type;
	-----------------------------------------------------------------
	if _area_domain is null
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as csp
						on w3.area_domain = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmspc
				where cmspc.area_domain_category in
					(
					select cspc.id from nfiesta_results.c_area_domain_category as cspc
					where cspc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.area_domain as area_domain,
						cspc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
						inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)	
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
				)
		select
				$2 as res_estimate_type,
				w16.atomic_type as res_id,
				(w16.label'||_lang_suffix||')::text AS res_label,
				w16.id as res_id_group,
				array[w16.atomic_type] as res_area_domain
		from
				w16;
		'
		using _id_group, _estimate_type;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as csp
						on w3.area_domain = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmspc
				where cmspc.area_domain_category in
					(
					select cspc.id from nfiesta_results.c_area_domain_category as cspc
					where cspc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.area_domain as area_domain,
						cspc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
						inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as area_domain,
								t.id
						from
								(
								select w11.area_domain, w11.id, w11.atomic_type from w11 union all
								select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						$3 as res_estimate_type,
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_area_domain
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _area_domain, _estimate_type
		into _check_records;	
		
		if _check_records = 0
		then
			if _area_domain = array[0]
			then
				return query
				select
						_estimate_type as res_estimate_type,
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						_area_domain as _res_area_domain;
			else
				return query
				select
						_estimate_type as res_estimate_type,
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						array_remove(_area_domain,0) as _res_area_domain;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.area_domain] as area_domain,
							t.id
					from
							(
							select w1.area_domain, array_agg(w1.id order by w1.id) as id
							from w1 where w1.area_domain = 0
							group by w1.area_domain
							) as t
					)
			,w3 as	(
					select w1.area_domain, array_agg(w1.id order by w1.id) as id
					from w1 where w1.area_domain > 0
					group by w1.area_domain
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_area_domain as csp
							on w3.area_domain = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_area_domain_category as cmspc
					where cmspc.area_domain_category in
						(
						select cspc.id from nfiesta_results.c_area_domain_category as cspc
						where cspc.area_domain in (select w6.area_domain from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.area_domain as area_domain,
							cspc2.area_domain as atomic_type
					from
							w7
							inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
							inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.area_domain from w8
					)
			,w10 as (
					select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.area_domain
					)
			,w11 as (
					select
							w6.area_domain,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.area_domain = w10.area_domain
					)
			,w12 as	(
					select
							w11.area_domain,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.area_domain,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.area_domain,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
							from	(
									select distinct a1.atomic_type, a1.area_domain
									from
										(
										select w13.atomic_type, w13.area_domain from w13
										union all
										select w5.area_domain as atomic_type, w5.area_domain from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as area_domain,
									null::integer[] as area_domain,
									t.id
							from
									(
									select w11.area_domain, w11.id, w11.atomic_type from w11 union all
									select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
					)
					select
							$3 as res_estimate_type,
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_area_domain
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _area_domain, _estimate_type;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_area_domain_numerator(character varying, integer[], integer[]) is
'The function returns list of available area domains for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of area domains.';

grant execute on function nfiesta_results.fn_get_user_options_area_domain_numerator(character varying, integer[], integer[]) to public;
-- </function>
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_get_user_options_sub_population_numerator" schema="nfiesta_results" src="functions/fn_get_user_options_sub_population_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_sub_population_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_sub_population_numerator(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_sub_population_numerator
(
	_jlang character varying,
	_id_group integer[],
	_sub_population integer[] default null::integer[]
)
returns table
(
	res_estimate_type integer,
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_sub_population integer[]
)
as
$$
declare
	_lang_suffix			character varying(3);
	_lang_covariate			text;
	_estimate_type			integer;
	_check_records			integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_sub_population_numerator: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select distinct estimate_type from nfiesta_results.t_result_group
	where id in (select unnest(_id_group))
	and web = true
	into _estimate_type;
	-----------------------------------------------------------------
	if _sub_population is null
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)	
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		select
				$2 as res_estimate_type,
				w16.atomic_type as res_id,
				(w16.label'||_lang_suffix||')::text AS res_label,
				w16.id as res_id_group,
				array[w16.atomic_type] as res_sub_population
		from
				w16;
		'
		using _id_group, _estimate_type;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as sub_population,
								t.id
						from
								(
								select w11.sub_population, w11.id, w11.atomic_type from w11 union all
								select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						$3 as res_estimate_type,
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_sub_population
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _sub_population, _estimate_type
		into _check_records;	
		
		if _check_records = 0
		then
			if _estimate_type = 1
			then
				if	array_length(_id_group,1) is distinct from 1
				then
					raise exception 'Error 02: fn_get_user_options_sub_population_numerator: Output argument _res_id_group contains more elements then one!';
				end if;
			end if;
		
			if _sub_population = array[0]
			then
				return query
				select
						_estimate_type as res_estimate_type,
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						_sub_population as _res_sub_population;
			else
				return query
				select
						_estimate_type as res_estimate_type,
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						array_remove(_sub_population,0) as _res_sub_population;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.sub_population] as sub_population,
							t.id
					from
							(
							select w1.sub_population, array_agg(w1.id order by w1.id) as id
							from w1 where w1.sub_population = 0
							group by w1.sub_population
							) as t
					)
			,w3 as	(
					select w1.sub_population, array_agg(w1.id order by w1.id) as id
					from w1 where w1.sub_population > 0
					group by w1.sub_population
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_sub_population as csp
							on w3.sub_population = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_sub_population_category as cmspc
					where cmspc.sub_population_category in
						(
						select cspc.id from nfiesta_results.c_sub_population_category as cspc
						where cspc.sub_population in (select w6.sub_population from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.sub_population as sub_population,
							cspc2.sub_population as atomic_type
					from
							w7
							inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
							inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.sub_population from w8
					)
			,w10 as (
					select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.sub_population
					)
			,w11 as (
					select
							w6.sub_population,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.sub_population = w10.sub_population
					)
			,w12 as	(
					select
							w11.sub_population,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.sub_population,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.sub_population,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
							from	(
									select distinct a1.atomic_type, a1.sub_population
									from
										(
										select w13.atomic_type, w13.sub_population from w13
										union all
										select w5.sub_population as atomic_type, w5.sub_population from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as sub_population,
									null::integer[] as sub_population,
									t.id
							from
									(
									select w11.sub_population, w11.id, w11.atomic_type from w11 union all
									select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
					)
					select
							$3 as res_estimate_type,
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_sub_population
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _sub_population, _estimate_type;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_sub_population_numerator(character varying, integer[], integer[]) is
'The function returns list of available sub populations for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of sub populations.';

grant execute on function nfiesta_results.fn_get_user_options_sub_population_numerator(character varying, integer[], integer[]) to public;
-- </function>
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_get_user_options_area_domain_denominator" schema="nfiesta_results" src="functions/fn_get_user_options_area_domain_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_area_domain_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_area_domain_denominator(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_area_domain_denominator
(
	_jlang character varying,
	_id_group integer[],
	_area_domain integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_area_domain integer[]
)
as
$$
declare
	_lang_suffix			character varying(3);
	_lang_covariate			text;
	_estimate_type			integer;
	_check_records			integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_area_domain_denominator: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select distinct estimate_type from nfiesta_results.t_result_group
	where id in (select unnest(_id_group))
	and web = true
	into _estimate_type;
	-----------------------------------------------------------------
	if _area_domain is null
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.area_domain_denom is null then 0 else trg.area_domain_denom end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as csp
						on w3.area_domain = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmspc
				where cmspc.area_domain_category in
					(
					select cspc.id from nfiesta_results.c_area_domain_category as cspc
					where cspc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.area_domain as area_domain,
						cspc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
						inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)	
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
				)
		select
				w16.atomic_type as res_id,
				(w16.label'||_lang_suffix||')::text AS res_label,
				w16.id as res_id_group,
				array[w16.atomic_type] as res_area_domain
		from
				w16;
		'
		using _id_group;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.area_domain_denom is null then 0 else trg.area_domain_denom end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as csp
						on w3.area_domain = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmspc
				where cmspc.area_domain_category in
					(
					select cspc.id from nfiesta_results.c_area_domain_category as cspc
					where cspc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.area_domain as area_domain,
						cspc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
						inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as area_domain,
								t.id
						from
								(
								select w11.area_domain, w11.id, w11.atomic_type from w11 union all
								select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_area_domain
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _area_domain
		into _check_records;	
		
		if _check_records = 0
		then
			if _area_domain = array[0]
			then
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						_area_domain as _res_area_domain;
			else
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						array_remove(_area_domain,0) as _res_area_domain;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.area_domain] as area_domain,
							t.id
					from
							(
							select w1.area_domain, array_agg(w1.id order by w1.id) as id
							from w1 where w1.area_domain = 0
							group by w1.area_domain
							) as t
					)
			,w3 as	(
					select w1.area_domain, array_agg(w1.id order by w1.id) as id
					from w1 where w1.area_domain > 0
					group by w1.area_domain
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_area_domain as csp
							on w3.area_domain = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_area_domain_category as cmspc
					where cmspc.area_domain_category in
						(
						select cspc.id from nfiesta_results.c_area_domain_category as cspc
						where cspc.area_domain in (select w6.area_domain from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.area_domain as area_domain,
							cspc2.area_domain as atomic_type
					from
							w7
							inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
							inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.area_domain from w8
					)
			,w10 as (
					select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.area_domain
					)
			,w11 as (
					select
							w6.area_domain,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.area_domain = w10.area_domain
					)
			,w12 as	(
					select
							w11.area_domain,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.area_domain,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.area_domain,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
							from	(
									select distinct a1.atomic_type, a1.area_domain
									from
										(
										select w13.atomic_type, w13.area_domain from w13
										union all
										select w5.area_domain as atomic_type, w5.area_domain from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as area_domain,
									null::integer[] as area_domain,
									t.id
							from
									(
									select w11.area_domain, w11.id, w11.atomic_type from w11 union all
									select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
					)
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_area_domain
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _area_domain;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_area_domain_denominator(character varying, integer[], integer[]) is
'The function returns list of available area domains for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of area domains.';

grant execute on function nfiesta_results.fn_get_user_options_area_domain_denominator(character varying, integer[], integer[]) to public;
-- </function>
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_get_user_options_sub_population_denominator" schema="nfiesta_results" src="functions/fn_get_user_options_sub_population_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_sub_population_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_sub_population_denominator(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_sub_population_denominator
(
	_jlang character varying,
	_id_group integer[],
	_sub_population integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_sub_population integer[]
)
as
$$
declare
	_lang_suffix			character varying(3);
	_lang_covariate			text;
	_estimate_type			integer;
	_check_records			integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_sub_population_denominator: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select distinct estimate_type from nfiesta_results.t_result_group
	where id in (select unnest(_id_group))
	and web = true
	into _estimate_type;
	-----------------------------------------------------------------
	if _sub_population is null
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)	
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		select
				w16.atomic_type as res_id,
				(w16.label'||_lang_suffix||')::text AS res_label,
				w16.id as res_id_group,
				array[w16.atomic_type] as res_sub_population
		from
				w16;
		'
		using _id_group;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as sub_population,
								t.id
						from
								(
								select w11.sub_population, w11.id, w11.atomic_type from w11 union all
								select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_sub_population
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _sub_population
		into _check_records;	
		
		if _check_records = 0 -- uz neexistuje dalsi
		then
			if	array_length(_id_group,1) is distinct from 1
			then
				raise exception 'Error 02: fn_get_user_options_sub_population_denominator: Output argument _res_id_group contains more elements then one!';
			end if;
		
			if _sub_population = array[0] -- na vstupu je hned bezrozliseni
			then
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						_sub_population as _res_sub_population;
			else
				-- vyberem doslo na konec
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						array_remove(_sub_population,0) as _res_sub_population;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.sub_population] as sub_population,
							t.id
					from
							(
							select w1.sub_population, array_agg(w1.id order by w1.id) as id
							from w1 where w1.sub_population = 0
							group by w1.sub_population
							) as t
					)
			,w3 as	(
					select w1.sub_population, array_agg(w1.id order by w1.id) as id
					from w1 where w1.sub_population > 0
					group by w1.sub_population
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_sub_population as csp
							on w3.sub_population = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_sub_population_category as cmspc
					where cmspc.sub_population_category in
						(
						select cspc.id from nfiesta_results.c_sub_population_category as cspc
						where cspc.sub_population in (select w6.sub_population from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.sub_population as sub_population,
							cspc2.sub_population as atomic_type
					from
							w7
							inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
							inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.sub_population from w8
					)
			,w10 as (
					select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.sub_population
					)
			,w11 as (
					select
							w6.sub_population,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.sub_population = w10.sub_population
					)
			,w12 as	(
					select
							w11.sub_population,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.sub_population,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.sub_population,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
							from	(
									select distinct a1.atomic_type, a1.sub_population
									from
										(
										select w13.atomic_type, w13.sub_population from w13
										union all
										select w5.sub_population as atomic_type, w5.sub_population from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as sub_population,
									null::integer[] as sub_population,
									t.id
							from
									(
									select w11.sub_population, w11.id, w11.atomic_type from w11 union all
									select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
					)
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_sub_population
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _sub_population;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_sub_population_denominator(character varying, integer[], integer[]) is
'The function returns list of available sub populations for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of sub populations.';

grant execute on function nfiesta_results.fn_get_user_options_sub_population_denominator(character varying, integer[], integer[]) to public;
-- </function>
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_array_compare" schema="nfiesta_results" src="functions/fn_array_compare.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_array_compare
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_array_compare(anyarray, anyarray);

create or replace function nfiesta_results.fn_array_compare(anyarray, anyarray)
 returns boolean
 language plpgsql
as $function$
declare
		_res	boolean;
begin
	_res := (	
			select
				case
			  		when array_dims($1) <> array_dims($2) then 'f'
			 		when array_length($1,1) <> array_length($2,1) then 'f'
			  	else
				    not exists	(
						        select 1
						        from unnest($1) a 
						        full join unnest($2) b on (a=b) 
						        where a is null or b is null
				   				)
		  		end
		  	);
	
	return _res;
end;
$function$
;


comment on function nfiesta_results.fn_array_compare(anyarray, anyarray) is
'The function returns true (the number of elements and the elements are identical) or false (the number of elements and the elements are not identical).';

grant execute on function nfiesta_results.fn_array_compare(anyarray, anyarray) to public;
-- </function>
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_get_language4query" schema="nfiesta_results" src="functions/fn_get_language4query.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_language4query
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_language4query(character varying);

create or replace function nfiesta_results.fn_get_language4query(_jlang character varying)
  returns character varying as
$$
declare
	_clang	integer;
	_res	character varying(3);
begin
	----------------------------------
	if _jlang is null
	then
		raise exception 'Error 01: fn_get_language4query: Input argument _jlang must not be NULL !';
	end if;
	----------------------------------
	select count(id) from nfiesta_results.c_language where label = _jlang
	into _clang;
	----------------------------------
	if _clang = 0
	then
		raise exception 'Error 02: fn_get_language4query: For input argument _jlang = % not exist any record in nfi_results4web.c_language table !', _jlang;
	END IF;
	----------------------------------
	if _clang > 1
	then
		raise exception 'Error 03: fn_get_language4query: For input argument _jlang = % exists more records then one in nfi_results4web.c_language table !', _jlang;
	END IF;
	----------------------------------
	select
		case
			when _jlang = 'cs-CZ' then ('')::character varying
			else ('_' || substring(_jlang,1,2))::character varying
		end
	into
		_res;
	----------------------------------
	if _res is null
	then
		raise exception 'Error 04: fn_get_language4query: Output argument _res is NULL!';
	END IF;	
	----------------------------------
	
	return _res;

end;
$$
  language plpgsql stable
  cost 100;

comment on function nfiesta_results.fn_get_language4query(character varying) is
'The function returns suffix from c_language table for given input language.';

grant execute on function nfiesta_results.fn_get_language4query(character varying) to public;
-- </function>
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- <function name="fn_get_user_options_metadata" schema="nfiesta_results" src="functions/fn_get_user_options_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	res_metadata_type			text,
	res_metadata_label_num		text,
	res_metadata_label_denom	text
)
language plpgsql
stable
as
$$
declare
	_lang_suffix					character varying(3);
	_lang							text;
	_target_variable_num			integer[];
	_target_variable_denom_check	integer;
	_target_variable_denom			integer;

	_cond4topic_num					text;
	_cond4topic_denom				text;

	_res_topic						text;
	_res_estimation_period			text;
	_res_estimation_cell_collection	text;
	_res_indicator_num				text;
	_res_indicator_denom			text;
	_res_state_or_change_num		text;
	_res_state_or_change_denom		text;
	_res_unit_num					text;
	_res_unit_denom					text;
	_res_ldsity_num					text;
	_res_ldsity_denom				text;
	_res_definition_variant_num		text;
	_res_definition_variant_denom	text;
	_res_area_domain_num			text;
	_res_area_domain_denom			text;
	_res_population_num				text;
	_res_population_denom			text;
	_res_unit_of_measure			text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	select count(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom_check;

	if _target_variable_denom_check is distinct from 0 -- target variable denom exists
	then
		if _target_variable_denom_check > 1
		then
			raise exception 'Error: 03: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
		end if;

		select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
		where trg.id in (select unnest(_id_group))
		and trg.web = true
		into _target_variable_denom;		
	else
		_target_variable_denom := null::integer;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	execute
	'
	with
	w1 as	(
			select crt.topic from nfiesta_results.cm_result2topic as crt
			where ' || _cond4topic_num ||'
			and ' || _cond4topic_denom ||'
			)
	select
			label'||_lang_suffix||' AS label
	from
			nfiesta_results.c_topic
	where
			id = (select distinct w1.topic from w1)
	'
	using _target_variable_num[1], _target_variable_denom
	into _res_topic;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	execute
	'
	select
			cep.label'||_lang_suffix||' AS label
	from
			nfiesta_results.c_estimation_period as cep
	where
			cep.id =	(
						select distinct trg.estimation_period
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_period;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	execute
	'
	select
			cecc.label'||_lang_suffix||' AS label
	from
			nfiesta_results.c_estimation_cell_collection as cecc
	where
			cecc.id =	(
						select distinct trg.estimation_cell_collection
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_cell_collection;
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
					((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label
	from
			w1
	'
	using _target_variable_num[1]
	into _res_indicator_num;
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - denominator
	if _target_variable_denom is null
	then
		_res_indicator_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
						((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.label'||_lang_suffix||' AS label
		from
				w1
		'
		using _target_variable_denom
		into _res_indicator_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state or change'')->>''label'' as label,
					((ctv.metadata->''en'')->''state or change'')->>''label'' as label_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label
	from
			w1
	'
	using _target_variable_num[1]
	into _res_state_or_change_num;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - denominator
	if _target_variable_denom is null
	then
		_res_state_or_change_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''state or change'')->>''label'' as label,
						((ctv.metadata->''en'')->''state or change'')->>''label'' as label_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.label'||_lang_suffix||' AS label
		from
				w1
		'
		using _target_variable_denom
		into _res_state_or_change_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label
	from
			w1
	'
	using _target_variable_num[1]
	into _res_unit_num;
	-----------------------------------------------------------------
	-- METADATA OF UNIT - denominator
	if _target_variable_denom is null
	then
		_res_unit_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
						((ctv.metadata->''en'')->''unit'')->>''label'' as label_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.label'||_lang_suffix||' AS label
		from
				w1
		'
		using _target_variable_denom
		into _res_unit_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA LDSITY - numerator
	execute
	'
	with			
	w1 as	(
			select
					t.id as target_variable,
					t.label'||_lang_suffix||' AS label
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as label,
							((ctv.metadata->''en'')->''local densities'') as label_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = $1
					) as t
			)									
	,w2 as	(
			select
					w1.target_variable,
					json_array_elements(w1.label) as label
			from
					w1
			)								
	,w3 as	(
			select
					row_number() over () as new_id,
					w2.target_variable,
					w2.label->>''label'' as label
			from w2
			)							
	,w4 as	(
			select
					w3.target_variable,
					array_to_string(array_agg(w3.label order by w3.new_id),'' | '') as label4user
			from
					w3 group by w3.target_variable
			)
	select w4.label4user from w4
	'
	using _target_variable_num[1]
	into _res_ldsity_num;
	-----------------------------------------------------------------
	-- METADATA LDSITY - denominator
	if _target_variable_denom is null
	then
		_res_ldsity_denom := null::text;
	else
		execute
		'
		with			
		w1 as	(
				select
						t.id as target_variable,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = $1
						) as t
				)									
		,w2 as	(
				select
						w1.target_variable,
						json_array_elements(w1.label) as label
				from
						w1
				)								
		,w3 as	(
				select
						row_number() over () as new_id,
						w2.target_variable,
						w2.label->>''label'' as label
				from w2
				)							
		,w4 as	(
				select
						w3.target_variable,
						array_to_string(array_agg(w3.label order by w3.new_id),'' | '') as label4user
				from
						w3 group by w3.target_variable
				)
		select w4.label4user from w4
		'
		using _target_variable_denom
		into _res_ldsity_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA DEFINITION VARIANT - numerator
	execute
	'
	with			
	w1 as	(
			select
					t.id as target_variable,
					t.label'||_lang_suffix||' AS label
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as label,
							((ctv.metadata->''en'')->''local densities'') as label_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = $1
					) as t
			)									
	,w2 as	(
			select
					w1.target_variable,
					json_array_elements(w1.label) as label
			from
					w1
			)								
	,w3 as	(
			select
					row_number() over () as new_id,
					w2.target_variable,
					coalesce((w2.label->''definition variant'')->>''label'',''null''::text) as label
			from w2
			)							
	,w4 as	(
			select
					w3.target_variable,
					array_to_string(array_agg(w3.label order by w3.new_id),'' | '') as label4user
			from
					w3 group by w3.target_variable
			)
	select w4.label4user from w4
	'
	using _target_variable_num[1]
	into _res_definition_variant_num;
	-----------------------------------------------------------------
	-- METADATA DEFINITION VARIANT - denominator
	if _target_variable_denom is null
	then
		_res_definition_variant_denom := null::text;
	else
		execute
		'
		with			
		w1 as	(
				select
						t.id as target_variable,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = $1
						) as t
				)									
		,w2 as	(
				select
						w1.target_variable,
						json_array_elements(w1.label) as label
				from
						w1
				)								
		,w3 as	(
				select
						row_number() over () as new_id,
						w2.target_variable,
						coalesce((w2.label->''definition variant'')->>''label'',''null''::text) as label
				from w2
				)							
		,w4 as	(
				select
						w3.target_variable,
						array_to_string(array_agg(w3.label order by w3.new_id),'' | '') as label4user
				from
						w3 group by w3.target_variable
				)
		select w4.label4user from w4
		'
		using _target_variable_denom
		into _res_definition_variant_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA AREA DOMAIN - numerator
	execute
	'
	with			
	w1 as	(
			select
					t.id as target_variable,
					t.label'||_lang_suffix||' AS label
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as label,
							((ctv.metadata->''en'')->''local densities'') as label_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = $1
					) as t
			)									
	,w2 as	(
			select
					w1.target_variable,
					json_array_elements(w1.label) as label
			from
					w1
			)
	,w3 as	(
			select
					row_number() over () as new_id,
					w2.target_variable,
					replace(
					replace(
					replace(
					replace(
					coalesce((w2.label->''area domain'')->>''label'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as label
			from w2
			)							
	,w4 as	(
			select
					w3.target_variable,
					array_to_string(array_agg(w3.label order by w3.new_id),'' | '') as label4user
			from
					w3 group by w3.target_variable
			)
	select w4.label4user from w4
	'
	using _target_variable_num[1]
	into _res_area_domain_num;
	-----------------------------------------------------------------
	-- METADATA AREA DOMAIN - denominator
	if _target_variable_denom is null
	then
		_res_area_domain_denom := null::text;
	else
		execute
		'
		with			
		w1 as	(
				select
						t.id as target_variable,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = $1
						) as t
				)									
		,w2 as	(
				select
						w1.target_variable,
						json_array_elements(w1.label) as label
				from
						w1
				)
		,w3 as	(
				select
						row_number() over () as new_id,
						w2.target_variable,
						replace(
						replace(
						replace(
						replace(
						coalesce((w2.label->''area domain'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as label
				from w2
				)							
		,w4 as	(
				select
						w3.target_variable,
						array_to_string(array_agg(w3.label order by w3.new_id),'' | '') as label4user
				from
						w3 group by w3.target_variable
				)
		select w4.label4user from w4
		'
		using _target_variable_denom
		into _res_area_domain_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA POPULATION - numerator
	execute
	'
	with			
	w1 as	(
			select
					t.id as target_variable,
					t.label'||_lang_suffix||' AS label
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as label,
							((ctv.metadata->''en'')->''local densities'') as label_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = $1
					) as t
			)									
	,w2 as	(
			select
					w1.target_variable,
					json_array_elements(w1.label) as label
			from
					w1
			)
	,w3 as	(
			select
					row_number() over () as new_id,
					w2.target_variable,
					replace(
					replace(
					replace(
					replace(
					coalesce((w2.label->''population'')->>''label'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as label
			from w2
			)							
	,w4 as	(
			select
					w3.target_variable,
					array_to_string(array_agg(w3.label order by w3.new_id),'' | '') as label4user
			from
					w3 group by w3.target_variable
			)
	select w4.label4user from w4
	'
	using _target_variable_num[1]
	into _res_population_num;
	-----------------------------------------------------------------
	-- METADATA POPULATION - denominator
	if _target_variable_denom is null
	then
		_res_population_denom := null::text;
	else
		execute
		'
		with			
		w1 as	(
				select
						t.id as target_variable,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = $1
						) as t
				)									
		,w2 as	(
				select
						w1.target_variable,
						json_array_elements(w1.label) as label
				from
						w1
				)
		,w3 as	(
				select
						row_number() over () as new_id,
						w2.target_variable,
						replace(
						replace(
						replace(
						replace(
						coalesce((w2.label->''population'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as label
				from w2
				)							
		,w4 as	(
				select
						w3.target_variable,
						array_to_string(array_agg(w3.label order by w3.new_id),'' | '') as label4user
				from
						w3 group by w3.target_variable
				)
		select w4.label4user from w4
		'
		using _target_variable_denom
		into _res_population_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASURE
	if _target_variable_denom is null
	then
		_res_unit_of_measure := _res_unit_num;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''label'' as unit_num,
						((w2.metadata->''cs'')->''unit'')->>''label'' as unit_denom,
						((w1.metadata->''en'')->''unit'')->>''label'' as unit_num_en,
						((w2.metadata->''en'')->''unit'')->>''label'' as unit_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_num'||_lang_suffix||' AS unit_num,
						w3.unit_denom'||_lang_suffix||' AS unit_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_num = w4.unit_denom) then ''%''
							else concat(w4.unit_num,'' / '',w4.unit_denom)
						end as label4user
				from
						w4
				)
		select w5.label4user from w5
		'
		using _target_variable_num[1], _target_variable_denom
		into _res_unit_of_measure;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _lang_suffix = ''
	then
		return query
		select
				'Tematický okruh:' as res_metadata_type,
				_res_topic as res_metadata_label_num,
				null::text as res_metadata_label_denom
		union all
		select
				'Období:' as res_metadata_type,
				_res_estimation_period as res_metadata_label_num,
				null::text as res_metadata_label_denom
		union all
		select
				'Geografické členění:' as res_metadata_type,
				_res_estimation_cell_collection as res_metadata_label_num,
				null::text as res_metadata_label_denom
		union all
		select
				'Indikátor:' as res_metadata_type,
				_res_indicator_num as res_metadata_label_num,
				_res_indicator_denom as res_metadata_label_denom
		union all
		select
				'Stav nebo změna:' as res_metadata_type,
				_res_state_or_change_num as res_metadata_label_num,
				_res_state_or_change_denom as res_metadata_label_denom
		union all
		select
				'Jednotka příspěvku:' as res_metadata_type,
				_res_unit_num as res_metadata_label_num,
				_res_unit_denom as res_metadata_label_denom
		union all
		select
				'Příspěvek lokální hustoty:' as res_metadata_type,
				_res_ldsity_num as res_metadata_label_num,
				_res_ldsity_denom as res_metadata_label_denom
		union all
		select
				'Varianta:' as res_metadata_type,
				_res_definition_variant_num as res_metadata_label_num,
				_res_definition_variant_denom as res_metadata_label_denom
		union all
		select
				'Plošná doména:' as res_metadata_type,
				_res_area_domain_num as res_metadata_label_num,
				_res_area_domain_denom as res_metadata_label_denom
		union all
		select
				'Populace:' as res_metadata_type,
				_res_population_num as res_metadata_label_num,
				_res_population_denom as res_metadata_label_denom
		union all
		select
				'Jednotka výstupu:' as res_metadata_type,
				_res_unit_of_measure as res_metadata_label_num,
				null::text as res_metadata_label_denom;
	else
		return query
		select
				'Topic:' as res_metadata_type,
				_res_topic as res_metadata_label_num,
				null::text as res_metadata_label_denom
		union all
		select
				'Period:' as res_metadata_type,
				_res_estimation_period as res_metadata_label_num,
				null::text as res_metadata_label_denom
		union all
		select
				'Geographic region:' as res_metadata_type,
				_res_estimation_cell_collection as res_metadata_label_num,
				null::text as res_metadata_label_denom
		union all
		select
				'Indicator:' as res_metadata_type,
				_res_indicator_num as res_metadata_label_num,
				_res_indicator_denom as res_metadata_label_denom
		union all
		select
				'State or change:' as res_metadata_type,
				_res_state_or_change_num as res_metadata_label_num,
				_res_state_or_change_denom as res_metadata_label_denom
		union all
		select
				'Unit:' as res_metadata_type,
				_res_unit_num as res_metadata_label_num,
				_res_unit_denom as res_metadata_label_denom
		union all
		select
				'Local densities:' as res_metadata_type,
				_res_ldsity_num as res_metadata_label_num,
				_res_ldsity_denom as res_metadata_label_denom
		union all
		select
				'Definition_variant:' as res_metadata_type,
				_res_definition_variant_num as res_metadata_label_num,
				_res_definition_variant_denom as res_metadata_label_denom
		union all
		select
				'Area domain:' as res_metadata_type,
				_res_area_domain_num as res_metadata_label_num,
				_res_area_domain_denom as res_metadata_label_denom
		union all
		select
				'Population:' as res_metadata_type,
				_res_population_num as res_metadata_label_num,
				_res_population_denom as res_metadata_label_denom
		union all
		select
				'Unit of measurement:' as res_metadata_type,
				_res_unit_of_measure as res_metadata_label_num,
				null::text as res_metadata_label_denom;
	end if;
	
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_metadata(character varying, integer[]) is
'The function returns options metadata for given input IDs (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_options_metadata(character varying, integer[]) to public;
-- </function>
-------------------------------------------------------------------------------


/*
create or replace function nfiesta_results.fn_get_user_query(_id_group integer)
returns table
(
	estimation_cell,
	variable,
	point_estimate,
	standard_deviation,
	variation_coeficient,
	sample_size,
	min_sample_size,
	interval_estimation
)
as
*/
--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------




-- <function name="fn_get_user_metadata" schema="nfiesta_results" src="functions/fn_get_user_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	res_metadata_type			text,
	res_metadata_description	text
)
language plpgsql
stable
as
$$
declare
	_lang_suffix					character varying(3);
	_lang							text;
	_estimate_type					integer;
	_target_variable_num				integer;
	_target_variable_denom			integer;
	_estimation_period				integer;
	_panel_refyearset_group			integer;
	_phase_estimate_type_num		integer;
	_phase_estimate_type_denom		integer;
	_estimation_cell_collection		integer;
	_area_domain_num				integer;
	_area_domain_denom				integer;
	_sub_population_num				integer;
	_sub_population_denom			integer;
	_cond4topic_num					text;
	_cond4topic_denom				text;
	_res_topic						text;
	_res_estimation_period			text;
	_res_estimation_cell_collection	text;
	_res_indicator_num				text;
	_res_indicator_denom			text;
	_res_state_or_change_num		text;
	_res_state_or_change_denom		text;
	_res_unit_num					text;
	_res_unit_denom					text;
	_res_ldsity_num					text;
	_res_ldsity_denom				text;
	_res_definition_variant_num		text;
	_res_definition_variant_denom	text;
	_res_area_domain_num			text;
	_res_area_domain_denom			text;
	_res_population_num				text;
	_res_population_denom			text;
	_res_unit_of_measure			text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select
			trg.estimate_type,
			trg.target_variable,
			trg.target_variable_denom,
			trg.estimation_period,
			trg.panel_refyearset_group,
			trg.phase_estimate_type,
			trg.phase_estimate_type_denom,
			trg.estimation_cell_collection,
			trg.area_domain,
			trg.area_domain_denom,
			trg.sub_population,
			trg.sub_population_denom
	from
			nfiesta_results.t_result_group as trg
	where
			trg.id = _id_group[1]
	into
			_estimate_type,
			_target_variable_num,
			_target_variable_denom,
			_estimation_period,
			_panel_refyearset_group,
			_phase_estimate_type_num,
			_phase_estimate_type_denom,
			_estimation_cell_collection,
			_area_domain_num,
			_area_domain_denom,
			_sub_population_num,
			_sub_population_denom;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	execute
	'
	with
	w1 as	(
			select crt.topic from nfiesta_results.cm_result2topic as crt
			where ' || _cond4topic_num ||'
			and ' || _cond4topic_denom ||'
			)
	select
			description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_topic
	where
			id = (select distinct w1.topic from w1)
	'
	using _target_variable_num, _target_variable_denom
	into _res_topic;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	execute
	'
	select
			cep.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_period as cep
	where
			cep.id = $1
	'
	using _estimation_period
	into _res_estimation_period;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	execute
	'
	select
			cecc.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_cell_collection as cecc
	where
			cecc.id = $1
	'
	using _estimation_cell_collection
	into _res_estimation_cell_collection;
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
					((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num
	into _res_indicator_num;
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - denominator
	if _target_variable_denom is null
	then
		_res_indicator_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
						((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.description'||_lang_suffix||' AS description
		from
				w1
		'
		using _target_variable_denom
		into _res_indicator_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state or change'')->>''description'' as description,
					((ctv.metadata->''en'')->''state or change'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num
	into _res_state_or_change_num;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - denominator
	if _target_variable_denom is null
	then
		_res_state_or_change_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''state or change'')->>''description'' as description,
						((ctv.metadata->''en'')->''state or change'')->>''description'' as description_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.description'||_lang_suffix||' AS description
		from
				w1
		'
		using _target_variable_denom
		into _res_state_or_change_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num
	into _res_unit_num;
	-----------------------------------------------------------------
	-- METADATA OF UNIT - denominator
	if _target_variable_denom is null
	then
		_res_unit_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
						((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.description'||_lang_suffix||' AS description
		from
				w1
		'
		using _target_variable_denom
		into _res_unit_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA LDSITY - numerator
	execute
	'
	with			
	w1 as	(
			select
					t.id as target_variable,
					t.description'||_lang_suffix||' AS description
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as description,
							((ctv.metadata->''en'')->''local densities'') as description_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = $1
					) as t
			)									
	,w2 as	(
			select
					w1.target_variable,
					json_array_elements(w1.description) as description
			from
					w1
			)								
	,w3 as	(
			select
					row_number() over () as new_id,
					w2.target_variable,
					w2.description->>''description'' as description
			from w2
			)							
	,w4 as	(
			select
					w3.target_variable,
					array_to_string(array_agg(w3.description order by w3.new_id),'' | '') as description4user
			from
					w3 group by w3.target_variable
			)
	select w4.description4user from w4
	'
	using _target_variable_num
	into _res_ldsity_num;
	-----------------------------------------------------------------
	-- METADATA LDSITY - denominator
	if _target_variable_denom is null
	then
		_res_ldsity_denom := null::text;
	else
		execute
		'
		with			
		w1 as	(
				select
						t.id as target_variable,
						t.description'||_lang_suffix||' AS description
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as description,
								((ctv.metadata->''en'')->''local densities'') as description_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = $1
						) as t
				)									
		,w2 as	(
				select
						w1.target_variable,
						json_array_elements(w1.description) as description
				from
						w1
				)								
		,w3 as	(
				select
						row_number() over () as new_id,
						w2.target_variable,
						w2.description->>''description'' as description
				from w2
				)							
		,w4 as	(
				select
						w3.target_variable,
						array_to_string(array_agg(w3.description order by w3.new_id),'' | '') as description4user
				from
						w3 group by w3.target_variable
				)
		select w4.description4user from w4
		'
		using _target_variable_denom
		into _res_ldsity_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA DEFINITION VARIANT - numerator
	execute
	'
	with			
	w1 as	(
			select
					t.id as target_variable,
					t.description'||_lang_suffix||' AS description
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as description,
							((ctv.metadata->''en'')->''local densities'') as description_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = $1
					) as t
			)									
	,w2 as	(
			select
					w1.target_variable,
					json_array_elements(w1.description) as description
			from
					w1
			)								
	,w3 as	(
			select
					row_number() over () as new_id,
					w2.target_variable,
					coalesce((w2.description->''definition variant'')->>''description'',''null''::text) as description
			from w2
			)							
	,w4 as	(
			select
					w3.target_variable,
					array_to_string(array_agg(w3.description order by w3.new_id),'' | '') as description4user
			from
					w3 group by w3.target_variable
			)
	select w4.description4user from w4
	'
	using _target_variable_num
	into _res_definition_variant_num;
	-----------------------------------------------------------------
	-- METADATA DEFINITION VARIANT - denominator
	if _target_variable_denom is null
	then
		_res_definition_variant_denom := null::text;
	else
		execute
		'
		with			
		w1 as	(
				select
						t.id as target_variable,
						t.description'||_lang_suffix||' AS description
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as description,
								((ctv.metadata->''en'')->''local densities'') as description_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = $1
						) as t
				)									
		,w2 as	(
				select
						w1.target_variable,
						json_array_elements(w1.description) as description
				from
						w1
				)								
		,w3 as	(
				select
						row_number() over () as new_id,
						w2.target_variable,
						coalesce((w2.description->''definition variant'')->>''description'',''null''::text) as description
				from w2
				)							
		,w4 as	(
				select
						w3.target_variable,
						array_to_string(array_agg(w3.description order by w3.new_id),'' | '') as description4user
				from
						w3 group by w3.target_variable
				)
		select w4.description4user from w4
		'
		using _target_variable_denom
		into _res_definition_variant_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA AREA DOMAIN - numerator
	execute
	'
	with			
	w1 as	(
			select
					t.id as target_variable,
					t.description'||_lang_suffix||' AS description
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as description,
							((ctv.metadata->''en'')->''local densities'') as description_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = $1
					) as t
			)									
	,w2 as	(
			select
					w1.target_variable,
					json_array_elements(w1.description) as description
			from
					w1
			)
	,w3 as	(
			select
					row_number() over () as new_id,
					w2.target_variable,
					replace(
					replace(
					replace(
					replace(
					coalesce((w2.description->''area domain'')->>''description'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as description
			from w2
			)							
	,w4 as	(
			select
					w3.target_variable,
					array_to_string(array_agg(w3.description order by w3.new_id),'' | '') as description4user
			from
					w3 group by w3.target_variable
			)
	select w4.description4user from w4
	'
	using _target_variable_num
	into _res_area_domain_num;
	-----------------------------------------------------------------
	-- METADATA AREA DOMAIN - denominator
	if _target_variable_denom is null
	then
		_res_area_domain_denom := null::text;
	else
		execute
		'
		with			
		w1 as	(
				select
						t.id as target_variable,
						t.description'||_lang_suffix||' AS description
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as description,
								((ctv.metadata->''en'')->''local densities'') as description_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = $1
						) as t
				)									
		,w2 as	(
				select
						w1.target_variable,
						json_array_elements(w1.description) as description
				from
						w1
				)
		,w3 as	(
				select
						row_number() over () as new_id,
						w2.target_variable,
						replace(
						replace(
						replace(
						replace(
						coalesce((w2.description->''area domain'')->>''description'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as description
				from w2
				)							
		,w4 as	(
				select
						w3.target_variable,
						array_to_string(array_agg(w3.description order by w3.new_id),'' | '') as description4user
				from
						w3 group by w3.target_variable
				)
		select w4.description4user from w4
		'
		using _target_variable_denom
		into _res_area_domain_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA POPULATION - numerator
	execute
	'
	with			
	w1 as	(
			select
					t.id as target_variable,
					t.description'||_lang_suffix||' AS description
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as description,
							((ctv.metadata->''en'')->''local densities'') as description_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = $1
					) as t
			)									
	,w2 as	(
			select
					w1.target_variable,
					json_array_elements(w1.description) as description
			from
					w1
			)
	,w3 as	(
			select
					row_number() over () as new_id,
					w2.target_variable,
					replace(
					replace(
					replace(
					replace(
					coalesce((w2.description->''population'')->>''description'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as description
			from w2
			)							
	,w4 as	(
			select
					w3.target_variable,
					array_to_string(array_agg(w3.description order by w3.new_id),'' | '') as description4user
			from
					w3 group by w3.target_variable
			)
	select w4.description4user from w4
	'
	using _target_variable_num
	into _res_population_num;
	-----------------------------------------------------------------
	-- METADATA POPULATION - denominator
	if _target_variable_denom is null
	then
		_res_population_denom := null::text;
	else
		execute
		'
		with			
		w1 as	(
				select
						t.id as target_variable,
						t.description'||_lang_suffix||' AS description
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as description,
								((ctv.metadata->''en'')->''local densities'') as description_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = $1
						) as t
				)									
		,w2 as	(
				select
						w1.target_variable,
						json_array_elements(w1.description) as description
				from
						w1
				)
		,w3 as	(
				select
						row_number() over () as new_id,
						w2.target_variable,
						replace(
						replace(
						replace(
						replace(
						coalesce((w2.description->''population'')->>''description'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as description
				from w2
				)							
		,w4 as	(
				select
						w3.target_variable,
						array_to_string(array_agg(w3.description order by w3.new_id),'' | '') as description4user
				from
						w3 group by w3.target_variable
				)
		select w4.description4user from w4
		'
		using _target_variable_denom
		into _res_population_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASURE
	if _target_variable_denom is null
	then
		_res_unit_of_measure := _res_unit_num;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''label'' as unit_num,
						((w2.metadata->''cs'')->''unit'')->>''label'' as unit_denom,
						((w1.metadata->''en'')->''unit'')->>''label'' as unit_num_en,
						((w2.metadata->''en'')->''unit'')->>''label'' as unit_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_num'||_lang_suffix||' AS unit_num,
						w3.unit_denom'||_lang_suffix||' AS unit_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_num = w4.unit_denom) then ''%''
							else concat(w4.unit_num,'' / '',w4.unit_denom)
						end as label4user
				from
						w4
				)
		select w5.label4user from w5
		'
		using _target_variable_num, _target_variable_denom
		into _res_unit_of_measure;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _lang_suffix = ''
	then
		if _estimate_type = 1
		then
			return query
			select
					'Tematický okruh:' as res_metadata_type,
					_res_topic as res_metadata_description
			union all
			select
					'Období:' as res_metadata_type,
					_res_estimation_period as res_metadata_description
			union all
			select
					'Geografické členění:' as res_metadata_type,
					_res_estimation_cell_collection as res_metadata_description
			union all
			select
					'Indikátor:' as res_metadata_type,
					_res_indicator_num as res_metadata_description
			union all
			select
					'Stav nebo změna:' as res_metadata_type,
					_res_state_or_change_num as res_metadata_description
			union all
			select
					'Jednotka příspěvku:' as res_metadata_type,
					_res_unit_num as res_metadata_description
			union all
			select
					'Příspěvek lokální hustoty:' as res_metadata_type,
					_res_ldsity_num as res_metadata_description
			union all
			select
					'Varianta:' as res_metadata_type,
					_res_definition_variant_num as res_metadata_description
			union all
			select
					'Plošná doména:' as res_metadata_type,
					_res_area_domain_num as res_metadata_description
			union all
			select
					'Populace:' as res_metadata_type,
					_res_population_num as res_metadata_description
			union all
			select
					'Jednotka výstupu:' as res_metadata_type,
					_res_unit_of_measure as res_metadata_description;
		else
			return query
			select
					'Tematický okruh:' as res_metadata_type,
					_res_topic as res_metadata_description
			union all
			select
					'Období:' as res_metadata_type,
					_res_estimation_period as res_metadata_description
			union all
			select
					'Geografické členění:' as res_metadata_type,
					_res_estimation_cell_collection as res_metadata_description
			union all
			select
					'Jednotka výstupu:' as res_metadata_type,
					_res_unit_of_measure as res_metadata_description			
			---------------------------------------------------------
			union all
			select
					null::text as res_metadata_type,
					null::text as res_metadata_description
			---------------------------------------------------------
			union all
			select
					'Informace čitatele' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					'Indikátor:' as res_metadata_type,
					_res_indicator_num as res_metadata_description
			union all
			select
					'Stav nebo změna:' as res_metadata_type,
					_res_state_or_change_num as res_metadata_description
			union all
			select
					'Jednotka příspěvku:' as res_metadata_type,
					_res_unit_num as res_metadata_description
			union all
			select
					'Příspěvek lokální hustoty:' as res_metadata_type,
					_res_ldsity_num as res_metadata_description
			union all
			select
					'Varianta:' as res_metadata_type,
					_res_definition_variant_num as res_metadata_description
			union all
			select
					'Plošná doména:' as res_metadata_type,
					_res_area_domain_num as res_metadata_description
			union all
			select
					'Populace:' as res_metadata_type,
					_res_population_num as res_metadata_description
			---------------------------------------------------------
			union all
			select
					null::text as res_metadata_type,
					null::text as res_metadata_description		
			---------------------------------------------------------
			union all
			select
					'Informace jmenovatele' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					'Indikátor:' as res_metadata_type,
					_res_indicator_denom as res_metadata_description
			union all
			select
					'Stav nebo změna:' as res_metadata_type,
					_res_state_or_change_denom as res_metadata_description
			union all
			select
					'Jednotka příspěvku:' as res_metadata_type,
					_res_unit_denom as res_metadata_description
			union all
			select
					'Příspěvek lokální hustoty:' as res_metadata_type,
					_res_ldsity_denom as res_metadata_description
			union all
			select
					'Varianta:' as res_metadata_type,
					_res_definition_variant_denom as res_metadata_description
			union all
			select
					'Plošná doména:' as res_metadata_type,
					_res_area_domain_denom as res_metadata_description
			union all
			select
					'Populace:' as res_metadata_type,
					_res_population_denom as res_metadata_description;		
		end if;
	else
		if _estimate_type = 1
		then
			return query
			select
					'Topic:' as res_metadata_type,
					_res_topic as res_metadata_description
			union all
			select
					'Period:' as res_metadata_type,
					_res_estimation_period as res_metadata_description
			union all
			select
					'Geographic region:' as res_metadata_type,
					_res_estimation_cell_collection as res_metadata_description
			union all
			select
					'Indicator:' as res_metadata_type,
					_res_indicator_num as res_metadata_description
			union all
			select
					'State or change:' as res_metadata_type,
					_res_state_or_change_num as res_metadata_description
			union all
			select
					'Unit:' as res_metadata_type,
					_res_unit_num as res_metadata_description
			union all
			select
					'Local densities:' as res_metadata_type,
					_res_ldsity_num as res_metadata_description
			union all
			select
					'Definition variant:' as res_metadata_type,
					_res_definition_variant_num as res_metadata_description
			union all
			select
					'Area domain:' as res_metadata_type,
					_res_area_domain_num as res_metadata_description
			union all
			select
					'Population:' as res_metadata_type,
					_res_population_num as res_metadata_description
			union all
			select
					'Unit of measurement:' as res_metadata_type,
					_res_unit_of_measure as res_metadata_description;
		else
			return query
			select
					'Topic:' as res_metadata_type,
					_res_topic as res_metadata_description
			union all
			select
					'Period:' as res_metadata_type,
					_res_estimation_period as res_metadata_description
			union all
			select
					'Geographic region:' as res_metadata_type,
					_res_estimation_cell_collection as res_metadata_description
			union all
			select
					'Unit of measurement:' as res_metadata_type,
					_res_unit_of_measure as res_metadata_description
			---------------------------------------------------------
			union all
			select
					null::text as res_metadata_type,
					null::text as res_metadata_description
			---------------------------------------------------------
			union all
			select
					'Numerator informations' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					'Indicator:' as res_metadata_type,
					_res_indicator_num as res_metadata_description
			union all
			select
					'State or change:' as res_metadata_type,
					_res_state_or_change_num as res_metadata_description
			union all
			select
					'Unit:' as res_metadata_type,
					_res_unit_num as res_metadata_description
			union all
			select
					'Local densities:' as res_metadata_type,
					_res_ldsity_num as res_metadata_description
			union all
			select
					'Definition variant:' as res_metadata_type,
					_res_definition_variant_num as res_metadata_description
			union all
			select
					'Area domain:' as res_metadata_type,
					_res_area_domain_num as res_metadata_description
			union all
			select
					'Population:' as res_metadata_type,
					_res_population_num as res_metadata_description
			---------------------------------------------------------
			union all
			select
					null::text as res_metadata_type,
					null::text as res_metadata_description
			---------------------------------------------------------
			union all
			select
					'Denominator informations' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					'Indicator:' as res_metadata_type,
					_res_indicator_denom as res_metadata_description
			union all
			select
					'State or change:' as res_metadata_type,
					_res_state_or_change_denom as res_metadata_description
			union all
			select
					'Unit:' as res_metadata_type,
					_res_unit_denom as res_metadata_description
			union all
			select
					'Local densities:' as res_metadata_type,
					_res_ldsity_denom as res_metadata_description
			union all
			select
					'Definition variant:' as res_metadata_type,
					_res_definition_variant_denom as res_metadata_description
			union all
			select
					'Area domain:' as res_metadata_type,
					_res_area_domain_denom as res_metadata_description
			union all
			select
					'Population:' as res_metadata_type,
					_res_population_denom as res_metadata_description;	
		end if;				
	end if;
	
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) is
'The function returns metadata for given input ID (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) to public;
-- </function>

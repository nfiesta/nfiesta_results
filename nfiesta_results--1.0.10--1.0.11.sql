--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------


alter table nfiesta_results.c_estimate_type drop constraint ukey__c_estimate_type__label_en;
alter table nfiesta_results.c_estimate_type drop column label_en;
alter table nfiesta_results.c_estimate_type drop column description_en;

alter table nfiesta_results.c_phase_estimate_type drop constraint ukey__c_phase_estimate_type__label_en;
alter table nfiesta_results.c_phase_estimate_type drop column label_en;
alter table nfiesta_results.c_phase_estimate_type drop column description_en;

alter table nfiesta_results.c_gui_header
add constraint ukey__c_gui_header__header__label_en unique(header, label_en);
comment on constraint ukey__c_gui_header__header__label_en on nfiesta_results.c_gui_header is 'Unique key on columns header and label_en.';

alter table nfiesta_results.c_language
add constraint ukey__c_language__label unique(label);
comment on constraint ukey__c_language__label on nfiesta_results.c_language is 'Unique key on column label.';

alter table nfiesta_results.cm_result2topic
add constraint ukey__cm_result2topic__tv_denom_topic unique(target_variable, denominator, topic);
comment on constraint ukey__cm_result2topic__tv_denom_topic on nfiesta_results.cm_result2topic is 'Unique key on columns target_variable, denominator and topic.';


-- <function name="fn_get_user_metadata" schema="nfiesta_results" src="functions/fn_get_user_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang_suffix								character varying(3);
	_lang										text;
	_target_variable_num						integer[];
	_target_variable_denom_check				integer;
	_target_variable_denom						integer;
	_cond4topic_num								text;
	_cond4topic_denom							text;
	_res_topic_label							text;
	_res_topic_description						text;
	_res_estimation_period_label				text;
	_res_estimation_period_description			text;
	_res_estimation_cell_collection_label		text;
	_res_estimation_cell_collection_description	text;
	_res_estimate_type_label					text;
	_res_estimate_type_description				text;
	_res_unit_num_label							text;
	_res_unit_num_description					text;
	_res_unit_of_measurement_label				text;
	_res_unit_of_measurement_description		text;
	_res										json;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	select count(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom_check;

	if _target_variable_denom_check is distinct from 0 -- target variable denom exists
	then
		if _target_variable_denom_check > 1
		then
			raise exception 'Error: 03: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
		end if;

		select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
		where trg.id in (select unnest(_id_group))
		and trg.web = true
		into _target_variable_denom;		
	else
		_target_variable_denom := null::integer;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	execute
	'
	with
	w1 as	(
			select crt.topic from nfiesta_results.cm_result2topic as crt
			where ' || _cond4topic_num ||'
			and ' || _cond4topic_denom ||'
			)
	,w2 as	(
			select
					label'||_lang_suffix||' AS label,
					description'||_lang_suffix||' AS description
			from
					nfiesta_results.c_topic
			where
					id in (select distinct w1.topic from w1)
			)
	,w3 as	(
			select
					array_agg(w2.label order by w2.description) as label,
					array_agg(w2.description order by w2.description) as description
			from
					w2
			)
	select
			array_to_string(w3.label, ''; ''),
			array_to_string(w3.description, ''; '')
	from
			w3
	'
	using _target_variable_num[1], _target_variable_denom
	into _res_topic_label, _res_topic_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	execute
	'
	select
			cep.label'||_lang_suffix||' AS label,
			cep.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_period as cep
	where
			cep.id =	(
						select distinct trg.estimation_period
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_period_label, _res_estimation_period_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	execute
	'
	select
			cecc.label'||_lang_suffix||' AS label,
			cecc.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_cell_collection as cecc
	where
			cecc.id =	(
						select distinct trg.estimation_cell_collection
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_cell_collection_label, _res_estimation_cell_collection_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATE TYPE
	if _target_variable_denom is null
	then
		execute
		'
		select
				cet.label,
				cet.description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 1
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;
	else
		execute
		'
		select
				cet.label,
				cet.description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 2
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num[1]
	into _res_unit_num_label, _res_unit_num_description;		
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASUREMENT
	if _target_variable_denom is null
	then
		_res_unit_of_measurement_label := _res_unit_num_label;
		_res_unit_of_measurement_description := _res_unit_num_description;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''label'' as unit_label_num,
						((w2.metadata->''cs'')->''unit'')->>''label'' as unit_label_denom,
						((w1.metadata->''en'')->''unit'')->>''label'' as unit_label_num_en,
						((w2.metadata->''en'')->''unit'')->>''label'' as unit_label_denom_en,						
						((w1.metadata->''cs'')->''unit'')->>''description'' as unit_description_num,
						((w2.metadata->''cs'')->''unit'')->>''description'' as unit_description_denom,
						((w1.metadata->''en'')->''unit'')->>''description'' as unit_description_num_en,
						((w2.metadata->''en'')->''unit'')->>''description'' as unit_description_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_label_num'||_lang_suffix||' AS unit_label_num,
						w3.unit_label_denom'||_lang_suffix||' AS unit_label_denom,						
						w3.unit_description_num'||_lang_suffix||' AS unit_description_num,
						w3.unit_description_denom'||_lang_suffix||' AS unit_description_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_label_num = w4.unit_label_denom) then ''%''
							else concat(w4.unit_label_num,'' / '',w4.unit_label_denom)
						end as label4user,						
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_description_num = w4.unit_description_denom) then ''%''
							else concat(w4.unit_description_num,'' / '',w4.unit_description_denom)
						end as description4user
				from
						w4
				)
		select
				w5.label4user,
				w5.description4user
		from
				w5
		'
		using _target_variable_num[1], _target_variable_denom
		into _res_unit_of_measurement_label, _res_unit_of_measurement_description;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _target_variable_denom is null
	then
		-- basic metadata + numerator metadata
		if _lang_suffix = ''
		then
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('cs-CZ',_id_group))
			)
			into _res;
		else
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('en-GB',_id_group))
			)
			into _res;		
		end if;
	else
		-- basic metadata + numerator metadata + denominator metadata
		if _lang_suffix = ''
		then
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('cs-CZ',_id_group)),
				'denominator',(select * from nfiesta_results.fn_get_user_metadata_denominator('cs-CZ',_id_group))
			)
			into _res;	
		else
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('en-GB',_id_group)),
				'denominator',(select * from nfiesta_results.fn_get_user_metadata_denominator('en-GB',_id_group))
			)
			into _res;			
		end if;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) is
'The function returns metadata for given input ID (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) to public;
-- </function>


--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	t_estimation_cell_hierarchy
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.t_estimation_cell_hierarchy;


create table nfiesta_results.t_estimation_cell_hierarchy
(
	id serial not null,
	cell int4 not null,
	cell_superior int4 not null,
	constraint pkey__t_estimation_cell_hierarchy primary key (id),
	constraint fkey__t_estimation_cell_hierarchy__c_estimation_cell foreign key (cell) references nfiesta_results.c_estimation_cell(id),
	constraint fkey__t_estimation_cell_hierarchy__c_estimation_cell__superior foreign key (cell_superior) references nfiesta_results.c_estimation_cell(id)
);

create unique index uidx__t_estimation_cell_hierarchy__all_columns on nfiesta_results.t_estimation_cell_hierarchy using btree (cell, cell_superior);

comment on table nfiesta_results.t_estimation_cell_hierarchy is 'Maping table for table c_estimation_cell. Storing hierarchy of cells.';
comment on column nfiesta_results.t_estimation_cell_hierarchy.id is 'Identificator of the record, primary key.';
comment on column nfiesta_results.t_estimation_cell_hierarchy.cell is 'Foreign key to id of c_estimation_cell.';
comment on column nfiesta_results.t_estimation_cell_hierarchy.cell_superior is 'Foreign key to id of c_estimation_cell which is superior in hierarchy.';

comment on constraint pkey__t_estimation_cell_hierarchy on nfiesta_results.t_estimation_cell_hierarchy is 'Database identifier, primary key.';
comment on constraint fkey__t_estimation_cell_hierarchy__c_estimation_cell ON nfiesta_results.t_estimation_cell_hierarchy is 'Foreign key to table c_estimation_cell.';
comment on constraint fkey__t_estimation_cell_hierarchy__c_estimation_cell__superior on nfiesta_results.t_estimation_cell_hierarchy is 'Foreign key to table c_estimation_cell.';

create index fki__t_estimation_cell_hierarchy__c_estimation_cell on nfiesta_results.t_estimation_cell_hierarchy using btree (cell);
comment on index nfiesta_results.fki__t_estimation_cell_hierarchy__c_estimation_cell is 'Index over foreign key fkey__t_estimation_cell_hierarchy__c_estimation_cell';

create index fki__t_estimation_cell_hierarchy__c_estimation_cell_superior on nfiesta_results.t_estimation_cell_hierarchy using btree (cell_superior);
comment on index nfiesta_results.fki__t_estimation_cell_hierarchy__c_estimation_cell_superior is 'Index over foreign key fkey__t_estimation_cell_hierarchy__c_estimation_cell__superior';

alter table nfiesta_results.t_estimation_cell_hierarchy owner to adm_nfiesta;
grant all on table nfiesta_results.t_estimation_cell_hierarchy to adm_nfiesta;
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------



-- <function name="fn_get_heighest_estimation_cell" schema="nfiesta_results" src="functions/fn_get_heighest_estimation_cell.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_heighest_estimation_cell
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_heighest_estimation_cell(integer[]);

create or replace function nfiesta_results.fn_get_heighest_estimation_cell(_estimation_cells integer[])
returns integer[]
as
$$
declare
		_res_1	integer;
		_res	integer[];
begin
		if _estimation_cells is null
		then
			raise exception 'Error 01: fn_get_heighest_estimation_cell: The input argument _estimation_cells must not be NULL !';
		end if;	
	
		select cell_superior from nfiesta_results.t_estimation_cell_hierarchy where cell = _estimation_cells[1]
		into _res_1;	
	
		if _res_1 is null
		then
			_res := _estimation_cells;
		else
			_res := nfiesta_results.fn_get_heighest_estimation_cell(array[_res_1] || _estimation_cells);
		end if;
	
	return _res;

end;
$$
language plpgsql
volatile
cost 100
security invoker;


comment on function nfiesta_results.fn_get_heighest_estimation_cell(integer[]) is
'The function returns the heighest estimation cell from t_estimation_cell_hierarchy table for given estimation_cell.';

grant execute on function nfiesta_results.fn_get_heighest_estimation_cell(integer[]) to public;
-- </function>



-- <function name="fn_get_user_query" schema="nfiesta_results" src="functions/fn_get_user_query.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_query
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_query(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_query
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	estimation_cell					text,
	variable_area_domain_num		text,
	variable_sub_population_num		text,
	variable_area_domain_denom		text,
	variable_sub_population_denom	text,	
	point_estimate					numeric,
	standard_deviation				numeric,
	variation_coeficient			numeric,
	--sample_size					integer,
	--min_sample_size				integer,
	interval_estimation				numeric
)
language plpgsql
stable
as
$$
declare
	_lang_suffix						character varying(3);
	_estimate_type						integer;
	_target_variable					integer;
	_target_variable_denom				integer;
	_estimation_period					integer;
	_panel_refyearset_group				integer;
	_phase_estimate_type				integer;
	_phase_estimate_type_denom			integer;
	_estimation_cell_collection			integer;
	_area_domain						integer;
	_area_domain_denom					integer;
	_sub_population						integer;
	_sub_population_denom				integer;
	_cond_area_domain					text;
	_cond_sub_population				text;
	_cond_area_domain_denom				text;
	_cond_sub_population_denom			text;
	_check4unit							integer;
	_coeficient4ratio					numeric;

	_estimation_cell_from_hierarchy_for_ecc_select_by_user			integer;
	_estimation_cell_from_hierarchy_for_ecc_select_by_user_superior	integer;
	_heighest_estimation_cell_select_by_user						integer;
	_heighest_estimation_cell_collection_select_by_user				integer;
	_estimation_cell_collection_array								integer[];
	_test_occurence_result_group									integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_query: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_query: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select
			trg.estimate_type,
			trg.target_variable,
			trg.target_variable_denom,
			trg.estimation_period,
			trg.panel_refyearset_group,
			trg.phase_estimate_type,
			trg.phase_estimate_type_denom,
			trg.estimation_cell_collection,
			trg.area_domain,
			trg.area_domain_denom,
			trg.sub_population,
			trg.sub_population_denom
	from
			nfiesta_results.t_result_group as trg
	where
			trg.id = _id_group[1]
	into
			_estimate_type,
			_target_variable,
			_target_variable_denom,
			_estimation_period,
			_panel_refyearset_group,
			_phase_estimate_type,
			_phase_estimate_type_denom,
			_estimation_cell_collection,
			_area_domain,
			_area_domain_denom,
			_sub_population,
			_sub_population_denom;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	select tech.cell from nfiesta_results.t_estimation_cell_hierarchy as tech
	where tech.cell in	(
						select cec.id from nfiesta_results.c_estimation_cell as cec
						where cec.estimation_cell_collection = _estimation_cell_collection
						)
	order by cell limit 1
	into _estimation_cell_from_hierarchy_for_ecc_select_by_user;

	if _estimation_cell_from_hierarchy_for_ecc_select_by_user is null
	then
		-- selected estimation cell collection is maybe the heighest
		select count(tech.cell_superior) from nfiesta_results.t_estimation_cell_hierarchy as tech
		where tech.cell_superior in	(
									select cec.id from nfiesta_results.c_estimation_cell as cec
									where cec.estimation_cell_collection = _estimation_cell_collection
									)
		into _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior;

		if _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior = 0
		then
			raise exception 'Error: 03: fn_get_user_query: For user selected estimation_cell_collection = % not exists any estimation cell in t_estimation_cell_hierarchy table!',_estimation_cell_collection;
		end if;

		-- user selected the heighest estimation cell collection => test for occurrence in t_result_group table is not needed
		_estimation_cell_collection_array := array[_estimation_cell_collection];
	else
		with
		w1 as	(
				select * from nfiesta_results.fn_get_heighest_estimation_cell(array[_estimation_cell_from_hierarchy_for_ecc_select_by_user]) as res
				)
		select w1.res[1] from w1
		into _heighest_estimation_cell_select_by_user; -- the heighest estimation cell from hierarchy

		select cec.estimation_cell_collection from nfiesta_results.c_estimation_cell as cec
		where cec.id = _heighest_estimation_cell_select_by_user
		into _heighest_estimation_cell_collection_select_by_user; -- the heighest estimation cell collection from hierarchy

		-- test for occurrence in t_result_group
		select id from nfiesta_results.t_result_group
		where estimate_type = _estimate_type
		and target_variable = _target_variable
		and coalesce(target_variable_denom,0) = coalesce(_target_variable_denom,0)
		and estimation_period = _estimation_period
		and phase_estimate_type = _phase_estimate_type
		and coalesce(phase_estimate_type_denom,0) = coalesce(_phase_estimate_type_denom,0)
		and estimation_cell_collection = _heighest_estimation_cell_collection_select_by_user
		and coalesce(area_domain,0) = coalesce(_area_domain,0)
		and coalesce(area_domain_denom,0) = coalesce(_area_domain_denom,0)
		and coalesce(sub_population,0) = coalesce(_sub_population,0)
		and coalesce(sub_population_denom,0) = coalesce(_sub_population_denom,0)
		and web = true
		into _test_occurence_result_group;

		if _test_occurence_result_group is null
		then
			_estimation_cell_collection_array := array[_estimation_cell_collection];
		else
			_estimation_cell_collection_array := array[_estimation_cell_collection] || array[_heighest_estimation_cell_collection_select_by_user];
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _estimate_type = 1 -- TOTAL
	then
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$2';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$3';
		end if;
		---------------------------------------------------	
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					null::integer,
					null::integer,
					null::integer
					)
				)
		,w2 as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $4
				and ttec.panel_refyearset_group = $5
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $6)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($6)))
				and ttec.phase_estimate_type = $7
				)
		,w3 as	(
				select
						w2.id as id_t_total_estimate_conf,
						w2.estimation_cell,
						w2.variable,
						w2.phase_estimate_type,
						w2.force_synthetic,
						w2.estimation_period,
						w2.panel_refyearset_group,
						w1.nominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,
						tv.area_domain_category,
						tv.sub_population_category
				from
						w2
						inner join w1 on w2.variable = w1.nominator_variable
						inner join nfiesta_results.t_variable as tv on w2.variable = tv.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf
				from
						nfiesta_results.t_estimate_conf as tec 
				where
						tec.total_estimate_conf in (select w3.id_t_total_estimate_conf from w3)
				and
						tec.estimate_type = $8
				)
		,w5 as	(
				select
						w4.*,
						w3.*
				from
						w4 inner join w3 on w4.total_estimate_conf = w3.id_t_total_estimate_conf
				)
		,w6 as	(
				select
						tr.*,
						w5.*
				from
						nfiesta_results.t_result as tr
						inner join w5 on tr.estimate_conf = w5.id_t_estimate_conf
				where
						tr.is_latest = true
				)
		,w7 as	(
				select
						cec.id as estimation_cell_id,
						cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w6.area_domain_category is null then 0
							else cadc.id
						end
							as variable_area_domain_id,	
						---------------------------------
						case
							when w6.area_domain_category is null then ''bez rozliseni''
							else replace(cadc.label,'';'',''; '')
						end
							as variable_area_domain,
						---------------------------------
						case
							when w6.area_domain_category is null then ''without distinction''
							else replace(cadc.label_en,'';'',''; '')
						end
							as variable_area_domain_en,
						-----------------------------------------------------
						case
							when w6.sub_population_category is null then 0
							else cspc.id
						end
							as variable_sub_population_id,
						---------------------------------
						case
							when w6.sub_population_category is null then ''bez rozliseni''
							else replace(cspc.label,'';'',''; '')
						end
							as variable_sub_population,
						---------------------------------
						case
							when w6.sub_population_category is null then ''without distinction''
							else replace(cspc.label_en,'';'',''; '')
						end
							as variable_sub_population_en,
						-----------------------------------------------------
						w6.point as point_estimate,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else sqrt(w6.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null or w6.point = 0.0::numeric then null::numeric
							else (sqrt(w6.var) / w6.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						--w6.act_ssize as sample_size,
						--w6.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else (1.96 * sqrt(w6.var))
						end
							as interval_estimation
				from
						w6
						inner join nfiesta_results.c_estimation_cell as cec on w6.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc on w6.area_domain_category = cadc.id
						left join nfiesta_results.c_sub_population_category as cspc on w6.sub_population_category = cspc.id
				)
		select
				w7.estimation_cell'||_lang_suffix||' as estimation_cell,
				w7.variable_area_domain'||_lang_suffix||' as variable_area_domain_num,
				w7.variable_sub_population'||_lang_suffix||' as variable_sub_population_num,
				null::text as variable_area_domain_denom,
				null::text as variable_sub_population_denom,
				round(w7.point_estimate::numeric,2) as point_estimate,
				round(w7.standard_deviation::numeric,2) as standard_deviation,
				round(w7.variation_coeficient::numeric,2) as variation_coeficient,
				--w7.sample_size::integer,
				--(ceil(w7.min_sample_size::numeric))::integer as min_sample_size,
				round(w7.interval_estimation::numeric,2) as interval_estimation
		from
				w7 order by w7.estimation_cell_id, variable_area_domain_id, variable_sub_population_id;
		'
		using
				_target_variable,
				_area_domain,
				_sub_population,
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection_array,
				_phase_estimate_type,
				_estimate_type;
				
	else -- RATIO
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$3';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$4';
		end if;
		---------------------------------------------------
		if _area_domain_denom is null
		then
			_cond_area_domain_denom := 'null::integer';
		else
			_cond_area_domain_denom := '$5';
		end if;
		---------------------------------------------------
		if _sub_population_denom is null
		then
			_cond_sub_population_denom := 'null::integer';
		else
			_cond_sub_population_denom := '$6';
		end if;
		---------------------------------------------------
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''label'' as unit_num,
						((w2.metadata->''cs'')->''unit'')->>''label'' as unit_denom,
						((w1.metadata->''en'')->''unit'')->>''label'' as unit_num_en,
						((w2.metadata->''en'')->''unit'')->>''label'' as unit_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_num'||_lang_suffix||' AS unit_num,
						w3.unit_denom'||_lang_suffix||' AS unit_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_num = w4.unit_denom) then 0
							else 1
						end as check4unit
				from
						w4
				)
		select w5.check4unit from w5
		'
		using _target_variable, _target_variable_denom
		into _check4unit;		
		---------------------------------------------------
		if _check4unit = 0
		then	
			_coeficient4ratio := 100.0::numeric;
		else
			_coeficient4ratio := 1.0::numeric;
		end if;
		---------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					$2,
					'||_cond_area_domain_denom||',
					'||_cond_sub_population_denom||'
					)
				)
		,w2a as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($9)))
				and ttec.phase_estimate_type = $10
				)
		,w2b as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.denominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($9)))
				and ttec.phase_estimate_type = $10
				)
		,w3 as	(
				select
						w2a.id as id_t_total_estimate_conf_nom,
						w2b.id as id_t_total_estimate_conf_denom,
						w2a.estimation_cell,
						w2a.variable,
						w2a.phase_estimate_type,
						w2a.force_synthetic,
						w2a.estimation_period,
						w2a.panel_refyearset_group,
						w1.nominator_variable,
						w1.denominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,	
						tv1.area_domain_category as area_domain_category_nom,
						tv2.area_domain_category as area_domain_category_denom,
						tv1.sub_population_category as sub_population_category_nom,
						tv2.sub_population_category as sub_population_category_denom
				from
						w2a
						inner join w1 on w2a.variable = w1.nominator_variable
						inner join w2b on w2b.variable = w1.denominator_variable
						inner join nfiesta_results.t_variable as tv1 on w2a.variable = tv1.id
						inner join nfiesta_results.t_variable as tv2 on w2b.variable = tv2.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf,
						tec.denominator,
						w3.*
				from
						nfiesta_results.t_estimate_conf as tec
						
						inner join w3
						on tec.total_estimate_conf = w3.id_t_total_estimate_conf_nom
						and tec.denominator = w3.id_t_total_estimate_conf_denom
				where
						tec.estimate_type = $11
				)
		,w5 as	(
				select
						tr.id,
						tr.estimate_conf,
						tr.point * $12 as point,
						tr.var * $12 as var,
						--tr.min_ssize,
						--tr.act_ssize,
						w4.*
				from
						nfiesta_results.t_result as tr
						inner join w4 on tr.estimate_conf = w4.id_t_estimate_conf
				where
						tr.is_latest = true
				)			
		,w6 as	(
				select
						cec.id as estimation_cell_id,
						cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w5.area_domain_category_nom is null then 0
							else cadc1.id
						end
							as variable_area_domain_id_nom,	
						---------------------------------
						case
							when w5.area_domain_category_denom is null then 0
							else cadc2.id
						end
							as variable_area_domain_id_denom,	
						---------------------------------
						case
							when w5.area_domain_category_nom is null then ''bez rozliseni''
							else replace(cadc1.label,'';'',''; '')
						end
							as variable_area_domain_nom,
						---------------------------------
						case
							when w5.area_domain_category_denom is null then ''bez rozliseni''
							else replace(cadc2.label,'';'',''; '')
						end
							as variable_area_domain_denom,
						---------------------------------
						case
							when w5.area_domain_category_nom is null then ''without distinction''
							else replace(cadc1.label_en,'';'',''; '')
						end
							as variable_area_domain_nom_en,
						---------------------------------
						case
							when w5.area_domain_category_denom is null then ''without distinction''
							else replace(cadc2.label_en,'';'',''; '')
						end
							as variable_area_domain_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						case
							when w5.sub_population_category_nom is null then 0
							else cspc1.id
						end
							as variable_sub_population_id_nom,	
						---------------------------------
						case
							when w5.sub_population_category_denom is null then 0
							else cspc2.id
						end
							as variable_sub_population_id_denom,	
						---------------------------------
						case
							when w5.sub_population_category_nom is null then ''bez rozliseni''
							else replace(cspc1.label,'';'',''; '')
						end
							as variable_sub_population_nom,
						---------------------------------
						case
							when w5.sub_population_category_denom is null then ''bez rozliseni''
							else replace(cspc2.label,'';'',''; '')
						end
							as variable_sub_population_denom,
						---------------------------------
						case
							when w5.sub_population_category_nom is null then ''without distinction''
							else replace(cspc1.label_en,'';'',''; '')
						end
							as variable_sub_population_nom_en,
						---------------------------------
						case
							when w5.sub_population_category_denom is null then ''without distinction''
							else replace(cspc2.label_en,'';'',''; '')
						end
							as variable_sub_population_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						w5.point as point_estimate,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else sqrt(w5.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null or w5.point = 0.0::numeric then null::numeric
							else (sqrt(w5.var) / w5.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						--w5.act_ssize as sample_size,
						--w5.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else (1.96 * sqrt(w5.var))
						end
							as interval_estimation
				from
						w5
						inner join nfiesta_results.c_estimation_cell as cec on w5.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc1 on w5.area_domain_category_nom = cadc1.id
						left join nfiesta_results.c_area_domain_category as cadc2 on w5.area_domain_category_denom = cadc2.id
						left join nfiesta_results.c_sub_population_category as cspc1 on w5.sub_population_category_nom = cspc1.id
						left join nfiesta_results.c_sub_population_category as cspc2 on w5.sub_population_category_denom = cspc2.id
				)
		select
				w6.estimation_cell'||_lang_suffix||' as estimation_cell,
				--concat(w6.variable_area_domain_nom'||_lang_suffix||','' / '',w6.variable_area_domain_denom'||_lang_suffix||') as variable_area_domain,
				--concat(w6.variable_sub_population_nom'||_lang_suffix||','' / '',w6.variable_sub_population_denom'||_lang_suffix||') as variable_sub_population,
				w6.variable_area_domain_nom'||_lang_suffix||' as variable_area_domain_num,
				w6.variable_sub_population_nom'||_lang_suffix||' as variable_sub_population_num,
				w6.variable_area_domain_denom'||_lang_suffix||' as variable_area_domain_denom,				
				w6.variable_sub_population_denom'||_lang_suffix||' as variable_sub_population_denom,				
				round(w6.point_estimate::numeric,2) as point_estimate,
				round(w6.standard_deviation::numeric,2) as standard_deviation,
				round(w6.variation_coeficient::numeric,2) as variation_coeficient,
				--w6.sample_size::integer,
				--(ceil(w6.min_sample_size::numeric))::integer as min_sample_size,
				round(w6.interval_estimation::numeric,2) as interval_estimation
		from
				w6
		order
				by	w6.estimation_cell_id,
					w6.variable_area_domain_id_nom,
					w6.variable_area_domain_id_denom,
					w6.variable_sub_population_id_nom,
					w6.variable_sub_population_id_denom;
		'
		using
				_target_variable,
				_target_variable_denom,
				_area_domain,
				_sub_population,
				_area_domain_denom,
				_sub_population_denom,				
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection_array,
				_phase_estimate_type,
				_estimate_type,
				_coeficient4ratio;	
	end if;

end;
$$
;

comment on function nfiesta_results.fn_get_user_query(character varying, integer[]) is
'The function returns result estimations for gived group of result and attribute variables from t_result table.';

grant execute on function nfiesta_results.fn_get_user_query(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata" schema="nfiesta_results" src="functions/fn_get_user_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang_suffix								character varying(3);
	_lang										text;
	_estimate_type								integer;
	_target_variable							integer;
	_target_variable_denom						integer;
	_estimation_period							integer;
	_panel_refyearset_group						integer;
	_phase_estimate_type						integer;
	_phase_estimate_type_denom					integer;
	_estimation_cell_collection					integer;
	_area_domain								integer;
	_area_domain_denom							integer;
	_sub_population								integer;
	_sub_population_denom						integer;
	--_target_variable_num						integer[];
	--_target_variable_denom_check				integer;
	--_target_variable_denom					integer;
	_cond4topic_num								text;
	_cond4topic_denom							text;
	_res_topic_label							text;
	_res_topic_description						text;
	_res_estimation_period_label				text;
	_res_estimation_period_description			text;
	_res_estimation_cell_collection_label		text;
	_res_estimation_cell_collection_description	text;
	_res_estimate_type_label					text;
	_res_estimate_type_description				text;
	_res_unit_num_label							text;
	_res_unit_num_description					text;
	_res_unit_of_measurement_label				text;
	_res_unit_of_measurement_description		text;
	_res										json;

	_estimation_cell_from_hierarchy_for_ecc_select_by_user			integer;
	_estimation_cell_from_hierarchy_for_ecc_select_by_user_superior	integer;
	_heighest_estimation_cell_select_by_user						integer;
	_heighest_estimation_cell_collection_select_by_user				integer;
	_estimation_cell_collection_array								integer[];
	_test_occurence_result_group									integer;	
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_query: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select
			trg.estimate_type,
			trg.target_variable,
			trg.target_variable_denom,
			trg.estimation_period,
			trg.panel_refyearset_group,
			trg.phase_estimate_type,
			trg.phase_estimate_type_denom,
			trg.estimation_cell_collection,
			trg.area_domain,
			trg.area_domain_denom,
			trg.sub_population,
			trg.sub_population_denom
	from
			nfiesta_results.t_result_group as trg
	where
			trg.id = _id_group[1]
	into
			_estimate_type,
			_target_variable,
			_target_variable_denom,
			_estimation_period,
			_panel_refyearset_group,
			_phase_estimate_type,
			_phase_estimate_type_denom,
			_estimation_cell_collection,
			_area_domain,
			_area_domain_denom,
			_sub_population,
			_sub_population_denom;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	select count(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom_check;

	if _target_variable_denom_check is distinct from 0 -- target variable denom exists
	then
		if _target_variable_denom_check > 1
		then
			raise exception 'Error: 03: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
		end if;

		select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
		where trg.id in (select unnest(_id_group))
		and trg.web = true
		into _target_variable_denom;		
	else
		_target_variable_denom := null::integer;
	end if;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	execute
	'
	with
	w1 as	(
			select crt.topic from nfiesta_results.cm_result2topic as crt
			where ' || _cond4topic_num ||'
			and ' || _cond4topic_denom ||'
			)
	,w2 as	(
			select
					label'||_lang_suffix||' AS label,
					description'||_lang_suffix||' AS description
			from
					nfiesta_results.c_topic
			where
					id in (select distinct w1.topic from w1)
			)
	,w3 as	(
			select
					array_agg(w2.label order by w2.description) as label,
					array_agg(w2.description order by w2.description) as description
			from
					w2
			)
	select
			array_to_string(w3.label, ''; ''),
			array_to_string(w3.description, ''; '')
	from
			w3
	'
	using _target_variable, _target_variable_denom
	into _res_topic_label, _res_topic_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	execute
	'
	select
			cep.label'||_lang_suffix||' AS label,
			cep.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_period as cep
	where
			cep.id =	(
						select distinct trg.estimation_period
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_period_label, _res_estimation_period_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	select tech.cell from nfiesta_results.t_estimation_cell_hierarchy as tech
	where tech.cell in	(
						select cec.id from nfiesta_results.c_estimation_cell as cec
						where cec.estimation_cell_collection =	_estimation_cell_collection
						)
	order by cell limit 1
	into _estimation_cell_from_hierarchy_for_ecc_select_by_user;

	if _estimation_cell_from_hierarchy_for_ecc_select_by_user is null
	then
		-- selected estimation cell collection is maybe the heighest
		select count(tech.cell_superior) from nfiesta_results.t_estimation_cell_hierarchy as tech
		where tech.cell_superior in	(
									select cec.id from nfiesta_results.c_estimation_cell as cec
									where cec.estimation_cell_collection = _estimation_cell_collection
									)
		into _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior;

		if _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior = 0
		then
			raise exception 'Error: 04: fn_get_user_metadata: For user selected estimation_cell_collection = % not exists any estimation cell in t_estimation_cell_hierarchy table!',_estimation_cell_collection;
		end if;

		-- user selected the heighest estimation cell collection => test for occurrence in t_result_group table is not needed
		_estimation_cell_collection_array := array[_estimation_cell_collection];		
	else
		with
		w1 as	(
				select * from nfiesta_results.fn_get_heighest_estimation_cell(array[_estimation_cell_from_hierarchy_for_ecc_select_by_user]) as res
				)
		select w1.res[1] from w1
		into _heighest_estimation_cell_select_by_user; -- the heighest estimation cell from hierarchy

		select cec.estimation_cell_collection from nfiesta_results.c_estimation_cell as cec
		where cec.id = _heighest_estimation_cell_select_by_user
		into _heighest_estimation_cell_collection_select_by_user; -- the heighest estimation cell collection from hierarchy

		-- test for occurrence in t_result_group
		select id from nfiesta_results.t_result_group
		where estimate_type = _estimate_type
		and target_variable = _target_variable
		and coalesce(target_variable_denom,0) = coalesce(_target_variable_denom,0)
		and estimation_period = _estimation_period
		and phase_estimate_type = _phase_estimate_type
		and coalesce(phase_estimate_type_denom,0) = coalesce(_phase_estimate_type_denom,0)
		and estimation_cell_collection = _heighest_estimation_cell_collection_select_by_user
		and coalesce(area_domain,0) = coalesce(_area_domain,0)
		and coalesce(area_domain_denom,0) = coalesce(_area_domain_denom,0)
		and coalesce(sub_population,0) = coalesce(_sub_population,0)
		and coalesce(sub_population_denom,0) = coalesce(_sub_population_denom,0)
		and web = true
		into _test_occurence_result_group;

		if _test_occurence_result_group is null
		then
			_estimation_cell_collection_array := array[_estimation_cell_collection];
		else
			_estimation_cell_collection_array := array[_estimation_cell_collection] || array[_heighest_estimation_cell_collection_select_by_user];
		end if;
	end if;	

	execute
	'
	with
	w1 as	(
			select
					cecc.id,
					cecc.label,
					cecc.description
			from
					nfiesta_results.c_estimation_cell_collection as cecc
			where
					cecc.id in (select unnest($1))
			)
	,w2 as	(
			select
					array_agg(w1.label order by w1.id) as label,
					array_agg(w1.description order by w1.id) as description
			from
					w1
			)
			select
					array_to_string(w2.label,''; '') as label,
					array_to_string(w2.description,''; '') as description
			from
				w2
	'
	using _estimation_cell_collection_array
	into _res_estimation_cell_collection_label, _res_estimation_cell_collection_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATE TYPE
	if _target_variable_denom is null
	then
		execute
		'
		select
				cet.label,
				cet.description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 1
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;
	else
		execute
		'
		select
				cet.label,
				cet.description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 2
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable
	into _res_unit_num_label, _res_unit_num_description;		
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASUREMENT
	if _target_variable_denom is null
	then
		_res_unit_of_measurement_label := _res_unit_num_label;
		_res_unit_of_measurement_description := _res_unit_num_description;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''label'' as unit_label_num,
						((w2.metadata->''cs'')->''unit'')->>''label'' as unit_label_denom,
						((w1.metadata->''en'')->''unit'')->>''label'' as unit_label_num_en,
						((w2.metadata->''en'')->''unit'')->>''label'' as unit_label_denom_en,						
						((w1.metadata->''cs'')->''unit'')->>''description'' as unit_description_num,
						((w2.metadata->''cs'')->''unit'')->>''description'' as unit_description_denom,
						((w1.metadata->''en'')->''unit'')->>''description'' as unit_description_num_en,
						((w2.metadata->''en'')->''unit'')->>''description'' as unit_description_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_label_num'||_lang_suffix||' AS unit_label_num,
						w3.unit_label_denom'||_lang_suffix||' AS unit_label_denom,						
						w3.unit_description_num'||_lang_suffix||' AS unit_description_num,
						w3.unit_description_denom'||_lang_suffix||' AS unit_description_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_label_num = w4.unit_label_denom) then ''%''
							else concat(w4.unit_label_num,'' / '',w4.unit_label_denom)
						end as label4user,						
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_description_num = w4.unit_description_denom) then ''%''
							else concat(w4.unit_description_num,'' / '',w4.unit_description_denom)
						end as description4user
				from
						w4
				)
		select
				w5.label4user,
				w5.description4user
		from
				w5
		'
		using _target_variable, _target_variable_denom
		into _res_unit_of_measurement_label, _res_unit_of_measurement_description;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _target_variable_denom is null
	then
		-- basic metadata + numerator metadata
		if _lang_suffix = ''
		then
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('cs-CZ',_id_group))
			)
			into _res;
		else
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('en-GB',_id_group))
			)
			into _res;		
		end if;
	else
		-- basic metadata + numerator metadata + denominator metadata
		if _lang_suffix = ''
		then
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('cs-CZ',_id_group)),
				'denominator',(select * from nfiesta_results.fn_get_user_metadata_denominator('cs-CZ',_id_group))
			)
			into _res;	
		else
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('en-GB',_id_group)),
				'denominator',(select * from nfiesta_results.fn_get_user_metadata_denominator('en-GB',_id_group))
			)
			into _res;			
		end if;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) is
'The function returns metadata for given input ID (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) to public;
-- </function>


--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------


GRANT USAGE ON SCHEMA nfiesta_results TO app_joomla;

GRANT SELECT ON TABLE nfiesta_results.c_area_domain TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_area_domain_category TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_estimate_type TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_estimation_cell TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_estimation_cell_collection TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_estimation_period TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_gui_header TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_gui_header_language TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_language TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_panel_refyearset_group TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_phase_estimate_type TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_sub_population TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_sub_population_category TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_target_variable TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.c_topic TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.cm_area_domain_category TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.cm_result2topic TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.cm_sub_population_category TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.f_a_cell TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.t_estimate_conf TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.t_result TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.t_result_group TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.t_total_estimate_conf TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.t_variable TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.t_variable_hierarchy TO app_joomla;
GRANT SELECT ON TABLE nfiesta_results.t_estimation_cell_hierarchy TO app_joomla;

GRANT EXECUTE ON FUNCTION nfiesta_results.fn_array_compare(anyarray, anyarray) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_area_sub_population_categories(int4, int4, int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_attribute_categories4target_variable(int4, int4, int4, int4, int4, int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_gui_headers(varchar) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_heighest_estimation_cell(_int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_language4query(varchar) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_num_denom_variables(_int4, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_metadata(varchar, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_metadata_denominator(varchar, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_metadata_numerator(varchar, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_options_denominator(varchar, _int4, bool, bool) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_options_denominator_area_domain(varchar, _int4, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_options_denominator_ldsity_core(varchar, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_options_denominator_ldsity_division(varchar, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_options_denominator_sub_population(varchar, _int4, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_options_numerator(varchar, int4, int4, int4, _int4, bool, bool, bool, bool) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_options_numerator_area_domain(varchar, _int4, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_options_numerator_ldsity_core(varchar, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_options_numerator_ldsity_division(varchar, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_options_numerator_metadata(varchar, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_options_numerator_sub_population(varchar, _int4, _int4) TO app_joomla;
GRANT EXECUTE ON FUNCTION nfiesta_results.fn_get_user_query(varchar, _int4) TO app_joomla;



-- <function name="fn_get_user_query" schema="nfiesta_results" src="functions/fn_get_user_query.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_query
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_query(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_query
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	estimation_cell					text,
	variable_area_domain_num		text,
	variable_sub_population_num		text,
	variable_area_domain_denom		text,
	variable_sub_population_denom	text,	
	point_estimate					numeric,
	standard_deviation				numeric,
	variation_coeficient			numeric,
	--sample_size					integer,
	--min_sample_size				integer,
	interval_estimation				numeric
)
language plpgsql
stable
as
$$
declare
	_lang_suffix						character varying(3);
	_estimate_type						integer;
	_target_variable					integer;
	_target_variable_denom				integer;
	_estimation_period					integer;
	_panel_refyearset_group				integer;
	_phase_estimate_type				integer;
	_phase_estimate_type_denom			integer;
	_estimation_cell_collection			integer;
	_area_domain						integer;
	_area_domain_denom					integer;
	_sub_population						integer;
	_sub_population_denom				integer;
	_cond_area_domain					text;
	_cond_sub_population				text;
	_cond_area_domain_denom				text;
	_cond_sub_population_denom			text;
	_check4unit							integer;
	_coeficient4ratio					numeric;

	_estimation_cell_from_hierarchy_for_ecc_select_by_user			integer;
	_estimation_cell_from_hierarchy_for_ecc_select_by_user_superior	integer;
	_heighest_estimation_cell_select_by_user						integer;
	_heighest_estimation_cell_collection_select_by_user				integer;
	_estimation_cell_collection_array								integer[];
	_test_occurence_result_group									integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_query: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_query: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select
			trg.estimate_type,
			trg.target_variable,
			trg.target_variable_denom,
			trg.estimation_period,
			trg.panel_refyearset_group,
			trg.phase_estimate_type,
			trg.phase_estimate_type_denom,
			trg.estimation_cell_collection,
			trg.area_domain,
			trg.area_domain_denom,
			trg.sub_population,
			trg.sub_population_denom
	from
			nfiesta_results.t_result_group as trg
	where
			trg.id = _id_group[1]
	into
			_estimate_type,
			_target_variable,
			_target_variable_denom,
			_estimation_period,
			_panel_refyearset_group,
			_phase_estimate_type,
			_phase_estimate_type_denom,
			_estimation_cell_collection,
			_area_domain,
			_area_domain_denom,
			_sub_population,
			_sub_population_denom;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	select tech.cell from nfiesta_results.t_estimation_cell_hierarchy as tech
	where tech.cell in	(
						select cec.id from nfiesta_results.c_estimation_cell as cec
						where cec.estimation_cell_collection = _estimation_cell_collection
						)
	order by cell limit 1
	into _estimation_cell_from_hierarchy_for_ecc_select_by_user;

	if _estimation_cell_from_hierarchy_for_ecc_select_by_user is null
	then
		-- selected estimation cell collection is maybe the heighest
		select count(tech.cell_superior) from nfiesta_results.t_estimation_cell_hierarchy as tech
		where tech.cell_superior in	(
									select cec.id from nfiesta_results.c_estimation_cell as cec
									where cec.estimation_cell_collection = _estimation_cell_collection
									)
		into _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior;

		if _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior = 0
		then
			raise exception 'Error: 03: fn_get_user_query: For user selected estimation_cell_collection = % not exists any estimation cell in t_estimation_cell_hierarchy table!',_estimation_cell_collection;
		end if;

		-- user selected the heighest estimation cell collection => test for occurrence in t_result_group table is not needed
		_estimation_cell_collection_array := array[_estimation_cell_collection];
	else
		with
		w1 as	(
				select * from nfiesta_results.fn_get_heighest_estimation_cell(array[_estimation_cell_from_hierarchy_for_ecc_select_by_user]) as res
				)
		select w1.res[1] from w1
		into _heighest_estimation_cell_select_by_user; -- the heighest estimation cell from hierarchy

		select cec.estimation_cell_collection from nfiesta_results.c_estimation_cell as cec
		where cec.id = _heighest_estimation_cell_select_by_user
		into _heighest_estimation_cell_collection_select_by_user; -- the heighest estimation cell collection from hierarchy

		-- test for occurrence in t_result_group
		select id from nfiesta_results.t_result_group
		where estimate_type = _estimate_type
		and target_variable = _target_variable
		and coalesce(target_variable_denom,0) = coalesce(_target_variable_denom,0)
		and estimation_period = _estimation_period
		and phase_estimate_type = _phase_estimate_type
		and coalesce(phase_estimate_type_denom,0) = coalesce(_phase_estimate_type_denom,0)
		and estimation_cell_collection = _heighest_estimation_cell_collection_select_by_user
		and coalesce(area_domain,0) = coalesce(_area_domain,0)
		and coalesce(area_domain_denom,0) = coalesce(_area_domain_denom,0)
		and coalesce(sub_population,0) = coalesce(_sub_population,0)
		and coalesce(sub_population_denom,0) = coalesce(_sub_population_denom,0)
		and web = true
		into _test_occurence_result_group;

		if _test_occurence_result_group is null
		then
			_estimation_cell_collection_array := array[_estimation_cell_collection];
		else
			_estimation_cell_collection_array := array[_estimation_cell_collection] || array[_heighest_estimation_cell_collection_select_by_user];
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _estimate_type = 1 -- TOTAL
	then
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$2';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$3';
		end if;
		---------------------------------------------------	
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					null::integer,
					null::integer,
					null::integer
					)
				)
		,w2 as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $4
				and ttec.panel_refyearset_group = $5
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $6)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($6)))
				and ttec.phase_estimate_type = $7
				)
		,w3 as	(
				select
						w2.id as id_t_total_estimate_conf,
						w2.estimation_cell,
						w2.variable,
						w2.phase_estimate_type,
						w2.force_synthetic,
						w2.estimation_period,
						w2.panel_refyearset_group,
						w1.nominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,
						tv.area_domain_category,
						tv.sub_population_category
				from
						w2
						inner join w1 on w2.variable = w1.nominator_variable
						inner join nfiesta_results.t_variable as tv on w2.variable = tv.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf
				from
						nfiesta_results.t_estimate_conf as tec 
				where
						tec.total_estimate_conf in (select w3.id_t_total_estimate_conf from w3)
				and
						tec.estimate_type = $8
				)
		,w5 as	(
				select
						w4.*,
						w3.*
				from
						w4 inner join w3 on w4.total_estimate_conf = w3.id_t_total_estimate_conf
				)
		,w6 as	(
				select
						tr.*,
						w5.*
				from
						nfiesta_results.t_result as tr
						inner join w5 on tr.estimate_conf = w5.id_t_estimate_conf
				where
						tr.is_latest = true
				)
		,w7 as	(
				select
						cec.id as estimation_cell_id,
						cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w6.area_domain_category is null then 0
							else cadc.id
						end
							as variable_area_domain_id,	
						---------------------------------
						case
							when w6.area_domain_category is null then ''bez rozlišení''
							else replace(cadc.label,'';'',''; '')
						end
							as variable_area_domain,
						---------------------------------
						case
							when w6.area_domain_category is null then ''without distinction''
							else replace(cadc.label_en,'';'',''; '')
						end
							as variable_area_domain_en,
						-----------------------------------------------------
						case
							when w6.sub_population_category is null then 0
							else cspc.id
						end
							as variable_sub_population_id,
						---------------------------------
						case
							when w6.sub_population_category is null then ''bez rozlišení''
							else replace(cspc.label,'';'',''; '')
						end
							as variable_sub_population,
						---------------------------------
						case
							when w6.sub_population_category is null then ''without distinction''
							else replace(cspc.label_en,'';'',''; '')
						end
							as variable_sub_population_en,
						-----------------------------------------------------
						w6.point as point_estimate,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else sqrt(w6.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null or w6.point = 0.0::numeric then null::numeric
							else (sqrt(w6.var) / w6.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						--w6.act_ssize as sample_size,
						--w6.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else (1.96 * sqrt(w6.var))
						end
							as interval_estimation
				from
						w6
						inner join nfiesta_results.c_estimation_cell as cec on w6.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc on w6.area_domain_category = cadc.id
						left join nfiesta_results.c_sub_population_category as cspc on w6.sub_population_category = cspc.id
				)
		select
				w7.estimation_cell'||_lang_suffix||' as estimation_cell,
				w7.variable_area_domain'||_lang_suffix||' as variable_area_domain_num,
				w7.variable_sub_population'||_lang_suffix||' as variable_sub_population_num,
				null::text as variable_area_domain_denom,
				null::text as variable_sub_population_denom,
				round(w7.point_estimate::numeric,2) as point_estimate,
				round(w7.standard_deviation::numeric,2) as standard_deviation,
				round(w7.variation_coeficient::numeric,2) as variation_coeficient,
				--w7.sample_size::integer,
				--(ceil(w7.min_sample_size::numeric))::integer as min_sample_size,
				round(w7.interval_estimation::numeric,2) as interval_estimation
		from
				w7 order by w7.estimation_cell_id, variable_area_domain_id, variable_sub_population_id;
		'
		using
				_target_variable,
				_area_domain,
				_sub_population,
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection_array,
				_phase_estimate_type,
				_estimate_type;
				
	else -- RATIO
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$3';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$4';
		end if;
		---------------------------------------------------
		if _area_domain_denom is null
		then
			_cond_area_domain_denom := 'null::integer';
		else
			_cond_area_domain_denom := '$5';
		end if;
		---------------------------------------------------
		if _sub_population_denom is null
		then
			_cond_sub_population_denom := 'null::integer';
		else
			_cond_sub_population_denom := '$6';
		end if;
		---------------------------------------------------
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''label'' as unit_num,
						((w2.metadata->''cs'')->''unit'')->>''label'' as unit_denom,
						((w1.metadata->''en'')->''unit'')->>''label'' as unit_num_en,
						((w2.metadata->''en'')->''unit'')->>''label'' as unit_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_num'||_lang_suffix||' AS unit_num,
						w3.unit_denom'||_lang_suffix||' AS unit_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_num = w4.unit_denom) then 0
							else 1
						end as check4unit
				from
						w4
				)
		select w5.check4unit from w5
		'
		using _target_variable, _target_variable_denom
		into _check4unit;		
		---------------------------------------------------
		if _check4unit = 0
		then	
			_coeficient4ratio := 100.0::numeric;
		else
			_coeficient4ratio := 1.0::numeric;
		end if;
		---------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					$2,
					'||_cond_area_domain_denom||',
					'||_cond_sub_population_denom||'
					)
				)
		,w2a as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($9)))
				and ttec.phase_estimate_type = $10
				)
		,w2b as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.denominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($9)))
				and ttec.phase_estimate_type = $10
				)
		,w3 as	(
				select
						w2a.id as id_t_total_estimate_conf_nom,
						w2b.id as id_t_total_estimate_conf_denom,
						w2a.estimation_cell,
						w2a.variable,
						w2a.phase_estimate_type,
						w2a.force_synthetic,
						w2a.estimation_period,
						w2a.panel_refyearset_group,
						w1.nominator_variable,
						w1.denominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,	
						tv1.area_domain_category as area_domain_category_nom,
						tv2.area_domain_category as area_domain_category_denom,
						tv1.sub_population_category as sub_population_category_nom,
						tv2.sub_population_category as sub_population_category_denom
				from
						w2a
						inner join w1 on w2a.variable = w1.nominator_variable
						inner join w2b on w2b.variable = w1.denominator_variable
						inner join nfiesta_results.t_variable as tv1 on w2a.variable = tv1.id
						inner join nfiesta_results.t_variable as tv2 on w2b.variable = tv2.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf,
						tec.denominator,
						w3.*
				from
						nfiesta_results.t_estimate_conf as tec
						
						inner join w3
						on tec.total_estimate_conf = w3.id_t_total_estimate_conf_nom
						and tec.denominator = w3.id_t_total_estimate_conf_denom
				where
						tec.estimate_type = $11
				)
		,w5 as	(
				select
						tr.id,
						tr.estimate_conf,
						tr.point * $12 as point,
						tr.var * $12 as var,
						--tr.min_ssize,
						--tr.act_ssize,
						w4.*
				from
						nfiesta_results.t_result as tr
						inner join w4 on tr.estimate_conf = w4.id_t_estimate_conf
				where
						tr.is_latest = true
				)			
		,w6 as	(
				select
						cec.id as estimation_cell_id,
						cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w5.area_domain_category_nom is null then 0
							else cadc1.id
						end
							as variable_area_domain_id_nom,	
						---------------------------------
						case
							when w5.area_domain_category_denom is null then 0
							else cadc2.id
						end
							as variable_area_domain_id_denom,	
						---------------------------------
						case
							when w5.area_domain_category_nom is null then ''bez rozlišení''
							else replace(cadc1.label,'';'',''; '')
						end
							as variable_area_domain_nom,
						---------------------------------
						case
							when w5.area_domain_category_denom is null then ''bez rozlišení''
							else replace(cadc2.label,'';'',''; '')
						end
							as variable_area_domain_denom,
						---------------------------------
						case
							when w5.area_domain_category_nom is null then ''without distinction''
							else replace(cadc1.label_en,'';'',''; '')
						end
							as variable_area_domain_nom_en,
						---------------------------------
						case
							when w5.area_domain_category_denom is null then ''without distinction''
							else replace(cadc2.label_en,'';'',''; '')
						end
							as variable_area_domain_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						case
							when w5.sub_population_category_nom is null then 0
							else cspc1.id
						end
							as variable_sub_population_id_nom,	
						---------------------------------
						case
							when w5.sub_population_category_denom is null then 0
							else cspc2.id
						end
							as variable_sub_population_id_denom,	
						---------------------------------
						case
							when w5.sub_population_category_nom is null then ''bez rozlišení''
							else replace(cspc1.label,'';'',''; '')
						end
							as variable_sub_population_nom,
						---------------------------------
						case
							when w5.sub_population_category_denom is null then ''bez rozlišení''
							else replace(cspc2.label,'';'',''; '')
						end
							as variable_sub_population_denom,
						---------------------------------
						case
							when w5.sub_population_category_nom is null then ''without distinction''
							else replace(cspc1.label_en,'';'',''; '')
						end
							as variable_sub_population_nom_en,
						---------------------------------
						case
							when w5.sub_population_category_denom is null then ''without distinction''
							else replace(cspc2.label_en,'';'',''; '')
						end
							as variable_sub_population_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						w5.point as point_estimate,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else sqrt(w5.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null or w5.point = 0.0::numeric then null::numeric
							else (sqrt(w5.var) / w5.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						--w5.act_ssize as sample_size,
						--w5.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else (1.96 * sqrt(w5.var))
						end
							as interval_estimation
				from
						w5
						inner join nfiesta_results.c_estimation_cell as cec on w5.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc1 on w5.area_domain_category_nom = cadc1.id
						left join nfiesta_results.c_area_domain_category as cadc2 on w5.area_domain_category_denom = cadc2.id
						left join nfiesta_results.c_sub_population_category as cspc1 on w5.sub_population_category_nom = cspc1.id
						left join nfiesta_results.c_sub_population_category as cspc2 on w5.sub_population_category_denom = cspc2.id
				)
		select
				w6.estimation_cell'||_lang_suffix||' as estimation_cell,
				--concat(w6.variable_area_domain_nom'||_lang_suffix||','' / '',w6.variable_area_domain_denom'||_lang_suffix||') as variable_area_domain,
				--concat(w6.variable_sub_population_nom'||_lang_suffix||','' / '',w6.variable_sub_population_denom'||_lang_suffix||') as variable_sub_population,
				w6.variable_area_domain_nom'||_lang_suffix||' as variable_area_domain_num,
				w6.variable_sub_population_nom'||_lang_suffix||' as variable_sub_population_num,
				w6.variable_area_domain_denom'||_lang_suffix||' as variable_area_domain_denom,				
				w6.variable_sub_population_denom'||_lang_suffix||' as variable_sub_population_denom,				
				round(w6.point_estimate::numeric,2) as point_estimate,
				round(w6.standard_deviation::numeric,2) as standard_deviation,
				round(w6.variation_coeficient::numeric,2) as variation_coeficient,
				--w6.sample_size::integer,
				--(ceil(w6.min_sample_size::numeric))::integer as min_sample_size,
				round(w6.interval_estimation::numeric,2) as interval_estimation
		from
				w6
		order
				by	w6.estimation_cell_id,
					w6.variable_area_domain_id_nom,
					w6.variable_area_domain_id_denom,
					w6.variable_sub_population_id_nom,
					w6.variable_sub_population_id_denom;
		'
		using
				_target_variable,
				_target_variable_denom,
				_area_domain,
				_sub_population,
				_area_domain_denom,
				_sub_population_denom,				
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection_array,
				_phase_estimate_type,
				_estimate_type,
				_coeficient4ratio;	
	end if;

end;
$$
;

comment on function nfiesta_results.fn_get_user_query(character varying, integer[]) is
'The function returns result estimations for gived group of result and attribute variables from t_result table.';

grant execute on function nfiesta_results.fn_get_user_query(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata_denominator" schema="nfiesta_results" src="functions/fn_get_user_metadata_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata_denominator
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang_suffix							character varying(3);
	_lang									text;
	_target_variable						integer[];
	_res_indicator_label					text;
	_res_indicator_description				text;
	_res_state_or_change_label				text;
	_res_state_or_change_description		text;
	_res_unit_label							text;
	_res_unit_description					text;
	_check_area_domain						integer[];
	_res_area_domain_attribute_json			json;
	_check_sub_population					integer[];
	_res_sub_population_attribute_json		json;
	--_check_core_ldsities					integer;
	_check_division_ldsities				integer;
	_not_specified_version_label			text;
	_not_specified_version_description		text;
	_not_specified_defintion_variant		json;
	_not_specified_adr_spr					json;
	_res_core_json							json;
	_res_division_json						json;	
	_res									json;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata_denominator: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata_denominator: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom) -- column for DENOMINATOR
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable;

	if array_length(_target_variable,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	if _lang_suffix = ''
	then
		_not_specified_version_label = 'neuvedeno'::text;
		_not_specified_version_description = 'Neuvedeno.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
	else
		_not_specified_version_label := 'not specified'::text;
		_not_specified_version_description = 'Not specified.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
					((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en,		
					((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
					((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_indicator_label, _res_indicator_description;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state_or_change'')->>''label'' as label,
					((ctv.metadata->''en'')->''state_or_change'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''state_or_change'')->>''description'' as description,
					((ctv.metadata->''en'')->''state_or_change'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_state_or_change_label, _res_state_or_change_description;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_unit_label, _res_unit_description;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 04: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_area_domain_attribute_json := json_agg(json_build_object('label','bez rozlišení','description','Bez rozlišení.'));
		else
			_res_area_domain_attribute_json := json_agg(json_build_object('label','without distinction','description','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(cad.label'||_lang_suffix||','';'') as label,
						string_to_array(cad.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_area_domain as cad
				where
						cad.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_area_domain[1]
		into _res_area_domain_attribute_json;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 05: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_sub_population_attribute_json := json_agg(json_build_object('label','bez rozlišení','description','Bez rozlišení.'));
		else
			_res_sub_population_attribute_json := json_agg(json_build_object('label','without distinction','description','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(csp.label'||_lang_suffix||','';'') as label,
						string_to_array(csp.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_sub_population as csp
				where
						csp.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_sub_population[1]
		into _res_sub_population_attribute_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local_densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'core'
	into _check_core_ldsities;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- CORE JSON
	execute
	'
	with
	w1 as	(
			select
					(ctv.metadata->''cs'')->''local_densities'' as local_densities,
					(ctv.metadata->''en'')->''local_densities'' as local_densities_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	,w2 as	(
			select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
			)
	,w3 as	(
			select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''core''
			)
	,w4 as	(
			select
					row_number() over() as new_id,
					(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
					(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
					(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
					(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
					(w3.local_densities->''ldsity'')->>''label'' as label,
					(w3.local_densities->''ldsity'')->>''description'' as description,	
					w3.local_densities->>''use_negative'' as use_negative,
					(w3.local_densities->''version'')->>''label'' as version_label,
					(w3.local_densities->''version'')->>''description'' as version_description,
					w3.local_densities->''definition_variant'' as definition_variant,
					w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
					w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
			from
					w3
			)
	,w5 as	(
			select
					w4.new_id,
					w4.object_type_label,
					w4.object_type_description,
					w4.object_label,
					w4.object_description,
					w4.label,
					w4.description,
					w4.use_negative,
					case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
					case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
					case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
					case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
					case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
			from
					w4
			)
	,w6 as	(
			select
					w5.new_id,
					json_build_object
						(
						''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
						''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
						''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
						''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
						''definition_variant'',w5.definition_variant,
						''area_domain_restrictions'',w5.area_domain_restrictions,
						''sub_population_restrictions'',w5.sub_population_restrictions,
						''use_negative'',w5.use_negative
						) as  local_densities
			from
					w5
			)
	select
			json_agg(w6.local_densities order by w6.new_id) as local_densities
	from
			w6
	'
	using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
	into _res_core_json;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- DIVISION JSON
	if _check_division_ldsities = 0
	then
		_res_division_json := null::json;
	else
		execute
		'
		with
		w1 as	(
				select
						(ctv.metadata->''cs'')->''local_densities'' as local_densities,
						(ctv.metadata->''en'')->''local_densities'' as local_densities_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		,w2 as	(
				select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
				)
		,w3 as	(
				select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w4 as	(
				select
						row_number() over() as new_id,
						(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w3.local_densities->''ldsity'')->>''label'' as label,
						(w3.local_densities->''ldsity'')->>''description'' as description,	
						w3.local_densities->>''use_negative'' as use_negative,
						(w3.local_densities->''version'')->>''label'' as version_label,
						(w3.local_densities->''version'')->>''description'' as version_description,
						w3.local_densities->''definition_variant'' as definition_variant,
						w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w3
				)
		,w5 as	(
				select
						w4.new_id,
						w4.object_type_label,
						w4.object_type_description,
						w4.object_label,
						w4.object_description,
						w4.label,
						w4.description,
						w4.use_negative,
						case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
						case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
						case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
						case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
						case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
				from
						w4
				)
		,w6 as	(
				select
						w5.new_id,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
							''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
							''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
							''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
							''definition_variant'',w5.definition_variant,
							''area_domain_restrictions'',w5.area_domain_restrictions,
							''sub_population_restrictions'',w5.sub_population_restrictions,
							''use_negative'',w5.use_negative
							) as  local_densities
				from
						w5
				)
		select
				json_agg(w6.local_densities order by w6.new_id) as local_densities
		from
				w6
		'
		using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
		into _res_division_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities = 0
	then
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json
		))
		into _res;
	else
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json,
			'division_contributions',_res_division_json
		))
		into _res;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) is
'The function returns metadata (description) for given input ID (that represents target variable for denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata_numerator" schema="nfiesta_results" src="functions/fn_get_user_metadata_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata_numerator
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang_suffix							character varying(3);
	_lang									text;
	_target_variable						integer[];
	_res_indicator_label					text;
	_res_indicator_description				text;
	_res_state_or_change_label				text;
	_res_state_or_change_description		text;
	_res_unit_label							text;
	_res_unit_description					text;
	_check_area_domain						integer[];
	_res_area_domain_attribute_json			json;
	_check_sub_population					integer[];
	_res_sub_population_attribute_json		json;
	--_check_core_ldsities					integer;
	_check_division_ldsities				integer;
	_not_specified_version_label			text;
	_not_specified_version_description		text;
	_not_specified_defintion_variant		json;
	_not_specified_adr_spr					json;
	_res_core_json							json;
	_res_division_json						json;	
	_res									json;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata_numerator: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata_numerator: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable) -- column for NUMERATOR
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable;

	if array_length(_target_variable,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	if _lang_suffix = ''
	then
		_not_specified_version_label = 'neuvedeno'::text;
		_not_specified_version_description = 'Neuvedeno.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
	else
		_not_specified_version_label := 'not specified'::text;
		_not_specified_version_description = 'Not specified.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
					((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en,		
					((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
					((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_indicator_label, _res_indicator_description;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state_or_change'')->>''label'' as label,
					((ctv.metadata->''en'')->''state_or_change'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''state_or_change'')->>''description'' as description,
					((ctv.metadata->''en'')->''state_or_change'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_state_or_change_label, _res_state_or_change_description;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_unit_label, _res_unit_description;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 04: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_area_domain_attribute_json := json_agg(json_build_object('label','bez rozlišení','description','Bez rozlišení.'));
		else
			_res_area_domain_attribute_json := json_agg(json_build_object('label','without distinction','description','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(cad.label'||_lang_suffix||','';'') as label,
						string_to_array(cad.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_area_domain as cad
				where
						cad.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_area_domain[1]
		into _res_area_domain_attribute_json;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 05: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_sub_population_attribute_json := json_agg(json_build_object('label','bez rozlišení','description','Bez rozlišení.'));
		else
			_res_sub_population_attribute_json := json_agg(json_build_object('label','without distinction','description','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(csp.label'||_lang_suffix||','';'') as label,
						string_to_array(csp.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_sub_population as csp
				where
						csp.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_sub_population[1]
		into _res_sub_population_attribute_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local_densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'core'
	into _check_core_ldsities;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- CORE JSON
	execute
	'
	with
	w1 as	(
			select
					(ctv.metadata->''cs'')->''local_densities'' as local_densities,
					(ctv.metadata->''en'')->''local_densities'' as local_densities_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	,w2 as	(
			select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
			)
	,w3 as	(
			select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''core''
			)
	,w4 as	(
			select
					row_number() over() as new_id,
					(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
					(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
					(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
					(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
					(w3.local_densities->''ldsity'')->>''label'' as label,
					(w3.local_densities->''ldsity'')->>''description'' as description,	
					w3.local_densities->>''use_negative'' as use_negative,
					(w3.local_densities->''version'')->>''label'' as version_label,
					(w3.local_densities->''version'')->>''description'' as version_description,
					w3.local_densities->''definition_variant'' as definition_variant,
					w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
					w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
			from
					w3
			)
	,w5 as	(
			select
					w4.new_id,
					w4.object_type_label,
					w4.object_type_description,
					w4.object_label,
					w4.object_description,
					w4.label,
					w4.description,
					w4.use_negative,
					case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
					case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
					case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
					case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
					case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
			from
					w4
			)
	,w6 as	(
			select
					w5.new_id,
					json_build_object
						(
						''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
						''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
						''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
						''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
						''definition_variant'',w5.definition_variant,
						''area_domain_restrictions'',w5.area_domain_restrictions,
						''sub_population_restrictions'',w5.sub_population_restrictions,
						''use_negative'',w5.use_negative
						) as  local_densities
			from
					w5
			)
	select
			json_agg(w6.local_densities order by w6.new_id) as local_densities
	from
			w6
	'
	using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
	into _res_core_json;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- DIVISION JSON
	if _check_division_ldsities = 0
	then
		_res_division_json := null::json;
	else
		execute
		'
		with
		w1 as	(
				select
						(ctv.metadata->''cs'')->''local_densities'' as local_densities,
						(ctv.metadata->''en'')->''local_densities'' as local_densities_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		,w2 as	(
				select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
				)
		,w3 as	(
				select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w4 as	(
				select
						row_number() over() as new_id,
						(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w3.local_densities->''ldsity'')->>''label'' as label,
						(w3.local_densities->''ldsity'')->>''description'' as description,	
						w3.local_densities->>''use_negative'' as use_negative,
						(w3.local_densities->''version'')->>''label'' as version_label,
						(w3.local_densities->''version'')->>''description'' as version_description,
						w3.local_densities->''definition_variant'' as definition_variant,
						w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w3
				)
		,w5 as	(
				select
						w4.new_id,
						w4.object_type_label,
						w4.object_type_description,
						w4.object_label,
						w4.object_description,
						w4.label,
						w4.description,
						w4.use_negative,
						case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
						case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
						case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
						case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
						case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
				from
						w4
				)
		,w6 as	(
				select
						w5.new_id,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
							''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
							''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
							''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
							''definition_variant'',w5.definition_variant,
							''area_domain_restrictions'',w5.area_domain_restrictions,
							''sub_population_restrictions'',w5.sub_population_restrictions,
							''use_negative'',w5.use_negative
							) as  local_densities
				from
						w5
				)
		select
				json_agg(w6.local_densities order by w6.new_id) as local_densities
		from
				w6
		'
		using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
		into _res_division_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities = 0
	then
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json
		))
		into _res;
	else
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json,
			'division_contributions',_res_division_json
		))
		into _res;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]) is
'The function returns metadata (description) for given input ID (that represents target variable for nominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata" schema="nfiesta_results" src="functions/fn_get_user_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang_suffix								character varying(3);
	_lang										text;
	_estimate_type								integer;
	_target_variable							integer;
	_target_variable_denom						integer;
	_estimation_period							integer;
	_panel_refyearset_group						integer;
	_phase_estimate_type						integer;
	_phase_estimate_type_denom					integer;
	_estimation_cell_collection					integer;
	_area_domain								integer;
	_area_domain_denom							integer;
	_sub_population								integer;
	_sub_population_denom						integer;
	--_target_variable_num						integer[];
	--_target_variable_denom_check				integer;
	--_target_variable_denom					integer;
	_cond4topic_num								text;
	_cond4topic_denom							text;
	_res_topic_label							text;
	_res_topic_description						text;
	_res_estimation_period_label				text;
	_res_estimation_period_description			text;
	_res_estimation_cell_collection_label		text;
	_res_estimation_cell_collection_description	text;
	_res_estimate_type_label					text;
	_res_estimate_type_description				text;
	_res_unit_num_label							text;
	_res_unit_num_description					text;
	_res_unit_of_measurement_label				text;
	_res_unit_of_measurement_description		text;
	_res										json;

	_estimation_cell_from_hierarchy_for_ecc_select_by_user			integer;
	_estimation_cell_from_hierarchy_for_ecc_select_by_user_superior	integer;
	_heighest_estimation_cell_select_by_user						integer;
	_heighest_estimation_cell_collection_select_by_user				integer;
	_estimation_cell_collection_array								integer[];
	_test_occurence_result_group									integer;	
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_query: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select
			trg.estimate_type,
			trg.target_variable,
			trg.target_variable_denom,
			trg.estimation_period,
			trg.panel_refyearset_group,
			trg.phase_estimate_type,
			trg.phase_estimate_type_denom,
			trg.estimation_cell_collection,
			trg.area_domain,
			trg.area_domain_denom,
			trg.sub_population,
			trg.sub_population_denom
	from
			nfiesta_results.t_result_group as trg
	where
			trg.id = _id_group[1]
	and
			trg.web = true
	into
			_estimate_type,
			_target_variable,
			_target_variable_denom,
			_estimation_period,
			_panel_refyearset_group,
			_phase_estimate_type,
			_phase_estimate_type_denom,
			_estimation_cell_collection,
			_area_domain,
			_area_domain_denom,
			_sub_population,
			_sub_population_denom;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	select count(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom_check;

	if _target_variable_denom_check is distinct from 0 -- target variable denom exists
	then
		if _target_variable_denom_check > 1
		then
			raise exception 'Error: 03: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
		end if;

		select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
		where trg.id in (select unnest(_id_group))
		and trg.web = true
		into _target_variable_denom;		
	else
		_target_variable_denom := null::integer;
	end if;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	execute
	'
	with
	w1 as	(
			select crt.topic from nfiesta_results.cm_result2topic as crt
			where ' || _cond4topic_num ||'
			and ' || _cond4topic_denom ||'
			)
	,w2 as	(
			select
					label'||_lang_suffix||' AS label,
					description'||_lang_suffix||' AS description
			from
					nfiesta_results.c_topic
			where
					id in (select distinct w1.topic from w1)
			)
	,w3 as	(
			select
					array_agg(w2.label order by w2.description) as label,
					array_agg(w2.description order by w2.description) as description
			from
					w2
			)
	select
			array_to_string(w3.label, ''; ''),
			array_to_string(w3.description, ''; '')
	from
			w3
	'
	using _target_variable, _target_variable_denom
	into _res_topic_label, _res_topic_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	execute
	'
	select
			cep.label'||_lang_suffix||' AS label,
			cep.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_period as cep
	where
			cep.id =	(
						select distinct trg.estimation_period
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						and trg.web = true
						)
	'
	using _id_group
	into _res_estimation_period_label, _res_estimation_period_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	select tech.cell from nfiesta_results.t_estimation_cell_hierarchy as tech
	where tech.cell in	(
						select cec.id from nfiesta_results.c_estimation_cell as cec
						where cec.estimation_cell_collection =	_estimation_cell_collection
						)
	order by cell limit 1
	into _estimation_cell_from_hierarchy_for_ecc_select_by_user;

	if _estimation_cell_from_hierarchy_for_ecc_select_by_user is null
	then
		-- selected estimation cell collection is maybe the heighest
		select count(tech.cell_superior) from nfiesta_results.t_estimation_cell_hierarchy as tech
		where tech.cell_superior in	(
									select cec.id from nfiesta_results.c_estimation_cell as cec
									where cec.estimation_cell_collection = _estimation_cell_collection
									)
		into _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior;

		if _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior = 0
		then
			raise exception 'Error: 04: fn_get_user_metadata: For user selected estimation_cell_collection = % not exists any estimation cell in t_estimation_cell_hierarchy table!',_estimation_cell_collection;
		end if;

		-- user selected the heighest estimation cell collection => test for occurrence in t_result_group table is not needed
		_estimation_cell_collection_array := array[_estimation_cell_collection];		
	else
		with
		w1 as	(
				select * from nfiesta_results.fn_get_heighest_estimation_cell(array[_estimation_cell_from_hierarchy_for_ecc_select_by_user]) as res
				)
		select w1.res[1] from w1
		into _heighest_estimation_cell_select_by_user; -- the heighest estimation cell from hierarchy

		select cec.estimation_cell_collection from nfiesta_results.c_estimation_cell as cec
		where cec.id = _heighest_estimation_cell_select_by_user
		into _heighest_estimation_cell_collection_select_by_user; -- the heighest estimation cell collection from hierarchy

		-- test for occurrence in t_result_group
		select id from nfiesta_results.t_result_group
		where estimate_type = _estimate_type
		and target_variable = _target_variable
		and coalesce(target_variable_denom,0) = coalesce(_target_variable_denom,0)
		and estimation_period = _estimation_period
		and phase_estimate_type = _phase_estimate_type
		and coalesce(phase_estimate_type_denom,0) = coalesce(_phase_estimate_type_denom,0)
		and estimation_cell_collection = _heighest_estimation_cell_collection_select_by_user
		and coalesce(area_domain,0) = coalesce(_area_domain,0)
		and coalesce(area_domain_denom,0) = coalesce(_area_domain_denom,0)
		and coalesce(sub_population,0) = coalesce(_sub_population,0)
		and coalesce(sub_population_denom,0) = coalesce(_sub_population_denom,0)
		and web = true
		into _test_occurence_result_group;

		if _test_occurence_result_group is null
		then
			_estimation_cell_collection_array := array[_estimation_cell_collection];
		else
			_estimation_cell_collection_array := array[_estimation_cell_collection] || array[_heighest_estimation_cell_collection_select_by_user];
		end if;
	end if;	

	execute
	'
	with
	w1 as	(
			select
					cecc.id,
					cecc.label'||_lang_suffix||' AS label,
					cecc.description'||_lang_suffix||' AS description
			from
					nfiesta_results.c_estimation_cell_collection as cecc
			where
					cecc.id in (select unnest($1))
			)
	,w2 as	(
			select
					array_agg(w1.label order by w1.id) as label,
					array_agg(w1.description order by w1.id) as description
			from
					w1
			)
			select
					array_to_string(w2.label,''; '') as label,
					array_to_string(w2.description,''; '') as description
			from
				w2
	'
	using _estimation_cell_collection_array
	into _res_estimation_cell_collection_label, _res_estimation_cell_collection_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATE TYPE
	if _target_variable_denom is null
	then
		execute
		'
		select
				cet.label,
				cet.description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 1
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;
	else
		execute
		'
		select
				cet.label,
				cet.description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 2
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable
	into _res_unit_num_label, _res_unit_num_description;		
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASUREMENT
	if _target_variable_denom is null
	then
		_res_unit_of_measurement_label := _res_unit_num_label;
		_res_unit_of_measurement_description := _res_unit_num_description;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''label'' as unit_label_num,
						((w2.metadata->''cs'')->''unit'')->>''label'' as unit_label_denom,
						((w1.metadata->''en'')->''unit'')->>''label'' as unit_label_num_en,
						((w2.metadata->''en'')->''unit'')->>''label'' as unit_label_denom_en,						
						((w1.metadata->''cs'')->''unit'')->>''description'' as unit_description_num,
						((w2.metadata->''cs'')->''unit'')->>''description'' as unit_description_denom,
						((w1.metadata->''en'')->''unit'')->>''description'' as unit_description_num_en,
						((w2.metadata->''en'')->''unit'')->>''description'' as unit_description_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_label_num'||_lang_suffix||' AS unit_label_num,
						w3.unit_label_denom'||_lang_suffix||' AS unit_label_denom,						
						w3.unit_description_num'||_lang_suffix||' AS unit_description_num,
						w3.unit_description_denom'||_lang_suffix||' AS unit_description_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_label_num = w4.unit_label_denom) then ''%''
							else concat(w4.unit_label_num,'' / '',w4.unit_label_denom)
						end as label4user,						
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_description_num = w4.unit_description_denom) then ''%''
							else concat(w4.unit_description_num,'' / '',w4.unit_description_denom)
						end as description4user
				from
						w4
				)
		select
				w5.label4user,
				w5.description4user
		from
				w5
		'
		using _target_variable, _target_variable_denom
		into _res_unit_of_measurement_label, _res_unit_of_measurement_description;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _target_variable_denom is null
	then
		-- basic metadata + numerator metadata
		if _lang_suffix = ''
		then
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('cs-CZ',_id_group))
			)
			into _res;
		else
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('en-GB',_id_group))
			)
			into _res;		
		end if;
	else
		-- basic metadata + numerator metadata + denominator metadata
		if _lang_suffix = ''
		then
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('cs-CZ',_id_group)),
				'denominator',(select * from nfiesta_results.fn_get_user_metadata_denominator('cs-CZ',_id_group))
			)
			into _res;	
		else
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('en-GB',_id_group)),
				'denominator',(select * from nfiesta_results.fn_get_user_metadata_denominator('en-GB',_id_group))
			)
			into _res;			
		end if;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) is
'The function returns metadata for given input ID (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_sub_population" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_sub_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_sub_population
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_sub_population
(
	_jlang character varying,
	_id_group integer[],
	_sub_population integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_sub_population integer[],
	res_division boolean
)
as
$$
declare
	_lang_suffix							character varying(3);
	_target_variable_denom					integer[];
	_check_records							integer;

	_count_target_variable					integer;
	_count_target_variable_with_core		integer;
	_count_target_variable_with_division	integer;
	_res_division							boolean;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_denominator_sub_population: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom;

	if array_length(_target_variable_denom,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_denominator_sub_population: The values in input argument _id_group = % must only be for the same target variable denominator!',_id_group;
	end if;
	-----------------------------------------------------------------
	if _sub_population is null -- first calling of function
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)	
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17a as	(
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							array[w16.atomic_type] as res_sub_population
					from
							w16
					)
		,w17 as	(
				select
						w17a.res_id,
						w17a.res_label,
						w17a.res_id_group,
						w17a.res_sub_population,
						null::boolean as res_division
				from
						w17a
				)
		-----------------------------------------
		,w18 as	(
				select
						a.*,
						b.target_variable
				from
						(
						select
								w17.res_id,
								w17.res_label,
								w17.res_sub_population,
								unnest(w17.res_id_group) as res_id_group
						from
								w17 where w17.res_id = 0
						) as a
				inner join (select * from nfiesta_results.t_result_group where web = true) as b on a.res_id_group = b.id
				)
		,w19 as	(
				select
						ctv.id,
						(ctv.metadata->''en'')->''local_densities'' as local_densities
				from
						nfiesta_results.c_target_variable as ctv where ctv.id in (select distinct w18.target_variable from w18)
				)
		,w20 as	(
				select
						w19.id,
						json_array_elements(w19.local_densities) as local_densities
				from
						w19
				)
		,w21 as	(
				select
						w20.id,
						(w20.local_densities->''ldsity_type'')->>''label'' as object_type_label
				from
						w20
				)
		,w22 as	(
				select w18.* from w18 where w18.target_variable in (select w21.id from w21 where w21.object_type_label = ''division'')
				)
		,w23 as	(
				select w18.* from w18 where w18.target_variable not in (select distinct w22.target_variable from w22) -- TARGET VARIABLE WITHOUT DIVISION
				)
		,w24a as	(
					select
							w23.res_id,
							w23.res_label,
							w23.res_sub_population,
							array_agg(res_id_group) as res_id_group
					from
							w23 group by w23.res_id, w23.res_label, w23.res_sub_population
					)
		,w24 as	(
				select
						w24a.res_id,
						w24a.res_label,
						w24a.res_sub_population,
						w24a.res_id_group,
						null::boolean as res_division
				from
						w24a
				)
				select w24.res_id, w24.res_label, w24.res_id_group, w24.res_sub_population, w24.res_division from w24 union all
				select w17.res_id, w17.res_label, w17.res_id_group, w17.res_sub_population, w17.res_division from w17 where w17.res_id is distinct from 0;
		'
		using _id_group;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as sub_population,
								t.id
						from
								(
								select w11.sub_population, w11.id, w11.atomic_type from w11 union all
								select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_sub_population
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _sub_population
		into _check_records;	
		
		if _check_records = 0
		then
			-------------------------------------
			-- input _id_group must be for the same group of target variables:
			-- variant 1 => target variables without division ldsity object(s)
			-- variant 2 => target variables with core ldsity object and with division ldsity object(s)
			-------------------------------------
			with
			w1 as	(
					select
							ctv.id,
							(ctv.metadata->'en')->'local_densities' as local_densities
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id in	(
										select trg.target_variable
										from nfiesta_results.t_result_group as trg
										where trg.id in (select unnest(_id_group))
										and trg.web = true
										)
					)
			,w2 as	(
					select
							w1.id,
							json_array_elements(w1.local_densities) as local_densities
					from
							w1
					)
			,w3 as	(
					select
							w2.id,
							(w2.local_densities->'ldsity_type')->>'label' as object_type_label
					from
							w2
					)
			-------------------------------
			,w4a as	(select distinct w3.id from w3)
			,w4b as	(select distinct w3.id from w3 where w3.object_type_label = 'core')
			,w4c as	(select distinct w3.id from w3 where w3.object_type_label = 'division')
			-------------------------------
			,w5a as	(select 1 as id, count(w4a.*) as count_target_variable from w4a)
			,w5b as	(select 1 as id, count(w4b.*) as count_target_variable_with_core from w4b)
			,w5c as	(select 1 as id, count(w4c.*) as count_target_variable_with_division from w4c)
			-------------------------------
			select
					w5a.count_target_variable,
					w5b.count_target_variable_with_core,
					w5c.count_target_variable_with_division
			from
					w5a
					inner join w5b on w5a.id = w5b.id
					inner join w5c on w5a.id = w5c.id
			into
					_count_target_variable,
					_count_target_variable_with_core,
					_count_target_variable_with_division;
			-------------------------------------
			-------------------------------------
			if _sub_population = array[0] -- first user selection was without distinction
			then
				if _count_target_variable_with_division is distinct from 0
				then
					raise exception 'Error: 03: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then the values in input argument _id_group = % must only be for target variables that are without any division local density!',_id_group;
				end if;

				-- variant 1
				if _count_target_variable is distinct from _count_target_variable_with_core
				then
					raise exception 'Error: 04: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						_sub_population as res_sub_population,
						false as res_division; 
			else
				-- second and more user selections

				if _count_target_variable_with_division = 0
				then
					-- variant 1 => not exists any target variable with division
					if _count_target_variable is distinct from _count_target_variable_with_core
					then
						raise exception 'Error: 05: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
					end if;

					_res_division := false;
				else
					-- variant 2 => exist target variable with any division
					if	(_count_target_variable is distinct from _count_target_variable_with_division)
						or
						(_count_target_variable_with_core is distinct from _count_target_variable_with_division)
					then
						raise exception 'Error: 06: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables must contains some division local density!',_id_group;
					end if;

					_res_division := true;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						array_remove(_sub_population,0) as res_sub_population,
						_res_division as res_division;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.sub_population] as sub_population,
							t.id
					from
							(
							select w1.sub_population, array_agg(w1.id order by w1.id) as id
							from w1 where w1.sub_population = 0
							group by w1.sub_population
							) as t
					)
			,w3 as	(
					select w1.sub_population, array_agg(w1.id order by w1.id) as id
					from w1 where w1.sub_population > 0
					group by w1.sub_population
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_sub_population as csp
							on w3.sub_population = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_sub_population_category as cmspc
					where cmspc.sub_population_category in
						(
						select cspc.id from nfiesta_results.c_sub_population_category as cspc
						where cspc.sub_population in (select w6.sub_population from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.sub_population as sub_population,
							cspc2.sub_population as atomic_type
					from
							w7
							inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
							inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.sub_population from w8
					)
			,w10 as (
					select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.sub_population
					)
			,w11 as (
					select
							w6.sub_population,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.sub_population = w10.sub_population
					)
			,w12 as	(
					select
							w11.sub_population,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.sub_population,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.sub_population,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
							from	(
									select distinct a1.atomic_type, a1.sub_population
									from
										(
										select w13.atomic_type, w13.sub_population from w13
										union all
										select w5.sub_population as atomic_type, w5.sub_population from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as sub_population,
									null::integer[] as sub_population,
									t.id
							from
									(
									select w11.sub_population, w11.id, w11.atomic_type from w11 union all
									select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
					)
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_sub_population,
							null::boolean as res_division
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _sub_population;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]) is
'The function returns list of available sub populations for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of sub populations.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_metadata" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	column_1	text,
	column_2	text,
	column_3	text
)
language plpgsql
stable
as
$$
declare
	_lang_suffix					character varying(3);
	_lang							text;
	_target_variable_num			integer[];
	_cond4topic_num					text;
	_res_topic						text;
	_res_estimation_period			text;
	_res_estimation_cell_collection	text;
	_res_indicator_num				text;
	_res_state_or_change_num		text;
	_res_unit_num					text;
	_check_area_domain				integer[];
	_res_area_domain_attribute		text;
	_check_sub_population			integer[];
	_res_sub_population_attribute	text;
	_check_core_ldsities			integer;
	_check_division_ldsities		integer;
	_main_query						text;
	_main_query_core				text;
	_main_query_division			text;
	_example_for_i_core				text;
	_example_for_i_division			text;
	_query_core_i					text;
	_query_core						text;
	_query_division_i				text;
	_query_division					text;
	_query_core_and_division		text;
	_query_local_densities			text;
	_res							text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_numerator_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_numerator_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	_cond4topic_num := 'crt.target_variable = $1';

	execute
	'
	with
	w1 as	(
			select crt.topic from nfiesta_results.cm_result2topic as crt
			where ' || _cond4topic_num ||'
			and crt.denominator is null
			)
	,w2 as	(
			select
					label'||_lang_suffix||' AS label
			from
					nfiesta_results.c_topic
			where
					id in (select distinct w1.topic from w1)
			)
	,w3 as	(
			select array_agg(w2.label order by w2.label) as label
			from w2
			)
			select array_to_string(w3.label, ''; '') from w3
	'
	using _target_variable_num[1]
	into _res_topic;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	execute
	'
	select
			cep.label'||_lang_suffix||' AS label
	from
			nfiesta_results.c_estimation_period as cep
	where
			cep.id =	(
						select distinct trg.estimation_period
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						and trg.web = true
						)
	'
	using _id_group
	into _res_estimation_period;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	execute
	'
	select
			cecc.label'||_lang_suffix||' AS label
	from
			nfiesta_results.c_estimation_cell_collection as cecc
	where
			cecc.id =	(
						select distinct trg.estimation_cell_collection
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						and trg.web = true
						)
	'
	using _id_group
	into _res_estimation_cell_collection;
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
					((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label
	from
			w1
	'
	using _target_variable_num[1]
	into _res_indicator_num;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state or change'')->>''label'' as label,
					((ctv.metadata->''en'')->''state or change'')->>''label'' as label_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label
	from
			w1
	'
	using _target_variable_num[1]
	into _res_state_or_change_num;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label
	from
			w1
	'
	using _target_variable_num[1]
	into _res_unit_num;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_options_numerator_metadata: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_area_domain_attribute := 'bez rozlišení';
		else
			_res_area_domain_attribute := 'without distinction';
		end if;
	else
		execute
		'
		select
				cad.label'||_lang_suffix||' as label
		from
				nfiesta_results.c_area_domain as cad
		where
				cad.id = $1
		'
		using _check_area_domain[1]
		into _res_area_domain_attribute;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 04: fn_get_user_options_numerator_metadata: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_sub_population_attribute := 'bez rozlišení';
		else
			_res_sub_population_attribute := 'without distinction';
		end if;
	else
		execute
		'
		select
				csp.label'||_lang_suffix||' as label
		from
				nfiesta_results.c_sub_population as csp
		where
				csp.id = $1
		'
		using _check_sub_population[1]
		into _res_sub_population_attribute;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable_num[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2 where w2.local_densities->>'object type label' = 'core'
	into _check_core_ldsities;
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable_num[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2 where w2.local_densities->>'object type label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	_main_query := concat
	(
	'
	,w1 as	(
			select
					t.local_densities'||_lang_suffix||' as local_densities
			from
					(select
							(ctv.metadata->''cs'')->''local densities'' as local_densities,
							(ctv.metadata->''en'')->''local densities'' as local_densities_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = ',_target_variable_num[1],'
					) as t
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	'
	);
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	_main_query_core :=
	'
	,w3_core as	(
				select
						row_number () over () as new_id,
						w2.*
				from
						w2 where w2.local_densities->>''object type label'' = ''core''
				)		
	';
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	_main_query_division :=
	'
	,w3_division as	(
					select
							row_number () over () as new_id,
							w2.*
					from
							w2 where w2.local_densities->>''object type label'' = ''division''
					)		
	';	
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- EXAMPLE for i-CORE
	if _lang_suffix = '' -- CS --
	then
		_example_for_i_core :=
		'
		--------------------------------------------------------------------------
		,w4_core_#I_CORE# as	(
								select ''   základní:'' as column_1, w3_core.local_densities->>''label'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      objekt:'' as column_1, w3_core.local_densities->>''object label'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      verze:'' as column_1, (w3_core.local_densities->''version'')->>''label'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      použití záporu:'' as column_1, w3_core.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE#
								)
		--------------------------------------------------------------------------
		,w5_core_#I_CORE# as	(	
								select
										case when t3.new_id_dv = 1 then ''      definiční varianty:'' else null::text end as column_1,
										t3.dv as column_2,
										null::text as column_3
								from
										(
										select
												row_number () over () as new_id_dv,
												t2.dv->>''label'' as dv
										from
												(
												select json_array_elements(t1.dv) as dv from 
												(select case when (w3_core.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''neuvedeno'',''description'',''neuvedeno'')))
												else (w3_core.local_densities->''definition variant'') end as dv from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w6_core_#I_CORE# as	(
								select null::text as column_1, ''objekt:'' as column_2, ''omezení:'' as column_3
								)
		--------------------------------------------------------------------------
		,w7_core_#I_CORE# as	(
								select
										case when t3.new_id_adr = 1 then ''      plošná omezení:'' else null::text end as column_1,
										t3.adr_object as column_2,
										t3.adr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_adr,
												(t2.adr->''object'')->>''label'' as adr_object,
												(t2.adr->''restriction'')->>''label'' as adr_restriction
										from
												(
												select json_array_elements(t1.adr) as adr from 
												(select case when (w3_core.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''label'',''neuvedeno''),''restriction'',json_build_object(''label'',''neuvedeno''))))
												else (w3_core.local_densities->''area domain restrictions'') end as adr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w8_core_#I_CORE# as	(
								select
										case when t3.new_id_spr = 1 then ''      sub-populační omezení:'' else null::text end as column_1,
										t3.spr_object as column_2,
										t3.spr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_spr,
												(t2.spr->''object'')->>''label'' as spr_object,
												(t2.spr->''restriction'')->>''label'' as spr_restriction
										from
												(
												select json_array_elements(t1.spr) as spr from 
												(select case when (w3_core.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''label'',''neuvedeno''),''restriction'',json_build_object(''label'',''neuvedeno''))))
												else (w3_core.local_densities->''sub population restrictions'') end as spr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)			
		--------------------------------------------------------------------------
		,w9_core_#I_CORE# as	(
								select * from w4_core_#I_CORE# union all
								select * from w5_core_#I_CORE# union all
								select * from w6_core_#I_CORE# union all
								select * from w7_core_#I_CORE# union all
								select * from w8_core_#I_CORE#
								)		
		';
	else
		_example_for_i_core :=
		'
		--------------------------------------------------------------------------
		,w4_core_#I_CORE# as	(
								select ''   main:'' as column_1, w3_core.local_densities->>''label'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      object:'' as column_1, w3_core.local_densities->>''object label'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      version:'' as column_1, (w3_core.local_densities->''version'')->>''label'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      use negative:'' as column_1, w3_core.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE#
								)
		--------------------------------------------------------------------------
		,w5_core_#I_CORE# as	(	
								select
										case when t3.new_id_dv = 1 then ''      definition variants:'' else null::text end as column_1,
										t3.dv as column_2,
										null::text as column_3
								from
										(
										select
												row_number () over () as new_id_dv,
												t2.dv->>''label'' as dv
										from
												(
												select json_array_elements(t1.dv) as dv from 
												(select case when (w3_core.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''not specified'',''description'',''not specified'')))
												else (w3_core.local_densities->''definition variant'') end as dv from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w6_core_#I_CORE# as	(
								select null::text as column_1, ''object:'' as column_2, ''restriction:'' as column_3
								)
		--------------------------------------------------------------------------
		,w7_core_#I_CORE# as	(
								select
										case when t3.new_id_adr = 1 then ''      area domain restrictions:'' else null::text end as column_1,
										t3.adr_object as column_2,
										t3.adr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_adr,
												(t2.adr->''object'')->>''label'' as adr_object,
												(t2.adr->''restriction'')->>''label'' as adr_restriction
										from
												(
												select json_array_elements(t1.adr) as adr from 
												(select case when (w3_core.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''label'',''not specified''),''restriction'',json_build_object(''label'',''not specified''))))
												else (w3_core.local_densities->''area domain restrictions'') end as adr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w8_core_#I_CORE# as	(
								select
										case when t3.new_id_spr = 1 then ''      sub population restrictions:'' else null::text end as column_1,
										t3.spr_object as column_2,
										t3.spr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_spr,
												(t2.spr->''object'')->>''label'' as spr_object,
												(t2.spr->''restriction'')->>''label'' as spr_restriction
										from
												(
												select json_array_elements(t1.spr) as spr from 
												(select case when (w3_core.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''label'',''not specified''),''restriction'',json_build_object(''label'',''not specified''))))
												else (w3_core.local_densities->''sub population restrictions'') end as spr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)			
		--------------------------------------------------------------------------
		,w9_core_#I_CORE# as	(
								select * from w4_core_#I_CORE# union all
								select * from w5_core_#I_CORE# union all
								select * from w6_core_#I_CORE# union all
								select * from w7_core_#I_CORE# union all
								select * from w8_core_#I_CORE#
								)		
		';	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- EXAMPLE for i-DIVISION
	if _lang_suffix = '' -- CS --
	then
		_example_for_i_division :=
		'
		--------------------------------------------------------------------------
		,w4_division_#I_DIVISION# as	(
										select ''   členící:'' as column_1, w3_division.local_densities->>''label'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      objekt:'' as column_1, w3_division.local_densities->>''object label'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      verze:'' as column_1, (w3_division.local_densities->''version'')->>''label'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      použití záporu:'' as column_1, w3_division.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION#
										)
		--------------------------------------------------------------------------
		,w5_division_#I_DIVISION# as	(	
										select
												case when t3.new_id_dv = 1 then ''      definiční varianty:'' else null::text end as column_1,
												t3.dv as column_2,
												null::text as column_3
										from
												(
												select
														row_number () over () as new_id_dv,
														t2.dv->>''label'' as dv
												from
														(
														select json_array_elements(t1.dv) as dv from 
														(select case when (w3_division.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''neuvedeno'',''description'',''neuvedeno'')))
														else (w3_division.local_densities->''definition variant'') end as dv from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w6_division_#I_DIVISION# as	(
										select null::text as column_1, ''objekt:'' as column_2, ''omezení:'' as column_3
										)
		--------------------------------------------------------------------------
		,w7_division_#I_DIVISION# as	(
										select
												case when t3.new_id_adr = 1 then ''      plošná omezení:'' else null::text end as column_1,
												t3.adr_object as column_2,
												t3.adr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_adr,
														(t2.adr->''object'')->>''label'' as adr_object,
														(t2.adr->''restriction'')->>''label'' as adr_restriction
												from
														(
														select json_array_elements(t1.adr) as adr from 
														(select case when (w3_division.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''label'',''neuvedeno''),''restriction'',json_build_object(''label'',''neuvedeno''))))
														else (w3_division.local_densities->''area domain restrictions'') end as adr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w8_division_#I_DIVISION# as	(
										select
												case when t3.new_id_spr = 1 then ''      sub-populační omezení:'' else null::text end as column_1,
												t3.spr_object as column_2,
												t3.spr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_spr,
														(t2.spr->''object'')->>''label'' as spr_object,
														(t2.spr->''restriction'')->>''label'' as spr_restriction
												from
														(
														select json_array_elements(t1.spr) as spr from 
														(select case when (w3_division.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''label'',''neuvedeno''),''restriction'',json_build_object(''label'',''neuvedeno''))))
														else (w3_division.local_densities->''sub population restrictions'') end as spr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)			
		--------------------------------------------------------------------------
		,w9_division_#I_DIVISION# as	(
										select * from w4_division_#I_DIVISION# union all
										select * from w5_division_#I_DIVISION# union all
										select * from w6_division_#I_DIVISION# union all
										select * from w7_division_#I_DIVISION# union all
										select * from w8_division_#I_DIVISION#
										)		
		';
	else
		_example_for_i_division :=
		'
		--------------------------------------------------------------------------
		,w4_division_#I_DIVISION# as	(
										select ''   separated:'' as column_1, w3_division.local_densities->>''label'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      object:'' as column_1, w3_division.local_densities->>''object label'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      version:'' as column_1, (w3_division.local_densities->''version'')->>''label'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      use negative:'' as column_1, w3_division.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION#
										)
		--------------------------------------------------------------------------
		,w5_division_#I_DIVISION# as	(	
										select
												case when t3.new_id_dv = 1 then ''      definition variants:'' else null::text end as column_1,
												t3.dv as column_2,
												null::text as column_3
										from
												(
												select
														row_number () over () as new_id_dv,
														t2.dv->>''label'' as dv
												from
														(
														select json_array_elements(t1.dv) as dv from 
														(select case when (w3_division.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''not specified'',''description'',''not specified'')))
														else (w3_division.local_densities->''definition variant'') end as dv from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w6_division_#I_DIVISION# as	(
										select null::text as column_1, ''object:'' as column_2, ''restriction:'' as column_3
										)
		--------------------------------------------------------------------------
		,w7_division_#I_DIVISION# as	(
										select
												case when t3.new_id_adr = 1 then ''      area domain restrictions:'' else null::text end as column_1,
												t3.adr_object as column_2,
												t3.adr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_adr,
														(t2.adr->''object'')->>''label'' as adr_object,
														(t2.adr->''restriction'')->>''label'' as adr_restriction
												from
														(
														select json_array_elements(t1.adr) as adr from 
														(select case when (w3_division.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''label'',''not specified''),''restriction'',json_build_object(''label'',''not specified''))))
														else (w3_division.local_densities->''area domain restrictions'') end as adr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w8_division_#I_DIVISION# as	(
										select
												case when t3.new_id_spr = 1 then ''      sub population restrictions:'' else null::text end as column_1,
												t3.spr_object as column_2,
												t3.spr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_spr,
														(t2.spr->''object'')->>''label'' as spr_object,
														(t2.spr->''restriction'')->>''label'' as spr_restriction
												from
														(
														select json_array_elements(t1.spr) as spr from 
														(select case when (w3_division.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''label'',''not specified''),''restriction'',json_build_object(''label'',''not specified''))))
														else (w3_division.local_densities->''sub population restrictions'') end as spr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)			
		--------------------------------------------------------------------------
		,w9_division_#I_DIVISION# as	(
										select * from w4_division_#I_DIVISION# union all
										select * from w5_division_#I_DIVISION# union all
										select * from w6_division_#I_DIVISION# union all
										select * from w7_division_#I_DIVISION# union all
										select * from w8_division_#I_DIVISION#
										)		
		';	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	for i_core in 1.._check_core_ldsities
	loop
		if i_core = 1
		then
			_query_core_i := replace(_example_for_i_core,'#I_CORE#'::text,i_core::text);
			_query_core := concat('select * from w9_core_',i_core);
		else
			_query_core_i := _query_core_i || replace(_example_for_i_core,'#I_CORE#'::text,i_core::text);
			_query_core := _query_core || concat(' union all select null::text as column_1, null::text as column_2, null::text as column_3 union all select * from w9_core_',i_core);
		end if;
	end loop;

	_query_core := concat(',w10_core as (',_query_core,')');
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities > 0
	then
		for i_division in 1.._check_division_ldsities
		loop
			if i_division = 1
			then
				_query_division_i := replace(_example_for_i_division,'#I_DIVISION#'::text,i_division::text);
				_query_division := concat('select * from w9_division_',i_division);
			else
				_query_division_i := _query_division_i || replace(_example_for_i_division,'#I_DIVISION#'::text,i_division::text);
				_query_division := _query_division || concat(' union all select null::text as column_1, null::text as column_2, null::text as column_3 union all select * from w9_division_',i_division);
			end if;
		end loop;

		_query_division := concat(',w10_division as (',_query_division,')');
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities > 0
	then
		_query_core_and_division :=
		'
		,w02 as	(
				select * from w10_core union all
				select null::text as column_1, null::text as column_2, null::text as column_3 union all
				select * from w10_division
				)';
	else
		_query_core_and_division := ',w02 as (select * from w10_core)';
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities > 0
	then
		_query_local_densities := concat
			(
			_main_query,
			_main_query_core,
			_main_query_division,
			_query_core_i,
			_query_core,
			_query_division_i,
			_query_division,
			_query_core_and_division
			);
	else
		_query_local_densities := concat
			(
			_main_query,
			_main_query_core,
			_query_core_i,
			_query_core,
			_query_core_and_division
			);	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _lang_suffix = '' -- CS
	then
		_res := concat(
		'
		with
		w01 as	(
				select
						''Tematický okruh:'' as column_1,
						''',_res_topic,''' as column_2,
						null::text as column_3
				union all
				select
						''Období:'' as column_1,
						''',_res_estimation_period,''' as column_2,
						null::text as column_3
				union all
				select
						''Geografické členění:'' as column_1,
						''',_res_estimation_cell_collection,''' as column_2,
						null::text as column_3
				union all
				select
						''Indikátor:'' as column_1,
						''',_res_indicator_num,''' as column_2,
						null::text as column_3
				union all
				select
						''Stav nebo změna:'' as column_1,
						''',_res_state_or_change_num,''' as column_2,
						null::text as column_3
				union all
				select
						''Jednotka indikátoru:'' as column_1,
						''',_res_unit_num,''' as column_2,
						null::text as column_3
				union all
				select
						''Plošná atributová členění:'' as column_1,
						''',_res_area_domain_attribute,''' as column_2,
						null::text as column
				union all
				select
						''Populační atributová členění:'' as column_1,
						''',_res_sub_population_attribute,''' as column_2,
						null::text as column
				union all
				select
						null::text as column_1,
						null::text as column_2,
						null::text as column_3
				)
		',_query_local_densities,
		'select
				w01.column_1,
				w01.column_2,
				w01.column_3
		from
				w01
		union all
		select
				''Příspěvky:'' as column_1,
				null::text as column_2,
				null::text as column_3
		union all
		select
				w02.column_1,
				w02.column_2,
				w02.column_3
		from
				w02;
		');		
	else -- EN
		_res := concat(
		'
		with
		w01 as	(
				select
						''Topic:'' as column_1,
						''',_res_topic,''' as column_2,
						null::text as column_3
				union all
				select
						''Period:'' as column_1,
						''',_res_estimation_period,''' as column_2,
						null::text as column_3
				union all
				select
						''Geographic region:'' as column_1,
						''',_res_estimation_cell_collection,''' as column_2,
						null::text as column_3
				union all
				select
						''Indicator:'' as column_1,
						''',_res_indicator_num,''' as column_2,
						null::text as column_3
				union all
				select
						''State or change:'' as column_1,
						''',_res_state_or_change_num,''' as column_2,
						null::text as column_3
				union all
				select
						''Unit of indicator:'' as column_1,
						''',_res_unit_num,''' as column_2,
						null::text as column_3
				union all
				select
						''Area domain classifactions:'' as column_1,
						''',_res_area_domain_attribute,''' as column_2,
						null::text as column
				union all
				select
						''Sub population classifications:'' as column_1,
						''',_res_sub_population_attribute,''' as column_2,
						null::text as column
				union all
				select
						null::text as column_1,
						null::text as column_2,
						null::text as column_3
				)
		',_query_local_densities,
		'select
				w01.column_1,
				w01.column_2,
				w01.column_3
		from
				w01
		union all
		select
				''Local densities:'' as column_1,
				null::text as column_2,
				null::text as column_3
		union all
		select
				w02.column_1,
				w02.column_2,
				w02.column_3
		from
				w02;
		');
	end if;

	return query execute ''||_res||'';
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_metadata(character varying, integer[]) is
'The function returns metadata for given input ID (that represents target variable for nominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_metadata(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_sub_population" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_sub_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_sub_population
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_sub_population(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_sub_population
(
	_jlang character varying,
	_id_group integer[],
	_sub_population integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_sub_population integer[],
	res_division boolean
)
as
$$
declare
	_lang_suffix							character varying(3);
	_target_variable_num					integer[];
	_check_records							integer;

	_count_target_variable					integer;
	_count_target_variable_with_core		integer;
	_count_target_variable_with_division	integer;
	_res_division							boolean;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_numerator_sub_population: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same target variable numerator!',_id_group;
	end if;
	-----------------------------------------------------------------
	if _sub_population is null -- first calling of function
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)	
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17a as	(
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							array[w16.atomic_type] as res_sub_population
					from
							w16
					)
		,w17 as	(
				select
						w17a.res_id,
						w17a.res_label,
						w17a.res_id_group,
						w17a.res_sub_population,
						null::boolean as res_division
				from
						w17a
				)
		-----------------------------------------
		,w18 as	(
				select
						a.*,
						b.target_variable
				from
						(
						select
								w17.res_id,
								w17.res_label,
								w17.res_sub_population,
								unnest(w17.res_id_group) as res_id_group
						from
								w17 where w17.res_id = 0
						) as a
				inner join (select * from nfiesta_results.t_result_group where web = true) as b on a.res_id_group = b.id
				)
				--select * from w18;
		,w19 as	(
				select
						ctv.id,
						(ctv.metadata->''en'')->''local_densities'' as local_densities
				from
						nfiesta_results.c_target_variable as ctv where ctv.id in (select distinct w18.target_variable from w18)
				)
		,w20 as	(
				select
						w19.id,
						json_array_elements(w19.local_densities) as local_densities
				from
						w19
				)
		,w21 as	(
				select
						w20.id,
						(w20.local_densities->''ldsity_type'')->>''label'' as object_type_label
				from
						w20
				)
		,w22 as	(
				select w18.* from w18 where w18.target_variable in (select w21.id from w21 where w21.object_type_label = ''division'')
				)
		,w23 as	(
				select w18.* from w18 where w18.target_variable not in (select distinct w22.target_variable from w22) -- TARGET VARIABLE WITHOUT DIVISION
				)
		,w24a as	(
					select
							w23.res_id,
							w23.res_label,
							w23.res_sub_population,
							array_agg(res_id_group) as res_id_group
					from
							w23 group by w23.res_id, w23.res_label, w23.res_sub_population
					)
		,w24 as	(
				select
						w24a.res_id,
						w24a.res_label,
						w24a.res_sub_population,
						w24a.res_id_group,
						null::boolean as res_division
				from
						w24a
				)
				select w24.res_id, w24.res_label, w24.res_id_group, w24.res_sub_population, w24.res_division from w24 union all
				select w17.res_id, w17.res_label, w17.res_id_group, w17.res_sub_population, w17.res_division from w17 where w17.res_id is distinct from 0;
		'
		using _id_group;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as sub_population,
								t.id
						from
								(
								select w11.sub_population, w11.id, w11.atomic_type from w11 union all
								select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_sub_population
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _sub_population
		into _check_records;	
		
		if _check_records = 0
		then
			-------------------------------------
			-- input _id_group must be for the same group of target variables:
			-- variant 1 => target variables without division ldsity object(s)
			-- variant 2 => target variables with core ldsity object and with division ldsity object(s)
			-------------------------------------
			with
			w1 as	(
					select
							ctv.id,
							(ctv.metadata->'en')->'local_densities' as local_densities
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id in	(
										select trg.target_variable from nfiesta_results.t_result_group as trg
										where trg.id in (select unnest(_id_group))
										and trg.web = true
										)
					)
			,w2 as	(
					select
							w1.id,
							json_array_elements(w1.local_densities) as local_densities
					from
							w1
					)
			,w3 as	(
					select
							w2.id,
							(w2.local_densities->'ldsity_type')->>'label' as object_type_label
					from
							w2
					)
			-------------------------------
			,w4a as	(select distinct w3.id from w3)
			,w4b as	(select distinct w3.id from w3 where w3.object_type_label = 'core')
			,w4c as	(select distinct w3.id from w3 where w3.object_type_label = 'division')
			-------------------------------
			,w5a as	(select 1 as id, count(w4a.*) as count_target_variable from w4a)
			,w5b as	(select 1 as id, count(w4b.*) as count_target_variable_with_core from w4b)
			,w5c as	(select 1 as id, count(w4c.*) as count_target_variable_with_division from w4c)
			-------------------------------
			select
					w5a.count_target_variable,
					w5b.count_target_variable_with_core,
					w5c.count_target_variable_with_division
			from
					w5a
					inner join w5b on w5a.id = w5b.id
					inner join w5c on w5a.id = w5c.id
			into
					_count_target_variable,
					_count_target_variable_with_core,
					_count_target_variable_with_division;
			-------------------------------------
			-------------------------------------
			if _sub_population = array[0] -- first user selection was without distinction
			then
				if _count_target_variable_with_division is distinct from 0
				then
					raise exception 'Error: 03: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then the values in input argument _id_group = % must only be for target variables that are without any division local density!',_id_group;
				end if;

				-- variant 1
				if _count_target_variable is distinct from _count_target_variable_with_core
				then
					raise exception 'Error: 04: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						_sub_population as res_sub_population,
						false as res_division; 
			else
				-- second and more user selections

				if _count_target_variable_with_division = 0
				then
					-- variant 1 => not exists any target variable with division
					if _count_target_variable is distinct from _count_target_variable_with_core
					then
						raise exception 'Error: 05: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
					end if;

					_res_division := false;
				else
					-- variant 2 => exist target variable with any division
					if	(_count_target_variable is distinct from _count_target_variable_with_division)
						or
						(_count_target_variable_with_core is distinct from _count_target_variable_with_division)
					then
						raise exception 'Error: 06: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables must contains some division local density!',_id_group;
					end if;

					_res_division := true;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						array_remove(_sub_population,0) as res_sub_population,
						_res_division as res_division;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.sub_population] as sub_population,
							t.id
					from
							(
							select w1.sub_population, array_agg(w1.id order by w1.id) as id
							from w1 where w1.sub_population = 0
							group by w1.sub_population
							) as t
					)
			,w3 as	(
					select w1.sub_population, array_agg(w1.id order by w1.id) as id
					from w1 where w1.sub_population > 0
					group by w1.sub_population
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_sub_population as csp
							on w3.sub_population = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_sub_population_category as cmspc
					where cmspc.sub_population_category in
						(
						select cspc.id from nfiesta_results.c_sub_population_category as cspc
						where cspc.sub_population in (select w6.sub_population from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.sub_population as sub_population,
							cspc2.sub_population as atomic_type
					from
							w7
							inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
							inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.sub_population from w8
					)
			,w10 as (
					select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.sub_population
					)
			,w11 as (
					select
							w6.sub_population,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.sub_population = w10.sub_population
					)
			,w12 as	(
					select
							w11.sub_population,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.sub_population,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.sub_population,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
							from	(
									select distinct a1.atomic_type, a1.sub_population
									from
										(
										select w13.atomic_type, w13.sub_population from w13
										union all
										select w5.sub_population as atomic_type, w5.sub_population from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as sub_population,
									null::integer[] as sub_population,
									t.id
							from
									(
									select w11.sub_population, w11.id, w11.atomic_type from w11 union all
									select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
					)
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_sub_population,
							null::boolean as res_division
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _sub_population;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_numerator_sub_population(character varying, integer[], integer[]) is
'The function returns list of available sub populations for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of sub populations.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_sub_population(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator" schema="nfiesta_results" src="functions/fn_get_user_options_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean);

create or replace function nfiesta_results.fn_get_user_options_numerator
(
	_jlang character varying,
	_topic integer default null::integer,
	_num_estimation_period integer default null::integer,
	_num_estimation_cell_collection integer default null::integer,
	_num_id_group integer[] default null::integer[],
	_num_indicator boolean default false,
	_num_state boolean default false,
	_num_unit_of_measure boolean default false,
	_unit_of_measurement boolean default false
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[]
)
language plpgsql
stable
as
$$
declare
	_lang_suffix			character varying(3);
	_lang_covariate			text;
	_check_num_id_group		integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	-- TOPIC --
	-----------------------------------------------------------------
	if _topic is null
	then
		return query execute
		'
		with
		w1 as	(
				select
						distinct
						target_variable,
						case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom
				from
						nfiesta_results.t_result_group where web = true
				)
		,w2 as	(
				select
						target_variable,
						case when denominator is null then 0 else denominator end as denominator,
						topic
				from
						nfiesta_results.cm_result2topic
				)
		,w3 as	(
				select distinct w2.topic from w1 inner join w2
				on w1.target_variable = w2.target_variable
				and w1.target_variable_denom = w2.denominator
				)
		,w4 as	(
				select
						id,
						label'||_lang_suffix||' AS label
				from
						nfiesta_results.c_topic
				where
						id in (select w3.topic from w3)
				)
		select
				w4.id as res_id,
				w4.label::text as res_label,
				null::integer[] as res_id_group
		from
				w4 order by w4.label;
		';
	else
		-----------------------------------------------------------------
		-- ESTIMATION_PERIOD --
		-----------------------------------------------------------------
		if _num_estimation_period is null
		then
			return query execute
			'
			with
			w1 as	(
					select
							target_variable,
							case when denominator is null then 0 else denominator end as denominator
					from
							nfiesta_results.cm_result2topic where topic = $1
					)
			,w2 as	(
					select
							target_variable,
							case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
							estimation_period
					from
							nfiesta_results.t_result_group where web = true
					)
			,w3 as	(
					select distinct w2.estimation_period from w2 inner join w1
					on w2.target_variable = w1.target_variable
					and w2.target_variable_denom = w1.denominator
					)
			,w4 as	(
					select
							id,
							label'||_lang_suffix||' AS label
					from
							nfiesta_results.c_estimation_period
					where
							id in (select w3.estimation_period from w3)
					)
			select
					w4.id as res_id,
					w4.label::text as res_label,
					null::integer[] as res_id_group
			from
					w4 order by w4.label;				
			'
			using _topic;
		else
			-----------------------------------------------------------------
			-- ESTIMATION_CELL_COLLECTION --
			-----------------------------------------------------------------
			if _num_estimation_cell_collection is null
			then
				return query execute
				'			
				with
				w1 as	(
						select
								target_variable,
								case when denominator is null then 0 else denominator end as denominator
						from
								nfiesta_results.cm_result2topic where topic = $1
						)
				,w2 as	(
						select
								target_variable,
								case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
								estimation_period,
								estimation_cell_collection
						from
								nfiesta_results.t_result_group where web = true
						)
				,w3 as	(
						select * from w2 inner join w1
						on w2.target_variable = w1.target_variable
						and w2.target_variable_denom = w1.denominator
						)
				,w4 as	(
						select distinct w3.estimation_cell_collection from w3 where w3.estimation_period = $2
						)
				,w5 as	(
						select
								id,
								label'||_lang_suffix||' AS label
						from
								nfiesta_results.c_estimation_cell_collection
						where
								id in (select w4.estimation_cell_collection from w4)
						)
				select
						w5.id as res_id,
						w5.label::text as res_label,
						null::integer[] as res_id_group
				from
						w5 order by w5.label;
				'
				using _topic, _num_estimation_period;
			else
				-----------------------------------------------------------------
				-- INDICATOR --
				-----------------------------------------------------------------
				if _num_indicator = false
				then
					return query execute
					'
					with
					w1 as	(
							select
									target_variable,
									case when denominator is null then 0 else denominator end as denominator
							from
									nfiesta_results.cm_result2topic where topic = $1
							)
					,w2 as	(
							select
									id,
									target_variable,
									case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
									estimation_period,
									estimation_cell_collection
							from
									nfiesta_results.t_result_group where web = true
							)
					,w3 as	(
							select w2.* from w2 inner join w1
							on w2.target_variable = w1.target_variable
							and w2.target_variable_denom = w1.denominator
							)
					,w4 as	(
							select w3.* from w3
							where w3.estimation_period = $2
							and w3.estimation_cell_collection = $3
							)
					,w5 as	(
							select
									target_variable,
									array_agg(w4.id order by w4.id) as id
							from
									w4 group by target_variable
							)
					,w6 as	(
							select
									w5.*,
									((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
									((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
							from
									w5
									inner join nfiesta_results.c_target_variable as ctv
									on w5.target_variable = ctv.id
							)
					,w7 as	(
							select
									w6.id,
									label'||_lang_suffix||' AS label
							from
									w6
							)
					,w8 as	(
							select
									w7.label,
									unnest(w7.id) as id
							from
									w7
							)
					,w9 as	(
							select w8.label, array_agg(w8.id) as id from w8 group by w8.label
							)
					select
							null::integer as res_id,
							w9.label as res_label,
							w9.id as res_id_group
					from
							w9 order by w9.label;
					'
					using _topic, _num_estimation_period, _num_estimation_cell_collection;
				else
					-----------------------------------------------------------------
					-- NUM_STATE --
					-----------------------------------------------------------------
					if _num_state = false
					then
						return query execute
						'
						with
						w1 as	(
								select * from nfiesta_results.t_result_group as trg
								where trg.id in (select unnest($1))
								and trg.web = true
								)
						,w2 as	(
								select
										target_variable,
										array_agg(w1.id order by w1.id) as id
								from
										w1 group by target_variable
								)
						,w3 as	(
								select
										w2.*,
										((ctv.metadata->''cs'')->''state_or_change'')->>''label'' as label,
										((ctv.metadata->''en'')->''state_or_change'')->>''label'' as label_en
								from
										w2
										inner join nfiesta_results.c_target_variable as ctv
										on w2.target_variable = ctv.id
								)
						,w4 as	(
								select
										w3.id,
										label'||_lang_suffix||' AS label
								from
										w3
								)
						,w5 as	(
								select
										w4.label,
										unnest(w4.id) as id
								from
										w4
								)
						,w6 as	(
								select w5.label, array_agg(w5.id) as id from w5 group by w5.label
								)
						select
								null::integer as res_id,
								w6.label as res_label,
								w6.id as res_id_group
						from
								w6 order by w6.label;
						'
						using _num_id_group;					
					else
						-----------------------------------------------------------------
						-- NUM_UNIT_OF_MEASURE --
						-----------------------------------------------------------------
						if _num_unit_of_measure = false
						then
							return query execute
							'
							with
							w1 as	(
									select * from nfiesta_results.t_result_group as trg
									where trg.id in (select unnest($1))
									and trg.web = true
									)
							,w2 as	(
									select
											target_variable,
											array_agg(w1.id order by w1.id) as id
									from
											w1 group by target_variable
									)
							,w3 as	(
									select
											w2.*,
											((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
											((ctv.metadata->''en'')->''unit'')->>''label'' as label_en
									from
											w2
											inner join nfiesta_results.c_target_variable as ctv
											on w2.target_variable = ctv.id
									)
							,w4 as	(
									select
											w3.id,
											label'||_lang_suffix||' AS label
									from
											w3
									)
							,w5 as	(
									select
											w4.label,
											unnest(w4.id) as id
									from
											w4
									)
							,w6 as	(
									select w5.label, array_agg(w5.id) as id from w5 group by w5.label
									)
							select
									null::integer as res_id,
									w6.label as res_label,
									w6.id as res_id_group
							from
									w6 order by w6.label;
							'
							using _num_id_group;
						else
							-----------------------------------------------------------------
							-- UNIT_OF_MEASUREMENT --
							-----------------------------------------------------------------
							if _unit_of_measurement = false
							then			
								select
										count(t.target_variable)
								from
										(
										select distinct trg.target_variable from nfiesta_results.t_result_group as trg
										where trg.id in (select unnest(_num_id_group))
										and trg.web = true
										) as t
								into _check_num_id_group;

								if _check_num_id_group is distinct from 1
								then
									raise exception 'Error 01: fn_get_user_options_numerator: The values in input argument _num_id_group = % must only be for the same target variable!',_num_id_group;
								end if;

								return query execute
								'
								with
								w1 as	(
										select
												id,
												target_variable,
												coalesce(target_variable_denom,0) as target_variable_denom 
										from
												nfiesta_results.t_result_group
										where
												id in (select unnest($1))
										and
												web = true
										)
								,w2 as	(
										select
												w1.target_variable,
												w1.target_variable_denom,
												array_agg(w1.id order by w1.id) as id
										from
												w1 group by w1.target_variable, w1.target_variable_denom
										)
								,w3 as	(
										select
												w2.*,
												((ctv1.metadata->''cs'')->''unit'')->>''label'' as unit_nom,
												((ctv2.metadata->''cs'')->''unit'')->>''label'' as unit_denom,
												((ctv1.metadata->''en'')->''unit'')->>''label'' as unit_nom_en,
												((ctv2.metadata->''en'')->''unit'')->>''label'' as unit_denom_en
										from
												w2
												inner join nfiesta_results.c_target_variable as ctv1 on w2.target_variable = ctv1.id
												left join nfiesta_results.c_target_variable as ctv2 on w2.target_variable_denom = ctv2.id
										)
								,w4 as	(
										select
												w3.target_variable,
												w3.target_variable_denom,
												w3.id,
												w3.unit_nom'||_lang_suffix||' AS unit_nom,
												w3.unit_denom'||_lang_suffix||' AS unit_denom
										from
												w3
										)
								,w5 as	(
										select
												w4.*,
												case
													when (w4.target_variable = w4.target_variable_denom) or (w4.unit_nom = w4.unit_denom) then ''%''
													when w4.target_variable_denom = 0 then w4.unit_nom
													else concat(w4.unit_nom,'' / '',w4.unit_denom)
												end as label4user,
												------------------
												case
													when w4.target_variable_denom = 0 then 0
													else 1
												end as res_id
										from
												w4
										)
								,w6 as	(
										select
												w5.res_id,
												w5.label4user as label,
												unnest(w5.id) as id
										from
												w5
										)
								,w7 as	(
										select
												w6.res_id,
												w6.label,
												array_agg(w6.id) as id
										from
												w6 group by w6.res_id, w6.label
										)
								select
										w7.res_id,
										w7.label as res_label,
										w7.id as res_id_group
								from
										w7 order by w7.label;
								'
								using _num_id_group;																	
							else
								raise exception 'Error 02: fn_get_user_options_numerator: This variant of function is not implemented !';
							end if;
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean) is
'The function returns list of available IDs and labels of topic, estimation period, estimation cell collection, indicator, state or change, unit of indicator and unit of measurement from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean) to public;
-- </function>
--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------


insert into nfiesta_results.c_gui_header(header,label_en) values
('not_specified_label','not specified'),
('not_specified_description','Not specified.'),
('without_distinction_label','without distinction'),
('without_distinction_description','Without distinction.');


-------------------------------------------------------------------------------
--	c_area_domain_language
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_area_domain_language;

create table nfiesta_results.c_area_domain_language
(
	id serial not null,
	"language" integer not null,
	area_domain integer not null,
	"label" varchar(250) not null,
	description text not null,
	constraint pkey__c_area_domain_language primary key (id),
	constraint ukey__c_area_domain_language unique ("language", area_domain, "label"),
	constraint fkey__c_area_domain_language__cl foreign key (language) references nfiesta_results.c_language(id),
	constraint fkey__c_area_domain_language__cad foreign key (area_domain) references nfiesta_results.c_area_domain(id)
);

comment on table nfiesta_results.c_area_domain_language is 'Table of area domains, language versions.';
comment on column nfiesta_results.c_area_domain_language.id is 'Identifier of area domain language version, primary key.';
comment on column nfiesta_results.c_area_domain_language.language is 'Identifier of language version, foreign key to c_language table.';
comment on column nfiesta_results.c_area_domain_language.area_domain is 'Identifier of international area domain, foreign key to c_area_domain table.';
comment on column nfiesta_results.c_area_domain_language.label is 'Label of area domain.';
comment on column nfiesta_results.c_area_domain_language.description is 'Description of area domain.';

comment on constraint pkey__c_area_domain_language on nfiesta_results.c_area_domain_language is 'Database identifier, primary key.';
comment on constraint ukey__c_area_domain_language on nfiesta_results.c_area_domain_language is 'Unique key on columns language, area_domain and label.';
comment on constraint fkey__c_area_domain_language__cl on nfiesta_results.c_area_domain_language is 'Foreign key to table c_language.';
comment on constraint fkey__c_area_domain_language__cad on nfiesta_results.c_area_domain_language is 'Foreign key to table c_area_domain.';

create index fki__c_area_domain_language__cl on nfiesta_results.c_area_domain_language using btree (language);
comment on index nfiesta_results.fki__c_area_domain_language__cl is 'Index over foreign key fkey__c_area_domain_language__cl';

create index fki__c_area_domain_language__cad on nfiesta_results.c_area_domain_language using btree (area_domain);
comment on index nfiesta_results.fki__c_area_domain_language__cad is 'Index over foreign key fkey__c_area_domain_language__cad';

alter table nfiesta_results.c_area_domain_language owner to adm_nfiesta;
grant all on table nfiesta_results.c_area_domain_language to adm_nfiesta;
grant select on table nfiesta_results.c_area_domain_language TO app_joomla;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	c_area_domain_category_language
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_area_domain_category_language;

create table nfiesta_results.c_area_domain_category_language
(
	id serial not null,
	"language" integer not null,
	area_domain_category integer not null,
	"label" varchar(250) not null,
	description text not null,
	constraint pkey__c_area_domain_category_language primary key (id),
	constraint ukey__c_area_domain_category_language unique ("language", area_domain_category, "label"),
	constraint fkey__c_area_domain_category_language__cl foreign key (language) references nfiesta_results.c_language(id),
	constraint fkey__c_area_domain_category_language__cadc foreign key (area_domain_category) references nfiesta_results.c_area_domain_category(id)
);

comment on table nfiesta_results.c_area_domain_category_language is 'Table of area domain categories, language versions.';
comment on column nfiesta_results.c_area_domain_category_language.id is 'Identifier of area domain category language version, primary key.';
comment on column nfiesta_results.c_area_domain_category_language.language is 'Identifier of language version, foreign key to c_language table.';
comment on column nfiesta_results.c_area_domain_category_language.area_domain_category is 'Identifier of international area domain category, foreign key to c_area_domain_category table.';
comment on column nfiesta_results.c_area_domain_category_language.label is 'Label of area domain category.';
comment on column nfiesta_results.c_area_domain_category_language.description is 'Description of area domain category.';

comment on constraint pkey__c_area_domain_category_language on nfiesta_results.c_area_domain_category_language is 'Database identifier, primary key.';
comment on constraint ukey__c_area_domain_category_language on nfiesta_results.c_area_domain_category_language is 'Unique key on columns language, area_domain_category and label.';
comment on constraint fkey__c_area_domain_category_language__cl on nfiesta_results.c_area_domain_category_language is 'Foreign key to table c_language.';
comment on constraint fkey__c_area_domain_category_language__cadc on nfiesta_results.c_area_domain_category_language is 'Foreign key to table c_area_domain_category.';

create index fki__c_area_domain_category_language__cl on nfiesta_results.c_area_domain_category_language using btree (language);
comment on index nfiesta_results.fki__c_area_domain_category_language__cl is 'Index over foreign key fkey__c_area_domain_category_language__cl';

create index fki__c_area_domain_category_language__cadc on nfiesta_results.c_area_domain_category_language using btree (area_domain_category);
comment on index nfiesta_results.fki__c_area_domain_category_language__cadc is 'Index over foreign key fkey__c_area_domain_category_language__cadc';

alter table nfiesta_results.c_area_domain_category_language owner to adm_nfiesta;
grant all on table nfiesta_results.c_area_domain_category_language to adm_nfiesta;
grant select on table nfiesta_results.c_area_domain_category_language TO app_joomla;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	c_estimation_cell_language
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_estimation_cell_language;

create table nfiesta_results.c_estimation_cell_language
(
	id serial not null,
	"language" integer not null,
	estimation_cell integer not null,
	"label" varchar(250) not null,
	description text not null,
	constraint pkey__c_estimation_cell_language primary key (id),
	constraint ukey__c_estimation_cell_language unique ("language", estimation_cell, "label"),
	constraint fkey__c_estimation_cell_language__cl foreign key (language) references nfiesta_results.c_language(id),
	constraint fkey__c_estimation_cell_language__cec foreign key (estimation_cell) references nfiesta_results.c_estimation_cell(id)
);

comment on table nfiesta_results.c_estimation_cell_language is 'Table of area estimation cells, language versions.';
comment on column nfiesta_results.c_estimation_cell_language.id is 'Identifier of estimation cell language version, primary key.';
comment on column nfiesta_results.c_estimation_cell_language.language is 'Identifier of language version, foreign key to c_language table.';
comment on column nfiesta_results.c_estimation_cell_language.estimation_cell is 'Identifier of international estimation cell, foreign key to c_estimation_cell table.';
comment on column nfiesta_results.c_estimation_cell_language.label is 'Label of estimation cell.';
comment on column nfiesta_results.c_estimation_cell_language.description is 'Description of estimation cell.';

comment on constraint pkey__c_estimation_cell_language on nfiesta_results.c_estimation_cell_language is 'Database identifier, primary key.';
comment on constraint ukey__c_estimation_cell_language on nfiesta_results.c_estimation_cell_language is 'Unique key on columns language, estimation_cell and label.';
comment on constraint fkey__c_estimation_cell_language__cl on nfiesta_results.c_estimation_cell_language is 'Foreign key to table c_language.';
comment on constraint fkey__c_estimation_cell_language__cec on nfiesta_results.c_estimation_cell_language is 'Foreign key to table c_estimation_cell.';

create index fki__c_estimation_cell_language__cl on nfiesta_results.c_estimation_cell_language using btree (language);
comment on index nfiesta_results.fki__c_estimation_cell_language__cl is 'Index over foreign key fkey__c_estimation_cell_language__cl';

create index fki__c_estimation_cell_language__cec on nfiesta_results.c_estimation_cell_language using btree (estimation_cell);
comment on index nfiesta_results.fki__c_estimation_cell_language__cec is 'Index over foreign key fkey__c_estimation_cell_language__cec';

alter table nfiesta_results.c_estimation_cell_language owner to adm_nfiesta;
grant all on table nfiesta_results.c_estimation_cell_language to adm_nfiesta;
grant select on table nfiesta_results.c_estimation_cell_language TO app_joomla;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	c_estimation_cell_collection_language
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_estimation_cell_collection_language;

create table nfiesta_results.c_estimation_cell_collection_language
(
	id serial not null,
	"language" integer not null,
	estimation_cell_collection integer not null,
	"label" varchar(250) not null,
	description text not null,
	constraint pkey__c_estimation_cell_collection_language primary key (id),
	constraint ukey__c_estimation_cell_collection_language unique ("language", estimation_cell_collection, "label"),
	constraint fkey__c_estimation_cell_collection_language__cl foreign key (language) references nfiesta_results.c_language(id),
	constraint fkey__c_estimation_cell_collection_language__cecc foreign key (estimation_cell_collection) references nfiesta_results.c_estimation_cell_collection(id)
);

comment on table nfiesta_results.c_estimation_cell_collection_language is 'Table of estimation cell collections, language versions.';
comment on column nfiesta_results.c_estimation_cell_collection_language.id is 'Identifier of estimation cell collection language version, primary key.';
comment on column nfiesta_results.c_estimation_cell_collection_language.language is 'Identifier of language version, foreign key to c_language table.';
comment on column nfiesta_results.c_estimation_cell_collection_language.estimation_cell_collection is 'Identifier of international estimation cell collection, foreign key to c_estimation_cell_collection table.';
comment on column nfiesta_results.c_estimation_cell_collection_language.label is 'Label of estimation cell collection.';
comment on column nfiesta_results.c_estimation_cell_collection_language.description is 'Description of estimation cell collection.';

comment on constraint pkey__c_estimation_cell_collection_language on nfiesta_results.c_estimation_cell_collection_language is 'Database identifier, primary key.';
comment on constraint ukey__c_estimation_cell_collection_language on nfiesta_results.c_estimation_cell_collection_language is 'Unique key on columns language, estimation_cell_collection and label.';
comment on constraint fkey__c_estimation_cell_collection_language__cl on nfiesta_results.c_estimation_cell_collection_language is 'Foreign key to table c_language.';
comment on constraint fkey__c_estimation_cell_collection_language__cecc on nfiesta_results.c_estimation_cell_collection_language is 'Foreign key to table c_estimation_cell_collection.';

create index fki__c_estimation_cell_collection_language__cl on nfiesta_results.c_estimation_cell_collection_language using btree (language);
comment on index nfiesta_results.fki__c_estimation_cell_collection_language__cl is 'Index over foreign key fkey__c_estimation_cell_collection_language__cl';

create index fki__c_estimation_cell_collection_language__cecc on nfiesta_results.c_estimation_cell_collection_language using btree (estimation_cell_collection);
comment on index nfiesta_results.fki__c_estimation_cell_collection_language__cecc is 'Index over foreign key fkey__c_estimation_cell_collection_language__cecc';

alter table nfiesta_results.c_estimation_cell_collection_language owner to adm_nfiesta;
grant all on table nfiesta_results.c_estimation_cell_collection_language to adm_nfiesta;
grant select on table nfiesta_results.c_estimation_cell_collection_language TO app_joomla;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	c_estimation_period_language
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_estimation_period_language;

create table nfiesta_results.c_estimation_period_language
(
	id serial not null,
	"language" integer not null,
	estimation_period integer not null,
	"label" varchar(250) not null,
	description text not null,
	constraint pkey__c_estimation_period_language primary key (id),
	constraint ukey__c_estimation_period_language unique ("language", estimation_period, "label"),
	constraint fkey__c_estimation_period_language__cl foreign key (language) references nfiesta_results.c_language(id),
	constraint fkey__c_estimation_period_language__cep foreign key (estimation_period) references nfiesta_results.c_estimation_period(id)
);

comment on table nfiesta_results.c_estimation_period_language is 'Table of estimation periods, language versions.';
comment on column nfiesta_results.c_estimation_period_language.id is 'Identifier of estimation period language version, primary key.';
comment on column nfiesta_results.c_estimation_period_language.language is 'Identifier of language version, foreign key to c_language table.';
comment on column nfiesta_results.c_estimation_period_language.estimation_period is 'Identifier of international estimation period, foreign key to c_estimation_period table.';
comment on column nfiesta_results.c_estimation_period_language.label is 'Label of estimation period.';
comment on column nfiesta_results.c_estimation_period_language.description is 'Description of estimation period.';

comment on constraint pkey__c_estimation_period_language on nfiesta_results.c_estimation_period_language is 'Database identifier, primary key.';
comment on constraint ukey__c_estimation_period_language on nfiesta_results.c_estimation_period_language is 'Unique key on columns language, estimation_period and label.';
comment on constraint fkey__c_estimation_period_language__cl on nfiesta_results.c_estimation_period_language is 'Foreign key to table c_language.';
comment on constraint fkey__c_estimation_period_language__cep on nfiesta_results.c_estimation_period_language is 'Foreign key to table c_estimation_period.';

create index fki__c_estimation_period_language__cl on nfiesta_results.c_estimation_period_language using btree (language);
comment on index nfiesta_results.fki__c_estimation_period_language__cl is 'Index over foreign key fkey__c_estimation_period_language__cl';

create index fki__c_estimation_period_language__cep on nfiesta_results.c_estimation_period_language using btree (estimation_period);
comment on index nfiesta_results.fki__c_estimation_period_language__cep is 'Index over foreign key fkey__c_estimation_period_language__cep';

alter table nfiesta_results.c_estimation_period_language owner to adm_nfiesta;
grant all on table nfiesta_results.c_estimation_period_language to adm_nfiesta;
grant select on table nfiesta_results.c_estimation_period_language TO app_joomla;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	c_panel_refyearset_group_language
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_panel_refyearset_group_language;

create table nfiesta_results.c_panel_refyearset_group_language
(
	id serial not null,
	"language" integer not null,
	panel_refyearset_group integer not null,
	"label" varchar(250) not null,
	description text not null,
	constraint pkey__c_panel_refyearset_group_language primary key (id),
	constraint ukey__c_panel_refyearset_group_language unique ("language", panel_refyearset_group, "label"),
	constraint fkey__c_panel_refyearset_group_language__cl foreign key (language) references nfiesta_results.c_language(id),
	constraint fkey__c_panel_refyearset_group_language__cprg foreign key (panel_refyearset_group) references nfiesta_results.c_panel_refyearset_group(id)
);

comment on table nfiesta_results.c_panel_refyearset_group_language is 'Table of panel refyearset groups, language versions.';
comment on column nfiesta_results.c_panel_refyearset_group_language.id is 'Identifier of panel refyearset group language version, primary key.';
comment on column nfiesta_results.c_panel_refyearset_group_language.language is 'Identifier of language version, foreign key to c_language table.';
comment on column nfiesta_results.c_panel_refyearset_group_language.panel_refyearset_group is 'Identifier of international panel refyearset group, foreign key to c_panel_refyearset_group table.';
comment on column nfiesta_results.c_panel_refyearset_group_language.label is 'Label of panel refyearset group.';
comment on column nfiesta_results.c_panel_refyearset_group_language.description is 'Description of panel refyearset group.';

comment on constraint pkey__c_panel_refyearset_group_language on nfiesta_results.c_panel_refyearset_group_language is 'Database identifier, primary key.';
comment on constraint ukey__c_panel_refyearset_group_language on nfiesta_results.c_panel_refyearset_group_language is 'Unique key on columns language, panel_refyearset_group and label.';
comment on constraint fkey__c_panel_refyearset_group_language__cl on nfiesta_results.c_panel_refyearset_group_language is 'Foreign key to table c_language.';
comment on constraint fkey__c_panel_refyearset_group_language__cprg on nfiesta_results.c_panel_refyearset_group_language is 'Foreign key to table c_panel_refyearset_group.';

create index fki__c_panel_refyearset_group_language__cl on nfiesta_results.c_panel_refyearset_group_language using btree (language);
comment on index nfiesta_results.fki__c_panel_refyearset_group_language__cl is 'Index over foreign key fkey__c_panel_refyearset_group_language__cl';

create index fki__c_panel_refyearset_group_language__cprg on nfiesta_results.c_panel_refyearset_group_language using btree (panel_refyearset_group);
comment on index nfiesta_results.fki__c_panel_refyearset_group_language__cprg is 'Index over foreign key fkey__c_panel_refyearset_group_language__cprg';

alter table nfiesta_results.c_panel_refyearset_group_language owner to adm_nfiesta;
grant all on table nfiesta_results.c_panel_refyearset_group_language to adm_nfiesta;
grant select on table nfiesta_results.c_panel_refyearset_group_language TO app_joomla;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	c_sub_population_language
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_sub_population_language;

create table nfiesta_results.c_sub_population_language
(
	id serial not null,
	"language" integer not null,
	sub_population integer not null,
	"label" varchar(250) not null,
	description text not null,
	constraint pkey__c_sub_population_language primary key (id),
	constraint ukey__c_sub_population_language unique ("language", sub_population, "label"),
	constraint fkey__c_sub_population_language__cl foreign key (language) references nfiesta_results.c_language(id),
	constraint fkey__c_sub_population_language__csp foreign key (sub_population) references nfiesta_results.c_sub_population(id)
);

comment on table nfiesta_results.c_sub_population_language is 'Table of sub populations, language versions.';
comment on column nfiesta_results.c_sub_population_language.id is 'Identifier of sub population language version, primary key.';
comment on column nfiesta_results.c_sub_population_language.language is 'Identifier of language version, foreign key to c_language table.';
comment on column nfiesta_results.c_sub_population_language.sub_population is 'Identifier of international sub population, foreign key to c_sub_population table.';
comment on column nfiesta_results.c_sub_population_language.label is 'Label of sub population.';
comment on column nfiesta_results.c_sub_population_language.description is 'Description of sub population.';

comment on constraint pkey__c_sub_population_language on nfiesta_results.c_sub_population_language is 'Database identifier, primary key.';
comment on constraint ukey__c_sub_population_language on nfiesta_results.c_sub_population_language is 'Unique key on columns language, sub_population and label.';
comment on constraint fkey__c_sub_population_language__cl on nfiesta_results.c_sub_population_language is 'Foreign key to table c_language.';
comment on constraint fkey__c_sub_population_language__csp on nfiesta_results.c_sub_population_language is 'Foreign key to table c_sub_population.';

create index fki__c_sub_population_language__cl on nfiesta_results.c_sub_population_language using btree (language);
comment on index nfiesta_results.fki__c_sub_population_language__cl is 'Index over foreign key fkey__c_sub_population_language__cl';

create index fki__c_sub_population_language__csp on nfiesta_results.c_sub_population_language using btree (sub_population);
comment on index nfiesta_results.fki__c_sub_population_language__csp is 'Index over foreign key fkey__c_sub_population_language__csp';

alter table nfiesta_results.c_sub_population_language owner to adm_nfiesta;
grant all on table nfiesta_results.c_sub_population_language to adm_nfiesta;
grant select on table nfiesta_results.c_sub_population_language TO app_joomla;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	c_sub_population_category_language
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_sub_population_category_language;

create table nfiesta_results.c_sub_population_category_language
(
	id serial not null,
	"language" integer not null,
	sub_population_category integer not null,
	"label" varchar(250) not null,
	description text not null,
	constraint pkey__c_sub_population_category_language primary key (id),
	constraint ukey__c_sub_population_category_language unique ("language", sub_population_category, "label"),
	constraint fkey__c_sub_population_category_language__cl foreign key (language) references nfiesta_results.c_language(id),
	constraint fkey__c_sub_population_category_language__cspc foreign key (sub_population_category) references nfiesta_results.c_sub_population_category(id)
);

comment on table nfiesta_results.c_sub_population_category_language is 'Table of sub population categories, language versions.';
comment on column nfiesta_results.c_sub_population_category_language.id is 'Identifier of sub population category language version, primary key.';
comment on column nfiesta_results.c_sub_population_category_language.language is 'Identifier of language version, foreign key to c_language table.';
comment on column nfiesta_results.c_sub_population_category_language.sub_population_category is 'Identifier of international sub population category, foreign key to c_sub_population_category table.';
comment on column nfiesta_results.c_sub_population_category_language.label is 'Label of sub population category.';
comment on column nfiesta_results.c_sub_population_category_language.description is 'Description of sub population category.';

comment on constraint pkey__c_sub_population_category_language on nfiesta_results.c_sub_population_category_language is 'Database identifier, primary key.';
comment on constraint ukey__c_sub_population_category_language on nfiesta_results.c_sub_population_category_language is 'Unique key on columns language, sub_population_category and label.';
comment on constraint fkey__c_sub_population_category_language__cl on nfiesta_results.c_sub_population_category_language is 'Foreign key to table c_language.';
comment on constraint fkey__c_sub_population_category_language__cspc on nfiesta_results.c_sub_population_category_language is 'Foreign key to table c_sub_population_category.';

create index fki__c_sub_population_category_language__cl on nfiesta_results.c_sub_population_category_language using btree (language);
comment on index nfiesta_results.fki__c_sub_population_category_language__cl is 'Index over foreign key fkey__c_sub_population_category_language__cl';

create index fki__c_sub_population_category_language__cspc on nfiesta_results.c_sub_population_category_language using btree (sub_population_category);
comment on index nfiesta_results.fki__c_sub_population_category_language__cspc is 'Index over foreign key fkey__c_sub_population_category_language__cspc';

alter table nfiesta_results.c_sub_population_category_language owner to adm_nfiesta;
grant all on table nfiesta_results.c_sub_population_category_language to adm_nfiesta;
grant select on table nfiesta_results.c_sub_population_category_language TO app_joomla;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--	c_topic_language
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_topic_language;

create table nfiesta_results.c_topic_language
(
	id serial not null,
	"language" integer not null,
	topic integer not null,
	"label" varchar(250) not null,
	description text not null,
	constraint pkey__c_topic_language primary key (id),
	constraint ukey__c_topic_language unique ("language", topic, "label"),
	constraint fkey__c_topic_language__cl foreign key (language) references nfiesta_results.c_language(id),
	constraint fkey__c_topic_language__ct foreign key (topic) references nfiesta_results.c_topic(id)
);

comment on table nfiesta_results.c_topic_language is 'Table of topics, language versions.';
comment on column nfiesta_results.c_topic_language.id is 'Identifier of topic language version, primary key.';
comment on column nfiesta_results.c_topic_language.language is 'Identifier of language version, foreign key to c_language table.';
comment on column nfiesta_results.c_topic_language.topic is 'Identifier of international topic, foreign key to c_topic table.';
comment on column nfiesta_results.c_topic_language.label is 'Label of topic.';
comment on column nfiesta_results.c_topic_language.description is 'Description of topic.';

comment on constraint pkey__c_topic_language on nfiesta_results.c_topic_language is 'Database identifier, primary key.';
comment on constraint ukey__c_topic_language on nfiesta_results.c_topic_language is 'Unique key on columns language, topic and label.';
comment on constraint fkey__c_topic_language__cl on nfiesta_results.c_topic_language is 'Foreign key to table c_language.';
comment on constraint fkey__c_topic_language__ct on nfiesta_results.c_topic_language is 'Foreign key to table c_topic.';

create index fki__c_topic_language__cl on nfiesta_results.c_topic_language using btree (language);
comment on index nfiesta_results.fki__c_topic_language__cl is 'Index over foreign key fkey__c_topic_language__cl';

create index fki__c_topic_language__ct on nfiesta_results.c_topic_language using btree (topic);
comment on index nfiesta_results.fki__c_topic_language__ct is 'Index over foreign key fkey__c_topic_language__ct';

alter table nfiesta_results.c_topic_language owner to adm_nfiesta;
grant all on table nfiesta_results.c_topic_language to adm_nfiesta;
grant select on table nfiesta_results.c_topic_language TO app_joomla;
-------------------------------------------------------------------------------




---------------------------------------------------------------------------------------------------
-- LANGUAGE INSERTS
---------------------------------------------------------------------------------------------------
-- c_area_domain_category_language
insert into nfiesta_results.c_area_domain_category_language("language",area_domain_category,label,description)
select t.language, t.id, t.label, t.description 
from	(
		select
				(select cl.id from nfiesta_results.c_language as cl where cl.label = 'cs'::varchar(2)) as language,
				id, label, description
		from
				nfiesta_results.c_area_domain_category
		) as t
where
		t.language is not null order by t.id;


-- c_area_domain_language
insert into nfiesta_results.c_area_domain_language("language",area_domain,label,description)
select t.language, t.id, t.label, t.description 
from	(
		select
				(select cl.id from nfiesta_results.c_language as cl where cl.label = 'cs'::varchar(2)) as language,
				id, label, description
		from
				nfiesta_results.c_area_domain
		) as t
where
		t.language is not null order by t.id;


-- c_estimation_cell_collection_language
insert into nfiesta_results.c_estimation_cell_collection_language("language",estimation_cell_collection,label,description)
select t.language, t.id, t.label, t.description 
from	(
		select
				(select cl.id from nfiesta_results.c_language as cl where cl.label = 'cs'::varchar(2)) as language,
				id, label, description
		from
				nfiesta_results.c_estimation_cell_collection
		) as t
where
		t.language is not null order by t.id;


-- c_estimation_cell_language
insert into nfiesta_results.c_estimation_cell_language("language",estimation_cell,label,description)
select t.language, t.id, t.label, t.description 
from	(
		select
				(select cl.id from nfiesta_results.c_language as cl where cl.label = 'cs'::varchar(2)) as language,
				id, label, description
		from
				nfiesta_results.c_estimation_cell
		) as t
where
		t.language is not null order by t.id;


-- c_estimation_period_language
insert into nfiesta_results.c_estimation_period_language("language",estimation_period,label,description)
select t.language, t.id, t.label, t.description 
from	(
		select
				(select cl.id from nfiesta_results.c_language as cl where cl.label = 'cs'::varchar(2)) as language,
				id, label, description
		from
				nfiesta_results.c_estimation_period
		) as t
where
		t.language is not null order by t.id;


-- c_panel_refyearset_group_language
insert into nfiesta_results.c_panel_refyearset_group_language("language",panel_refyearset_group,label,description)
select t.language, t.id, t.label, t.description 
from	(
		select
				(select cl.id from nfiesta_results.c_language as cl where cl.label = 'cs'::varchar(2)) as language,
				id, label, description
		from
				nfiesta_results.c_panel_refyearset_group
		) as t
where
		t.language is not null order by t.id;


-- c_sub_population_category_language
insert into nfiesta_results.c_sub_population_category_language("language",sub_population_category,label,description)
select t.language, t.id, t.label, t.description 
from	(
		select
				(select cl.id from nfiesta_results.c_language as cl where cl.label = 'cs'::varchar(2)) as language,
				id, label, description
		from
				nfiesta_results.c_sub_population_category
		) as t
where
		t.language is not null order by t.id;


-- c_sub_population_language
insert into nfiesta_results.c_sub_population_language("language",sub_population,label,description)
select t.language, t.id, t.label, t.description 
from	(
		select
				(select cl.id from nfiesta_results.c_language as cl where cl.label = 'cs'::varchar(2)) as language,
				id, label, description
		from
				nfiesta_results.c_sub_population
		) as t
where
		t.language is not null order by t.id;


-- c_topic_language
insert into nfiesta_results.c_topic_language("language",topic,label,description)
select t.language, t.id, t.label, t.description 
from	(
		select
				(select cl.id from nfiesta_results.c_language as cl where cl.label = 'cs'::varchar(2)) as language,
				id, label, description
		from
				nfiesta_results.c_topic
		) as t
where
		t.language is not null order by t.id;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------




---------------------------------------------------------------------------------------------------
-- DELETE COLUMNS LABEL and DESCRIPTION
---------------------------------------------------------------------------------------------------
alter table nfiesta_results.c_area_domain drop column "label";
alter table nfiesta_results.c_area_domain drop column description;

alter table nfiesta_results.c_area_domain_category drop column "label";
alter table nfiesta_results.c_area_domain_category drop column description;

alter table nfiesta_results.c_estimation_cell drop column "label";
alter table nfiesta_results.c_estimation_cell drop column description;

alter table nfiesta_results.c_estimation_cell_collection drop column "label";
alter table nfiesta_results.c_estimation_cell_collection drop column description;

alter table nfiesta_results.c_estimation_period drop column "label";
alter table nfiesta_results.c_estimation_period drop column description;

alter table nfiesta_results.c_panel_refyearset_group drop column "label";
alter table nfiesta_results.c_panel_refyearset_group drop column description;

alter table nfiesta_results.c_sub_population drop column "label";
alter table nfiesta_results.c_sub_population drop column description;

alter table nfiesta_results.c_sub_population_category drop column "label";
alter table nfiesta_results.c_sub_population_category drop column description;

alter table nfiesta_results.c_topic drop column "label";
alter table nfiesta_results.c_topic drop column description;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------



-- FUNCTIONS --




-- <function name="fn_get_language_id" schema="nfiesta_results" src="functions/fn_get_language_id.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_language_id
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_language_id(character varying);

create or replace function nfiesta_results.fn_get_language_id
(
	_jlang character varying -- string value from web joomla
)
  returns integer as
$$
declare
	_lang	integer;
begin
	----------------------------------
	if _jlang is null
	then
		raise exception 'Error 01: fn_get_language_id: Input argument _jlang must not be NULL!';
	end if;
	----------------------------------
	select cl.id from nfiesta_results.c_language as cl where cl.label = substring(_jlang,1,2)
	into _lang;
	----------------------------------
	if _lang is null
	then
		raise exception 'Error 02: fn_get_language_id: For input argument _jlang = % not exist any record in c_language table!', _jlang;
	end if;
	----------------------------------
	return _lang;
end;
$$
  language plpgsql stable
  cost 100;

comment on function nfiesta_results.fn_get_language_id(character varying) is
'The function returns ID from c_language table for given input language.';

grant execute on function nfiesta_results.fn_get_language_id(character varying) to public;
-- </function>



-- <function name="fn_get_gui_header" schema="nfiesta_results" src="functions/fn_get_gui_header.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_gui_header
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_gui_header(character varying, character varying);

create or replace function nfiesta_results.fn_get_gui_header
(
	_jlang character varying,
	_header character varying
)
  returns varchar as
$$
declare
	_lang			integer;
	_lang_label		varchar;
	_res			varchar;
begin
	----------------------------------
	if _jlang is null
	then
		raise exception 'Error 01: fn_get_gui_header: Input argument _jlang must not be NULL !';
	end if;
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	-----------------------------------------------------------------
	if _lang_label = 'en'::varchar(2)
	then
		select cgh.label_en from nfiesta_results.c_gui_header as cgh
		where cgh.header = _header
		into _res;
	else
		select cghl.label from nfiesta_results.c_gui_header_language as cghl
		where cghl.gui_header = (select cgh.id from nfiesta_results.c_gui_header as cgh where cgh.header = _header)
		and cghl.language = _lang
		into _res;
	end if;
	----------------------------------
	if _res is null
	then
		raise exception 'Error 02: fn_get_gui_header: Output argument _res is NULL! Not exists any gui header label for given jlang = % and header = %!',_jlang, _header;
	end if;	
	----------------------------------
	return _res;
end;
$$
  language plpgsql stable
  cost 100;

comment on function nfiesta_results.fn_get_gui_header(character varying, character varying) is
'The function returns gui header label for given input language and header.';

grant execute on function nfiesta_results.fn_get_gui_header(character varying, character varying) to public;
-- </function>



-- <function name="fn_get_gui_headers" schema="nfiesta_results" src="functions/fn_get_gui_headers.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_gui_headers
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_gui_headers(character varying);

create or replace function nfiesta_results.fn_get_gui_headers(_jlang character varying)
  returns json as
$$
declare
	_lang			integer;
	_lang_label		varchar;
	_lang_column	text;
	_res			json;
begin
	----------------------------------
	if _jlang is null
	then
		raise exception 'Error 01: fn_get_gui_headers: Input argument _jlang must not be NULL !';
	end if;
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	if _lang_label = 'en'
	then
		with
		w1 as	(
				select id, "header", label_en from nfiesta_results.c_gui_header
				)
		select json_agg(json_build_object('gui_header',w1.header,'label',w1.label_en)) from w1
		into _res;				
	else
		with
		w1 as	(
				select id, "header", label_en from nfiesta_results.c_gui_header 
				)
		,w2 as	(
				select * from nfiesta_results.c_gui_header_language
				where language = _lang
				)
		,w3 as	(
				select w1.id, w1.header, w1.label_en, w2.label from w1 inner join w2
				on w1.id = w2.gui_header
				)
		,w4 as	(
				select w3.id, w3.header, w3.label as label from w3
				)
		select json_agg(json_build_object('gui_header',w4.header,'label',w4.label)) from w4
		into _res;
	end if;
	----------------------------------
	if _res is null
	then
		raise exception 'Error 02: fn_get_gui_headers: Output argument _res is NULL! Not exists any gui header!';
	end if;	
	----------------------------------
	
	return _res;

end;
$$
  language plpgsql stable
  cost 100;

comment on function nfiesta_results.fn_get_gui_headers(character varying) is
'The function returns gui headers for given input language.';

grant execute on function nfiesta_results.fn_get_gui_headers(character varying) to public;
-- </function>



-- <function name="fn_get_user_options_numerator" schema="nfiesta_results" src="functions/fn_get_user_options_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean);

create or replace function nfiesta_results.fn_get_user_options_numerator
(
	_jlang character varying,
	_topic integer default null::integer,
	_num_estimation_period integer default null::integer,
	_num_estimation_cell_collection integer default null::integer,
	_num_id_group integer[] default null::integer[],
	_num_indicator boolean default false,
	_num_state boolean default false,
	_num_unit_of_measure boolean default false,
	_unit_of_measurement boolean default false
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[]
)
language plpgsql
stable
as
$$
declare
	_lang					integer;
	_lang_label				varchar;
	_lang_column			text;
	_lang_covariate			text;
	_check_num_id_group		integer;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	-- TOPIC --
	-----------------------------------------------------------------
	if _topic is null
	then
		return query execute
		'
		with
		w1 as	(
				select
						distinct
						target_variable,
						case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom
				from
						nfiesta_results.t_result_group where web = true
				)
		,w2 as	(
				select
						target_variable,
						case when denominator is null then 0 else denominator end as denominator,
						topic
				from
						nfiesta_results.cm_result2topic
				)
		,w3 as	(
				select distinct w2.topic from w1 inner join w2
				on w1.target_variable = w2.target_variable
				and w1.target_variable_denom = w2.denominator
				)
		-----------------------------------------
		,w4a as	(
				select
						id,
						label_en
				from
						nfiesta_results.c_topic
				where
						id in (select w3.topic from w3)
				)
		,w4b as	(
				select ctl.* from nfiesta_results.c_topic_language as ctl
				where ctl.language = $1
				and ctl.topic in (select w4a.id from w4a)
				)
		,w4c as	(
				select w4a.id, w4a.label_en, w4b.label
				from w4a left join w4b
				on w4a.id = w4b.topic
				)
		,w4 as	(
				select
						w4c.id,
						w4c.'||_lang_column||' AS label
				from
						w4c where w4c.'||_lang_column||' is not null
				)
		-----------------------------------------
		select
				w4.id as res_id,
				w4.label::text as res_label,
				null::integer[] as res_id_group
		from
				w4 order by w4.label;
		'
		using _lang;
	else
		-----------------------------------------------------------------
		-- ESTIMATION_PERIOD --
		-----------------------------------------------------------------
		if _num_estimation_period is null
		then
			return query execute
			'
			with
			w1 as	(
					select
							target_variable,
							case when denominator is null then 0 else denominator end as denominator
					from
							nfiesta_results.cm_result2topic where topic = $1
					)
			,w2 as	(
					select
							target_variable,
							case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
							estimation_period
					from
							nfiesta_results.t_result_group where web = true
					)
			,w3 as	(
					select distinct w2.estimation_period from w2 inner join w1
					on w2.target_variable = w1.target_variable
					and w2.target_variable_denom = w1.denominator
					)
			-----------------------------------------
			,w4a as	(
					select
							id,
							label_en
					from
							nfiesta_results.c_estimation_period
					where
							id in (select w3.estimation_period from w3)
					)
			,w4b as	(
					select cepl.* from nfiesta_results.c_estimation_period_language as cepl
					where cepl.language = $2
					and cepl.estimation_period in (select w4a.id from w4a)
					)
			,w4c as	(
					select w4a.id, w4a.label_en, w4b.label
					from w4a left join w4b
					on w4a.id = w4b.estimation_period
					)
			,w4 as	(
					select
							w4c.id,
							w4c.'||_lang_column||' AS label
					from
							w4c where w4c.'||_lang_column||' is not null
					)
			-----------------------------------------
			select
					w4.id as res_id,
					w4.label::text as res_label,
					null::integer[] as res_id_group
			from
					w4 order by w4.label;				
			'
			using _topic, _lang;
		else
			-----------------------------------------------------------------
			-- ESTIMATION_CELL_COLLECTION --
			-----------------------------------------------------------------
			if _num_estimation_cell_collection is null
			then
				return query execute
				'			
				with
				w1 as	(
						select
								target_variable,
								case when denominator is null then 0 else denominator end as denominator
						from
								nfiesta_results.cm_result2topic where topic = $1
						)
				,w2 as	(
						select
								target_variable,
								case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
								estimation_period,
								estimation_cell_collection
						from
								nfiesta_results.t_result_group where web = true
						)
				,w3 as	(
						select * from w2 inner join w1
						on w2.target_variable = w1.target_variable
						and w2.target_variable_denom = w1.denominator
						)
				,w4 as	(
						select distinct w3.estimation_cell_collection from w3 where w3.estimation_period = $2
						)
				-----------------------------------------
				,w5a as	(
						select
								id,
								label_en
						from
								nfiesta_results.c_estimation_cell_collection
						where
								id in (select w4.estimation_cell_collection from w4)
						)
				,w5b as	(
						select cecc.* from nfiesta_results.c_estimation_cell_collection_language as cecc
						where cecc.language = $3
						and cecc.estimation_cell_collection in (select w5a.id from w5a)
						)
				,w5c as	(
						select w5a.id, w5a.label_en, w5b.label
						from w5a left join w5b
						on w5a.id = w5b.estimation_cell_collection
						)
				,w5 as	(
						select
								w5c.id,
								w5c.'||_lang_column||' AS label
						from
								w5c where w5c.'||_lang_column||' is not null
						)
				-----------------------------------------
				select
						w5.id as res_id,
						w5.label::text as res_label,
						null::integer[] as res_id_group
				from
						w5 order by w5.label;
				'
				using _topic, _num_estimation_period, _lang;
			else
				-----------------------------------------------------------------
				-- INDICATOR --
				-----------------------------------------------------------------
				if _num_indicator = false
				then
					return query execute
					'
					with
					w1 as	(
							select
									target_variable,
									case when denominator is null then 0 else denominator end as denominator
							from
									nfiesta_results.cm_result2topic where topic = $1
							)
					,w2 as	(
							select
									id,
									target_variable,
									case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
									estimation_period,
									estimation_cell_collection
							from
									nfiesta_results.t_result_group where web = true
							)
					,w3 as	(
							select w2.* from w2 inner join w1
							on w2.target_variable = w1.target_variable
							and w2.target_variable_denom = w1.denominator
							)
					,w4 as	(
							select w3.* from w3
							where w3.estimation_period = $2
							and w3.estimation_cell_collection = $3
							)
					,w5 as	(
							select
									target_variable,
									array_agg(w4.id order by w4.id) as id
							from
									w4 group by target_variable
							)
					,w6 as	(
							select
									w5.*,
									((ctv.metadata->'''||_lang_label||''')->''indicator'')->>''label'' as label
							from
									w5
									inner join nfiesta_results.c_target_variable as ctv
									on w5.target_variable = ctv.id
							)
					,w7 as	(
							select
									w6.label,
									unnest(w6.id) as id
							from
									w6
							)
					,w8 as	(
							select w7.label, array_agg(w7.id) as id from w7 group by w7.label
							)
					select
							null::integer as res_id,
							w8.label as res_label,
							w8.id as res_id_group
					from
							w8 order by w8.label;
					'
					using _topic, _num_estimation_period, _num_estimation_cell_collection;
				else
					-----------------------------------------------------------------
					-- NUM_STATE --
					-----------------------------------------------------------------
					if _num_state = false
					then
						return query execute
						'
						with
						w1 as	(
								select * from nfiesta_results.t_result_group as trg
								where trg.id in (select unnest($1))
								and trg.web = true
								)
						,w2 as	(
								select
										target_variable,
										array_agg(w1.id order by w1.id) as id
								from
										w1 group by target_variable
								)
						,w3 as	(
								select
										w2.*,
										((ctv.metadata->'''||_lang_label||''')->''state_or_change'')->>''label'' as label
								from
										w2
										inner join nfiesta_results.c_target_variable as ctv
										on w2.target_variable = ctv.id
								)
						,w4 as	(
								select
										w3.label,
										unnest(w3.id) as id
								from
										w3
								)
						,w5 as	(
								select w4.label, array_agg(w4.id) as id from w4 group by w4.label
								)
						select
								null::integer as res_id,
								w5.label as res_label,
								w5.id as res_id_group
						from
								w5 order by w5.label;
						'
						using _num_id_group;					
					else
						-----------------------------------------------------------------
						-- NUM_UNIT_OF_MEASURE --
						-----------------------------------------------------------------
						if _num_unit_of_measure = false
						then
							return query execute
							'
							with
							w1 as	(
									select * from nfiesta_results.t_result_group as trg
									where trg.id in (select unnest($1))
									and trg.web = true
									)
							,w2 as	(
									select
											target_variable,
											array_agg(w1.id order by w1.id) as id
									from
											w1 group by target_variable
									)
							,w3 as	(
									select
											w2.*,
											((ctv.metadata->'''||_lang_label||''')->''unit'')->>''label'' as label
									from
											w2
											inner join nfiesta_results.c_target_variable as ctv
											on w2.target_variable = ctv.id
									)
							,w4 as	(
									select
											w3.label,
											unnest(w3.id) as id
									from
											w3
									)
							,w5 as	(
									select w4.label, array_agg(w4.id) as id from w4 group by w4.label
									)
							select
									null::integer as res_id,
									w5.label as res_label,
									w5.id as res_id_group
							from
									w5 order by w5.label;
							'
							using _num_id_group;
						else
							-----------------------------------------------------------------
							-- UNIT_OF_MEASUREMENT --
							-----------------------------------------------------------------
							if _unit_of_measurement = false
							then			
								select
										count(t.target_variable)
								from
										(
										select distinct trg.target_variable from nfiesta_results.t_result_group as trg
										where trg.id in (select unnest(_num_id_group))
										and trg.web = true
										) as t
								into _check_num_id_group;

								if _check_num_id_group is distinct from 1
								then
									raise exception 'Error 01: fn_get_user_options_numerator: The values in input argument _num_id_group = % must only be for the same target variable!',_num_id_group;
								end if;

								return query execute
								'
								with
								w1 as	(
										select
												id,
												target_variable,
												coalesce(target_variable_denom,0) as target_variable_denom 
										from
												nfiesta_results.t_result_group
										where
												id in (select unnest($1))
										and
												web = true
										)
								,w2 as	(
										select
												w1.target_variable,
												w1.target_variable_denom,
												array_agg(w1.id order by w1.id) as id
										from
												w1 group by w1.target_variable, w1.target_variable_denom
										)
								,w3 as	(
										select
												w2.*,
												((ctv1.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_nom,
												((ctv2.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_denom
										from
												w2
												inner join nfiesta_results.c_target_variable as ctv1 on w2.target_variable = ctv1.id
												left join nfiesta_results.c_target_variable as ctv2 on w2.target_variable_denom = ctv2.id
										)
								,w4 as	(
										select
												w3.target_variable,
												w3.target_variable_denom,
												w3.id,
												w3.unit_nom,
												w3.unit_denom
										from
												w3
										)
								,w5 as	(
										select
												w4.*,
												case
													when (w4.target_variable = w4.target_variable_denom) or (w4.unit_nom = w4.unit_denom) then ''%''
													when w4.target_variable_denom = 0 then w4.unit_nom
													else concat(w4.unit_nom,'' / '',w4.unit_denom)
												end as label4user,
												------------------
												case
													when w4.target_variable_denom = 0 then 0
													else 1
												end as res_id
										from
												w4
										)
								,w6 as	(
										select
												w5.res_id,
												w5.label4user as label,
												unnest(w5.id) as id
										from
												w5
										)
								,w7 as	(
										select
												w6.res_id,
												w6.label,
												array_agg(w6.id) as id
										from
												w6 group by w6.res_id, w6.label
										)
								select
										w7.res_id,
										w7.label as res_label,
										w7.id as res_id_group
								from
										w7 order by w7.label;
								'
								using _num_id_group;																	
							else
								raise exception 'Error 02: fn_get_user_options_numerator: This variant of function is not implemented !';
							end if;
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean) is
'The function returns list of available IDs and labels of topic, estimation period, estimation cell collection, indicator, state or change, unit of indicator and unit of measurement from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_ldsity_core" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_ldsity_core.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_ldsity_core
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_ldsity_core
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang								integer;
		_lang_label							varchar;
		_not_specified_label				text;
		_not_specified_description			text;
		_not_specified_definition_variant	json;
		_not_specified_adr_spr				json;
begin	
		-----------------------------------------------------------------
		_lang := nfiesta_results.fn_get_language_id(_jlang);
		_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_numerator_ldsity_core: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		_not_specified_label := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_label'::character varying))::text;
		_not_specified_description := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_description'::character varying))::text;
		-----------------------------------------------------------------
		_not_specified_definition_variant := json_agg(json_build_object('label',_not_specified_label,'description',_not_specified_description));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label',_not_specified_label,'description',_not_specified_description),'restriction',json_build_object('label',_not_specified_label,'description',_not_specified_description)));
		-----------------------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable
				)
		,w3 as	(
				select
						w2.target_variable,
						w2.id,
						((ctv.metadata->'''||_lang_label||''')->''local_densities'') as local_densities
				from
						w2
						inner join nfiesta_results.c_target_variable as ctv
						on w2.target_variable = ctv.id
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where (w4.local_densities->''ldsity_type'')->>''label'' = ''core''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						(w5.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w5.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w5.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w5.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w5.local_densities->''ldsity'')->>''label'' as label,
						(w5.local_densities->''ldsity'')->>''description'' as description,	
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition_variant'' as definition_variant,
						w5.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w5
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w7.object_type_label,''description'',w7.object_type_description),
							''ldsity_object'',json_build_object(''label'',w7.object_label,''description'',w7.object_description),
							''ldsity'',json_build_object(''label'',w7.label,''description'',w7.description),
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition_variant'',w7.definition_variant,
							''area_domain_restrictions'',w7.area_domain_restrictions,
							''sub_population_restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						--w8.new_id,
						json_agg(w8.local_densities order by w8.new_id) as local_densities
				from
						w8 group by w8.target_variable --, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_label, _not_specified_description, _not_specified_definition_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]) is
'The function returns list of available labels of standart local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_area_domain" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_area_domain.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_area_domain
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_area_domain(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_area_domain
(
	_jlang character varying,
	_id_group integer[],
	_area_domain integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_area_domain integer[]
)
as
$$
declare
	_lang						integer;
	_lang_label					varchar;
	_lang_column				text;
	_without_distinction_label	text;
	_target_variable_num		integer[];
	_check_records				integer;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_numerator_area_domain: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_numerator_area_domain: The values in input argument _id_group = % must only be for the same target variable numerator!',_id_group;
	end if;
	-----------------------------------------------------------------
	_without_distinction_label := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_label'::character varying))::text;
	-----------------------------------------------------------------
	if _area_domain is null
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						cad.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as cad
						on w3.area_domain = cad.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmadc
				where cmadc.area_domain_category in
					(
					select cadc.id from nfiesta_results.c_area_domain_category as cadc
					where cadc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cadc1.area_domain as area_domain,
						cadc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cadc1 on w7.area_domain_category = cadc1.id
						inner join nfiesta_results.c_area_domain_category as cadc2 on w7.atomic_category = cadc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)	
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when cad.label_en is null then $2 else cad.label_en end as label_en,
						case when t.label is null then $2 else t.label end as label
				from
						w15
						left join nfiesta_results.c_area_domain as cad on w15.atomic_type = cad.id
						left join	(
									select cadl.* from nfiesta_results.c_area_domain_language as cadl
									where cadl.language = $3
									and cadl.area_domain in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.area_domain
				)
		---------------------------------------------------
		select
				w16.atomic_type as res_id,
				(w16.'||_lang_column||')::text AS res_label,
				w16.id as res_id_group,
				array[w16.atomic_type] as res_area_domain
		from
				w16;
		'
		using _id_group, _without_distinction_label, _lang;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						cad.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as cad
						on w3.area_domain = cad.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmadc
				where cmadc.area_domain_category in
					(
					select cadc.id from nfiesta_results.c_area_domain_category as cadc
					where cadc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cadc1.area_domain as area_domain,
						cadc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cadc1 on w7.area_domain_category = cadc1.id
						inner join nfiesta_results.c_area_domain_category as cadc2 on w7.atomic_category = cadc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as area_domain,
								t.id
						from
								(
								select w11.area_domain, w11.id, w11.atomic_type from w11 union all
								select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when cad.label_en is null then $3 else cad.label_en end as label_en,
						case when t.label is null then $3 else t.label end as label
				from
						w15
						left join nfiesta_results.c_area_domain as cad on w15.atomic_type = cad.id
						left join	(
									select cadl.* from nfiesta_results.c_area_domain_language as cadl
									where cadl.language = $4
									and cadl.area_domain in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.area_domain
				)
		---------------------------------------------------
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.'||_lang_column||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_area_domain
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _area_domain, _without_distinction_label, _lang
		into _check_records;	
		
		if _check_records = 0
		then
			if _area_domain = array[0]
			then
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						_area_domain as _res_area_domain;
			else
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						array_remove(_area_domain,0) as _res_area_domain;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.area_domain] as area_domain,
							t.id
					from
							(
							select w1.area_domain, array_agg(w1.id order by w1.id) as id
							from w1 where w1.area_domain = 0
							group by w1.area_domain
							) as t
					)
			,w3 as	(
					select w1.area_domain, array_agg(w1.id order by w1.id) as id
					from w1 where w1.area_domain > 0
					group by w1.area_domain
					)
			,w4 as	(
					select
							w3.*,
							cad.atomic
					from 
							w3
							inner join nfiesta_results.c_area_domain as cad
							on w3.area_domain = cad.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_area_domain_category as cmadc
					where cmadc.area_domain_category in
						(
						select cadc.id from nfiesta_results.c_area_domain_category as cadc
						where cadc.area_domain in (select w6.area_domain from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cadc1.area_domain as area_domain,
							cadc2.area_domain as atomic_type
					from
							w7
							inner join nfiesta_results.c_area_domain_category as cadc1 on w7.area_domain_category = cadc1.id
							inner join nfiesta_results.c_area_domain_category as cadc2 on w7.atomic_category = cadc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.area_domain from w8
					)
			,w10 as (
					select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.area_domain
					)
			,w11 as (
					select
							w6.area_domain,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.area_domain = w10.area_domain
					)
			,w12 as	(
					select
							w11.area_domain,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.area_domain,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.area_domain,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
							from	(
									select distinct a1.atomic_type, a1.area_domain
									from
										(
										select w13.atomic_type, w13.area_domain from w13
										union all
										select w5.area_domain as atomic_type, w5.area_domain from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as area_domain,
									null::integer[] as area_domain,
									t.id
							from
									(
									select w11.area_domain, w11.id, w11.atomic_type from w11 union all
									select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			---------------------------------------------------
			,w16 as	(
					select
							w15.*,
							case when cad.label_en is null then $3 else cad.label_en end as label_en,
							case when t.label is null then $3 else t.label end as label
					from
							w15
							left join nfiesta_results.c_area_domain as cad on w15.atomic_type = cad.id
							left join	(
										select cadl.* from nfiesta_results.c_area_domain_language as cadl
										where cadl.language = $4
										and cadl.area_domain in (select w15.atomic_type from w15)
										) as t
							on w15.atomic_type = t.area_domain
					)
			---------------------------------------------------
			select
					w16.atomic_type as res_id,
					(w16.'||_lang_column||')::text AS res_label,
					w16.id as res_id_group,
					$2 || array[w16.atomic_type] as res_area_domain
			from
					w16
					
					where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _area_domain, _without_distinction_label, _lang;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_numerator_area_domain(character varying, integer[], integer[]) is
'The function returns list of available area domains for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of area domains.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_area_domain(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_sub_population" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_sub_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_sub_population
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_sub_population(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_sub_population
(
	_jlang character varying,
	_id_group integer[],
	_sub_population integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_sub_population integer[],
	res_division boolean
)
as
$$
declare
	_lang									integer;
	_lang_label								varchar;
	_lang_column							text;
	_without_distinction_label				text;
	_target_variable_num					integer[];
	_check_records							integer;
	_count_target_variable					integer;
	_count_target_variable_with_core		integer;
	_count_target_variable_with_division	integer;
	_res_division							boolean;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_numerator_sub_population: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same target variable numerator!',_id_group;
	end if;
	-----------------------------------------------------------------
	_without_distinction_label := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_label'::character varying))::text;
	-----------------------------------------------------------------
	if _sub_population is null -- first calling of function
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)	
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when csp.label_en is null then $2 else csp.label_en end as label_en,
						case when t.label is null then $2 else t.label end as label
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
						left join	(
									select cspl.* from nfiesta_results.c_sub_population_language as cspl
									where cspl.language = $3
									and cspl.sub_population in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.sub_population
				)
		---------------------------------------------------
		,w17a as	(
					select
							w16.atomic_type as res_id,
							(w16.'||_lang_column||')::text AS res_label,
							w16.id as res_id_group,
							array[w16.atomic_type] as res_sub_population
					from
							w16
					)
		,w17 as	(
				select
						w17a.res_id,
						w17a.res_label,
						w17a.res_id_group,
						w17a.res_sub_population,
						null::boolean as res_division
				from
						w17a
				)
		-----------------------------------------
		,w18 as	(
				select
						a.*,
						b.target_variable
				from
						(
						select
								w17.res_id,
								w17.res_label,
								w17.res_sub_population,
								unnest(w17.res_id_group) as res_id_group
						from
								w17 where w17.res_id = 0
						) as a
				inner join (select * from nfiesta_results.t_result_group where web = true) as b on a.res_id_group = b.id
				)
		,w19 as	(
				select
						ctv.id,
						(ctv.metadata->''en'')->''local_densities'' as local_densities
				from
						nfiesta_results.c_target_variable as ctv where ctv.id in (select distinct w18.target_variable from w18)
				)
		,w20 as	(
				select
						w19.id,
						json_array_elements(w19.local_densities) as local_densities
				from
						w19
				)
		,w21 as	(
				select
						w20.id,
						(w20.local_densities->''ldsity_type'')->>''label'' as object_type_label
				from
						w20
				)
		,w22 as	(
				select w18.* from w18 where w18.target_variable in (select w21.id from w21 where w21.object_type_label = ''division'')
				)
		,w23 as	(
				select w18.* from w18 where w18.target_variable not in (select distinct w22.target_variable from w22) -- TARGET VARIABLE WITHOUT DIVISION
				)
		,w24a as	(
					select
							w23.res_id,
							w23.res_label,
							w23.res_sub_population,
							array_agg(res_id_group) as res_id_group
					from
							w23 group by w23.res_id, w23.res_label, w23.res_sub_population
					)
		,w24 as	(
				select
						w24a.res_id,
						w24a.res_label,
						w24a.res_sub_population,
						w24a.res_id_group,
						null::boolean as res_division
				from
						w24a
				)
				select w24.res_id, w24.res_label, w24.res_id_group, w24.res_sub_population, w24.res_division from w24 union all
				select w17.res_id, w17.res_label, w17.res_id_group, w17.res_sub_population, w17.res_division from w17 where w17.res_id is distinct from 0;
		'
		using _id_group, _without_distinction_label, _lang;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as sub_population,
								t.id
						from
								(
								select w11.sub_population, w11.id, w11.atomic_type from w11 union all
								select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when csp.label_en is null then $3 else csp.label_en end as label_en,
						case when t.label is null then $3 else t.label end as label
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
						left join	(
									select cspl.* from nfiesta_results.c_sub_population_language as cspl
									where cspl.language = $4
									and cspl.sub_population in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.sub_population
				)
		---------------------------------------------------
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.'||_lang_column||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_sub_population
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _sub_population, _without_distinction_label, _lang
		into _check_records;	
		
		if _check_records = 0
		then
			-------------------------------------
			-- input _id_group must be for the same group of target variables:
			-- variant 1 => target variables without division ldsity object(s)
			-- variant 2 => target variables with core ldsity object and with division ldsity object(s)
			-------------------------------------
			with
			w1 as	(
					select
							ctv.id,
							(ctv.metadata->'en')->'local_densities' as local_densities
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id in	(
										select trg.target_variable from nfiesta_results.t_result_group as trg
										where trg.id in (select unnest(_id_group))
										and trg.web = true
										)
					)
			,w2 as	(
					select
							w1.id,
							json_array_elements(w1.local_densities) as local_densities
					from
							w1
					)
			,w3 as	(
					select
							w2.id,
							(w2.local_densities->'ldsity_type')->>'label' as object_type_label
					from
							w2
					)
			-------------------------------
			,w4a as	(select distinct w3.id from w3)
			,w4b as	(select distinct w3.id from w3 where w3.object_type_label = 'core')
			,w4c as	(select distinct w3.id from w3 where w3.object_type_label = 'division')
			-------------------------------
			,w5a as	(select 1 as id, count(w4a.*) as count_target_variable from w4a)
			,w5b as	(select 1 as id, count(w4b.*) as count_target_variable_with_core from w4b)
			,w5c as	(select 1 as id, count(w4c.*) as count_target_variable_with_division from w4c)
			-------------------------------
			select
					w5a.count_target_variable,
					w5b.count_target_variable_with_core,
					w5c.count_target_variable_with_division
			from
					w5a
					inner join w5b on w5a.id = w5b.id
					inner join w5c on w5a.id = w5c.id
			into
					_count_target_variable,
					_count_target_variable_with_core,
					_count_target_variable_with_division;
			-------------------------------------
			-------------------------------------
			if _sub_population = array[0] -- first user selection was without distinction
			then
				if _count_target_variable_with_division is distinct from 0
				then
					raise exception 'Error: 03: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then the values in input argument _id_group = % must only be for target variables that are without any division local density!',_id_group;
				end if;

				-- variant 1
				if _count_target_variable is distinct from _count_target_variable_with_core
				then
					raise exception 'Error: 04: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						_sub_population as res_sub_population,
						false as res_division; 
			else
				-- second and more user selections

				if _count_target_variable_with_division = 0
				then
					-- variant 1 => not exists any target variable with division
					if _count_target_variable is distinct from _count_target_variable_with_core
					then
						raise exception 'Error: 05: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
					end if;

					_res_division := false;
				else
					-- variant 2 => exist target variable with any division
					if	(_count_target_variable is distinct from _count_target_variable_with_division)
						or
						(_count_target_variable_with_core is distinct from _count_target_variable_with_division)
					then
						raise exception 'Error: 06: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables must contains some division local density!',_id_group;
					end if;

					_res_division := true;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						array_remove(_sub_population,0) as res_sub_population,
						_res_division as res_division;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.sub_population] as sub_population,
							t.id
					from
							(
							select w1.sub_population, array_agg(w1.id order by w1.id) as id
							from w1 where w1.sub_population = 0
							group by w1.sub_population
							) as t
					)
			,w3 as	(
					select w1.sub_population, array_agg(w1.id order by w1.id) as id
					from w1 where w1.sub_population > 0
					group by w1.sub_population
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_sub_population as csp
							on w3.sub_population = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_sub_population_category as cmspc
					where cmspc.sub_population_category in
						(
						select cspc.id from nfiesta_results.c_sub_population_category as cspc
						where cspc.sub_population in (select w6.sub_population from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.sub_population as sub_population,
							cspc2.sub_population as atomic_type
					from
							w7
							inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
							inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.sub_population from w8
					)
			,w10 as (
					select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.sub_population
					)
			,w11 as (
					select
							w6.sub_population,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.sub_population = w10.sub_population
					)
			,w12 as	(
					select
							w11.sub_population,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.sub_population,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.sub_population,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
							from	(
									select distinct a1.atomic_type, a1.sub_population
									from
										(
										select w13.atomic_type, w13.sub_population from w13
										union all
										select w5.sub_population as atomic_type, w5.sub_population from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as sub_population,
									null::integer[] as sub_population,
									t.id
							from
									(
									select w11.sub_population, w11.id, w11.atomic_type from w11 union all
									select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			---------------------------------------------------
			,w16 as	(
					select
							w15.*,
							case when csp.label_en is null then $3 else csp.label_en end as label_en,
							case when t.label is null then $3 else t.label end as label
					from
							w15
							left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
							left join	(
										select cspl.* from nfiesta_results.c_sub_population_language as cspl
										where cspl.language = $4
										and cspl.sub_population in (select w15.atomic_type from w15)
										) as t
							on w15.atomic_type = t.sub_population
					)
			---------------------------------------------------
			select
					w16.atomic_type as res_id,
					(w16.'||_lang_column||')::text AS res_label,
					w16.id as res_id_group,
					$2 || array[w16.atomic_type] as res_sub_population,
					null::boolean as res_division
			from
					w16
					
					where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _sub_population, _without_distinction_label, _lang;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_numerator_sub_population(character varying, integer[], integer[]) is
'The function returns list of available sub populations for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of sub populations.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_sub_population(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_ldsity_division" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_ldsity_division.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_ldsity_division
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_ldsity_division(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_ldsity_division
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang								integer;
		_lang_label							varchar;
		_not_specified_label				text;
		_not_specified_description			text;
		_not_specified_definition_variant	json;
		_not_specified_adr_spr				json;		
begin	
		-----------------------------------------------------------------
		_lang := nfiesta_results.fn_get_language_id(_jlang);
		_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_numerator_ldsity_division: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		_not_specified_label := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_label'::character varying))::text;
		_not_specified_description := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_description'::character varying))::text;
		-----------------------------------------------------------------
		_not_specified_definition_variant := json_agg(json_build_object('label',_not_specified_label,'description',_not_specified_description));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label',_not_specified_label,'description',_not_specified_description),'restriction',json_build_object('label',_not_specified_label,'description',_not_specified_description)));
		-----------------------------------------------------------------		
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable
				)
		,w3 as	(
				select
						w2.target_variable,
						w2.id,
						((ctv.metadata->'''||_lang_label||''')->''local_densities'') as local_densities
				from
						w2
						inner join nfiesta_results.c_target_variable as ctv
						on w2.target_variable = ctv.id
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where (w4.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						(w5.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w5.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w5.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w5.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w5.local_densities->''ldsity'')->>''label'' as label,
						(w5.local_densities->''ldsity'')->>''description'' as description,	
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition_variant'' as definition_variant,
						w5.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w5						
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w7.object_type_label,''description'',w7.object_type_description),
							''ldsity_object'',json_build_object(''label'',w7.object_label,''description'',w7.object_description),
							''ldsity'',json_build_object(''label'',w7.label,''description'',w7.description),							
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition_variant'',w7.definition_variant,
							''area_domain_restrictions'',w7.area_domain_restrictions,
							''sub_population_restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						--w8.new_id,
						json_agg(w8.local_densities order by w8.new_id) as local_densities
				from
						w8 group by w8.target_variable --, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_label, _not_specified_description, _not_specified_definition_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_ldsity_division(character varying, integer[]) is
'The function returns list of available labels of division local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_ldsity_division(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator" schema="nfiesta_results" src="functions/fn_get_user_options_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean);

create or replace function nfiesta_results.fn_get_user_options_denominator
(
	_jlang character varying,
	_denom_id_group integer[],
	_denom_indicator boolean DEFAULT false,
	_denom_state boolean DEFAULT false
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[]
)
language plpgsql
stable
as
$$
declare
	_lang					integer;
	_lang_label				varchar;
	_lang_column			text;
	_lang_covariate			text;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	if _denom_id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_denominator: Input argument _denom_id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	-- DENOM_INDICATOR --
	-----------------------------------------------------------------
	if _denom_indicator = false
	then
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group
				where id in (select unnest($1))
				and web = true
				)
		,w2 as	(
				select
						w1.target_variable_denom,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						w2.*,
						((ctv.metadata->'''||_lang_label||''')->''indicator'')->>''label'' as label
				from
						w2
						inner join nfiesta_results.c_target_variable as ctv
						on w2.target_variable_denom = ctv.id
				)
		,w4 as	(
				select
						w3.label,
						unnest(w3.id) as id
				from
						w3
				)
		,w5 as	(
				select w4.label, array_agg(w4.id) as id from w4 group by w4.label
				)
		select
				1 as res_id,
				w5.label as res_label,
				w5.id as res_id_group
		from
				w5 order by w5.label;
		'
		using _denom_id_group;
	else
		-----------------------------------------------------------------
		-- DENOM_STATE --
		-----------------------------------------------------------------
		if _denom_state = false
		then
			return query execute
			'
			with
			w1 as	(
					select * from nfiesta_results.t_result_group
					where id in (select unnest($1))
					and web = true
					)
			,w2 as	(
					select
							w1.target_variable_denom,
							array_agg(w1.id order by w1.id) as id
					from
							w1 group by target_variable_denom
					)
			,w3 as	(
					select
							w2.*,
							((ctv.metadata->'''||_lang_label||''')->''state_or_change'')->>''label'' as label
					from
							w2
							inner join nfiesta_results.c_target_variable as ctv
							on w2.target_variable_denom = ctv.id
					)
			,w4 as	(
					select
							w3.label,
							unnest(w3.id) as id
					from
							w3
					)
			,w5 as	(
					select w4.label, array_agg(w4.id) as id from w4 group by w4.label
					)
			select
					1 as res_id,
					w5.label as res_label,
					w5.id as res_id_group
			from
					w5 order by w5.label;
			'
			using _denom_id_group;
		else
			raise exception 'Error 02: fn_get_user_options_denominator: This variant of function is not implemented !';
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean) is
'The function returns list of available IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_ldsity_core" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_ldsity_core.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_ldsity_core
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_ldsity_core(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_ldsity_core
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang								integer;
		_lang_label							varchar;
		_not_specified_label				text;
		_not_specified_description			text;
		_not_specified_definition_variant	json;
		_not_specified_adr_spr				json;		
begin	
		-----------------------------------------------------------------
		_lang := nfiesta_results.fn_get_language_id(_jlang);
		_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_denominator_ldsity_core: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		_not_specified_label := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_label'::character varying))::text;
		_not_specified_description := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_description'::character varying))::text;
		-----------------------------------------------------------------
		_not_specified_definition_variant := json_agg(json_build_object('label',_not_specified_label,'description',_not_specified_description));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label',_not_specified_label,'description',_not_specified_description),'restriction',json_build_object('label',_not_specified_label,'description',_not_specified_description)));
		-----------------------------------------------------------------		
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable_denom as target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						w2.target_variable,
						w2.id,
						((ctv.metadata->'''||_lang_label||''')->''local_densities'') as local_densities
				from
						w2
						inner join nfiesta_results.c_target_variable as ctv
						on w2.target_variable = ctv.id
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where (w4.local_densities->''ldsity_type'')->>''label'' = ''core''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						(w5.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w5.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w5.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w5.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w5.local_densities->''ldsity'')->>''label'' as label,
						(w5.local_densities->''ldsity'')->>''description'' as description,	
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition_variant'' as definition_variant,
						w5.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w5						
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w7.object_type_label,''description'',w7.object_type_description),
							''ldsity_object'',json_build_object(''label'',w7.object_label,''description'',w7.object_description),
							''ldsity'',json_build_object(''label'',w7.label,''description'',w7.description),							
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition_variant'',w7.definition_variant,
							''area_domain_restrictions'',w7.area_domain_restrictions,
							''sub_population_restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						--w8.new_id,
						json_agg(w8.local_densities order by w8.new_id) as local_densities
				from
						w8 group by w8.target_variable --, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_label, _not_specified_description, _not_specified_definition_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator_ldsity_core(character varying, integer[]) is
'The function returns list of available labels of standart local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_ldsity_core(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_area_domain" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_area_domain.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_area_domain
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_area_domain(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_area_domain
(
	_jlang character varying,
	_id_group integer[],
	_area_domain integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_area_domain integer[]
)
as
$$
declare
	_lang						integer;
	_lang_label					varchar;
	_lang_column				text;
	_without_distinction_label	text;
	_target_variable_denom	integer[];
	_check_records			integer;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_denominator_area_domain: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom;

	if array_length(_target_variable_denom,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_denominator_area_domain: The values in input argument _id_group = % must only be for the same target variable denominator!',_id_group;
	end if;
	-----------------------------------------------------------------
	_without_distinction_label := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_label'::character varying))::text;
	-----------------------------------------------------------------
	if _area_domain is null
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.area_domain_denom is null then 0 else trg.area_domain_denom end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						cad.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as cad
						on w3.area_domain = cad.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmadc
				where cmadc.area_domain_category in
					(
					select cadc.id from nfiesta_results.c_area_domain_category as cadc
					where cadc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cadc1.area_domain as area_domain,
						cadc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cadc1 on w7.area_domain_category = cadc1.id
						inner join nfiesta_results.c_area_domain_category as cadc2 on w7.atomic_category = cadc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)	
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when cad.label_en is null then $2 else cad.label_en end as label_en,
						case when t.label is null then $2 else t.label end as label
				from
						w15
						left join nfiesta_results.c_area_domain as cad on w15.atomic_type = cad.id
						left join	(
									select cadl.* from nfiesta_results.c_area_domain_language as cadl
									where cadl.language = $3
									and cadl.area_domain in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.area_domain
				)
		---------------------------------------------------
		select
				w16.atomic_type as res_id,
				(w16.'||_lang_column||')::text AS res_label,
				w16.id as res_id_group,
				array[w16.atomic_type] as res_area_domain
		from
				w16;
		'
		using _id_group, _without_distinction_label, _lang;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.area_domain_denom is null then 0 else trg.area_domain_denom end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						cad.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as cad
						on w3.area_domain = cad.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmadc
				where cmadc.area_domain_category in
					(
					select cadc.id from nfiesta_results.c_area_domain_category as cadc
					where cadc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cadc1.area_domain as area_domain,
						cadc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cadc1 on w7.area_domain_category = cadc1.id
						inner join nfiesta_results.c_area_domain_category as cadc2 on w7.atomic_category = cadc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as area_domain,
								t.id
						from
								(
								select w11.area_domain, w11.id, w11.atomic_type from w11 union all
								select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when cad.label_en is null then $3 else cad.label_en end as label_en,
						case when t.label is null then $3 else t.label end as label
				from
						w15
						left join nfiesta_results.c_area_domain as cad on w15.atomic_type = cad.id
						left join	(
									select cadl.* from nfiesta_results.c_area_domain_language as cadl
									where cadl.language = $4
									and cadl.area_domain in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.area_domain
				)
		---------------------------------------------------
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.'||_lang_column||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_area_domain
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _area_domain, _without_distinction_label, _lang
		into _check_records;	
		
		if _check_records = 0
		then
			if _area_domain = array[0]
			then
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						_area_domain as _res_area_domain;
			else
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						array_remove(_area_domain,0) as _res_area_domain;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.area_domain_denom is null then 0 else trg.area_domain_denom end as area_domain
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.area_domain] as area_domain,
							t.id
					from
							(
							select w1.area_domain, array_agg(w1.id order by w1.id) as id
							from w1 where w1.area_domain = 0
							group by w1.area_domain
							) as t
					)
			,w3 as	(
					select w1.area_domain, array_agg(w1.id order by w1.id) as id
					from w1 where w1.area_domain > 0
					group by w1.area_domain
					)
			,w4 as	(
					select
							w3.*,
							cad.atomic
					from 
							w3
							inner join nfiesta_results.c_area_domain as cad
							on w3.area_domain = cad.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_area_domain_category as cmadc
					where cmadc.area_domain_category in
						(
						select cadc.id from nfiesta_results.c_area_domain_category as cadc
						where cadc.area_domain in (select w6.area_domain from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cadc1.area_domain as area_domain,
							cadc2.area_domain as atomic_type
					from
							w7
							inner join nfiesta_results.c_area_domain_category as cadc1 on w7.area_domain_category = cadc1.id
							inner join nfiesta_results.c_area_domain_category as cadc2 on w7.atomic_category = cadc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.area_domain from w8
					)
			,w10 as (
					select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.area_domain
					)
			,w11 as (
					select
							w6.area_domain,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.area_domain = w10.area_domain
					)
			,w12 as	(
					select
							w11.area_domain,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.area_domain,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.area_domain,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
							from	(
									select distinct a1.atomic_type, a1.area_domain
									from
										(
										select w13.atomic_type, w13.area_domain from w13
										union all
										select w5.area_domain as atomic_type, w5.area_domain from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as area_domain,
									null::integer[] as area_domain,
									t.id
							from
									(
									select w11.area_domain, w11.id, w11.atomic_type from w11 union all
									select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			---------------------------------------------------
			,w16 as	(
					select
							w15.*,
							case when cad.label_en is null then $3 else cad.label_en end as label_en,
							case when t.label is null then $3 else t.label end as label
					from
							w15
							left join nfiesta_results.c_area_domain as cad on w15.atomic_type = cad.id
							left join	(
										select cadl.* from nfiesta_results.c_area_domain_language as cadl
										where cadl.language = $4
										and cadl.area_domain in (select w15.atomic_type from w15)
										) as t
							on w15.atomic_type = t.area_domain
					)
			---------------------------------------------------
			select
					w16.atomic_type as res_id,
					(w16.'||_lang_column||')::text AS res_label,
					w16.id as res_id_group,
					$2 || array[w16.atomic_type] as res_area_domain
			from
					w16
					
					where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _area_domain, _without_distinction_label, _lang;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_denominator_area_domain(character varying, integer[], integer[]) is
'The function returns list of available area domains for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of area domains.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_area_domain(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_sub_population" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_sub_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_sub_population
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_sub_population
(
	_jlang character varying,
	_id_group integer[],
	_sub_population integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_sub_population integer[],
	res_division boolean
)
as
$$
declare
	_lang									integer;
	_lang_label								varchar;
	_lang_column							text;
	_without_distinction_label				text;
	_target_variable_denom					integer[];
	_check_records							integer;
	_count_target_variable					integer;
	_count_target_variable_with_core		integer;
	_count_target_variable_with_division	integer;
	_res_division							boolean;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_column = 'label_en'; else _lang_column := 'label'; end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_denominator_sub_population: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom;

	if array_length(_target_variable_denom,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_denominator_sub_population: The values in input argument _id_group = % must only be for the same target variable denominator!',_id_group;
	end if;
	-----------------------------------------------------------------
	_without_distinction_label := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_label'::character varying))::text;
	-----------------------------------------------------------------
	if _sub_population is null -- first calling of function
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)	
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when csp.label_en is null then $2 else csp.label_en end as label_en,
						case when t.label is null then $2 else t.label end as label
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
						left join	(
									select cspl.* from nfiesta_results.c_sub_population_language as cspl
									where cspl.language = $3
									and cspl.sub_population in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.sub_population
				)
		---------------------------------------------------
		,w17a as	(
					select
							w16.atomic_type as res_id,
							(w16.'||_lang_column||')::text AS res_label,
							w16.id as res_id_group,
							array[w16.atomic_type] as res_sub_population
					from
							w16
					)
		,w17 as	(
				select
						w17a.res_id,
						w17a.res_label,
						w17a.res_id_group,
						w17a.res_sub_population,
						null::boolean as res_division
				from
						w17a
				)
		-----------------------------------------
		,w18 as	(
				select
						a.*,
						b.target_variable
				from
						(
						select
								w17.res_id,
								w17.res_label,
								w17.res_sub_population,
								unnest(w17.res_id_group) as res_id_group
						from
								w17 where w17.res_id = 0
						) as a
				inner join (select * from nfiesta_results.t_result_group where web = true) as b on a.res_id_group = b.id
				)
		,w19 as	(
				select
						ctv.id,
						(ctv.metadata->''en'')->''local_densities'' as local_densities
				from
						nfiesta_results.c_target_variable as ctv where ctv.id in (select distinct w18.target_variable from w18)
				)
		,w20 as	(
				select
						w19.id,
						json_array_elements(w19.local_densities) as local_densities
				from
						w19
				)
		,w21 as	(
				select
						w20.id,
						(w20.local_densities->''ldsity_type'')->>''label'' as object_type_label
				from
						w20
				)
		,w22 as	(
				select w18.* from w18 where w18.target_variable in (select w21.id from w21 where w21.object_type_label = ''division'')
				)
		,w23 as	(
				select w18.* from w18 where w18.target_variable not in (select distinct w22.target_variable from w22) -- TARGET VARIABLE WITHOUT DIVISION
				)
		,w24a as	(
					select
							w23.res_id,
							w23.res_label,
							w23.res_sub_population,
							array_agg(res_id_group) as res_id_group
					from
							w23 group by w23.res_id, w23.res_label, w23.res_sub_population
					)
		,w24 as	(
				select
						w24a.res_id,
						w24a.res_label,
						w24a.res_sub_population,
						w24a.res_id_group,
						null::boolean as res_division
				from
						w24a
				)
				select w24.res_id, w24.res_label, w24.res_id_group, w24.res_sub_population, w24.res_division from w24 union all
				select w17.res_id, w17.res_label, w17.res_id_group, w17.res_sub_population, w17.res_division from w17 where w17.res_id is distinct from 0;
		'
		using _id_group, _without_distinction_label, _lang;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as sub_population,
								t.id
						from
								(
								select w11.sub_population, w11.id, w11.atomic_type from w11 union all
								select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		---------------------------------------------------
		,w16 as	(
				select
						w15.*,
						case when csp.label_en is null then $3 else csp.label_en end as label_en,
						case when t.label is null then $3 else t.label end as label
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
						left join	(
									select cspl.* from nfiesta_results.c_sub_population_language as cspl
									where cspl.language = $4
									and cspl.sub_population in (select w15.atomic_type from w15)
									) as t
						on w15.atomic_type = t.sub_population
				)
		---------------------------------------------------
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.'||_lang_column||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_sub_population
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _sub_population, _without_distinction_label, _lang
		into _check_records;	
		
		if _check_records = 0
		then
			-------------------------------------
			-- input _id_group must be for the same group of target variables:
			-- variant 1 => target variables without division ldsity object(s)
			-- variant 2 => target variables with core ldsity object and with division ldsity object(s)
			-------------------------------------
			with
			w1 as	(
					select
							ctv.id,
							(ctv.metadata->'en')->'local_densities' as local_densities
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id in	(
										select trg.target_variable
										from nfiesta_results.t_result_group as trg
										where trg.id in (select unnest(_id_group))
										and trg.web = true
										)
					)
			,w2 as	(
					select
							w1.id,
							json_array_elements(w1.local_densities) as local_densities
					from
							w1
					)
			,w3 as	(
					select
							w2.id,
							(w2.local_densities->'ldsity_type')->>'label' as object_type_label
					from
							w2
					)
			-------------------------------
			,w4a as	(select distinct w3.id from w3)
			,w4b as	(select distinct w3.id from w3 where w3.object_type_label = 'core')
			,w4c as	(select distinct w3.id from w3 where w3.object_type_label = 'division')
			-------------------------------
			,w5a as	(select 1 as id, count(w4a.*) as count_target_variable from w4a)
			,w5b as	(select 1 as id, count(w4b.*) as count_target_variable_with_core from w4b)
			,w5c as	(select 1 as id, count(w4c.*) as count_target_variable_with_division from w4c)
			-------------------------------
			select
					w5a.count_target_variable,
					w5b.count_target_variable_with_core,
					w5c.count_target_variable_with_division
			from
					w5a
					inner join w5b on w5a.id = w5b.id
					inner join w5c on w5a.id = w5c.id
			into
					_count_target_variable,
					_count_target_variable_with_core,
					_count_target_variable_with_division;
			-------------------------------------
			-------------------------------------
			if _sub_population = array[0] -- first user selection was without distinction
			then
				if _count_target_variable_with_division is distinct from 0
				then
					raise exception 'Error: 03: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then the values in input argument _id_group = % must only be for target variables that are without any division local density!',_id_group;
				end if;

				-- variant 1
				if _count_target_variable is distinct from _count_target_variable_with_core
				then
					raise exception 'Error: 04: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						_sub_population as res_sub_population,
						false as res_division; 
			else
				-- second and more user selections

				if _count_target_variable_with_division = 0
				then
					-- variant 1 => not exists any target variable with division
					if _count_target_variable is distinct from _count_target_variable_with_core
					then
						raise exception 'Error: 05: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
					end if;

					_res_division := false;
				else
					-- variant 2 => exist target variable with any division
					if	(_count_target_variable is distinct from _count_target_variable_with_division)
						or
						(_count_target_variable_with_core is distinct from _count_target_variable_with_division)
					then
						raise exception 'Error: 06: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables must contains some division local density!',_id_group;
					end if;

					_res_division := true;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						array_remove(_sub_population,0) as res_sub_population,
						_res_division as res_division;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.sub_population] as sub_population,
							t.id
					from
							(
							select w1.sub_population, array_agg(w1.id order by w1.id) as id
							from w1 where w1.sub_population = 0
							group by w1.sub_population
							) as t
					)
			,w3 as	(
					select w1.sub_population, array_agg(w1.id order by w1.id) as id
					from w1 where w1.sub_population > 0
					group by w1.sub_population
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_sub_population as csp
							on w3.sub_population = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_sub_population_category as cmspc
					where cmspc.sub_population_category in
						(
						select cspc.id from nfiesta_results.c_sub_population_category as cspc
						where cspc.sub_population in (select w6.sub_population from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.sub_population as sub_population,
							cspc2.sub_population as atomic_type
					from
							w7
							inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
							inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.sub_population from w8
					)
			,w10 as (
					select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.sub_population
					)
			,w11 as (
					select
							w6.sub_population,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.sub_population = w10.sub_population
					)
			,w12 as	(
					select
							w11.sub_population,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.sub_population,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.sub_population,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
							from	(
									select distinct a1.atomic_type, a1.sub_population
									from
										(
										select w13.atomic_type, w13.sub_population from w13
										union all
										select w5.sub_population as atomic_type, w5.sub_population from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as sub_population,
									null::integer[] as sub_population,
									t.id
							from
									(
									select w11.sub_population, w11.id, w11.atomic_type from w11 union all
									select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			---------------------------------------------------
			,w16 as	(
					select
							w15.*,
							case when csp.label_en is null then $3 else csp.label_en end as label_en,
							case when t.label is null then $3 else t.label end as label
					from
							w15
							left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
							left join	(
										select cspl.* from nfiesta_results.c_sub_population_language as cspl
										where cspl.language = $4
										and cspl.sub_population in (select w15.atomic_type from w15)
										) as t
							on w15.atomic_type = t.sub_population
					)
			---------------------------------------------------
			select
					w16.atomic_type as res_id,
					(w16.'||_lang_column||')::text AS res_label,
					w16.id as res_id_group,
					$2 || array[w16.atomic_type] as res_sub_population,
					null::boolean as res_division
			from
					w16
					
					where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _sub_population, _without_distinction_label, _lang;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]) is
'The function returns list of available sub populations for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of sub populations.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_ldsity_division" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_ldsity_division.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_ldsity_division
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_ldsity_division(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_ldsity_division
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang								integer;
		_lang_label							varchar;
		_not_specified_label				text;
		_not_specified_description			text;
		_not_specified_definition_variant	json;
		_not_specified_adr_spr				json;	
begin	
		-----------------------------------------------------------------
		_lang := nfiesta_results.fn_get_language_id(_jlang);
		_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_denominator_ldsity_division: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		_not_specified_label := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_label'::character varying))::text;
		_not_specified_description := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_description'::character varying))::text;
		-----------------------------------------------------------------
		_not_specified_definition_variant := json_agg(json_build_object('label',_not_specified_label,'description',_not_specified_description));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label',_not_specified_label,'description',_not_specified_description),'restriction',json_build_object('label',_not_specified_label,'description',_not_specified_description)));
		-----------------------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable_denom as target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						w2.target_variable,
						w2.id,
						((ctv.metadata->'''||_lang_label||''')->''local_densities'') as local_densities
				from
						w2
						inner join nfiesta_results.c_target_variable as ctv
						on w2.target_variable = ctv.id
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where (w4.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						(w5.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w5.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w5.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w5.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w5.local_densities->''ldsity'')->>''label'' as label,
						(w5.local_densities->''ldsity'')->>''description'' as description,	
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition_variant'' as definition_variant,
						w5.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w5	
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w7.object_type_label,''description'',w7.object_type_description),
							''ldsity_object'',json_build_object(''label'',w7.object_label,''description'',w7.object_description),
							''ldsity'',json_build_object(''label'',w7.label,''description'',w7.description),
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition_variant'',w7.definition_variant,
							''area_domain_restrictions'',w7.area_domain_restrictions,
							''sub_population_restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						--w8.new_id,
						json_agg(w8.local_densities order by w8.new_id) as local_densities
				from
						w8 group by w8.target_variable --, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_label, _not_specified_description, _not_specified_definition_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator_ldsity_division(character varying, integer[]) is
'The function returns list of available labels of division local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_ldsity_division(character varying, integer[]) to public;
-- </function>



DROP FUNCTION nfiesta_results.fn_get_area_sub_population_categories(integer, integer, integer);
-- <function name="fn_get_area_sub_population_categories" schema="nfiesta_results" src="functions/fn_get_area_sub_population_categories.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta_results.fn_get_area_sub_population_categories(integer, integer, integer, integer)
--DROP FUNCTION nfiesta_results.fn_get_area_sub_population_categories(integer, integer, integer, integer);
CREATE OR REPLACE FUNCTION nfiesta_results.fn_get_area_sub_population_categories(
		_target_variable integer, _area_or_sub_pop integer, _id integer, _lang integer)
RETURNS TABLE (
variable 	integer,
attype 		integer,
category 	integer,
label		varchar,
description	text,
label_en	varchar,
description_en	text
)
AS
$function$
DECLARE
	_lang_label					varchar;
	_lang_column_label			text;
	_lang_column_description	text;
BEGIN

IF _target_variable IS NULL THEN
	RAISE WARNING 'Target variable is NULL.';
	RETURN QUERY SELECT NULL::int, NULL::int, NULL::int, NULL::varchar, NULL::text, NULL::varchar, NULL::text;
END IF;

IF _area_or_sub_pop IS NULL
THEN
	RAISE EXCEPTION 'Parameter area domain or sub_population is not choosen!';
END IF; 

IF _area_or_sub_pop NOT IN (100,200)
THEN
	RAISE EXCEPTION 'Parameter area domain or sub_population has to be either 100 (area_domain) or 200 (sub_population)!';
END IF; 

IF _target_variable IS NOT NULL AND NOT EXISTS (SELECT id FROM nfiesta_results.c_target_variable WHERE id = _target_variable)
THEN
	RAISE EXCEPTION 'Specified target_variable (%) does not exist in table c_target_variable!', _target_variable;
END IF;

IF _target_variable IS NOT NULL AND NOT EXISTS (
	SELECT t1.id
	FROM nfiesta_results.t_variable AS t1
	WHERE t1.target_variable = _target_variable)
THEN 
	RAISE EXCEPTION 'Specified target_variable(%) does not exist in the data (t_variable)!', _target_variable;
END IF;

---------------------------------------
_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);

if _lang_label = 'en'::varchar(2)
then
	_lang_column_label = 't2.label_en';
	_lang_column_description = 't2.description_en';
else
	_lang_column_label = 't4.label';
	_lang_column_description = 't4.description';
end if;
---------------------------------------

CASE WHEN _area_or_sub_pop = 100
THEN
	IF _id IS NOT NULL AND _id != 0 AND NOT EXISTS (SELECT id FROM nfiesta_results.c_area_domain WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Specified area_domain (%) does not exist in table c_area_domain!', _id;
	END IF;

	IF _id IS NOT NULL AND NOT EXISTS (
		SELECT t1.id
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.c_area_domain_category AS t2
		ON t1.area_domain_category = t2.id
		WHERE t1.target_variable = _target_variable AND
			CASE WHEN _id != 0 THEN t2.area_domain = _id
			WHEN _id = 0 THEN t2.area_domain IS NULL	
			ELSE true END)
	THEN 
		RAISE EXCEPTION 'Specified combination of target_variable(%) and area_domain (%) does not exist in the data (t_variable)!', _target_variable, _id;
	END IF;

	RETURN QUERY EXECUTE
	'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
		SELECT
			t1.target_variable,
			coalesce(t2.variable_superior,t1.id) AS variable_superior,
			coalesce(t2.variable, t1.id) AS variable
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t2.variable = t1.id
		LEFT JOIN nfiesta_results.c_area_domain_category AS t3
		ON t1.area_domain_category = t3.id
		WHERE 	t1.target_variable = $1 AND
			CASE WHEN $2 IS NOT NULL AND $2 != 0 THEN t3.area_domain = $2 ELSE t1.area_domain_category IS NULL END
		UNION ALL
		SELECT
			t1.target_variable,
			t2.variable_superior,
			t2.variable
		FROM w_variables AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t1.variable_superior = t2.variable
		WHERE t2.variable IS NOT NULL
	)
	, w_attr_types AS (
		SELECT
			t1.target_variable,
			t1.variable_superior,
			t1.variable, 
			t3.area_domain AS area_domain_sup,
			t5.area_domain, 
			t3.id AS id_cat_sup,
			--t3.label AS category_sup,
			t5.id AS id_cat
			--t5.label AS category
		FROM w_variables AS t1
		INNER JOIN
			nfiesta_results.t_variable AS t2
		ON 
			t1.variable_superior = t2.id
		LEFT JOIN
			nfiesta_results.c_area_domain_category AS t3
		ON t2.area_domain_category = t3.id
		INNER JOIN
			nfiesta_results.t_variable AS t4
		ON 
			t1.variable = t4.id
		LEFT JOIN
			nfiesta_results.c_area_domain_category AS t5
		ON t4.area_domain_category = t5.id
	),
	w_union AS (
		SELECT	target_variable, variable_superior AS variable,  area_domain_sup AS area_domain
		FROM	w_attr_types
		UNION ALL
		SELECT	target_variable, variable,  area_domain
		FROM	w_attr_types
	)
	,w_distinct AS (
		SELECT DISTINCT target_variable, area_domain
		FROM w_union
	)
	SELECT
		t3.id AS variable,
		t2.area_domain AS type,
		t2.id AS category,
		'||_lang_column_label||' as label,
		'||_lang_column_description||' as description,
		t2.label_en,
		t2.description_en
	FROM
		w_distinct AS t1
	LEFT JOIN
		nfiesta_results.c_area_domain_category AS t2
	ON 	t1.area_domain = t2.area_domain
	-----------------------------------
	left join	(
				select cadcl.* from nfiesta_results.c_area_domain_category_language as cadcl
				where cadcl.language = $3
				and cadcl.area_domain_category in	(
													select cadc.id from nfiesta_results.c_area_domain_category as cadc
													where cadc.area_domain in (select w_distinct.area_domain from w_distinct)
													)
				) as t4
	on t2.id = t4.area_domain_category
	-----------------------------------
	INNER JOIN
		nfiesta_results.t_variable AS t3
	ON t1.target_variable = t3.target_variable AND
	CASE WHEN t2.id IS NOT NULL THEN t3.area_domain_category = t2.id
	ELSE t3.area_domain_category IS NULL END
	ORDER BY t3.id
	'
	USING _target_variable, _id, _lang;

WHEN _area_or_sub_pop = 200
THEN
	IF _id IS NOT NULL AND _id != 0 AND NOT EXISTS (SELECT id FROM nfiesta_results.c_sub_population WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Specified sub_population (%) does not exist in table c_sub_population!', _id;
	END IF;

	IF _id IS NOT NULL AND NOT EXISTS (
		SELECT t1.id
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.c_sub_population_category AS t2
		ON t1.sub_population_category = t2.id
		WHERE t1.target_variable = _target_variable AND
			CASE WHEN _id != 0 THEN t2.sub_population = _id
			WHEN _id = 0 THEN t2.sub_population IS NULL	
			ELSE true END)
	THEN 
		RAISE EXCEPTION 'Specified combination of target_variable(%) and sub_population (%) does not exist in the data (t_variable)!', _target_variable, _id;
	END IF;

	RETURN QUERY EXECUTE
	'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
		SELECT
			t1.target_variable,
			coalesce(t2.variable_superior,t1.id) AS variable_superior,
			coalesce(t2.variable, t1.id) AS variable
		FROM nfiesta_results.t_variable AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t2.variable = t1.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t3
		ON t1.sub_population_category = t3.id
		WHERE 	t1.target_variable = $1 AND
			CASE WHEN $2 IS NOT NULL AND $2 != 0 THEN t3.sub_population = $2 ELSE t1.sub_population_category IS NULL END
		UNION ALL
		SELECT
			t1.target_variable,
			t2.variable_superior,
			t2.variable
		FROM w_variables AS t1
		LEFT JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t1.variable_superior = t2.variable
		WHERE t2.variable IS NOT NULL
	)
	, w_attr_types AS (
		SELECT
			t1.target_variable,
			t1.variable_superior,
			t1.variable, 
			t3.sub_population AS sub_population_sup,
			t5.sub_population, 
			t3.id AS id_cat_sup,
			--t3.label AS category_sup,
			t5.id AS id_cat
			--t5.label AS category
		FROM w_variables AS t1
		INNER JOIN
			nfiesta_results.t_variable AS t2
		ON 
			t1.variable_superior = t2.id
		LEFT JOIN
			nfiesta_results.c_sub_population_category AS t3
		ON t2.sub_population_category = t3.id
		INNER JOIN
			nfiesta_results.t_variable AS t4
		ON 
			t1.variable = t4.id
		LEFT JOIN
			nfiesta_results.c_sub_population_category AS t5
		ON t4.sub_population_category = t5.id
	),
	w_union AS (
		SELECT	target_variable, variable_superior AS variable,  sub_population_sup AS sub_population
		FROM	w_attr_types
		UNION ALL
		SELECT	target_variable, variable,  sub_population
		FROM	w_attr_types
	)
	,w_distinct AS (
		SELECT DISTINCT target_variable, sub_population
		FROM w_union
	)
	SELECT
		t3.id AS variable,
		t2.sub_population AS type,
		t2.id AS category,
		'||_lang_column_label||' as label,
		'||_lang_column_description||' as description,
		t2.label_en,
		t2.description_en
	FROM
		w_distinct AS t1
	LEFT JOIN
		nfiesta_results.c_sub_population_category AS t2
	ON 	t1.sub_population = t2.sub_population
	-----------------------------------
	left join	(
				select cspcl.* from nfiesta_results.c_sub_population_category_language as cspcl
				where cspcl.language = $3
				and cspcl.sub_population_category in	(
														select cspc.id from nfiesta_results.c_sub_population_category as cspc
														where cspc.sub_population in (select w_distinct.sub_population from w_distinct)
														)
				) as t4
	on t2.id = t4.sub_population_category
	-----------------------------------
	INNER JOIN
		nfiesta_results.t_variable AS t3
	ON t1.target_variable = t3.target_variable AND
	CASE WHEN t2.id IS NOT NULL THEN t3.sub_population_category = t2.id
	ELSE t3.sub_population_category IS NULL END
	ORDER BY t3.id'
	USING _target_variable, _id, _lang;
ELSE
	RAISE EXCEPTION 'Uknown attribute type!';
END CASE;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta_results.fn_get_area_sub_population_categories(integer,integer,integer,integer) IS 'Function returns table with all hierarchically superior variables and its complementary categories within area domain or sub_population. Input parameters are target_variable (id from table c_target_variable), area_or_sub_pop (100 for area_domain, 200 for sub_population) and id (id from table c_area_domain or id from table c_sub_population)';
-- </function>



DROP FUNCTION nfiesta_results.fn_get_attribute_categories4target_variable(integer, integer, integer, integer, integer, integer);
-- <function name="fn_get_attribute_categories4target_variable" schema="nfiesta_results" src="functions/fn_get_attribute_categories4target_variable.sql">
--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta_results.fn_get_attribute_categories4target_variable(integer, integer, integer, integer, integer, integer, integer)
--DROP FUNCTION nfiesta_results.fn_get_attribute_categories4target_variable(integer, integer, integer, integer, integer, integer, integer);
CREATE OR REPLACE FUNCTION nfiesta_results.fn_get_attribute_categories4target_variable(
	_lang integer,
	_numerator_target_variable integer,
	_numerator_area_domain integer DEFAULT NULL::int,
	_numerator_sub_population integer DEFAULT NULL::int,
	_denominator_target_variable integer DEFAULT NULL::int,
	_denominator_area_domain integer DEFAULT NULL::int,
	_denominator_sub_population integer DEFAULT NULL::int)
RETURNS TABLE (
	nominator_variable integer,
	denominator_variable integer, 
	label varchar,
	description text,
	label_en varchar, 
	description_en text
)
AS
$function$
DECLARE
	_without_distinction_label			text;
	_without_distinction_description	text;
BEGIN

IF _lang IS NULL THEN
	RAISE EXCEPTION 'Input argument _lang must not be NULL!';
END IF;	

IF _numerator_target_variable IS NULL THEN
	RAISE EXCEPTION 'Target variable for numerator is NULL!';
END IF;

IF NOT EXISTS (SELECT id FROM nfiesta_results.c_target_variable WHERE id = _numerator_target_variable)
THEN
	RAISE EXCEPTION 'Specified numerator target variable (%) does not exist in table c_target_variable!', _numerator_target_variable;
END IF;

IF _numerator_area_domain IS NOT NULL AND _numerator_area_domain != 0
THEN
	IF NOT EXISTS (SELECT * FROM nfiesta_results.c_area_domain WHERE id = _numerator_area_domain)
	THEN RAISE EXCEPTION 'Given numerator area domain (%) does not exist in table c_area_domain!', _numerator_area_domain;
	END IF;
END IF;

IF _numerator_sub_population IS NOT NULL AND _numerator_sub_population != 0
THEN
	IF NOT EXISTS (SELECT * FROM nfiesta_results.c_sub_population WHERE id = _numerator_sub_population)
	THEN RAISE EXCEPTION 'Given numerator sub population (%) does not exist in table c_sub_population!', _numerator_sub_population;
	END IF;
END IF;

IF _denominator_target_variable IS NOT NULL
THEN
	IF NOT EXISTS (SELECT * FROM nfiesta_results.c_target_variable WHERE id = _denominator_target_variable)
	THEN RAISE EXCEPTION 'Given denominator target variable (%) does not exist in table c_target_variable!', _denominator_targer_variable;
	END IF;
END IF;

IF _denominator_area_domain IS NOT NULL AND _denominator_area_domain != 0
THEN
	IF NOT EXISTS (SELECT * FROM nfiesta_results.c_area_domain WHERE id = _denominator_area_domain)
	THEN RAISE EXCEPTION 'Given denominator area domain (%) does not exist in table c_area_domain!', _denominator_area_domain;
	END IF;
END IF;

IF _denominator_sub_population IS NOT NULL AND _denominator_sub_population != 0
THEN
	IF NOT EXISTS (SELECT * FROM nfiesta_results.c_sub_population WHERE id = _denominator_sub_population)
	THEN RAISE EXCEPTION 'Given denominator sub population (%) does not exist in table c_sub_population!', _denominator_sub_population;
	END IF;
END IF;

-----------------------------------------------------------------
if	(
	select cl.label = 'en'::varchar(2)
	from nfiesta_results.c_language as cl
	where cl.id = _lang
	)
then
	select cgh.label_en from nfiesta_results.c_gui_header as cgh
	where cgh.header = 'without_distinction_label'::character varying
	into _without_distinction_label;

	select cgh.label_en from nfiesta_results.c_gui_header as cgh
	where cgh.header = 'without_distinction_description'::character varying
	into _without_distinction_description;
else
	select cghl.label from nfiesta_results.c_gui_header_language as cghl
	where cghl.language = _lang
	and cghl.gui_header =	(
							select cgh.id from nfiesta_results.c_gui_header as cgh
							where cgh.header = 'without_distinction_label'::character varying
							)
	into _without_distinction_label;

	select cghl.label from nfiesta_results.c_gui_header_language as cghl
	where cghl.language = _lang
	and cghl.gui_header =	(
							select cgh.id from nfiesta_results.c_gui_header as cgh
							where cgh.header = 'without_distinction_description'::character varying
							)
	into _without_distinction_description;
end if;
-----------------------------------------------------------------

RETURN QUERY
	WITH w_ad_num AS(
		SELECT t1.variable, t1.category AS adc, t1.label AS adc_label, t1.description AS adc_description, t1.label_en AS adc_label_en, t1.description_en AS adc_description_en
		FROM nfiesta_results.fn_get_area_sub_population_categories(_numerator_target_variable,100,_numerator_area_domain,_lang) AS t1
	), w_sp_num AS (
		SELECT t1.variable, t1.category AS spc, t1.label AS spc_label, t1.description AS spc_description, t1.label_en AS spc_label_en, t1.description_en AS spc_description_en
		FROM nfiesta_results.fn_get_area_sub_population_categories(_numerator_target_variable,200,_numerator_sub_population,_lang) AS t1
	), w_numerator_variables AS (
		SELECT 	t1.variable AS id,
			t1.adc, adc_label, adc_description, adc_label_en, adc_description_en, 
			t2.spc, spc_label, spc_description, spc_label_en, spc_description_en
		FROM w_ad_num AS t1
		INNER JOIN w_sp_num AS t2
		ON t1.variable = t2.variable
	), w_numerator_variables_agg AS (
		SELECT array_agg(id ORDER BY id) AS num_variables
		FROM w_numerator_variables
	), w_ad_denom AS(
		SELECT t1.variable, t1.category AS adc, t1.label AS adc_label, t1.description AS adc_description, t1.label_en AS adc_label_en, t1.description_en AS adc_description_en
		FROM nfiesta_results.fn_get_area_sub_population_categories(_denominator_target_variable,100,_denominator_area_domain,_lang) AS t1
	), w_sp_denom AS (
		SELECT t1.variable, t1.category AS spc, t1.label AS spc_label, t1.description AS spc_description, t1.label_en AS spc_label_en, t1.description_en AS spc_description_en
		FROM nfiesta_results.fn_get_area_sub_population_categories(_denominator_target_variable,200,_denominator_sub_population,_lang) AS t1
	), w_denominator_variables AS (
		SELECT	t1.variable AS id, 
			t1.adc, adc_label, adc_description, adc_label_en, adc_description_en, 
			t2.spc, spc_label, spc_description, spc_label_en, spc_description_en
		FROM w_ad_denom AS t1
		INNER JOIN w_sp_denom AS t2
		ON t1.variable = t2.variable
	), w_denominator_variables_agg AS (
		SELECT array_agg(id ORDER BY id) AS denom_variables
		FROM w_denominator_variables
	),
	w_num_denom AS (
		SELECT t3.variable_numerator, t3.variable_denominator
		FROM 	w_numerator_variables_agg AS t1,
			w_denominator_variables_agg AS t2,
			nfiesta_results.fn_get_num_denom_variables(t1.num_variables, t2.denom_variables) AS t3
	), w_final AS (
		SELECT 
			t1.variable_numerator,
			t1.variable_denominator,
			coalesce(nullif(concat_ws(' - ', t2.adc_label, t2.spc_label),''),_without_distinction_label)::varchar AS label_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_label, t3.spc_label),''),_without_distinction_label)::varchar 
			ELSE NULL::varchar
			END AS label_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_description, t2.spc_description),''),_without_distinction_description)::text AS description_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_description, t3.spc_description),''),_without_distinction_description)::text
			ELSE NULL::text
			END AS description_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_label_en, t2.spc_label_en),''),'without distinction')::varchar AS label_en_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_label_en, t3.spc_label_en),''),'without distinction')::varchar 
			ELSE NULL::varchar
			END AS label_en_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_description_en, t2.spc_description_en),''),'Without distinction.')::text AS description_en_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_description_en, t3.spc_description_en),''),'Without distinction.')::text
			ELSE NULL::text
			END AS description_en_denom
		FROM
			w_num_denom AS t1
		INNER JOIN 
			w_numerator_variables AS t2
		ON t1.variable_numerator = t2.id
		LEFT JOIN
			w_denominator_variables AS t3
		ON t1.variable_denominator = t3.id
	) 
	SELECT
		variable_numerator,
		variable_denominator,
		concat_ws(' / ', label_num, label_denom)::varchar AS label, 
		concat_ws(' / ', description_num, description_denom)::text AS description, 
		concat_ws(' / ', label_en_num, label_en_denom)::varchar AS label_en, 
		concat_ws(' / ', description_en_num, description_en_denom)::text AS description_en 
	FROM w_final
	ORDER BY variable_numerator, variable_denominator;

/*		nfiesta_results.t_variable AS t2
	ON t1.variable_numerator = t2.id
	LEFT JOIN nfiesta_results.c_area_domain_category AS t3
	ON t2.area_domain_category = t3.id
	LEFT JOIN nfiesta_results.c_sub_population_category AS t4
	ON t2.sub_population_category = t4.id
	INNER JOIN nfiesta_results.t_variable AS t5
	ON t1.variable_denominator = t5.id;	
	LEFT JOIN nfiesta_results.c_area_domain_category AS t6
	ON t5.area_domain_category = t6.id
	LEFT JOIN nfiesta_results.c_sub_population_category AS t7
	ON t5.sub_population_category = t7.id;
*/

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta_results.fn_get_attribute_categories4target_variable(integer,integer,integer,integer,integer,integer,integer) IS 'Function returns table with attribute categories (area domain and sub population categories) for given target variable and area domain/sub population. It also solves the right combination of numerator and denominator categories.';
-- </function>



-- <function name="fn_get_user_query" schema="nfiesta_results" src="functions/fn_get_user_query.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_query
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_query(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_query
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	estimation_cell					text,
	variable_area_domain_num		text,
	variable_sub_population_num		text,
	variable_area_domain_denom		text,
	variable_sub_population_denom	text,	
	point_estimate					numeric,
	standard_deviation				numeric,
	variation_coeficient			numeric,
	--sample_size					integer,
	--min_sample_size				integer,
	interval_estimation				numeric
)
language plpgsql
stable
as
$$
declare
	_lang								integer;
	_lang_label							varchar;
	_lang_suffix						text;
	_without_distinction_label			text;
	_estimate_type						integer;
	_target_variable					integer;
	_target_variable_denom				integer;
	_estimation_period					integer;
	_panel_refyearset_group				integer;
	_phase_estimate_type				integer;
	_phase_estimate_type_denom			integer;
	_estimation_cell_collection			integer;
	_area_domain						integer;
	_area_domain_denom					integer;
	_sub_population						integer;
	_sub_population_denom				integer;
	_cond_area_domain					text;
	_cond_sub_population				text;
	_cond_area_domain_denom				text;
	_cond_sub_population_denom			text;
	_check4unit							integer;
	_coeficient4ratio					numeric;

	_estimation_cell_from_hierarchy_for_ecc_select_by_user			integer;
	_estimation_cell_from_hierarchy_for_ecc_select_by_user_superior	integer;
	_heighest_estimation_cell_select_by_user						integer;
	_heighest_estimation_cell_collection_select_by_user				integer;
	_estimation_cell_collection_array								integer[];
	_test_occurence_result_group									integer;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	if _lang_label = 'en'::varchar(2) then _lang_suffix = '_en'; else _lang_suffix := ''; end if;
	-----------------------------------------------------------------
	_without_distinction_label := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_label'::character varying))::text;	
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_query: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_query: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select
			trg.estimate_type,
			trg.target_variable,
			trg.target_variable_denom,
			trg.estimation_period,
			trg.panel_refyearset_group,
			trg.phase_estimate_type,
			trg.phase_estimate_type_denom,
			trg.estimation_cell_collection,
			trg.area_domain,
			trg.area_domain_denom,
			trg.sub_population,
			trg.sub_population_denom
	from
			nfiesta_results.t_result_group as trg
	where
			trg.id = _id_group[1]
	into
			_estimate_type,
			_target_variable,
			_target_variable_denom,
			_estimation_period,
			_panel_refyearset_group,
			_phase_estimate_type,
			_phase_estimate_type_denom,
			_estimation_cell_collection,
			_area_domain,
			_area_domain_denom,
			_sub_population,
			_sub_population_denom;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	select tech.cell from nfiesta_results.t_estimation_cell_hierarchy as tech
	where tech.cell in	(
						select cec.id from nfiesta_results.c_estimation_cell as cec
						where cec.estimation_cell_collection = _estimation_cell_collection
						)
	order by cell limit 1
	into _estimation_cell_from_hierarchy_for_ecc_select_by_user;

	if _estimation_cell_from_hierarchy_for_ecc_select_by_user is null
	then
		-- selected estimation cell collection is maybe the heighest
		select count(tech.cell_superior) from nfiesta_results.t_estimation_cell_hierarchy as tech
		where tech.cell_superior in	(
									select cec.id from nfiesta_results.c_estimation_cell as cec
									where cec.estimation_cell_collection = _estimation_cell_collection
									)
		into _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior;

		if _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior = 0
		then
			raise exception 'Error: 03: fn_get_user_query: For user selected estimation_cell_collection = % not exists any estimation cell in t_estimation_cell_hierarchy table!',_estimation_cell_collection;
		end if;

		-- user selected the heighest estimation cell collection => test for occurrence in t_result_group table is not needed
		_estimation_cell_collection_array := array[_estimation_cell_collection];
	else
		with
		w1 as	(
				select * from nfiesta_results.fn_get_heighest_estimation_cell(array[_estimation_cell_from_hierarchy_for_ecc_select_by_user]) as res
				)
		select w1.res[1] from w1
		into _heighest_estimation_cell_select_by_user; -- the heighest estimation cell from hierarchy

		select cec.estimation_cell_collection from nfiesta_results.c_estimation_cell as cec
		where cec.id = _heighest_estimation_cell_select_by_user
		into _heighest_estimation_cell_collection_select_by_user; -- the heighest estimation cell collection from hierarchy

		-- test for occurrence in t_result_group
		select id from nfiesta_results.t_result_group
		where estimate_type = _estimate_type
		and target_variable = _target_variable
		and coalesce(target_variable_denom,0) = coalesce(_target_variable_denom,0)
		and estimation_period = _estimation_period
		and phase_estimate_type = _phase_estimate_type
		and coalesce(phase_estimate_type_denom,0) = coalesce(_phase_estimate_type_denom,0)
		and estimation_cell_collection = _heighest_estimation_cell_collection_select_by_user
		and coalesce(area_domain,0) = coalesce(_area_domain,0)
		and coalesce(area_domain_denom,0) = coalesce(_area_domain_denom,0)
		and coalesce(sub_population,0) = coalesce(_sub_population,0)
		and coalesce(sub_population_denom,0) = coalesce(_sub_population_denom,0)
		and web = true
		into _test_occurence_result_group;

		if _test_occurence_result_group is null
		then
			_estimation_cell_collection_array := array[_estimation_cell_collection];
		else
			_estimation_cell_collection_array := array[_estimation_cell_collection] || array[_heighest_estimation_cell_collection_select_by_user];
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _estimate_type = 1 -- TOTAL
	then
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$2';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$3';
		end if;
		---------------------------------------------------	
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$9,
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					null::integer,
					null::integer,
					null::integer
					)
				)
		,w2 as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $4
				and ttec.panel_refyearset_group = $5
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $6)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($6)))
				and ttec.phase_estimate_type = $7
				)
		,w3 as	(
				select
						w2.id as id_t_total_estimate_conf,
						w2.estimation_cell,
						w2.variable,
						w2.phase_estimate_type,
						w2.force_synthetic,
						w2.estimation_period,
						w2.panel_refyearset_group,
						w1.nominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,
						tv.area_domain_category,
						tv.sub_population_category
				from
						w2
						inner join w1 on w2.variable = w1.nominator_variable
						inner join nfiesta_results.t_variable as tv on w2.variable = tv.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf
				from
						nfiesta_results.t_estimate_conf as tec 
				where
						tec.total_estimate_conf in (select w3.id_t_total_estimate_conf from w3)
				and
						tec.estimate_type = $8
				)
		,w5 as	(
				select
						w4.*,
						w3.*
				from
						w4 inner join w3 on w4.total_estimate_conf = w3.id_t_total_estimate_conf
				)
		,w6 as	(
				select
						tr.*,
						w5.*
				from
						nfiesta_results.t_result as tr
						inner join w5 on tr.estimate_conf = w5.id_t_estimate_conf
				where
						tr.is_latest = true
				)
		,w7 as	(
				select
						cec.id as estimation_cell_id,
						--cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w6.area_domain_category is null then 0
							else cadc.id
						end
							as variable_area_domain_id,	
						---------------------------------
						--case
						--	when w6.area_domain_category is null then ''bez rozlišení''
						--	else replace(cadc.label,'';'',''; '')
						--end
						--	as variable_area_domain,
						---------------------------------
						case
							when w6.area_domain_category is null then ''without distinction''
							else replace(cadc.label_en,'';'',''; '')
						end
							as variable_area_domain_en,
						-----------------------------------------------------
						case
							when w6.sub_population_category is null then 0
							else cspc.id
						end
							as variable_sub_population_id,
						---------------------------------
						--case
						--	when w6.sub_population_category is null then ''bez rozlišení''
						--	else replace(cspc.label,'';'',''; '')
						--end
						--	as variable_sub_population,
						---------------------------------
						case
							when w6.sub_population_category is null then ''without distinction''
							else replace(cspc.label_en,'';'',''; '')
						end
							as variable_sub_population_en,
						-----------------------------------------------------
						w6.point as point_estimate,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else sqrt(w6.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null or w6.point = 0.0::numeric then null::numeric
							else (sqrt(w6.var) / w6.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						--w6.act_ssize as sample_size,
						--w6.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else (1.96 * sqrt(w6.var))
						end
							as interval_estimation
				from
						w6
						inner join nfiesta_results.c_estimation_cell as cec on w6.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc on w6.area_domain_category = cadc.id
						left join nfiesta_results.c_sub_population_category as cspc on w6.sub_population_category = cspc.id
				)
		,w8 as	(
				select
						w7.*,
						-----------------------------------------------------
						t1.description as estimation_cell,
						-----------------------------------------------------
						case
							when t2.area_domain_category is null then $10
							else replace(t2.label,'';'',''; '')
						end
							as variable_area_domain,
						-----------------------------------------------------
						case
							when t3.sub_population_category is null then $10
							else replace(t3.label,'';'',''; '')
						end
							as variable_sub_population			
				from
						w7
						left join	(
									select cecl.* from nfiesta_results.c_estimation_cell_language as cecl
									where cecl.language = $9
									and cecl.estimation_cell in (select w7.estimation_cell_id from w7)
									) as t1
						on w7.estimation_cell_id = t1.estimation_cell

						left join	(
									select cadcl.* from nfiesta_results.c_area_domain_category_language as cadcl
									where cadcl.language = $9
									and cadcl.area_domain_category in (select w7.variable_area_domain_id from w7)
									) as t2
						on w7.variable_area_domain_id = t2.area_domain_category

						left join	(
									select cspcl.* from nfiesta_results.c_sub_population_category_language as cspcl
									where cspcl.language = $9
									and cspcl.sub_population_category in (select w7.variable_sub_population_id from w7)
									) as t3
						on w7.variable_sub_population_id = t3.sub_population_category						
				)
		select
				w8.estimation_cell'||_lang_suffix||' as estimation_cell,
				w8.variable_area_domain'||_lang_suffix||' as variable_area_domain_num,
				w8.variable_sub_population'||_lang_suffix||' as variable_sub_population_num,
				null::text as variable_area_domain_denom,
				null::text as variable_sub_population_denom,
				round(w8.point_estimate::numeric,2) as point_estimate,
				round(w8.standard_deviation::numeric,2) as standard_deviation,
				round(w8.variation_coeficient::numeric,2) as variation_coeficient,
				--w8.sample_size::integer,
				--(ceil(w8.min_sample_size::numeric))::integer as min_sample_size,
				round(w8.interval_estimation::numeric,2) as interval_estimation
		from
				w8 order by w8.estimation_cell_id, w8.variable_area_domain_id, w8.variable_sub_population_id;
		'
		using
				_target_variable,
				_area_domain,
				_sub_population,
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection_array,
				_phase_estimate_type,
				_estimate_type,
				_lang,
				_without_distinction_label;
				
	else -- RATIO
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$3';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$4';
		end if;
		---------------------------------------------------
		if _area_domain_denom is null
		then
			_cond_area_domain_denom := 'null::integer';
		else
			_cond_area_domain_denom := '$5';
		end if;
		---------------------------------------------------
		if _sub_population_denom is null
		then
			_cond_sub_population_denom := 'null::integer';
		else
			_cond_sub_population_denom := '$6';
		end if;
		---------------------------------------------------
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_num,
						((w2.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_denom
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.*,
						case
							when (w3.target_variable_num = w3.target_variable_denom) or (w3.unit_num = w3.unit_denom) then 0
							else 1
						end as check4unit
				from
						w3
				)
		select w4.check4unit from w4
		'
		using _target_variable, _target_variable_denom
		into _check4unit;		
		---------------------------------------------------
		if _check4unit = 0
		then	
			_coeficient4ratio := 100.0::numeric;
		else
			_coeficient4ratio := 1.0::numeric;
		end if;
		---------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$13,
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					$2,
					'||_cond_area_domain_denom||',
					'||_cond_sub_population_denom||'
					)
				)
		,w2a as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($9)))
				and ttec.phase_estimate_type = $10
				)
		,w2b as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.denominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				--and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection in (select unnest($9)))
				and ttec.phase_estimate_type = $10
				)
		,w3 as	(
				select
						w2a.id as id_t_total_estimate_conf_nom,
						w2b.id as id_t_total_estimate_conf_denom,
						w2a.estimation_cell,
						w2a.variable,
						w2a.phase_estimate_type,
						w2a.force_synthetic,
						w2a.estimation_period,
						w2a.panel_refyearset_group,
						w1.nominator_variable,
						w1.denominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,	
						tv1.area_domain_category as area_domain_category_nom,
						tv2.area_domain_category as area_domain_category_denom,
						tv1.sub_population_category as sub_population_category_nom,
						tv2.sub_population_category as sub_population_category_denom
				from
						w2a
						inner join w1 on w2a.variable = w1.nominator_variable
						inner join w2b on w2b.variable = w1.denominator_variable
						inner join nfiesta_results.t_variable as tv1 on w2a.variable = tv1.id
						inner join nfiesta_results.t_variable as tv2 on w2b.variable = tv2.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf,
						tec.denominator,
						w3.*
				from
						nfiesta_results.t_estimate_conf as tec
						
						inner join w3
						on tec.total_estimate_conf = w3.id_t_total_estimate_conf_nom
						and tec.denominator = w3.id_t_total_estimate_conf_denom
				where
						tec.estimate_type = $11
				)
		,w5 as	(
				select
						tr.id,
						tr.estimate_conf,
						tr.point * $12 as point,
						tr.var * $12 as var,
						--tr.min_ssize,
						--tr.act_ssize,
						w4.*
				from
						nfiesta_results.t_result as tr
						inner join w4 on tr.estimate_conf = w4.id_t_estimate_conf
				where
						tr.is_latest = true
				)			
		,w6 as	(
				select
						cec.id as estimation_cell_id,
						--cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w5.area_domain_category_nom is null then 0
							else cadc1.id
						end
							as variable_area_domain_id_nom,	
						---------------------------------
						case
							when w5.area_domain_category_denom is null then 0
							else cadc2.id
						end
							as variable_area_domain_id_denom,	
						---------------------------------
						--case
						--	when w5.area_domain_category_nom is null then ''bez rozlišení''
						--	else replace(cadc1.label,'';'',''; '')
						--end
						--	as variable_area_domain_nom,
						---------------------------------
						--case
						--	when w5.area_domain_category_denom is null then ''bez rozlišení''
						--	else replace(cadc2.label,'';'',''; '')
						--end
						--	as variable_area_domain_denom,
						---------------------------------
						case
							when w5.area_domain_category_nom is null then ''without distinction''
							else replace(cadc1.label_en,'';'',''; '')
						end
							as variable_area_domain_nom_en,
						---------------------------------
						case
							when w5.area_domain_category_denom is null then ''without distinction''
							else replace(cadc2.label_en,'';'',''; '')
						end
							as variable_area_domain_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						case
							when w5.sub_population_category_nom is null then 0
							else cspc1.id
						end
							as variable_sub_population_id_nom,	
						---------------------------------
						case
							when w5.sub_population_category_denom is null then 0
							else cspc2.id
						end
							as variable_sub_population_id_denom,	
						---------------------------------
						--case
						--	when w5.sub_population_category_nom is null then ''bez rozlišení''
						--	else replace(cspc1.label,'';'',''; '')
						--end
						--	as variable_sub_population_nom,
						---------------------------------
						--case
						--	when w5.sub_population_category_denom is null then ''bez rozlišení''
						--	else replace(cspc2.label,'';'',''; '')
						--end
						--	as variable_sub_population_denom,
						---------------------------------
						case
							when w5.sub_population_category_nom is null then ''without distinction''
							else replace(cspc1.label_en,'';'',''; '')
						end
							as variable_sub_population_nom_en,
						---------------------------------
						case
							when w5.sub_population_category_denom is null then ''without distinction''
							else replace(cspc2.label_en,'';'',''; '')
						end
							as variable_sub_population_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						w5.point as point_estimate,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else sqrt(w5.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null or w5.point = 0.0::numeric then null::numeric
							else (sqrt(w5.var) / w5.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						--w5.act_ssize as sample_size,
						--w5.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else (1.96 * sqrt(w5.var))
						end
							as interval_estimation
				from
						w5
						inner join nfiesta_results.c_estimation_cell as cec on w5.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc1 on w5.area_domain_category_nom = cadc1.id
						left join nfiesta_results.c_area_domain_category as cadc2 on w5.area_domain_category_denom = cadc2.id
						left join nfiesta_results.c_sub_population_category as cspc1 on w5.sub_population_category_nom = cspc1.id
						left join nfiesta_results.c_sub_population_category as cspc2 on w5.sub_population_category_denom = cspc2.id
				)
		,w7 as	(
				select
						w6.*,
						-----------------------------------------------------
						t1.description as estimation_cell,
						-----------------------------------------------------
						case
							when t2.area_domain_category is null then $14
							else replace(t2.label,'';'',''; '')
						end
							as variable_area_domain_nom,
						-----------------------------------------------------
						case
							when t3.area_domain_category is null then $14
							else replace(t3.label,'';'',''; '')
						end
							as variable_area_domain_denom,
						-----------------------------------------------------							
						-----------------------------------------------------
						case
							when t4.sub_population_category is null then $14
							else replace(t4.label,'';'',''; '')
						end
							as variable_sub_population_nom,
						-----------------------------------------------------
						case
							when t5.sub_population_category is null then $14
							else replace(t5.label,'';'',''; '')
						end
							as variable_sub_population_denom
				from
						w6
						left join	(
									select cecl.* from nfiesta_results.c_estimation_cell_language as cecl
									where cecl.language = $13
									and cecl.estimation_cell in (select w6.estimation_cell_id from w6)
									) as t1
						on w6.estimation_cell_id = t1.estimation_cell

						left join	(
									select cadcl.* from nfiesta_results.c_area_domain_category_language as cadcl
									where cadcl.language = $13
									and cadcl.area_domain_category in (select w6.variable_area_domain_id_nom from w6)
									) as t2
						on w6.variable_area_domain_id_nom = t2.area_domain_category

						left join	(
									select cadcl.* from nfiesta_results.c_area_domain_category_language as cadcl
									where cadcl.language = $13
									and cadcl.area_domain_category in (select w6.variable_area_domain_id_denom from w6)
									) as t3
						on w6.variable_area_domain_id_denom = t3.area_domain_category						

						left join	(
									select cspcl.* from nfiesta_results.c_sub_population_category_language as cspcl
									where cspcl.language = $13
									and cspcl.sub_population_category in (select w6.variable_sub_population_id_nom from w6)
									) as t4
						on w6.variable_sub_population_id_nom = t4.sub_population_category

						left join	(
									select cspcl.* from nfiesta_results.c_sub_population_category_language as cspcl
									where cspcl.language = $13
									and cspcl.sub_population_category in (select w6.variable_sub_population_id_denom from w6)
									) as t5
						on w6.variable_sub_population_id_denom = t5.sub_population_category
				)
		select
				w7.estimation_cell'||_lang_suffix||' as estimation_cell,
				--concat(w7.variable_area_domain_nom'||_lang_suffix||','' / '',w6.variable_area_domain_denom'||_lang_suffix||') as variable_area_domain,
				--concat(w7.variable_sub_population_nom'||_lang_suffix||','' / '',w6.variable_sub_population_denom'||_lang_suffix||') as variable_sub_population,
				w7.variable_area_domain_nom'||_lang_suffix||' as variable_area_domain_num,
				w7.variable_sub_population_nom'||_lang_suffix||' as variable_sub_population_num,
				w7.variable_area_domain_denom'||_lang_suffix||' as variable_area_domain_denom,				
				w7.variable_sub_population_denom'||_lang_suffix||' as variable_sub_population_denom,				
				round(w7.point_estimate::numeric,2) as point_estimate,
				round(w7.standard_deviation::numeric,2) as standard_deviation,
				round(w7.variation_coeficient::numeric,2) as variation_coeficient,
				--w7.sample_size::integer,
				--(ceil(w7.min_sample_size::numeric))::integer as min_sample_size,
				round(w7.interval_estimation::numeric,2) as interval_estimation
		from
				w7
		order
				by	w7.estimation_cell_id,
					w7.variable_area_domain_id_nom,
					w7.variable_area_domain_id_denom,
					w7.variable_sub_population_id_nom,
					w7.variable_sub_population_id_denom;
		'
		using
				_target_variable,
				_target_variable_denom,
				_area_domain,
				_sub_population,
				_area_domain_denom,
				_sub_population_denom,				
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection_array,
				_phase_estimate_type,
				_estimate_type,
				_coeficient4ratio,
				_lang,
				_without_distinction_label;	
	end if;

end;
$$
;

comment on function nfiesta_results.fn_get_user_query(character varying, integer[]) is
'The function returns result estimations for gived group of result and attribute variables from t_result table.';

grant execute on function nfiesta_results.fn_get_user_query(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata_numerator" schema="nfiesta_results" src="functions/fn_get_user_metadata_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata_numerator
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang									integer;
	_lang_label								varchar;
	_not_specified_label					text;
	_not_specified_description				text;
	_without_distinction_label				text;
	_without_distinction_description		text;	
	_not_specified_definition_variant		json;
	_not_specified_adr_spr					json;
	_target_variable						integer[];
	_res_indicator_label					text;
	_res_indicator_description				text;
	_res_state_or_change_label				text;
	_res_state_or_change_description		text;
	_res_unit_label							text;
	_res_unit_description					text;
	_check_area_domain						integer[];
	_res_area_domain_attribute_json			json;
	_check_sub_population					integer[];
	_res_sub_population_attribute_json		json;
	--_check_core_ldsities					integer;
	_check_division_ldsities				integer;
	_res_core_json							json;
	_res_division_json						json;	
	_res									json;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata_numerator: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata_numerator: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable) -- column for NUMERATOR
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable;

	if array_length(_target_variable,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	_not_specified_label := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_label'::character varying))::text;
	_not_specified_description := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_description'::character varying))::text;
	_without_distinction_label := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_label'::character varying))::text;
	_without_distinction_description := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_description'::character varying))::text;
	-----------------------------------------------------------------
	_not_specified_definition_variant := json_agg(json_build_object('label',_not_specified_label,'description',_not_specified_description));
	_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label',_not_specified_label,'description',_not_specified_description),'restriction',json_build_object('label',_not_specified_label,'description',_not_specified_description)));
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	select
			((ctv.metadata->'''||_lang_label||''')->''indicator'')->>''label'' as label,		
			((ctv.metadata->'''||_lang_label||''')->''indicator'')->>''description'' as description
	from
			nfiesta_results.c_target_variable as ctv
	where
			ctv.id = $1
	'
	using _target_variable[1]
	into _res_indicator_label, _res_indicator_description;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	select
			((ctv.metadata->'''||_lang_label||''')->''state_or_change'')->>''label'' as label,		
			((ctv.metadata->'''||_lang_label||''')->''state_or_change'')->>''description'' as description
	from
			nfiesta_results.c_target_variable as ctv
	where
			ctv.id = $1
	'
	using _target_variable[1]
	into _res_state_or_change_label, _res_state_or_change_description;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	select
			((ctv.metadata->'''||_lang_label||''')->''unit'')->>''label'' as label,		
			((ctv.metadata->'''||_lang_label||''')->''unit'')->>''description'' as description
	from
			nfiesta_results.c_target_variable as ctv
	where
			ctv.id = $1
	'
	using _target_variable[1]
	into _res_unit_label, _res_unit_description;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 04: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		_res_area_domain_attribute_json := json_agg(json_build_object('label',_without_distinction_label,'description',_without_distinction_description));
	else
		if _lang_label = 'en'::varchar(2)
		then
			execute
			'
			with
			w1 as	(
					select
							string_to_array(cad.label_en,'';'') as label,
							string_to_array(cad.description_en,'';'') as description
					from
							nfiesta_results.c_area_domain as cad
					where
							cad.id = $1
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_area_domain[1]
			into _res_area_domain_attribute_json;
		else
			execute
			'
			with
			w1 as	(
					select
							string_to_array(cadl.label,'';'') as label,
							string_to_array(cadl.description,'';'') as description
					from
							nfiesta_results.c_area_domain_language as cadl
					where
							cadl.area_domain = $1
					and
							cadl.language = $2
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_area_domain[1], _lang
			into _res_area_domain_attribute_json;		
		end if;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 05: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		_res_sub_population_attribute_json := json_agg(json_build_object('label',_without_distinction_label,'description',_without_distinction_description));
	else
		if _lang_label = 'en'::varchar(2)
		then
			execute
			'
			with
			w1 as	(
					select
							string_to_array(csp.label_en,'';'') as label,
							string_to_array(csp.description_en,'';'') as description
					from
							nfiesta_results.c_sub_population as csp
					where
							csp.id = $1
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_sub_population[1]
			into _res_sub_population_attribute_json;
		else
			execute
			'
			with
			w1 as	(
					select
							string_to_array(cspl.label,'';'') as label,
							string_to_array(cspl.description,'';'') as description
					from
							nfiesta_results.c_sub_population_language as cspl
					where
							cspl.sub_population = $1
					and
							cspl.language = $2
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_sub_population[1], _lang
			into _res_sub_population_attribute_json;		
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local_densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'core'
	into _check_core_ldsities;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- CORE JSON
	execute
	'
	with
	w1 as	(
			select
					(ctv.metadata->'''||_lang_label||''')->''local_densities'' as local_densities
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)		
	,w3 as	(
			select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''core''
			)
	,w4 as	(
			select
					row_number() over() as new_id,
					(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
					(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
					(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
					(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
					(w3.local_densities->''ldsity'')->>''label'' as label,
					(w3.local_densities->''ldsity'')->>''description'' as description,	
					w3.local_densities->>''use_negative'' as use_negative,
					(w3.local_densities->''version'')->>''label'' as version_label,
					(w3.local_densities->''version'')->>''description'' as version_description,
					w3.local_densities->''definition_variant'' as definition_variant,
					w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
					w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
			from
					w3
			)
	,w5 as	(
			select
					w4.new_id,
					w4.object_type_label,
					w4.object_type_description,
					w4.object_label,
					w4.object_description,
					w4.label,
					w4.description,
					w4.use_negative,
					case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
					case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
					case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
					case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
					case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
			from
					w4
			)
	,w6 as	(
			select
					w5.new_id,
					json_build_object
						(
						''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
						''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
						''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
						''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
						''definition_variant'',w5.definition_variant,
						''area_domain_restrictions'',w5.area_domain_restrictions,
						''sub_population_restrictions'',w5.sub_population_restrictions,
						''use_negative'',w5.use_negative
						) as  local_densities
			from
					w5
			)
	select
			json_agg(w6.local_densities order by w6.new_id) as local_densities
	from
			w6
	'
	using _target_variable[1], _not_specified_label, _not_specified_description, _not_specified_definition_variant, _not_specified_adr_spr
	into _res_core_json;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- DIVISION JSON
	if _check_division_ldsities = 0
	then
		_res_division_json := null::json;
	else
		execute
		'
		with
		w1 as	(
				select
						(ctv.metadata->'''||_lang_label||''')->''local_densities'' as local_densities
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		,w2 as	(
				select json_array_elements(w1.local_densities) as local_densities from w1 
				)			
		,w3 as	(
				select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w4 as	(
				select
						row_number() over() as new_id,
						(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w3.local_densities->''ldsity'')->>''label'' as label,
						(w3.local_densities->''ldsity'')->>''description'' as description,	
						w3.local_densities->>''use_negative'' as use_negative,
						(w3.local_densities->''version'')->>''label'' as version_label,
						(w3.local_densities->''version'')->>''description'' as version_description,
						w3.local_densities->''definition_variant'' as definition_variant,
						w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w3
				)
		,w5 as	(
				select
						w4.new_id,
						w4.object_type_label,
						w4.object_type_description,
						w4.object_label,
						w4.object_description,
						w4.label,
						w4.description,
						w4.use_negative,
						case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
						case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
						case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
						case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
						case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
				from
						w4
				)
		,w6 as	(
				select
						w5.new_id,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
							''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
							''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
							''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
							''definition_variant'',w5.definition_variant,
							''area_domain_restrictions'',w5.area_domain_restrictions,
							''sub_population_restrictions'',w5.sub_population_restrictions,
							''use_negative'',w5.use_negative
							) as  local_densities
				from
						w5
				)
		select
				json_agg(w6.local_densities order by w6.new_id) as local_densities
		from
				w6
		'
		using _target_variable[1], _not_specified_label, _not_specified_description, _not_specified_definition_variant, _not_specified_adr_spr
		into _res_division_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities = 0
	then
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json
		))
		into _res;
	else
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json,
			'division_contributions',_res_division_json
		))
		into _res;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]) is
'The function returns metadata (description) for given input ID (that represents target variable for nominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata_denominator" schema="nfiesta_results" src="functions/fn_get_user_metadata_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata_denominator
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang									integer;
	_lang_label								varchar;
	_not_specified_label					text;
	_not_specified_description				text;
	_without_distinction_label				text;
	_without_distinction_description		text;	
	_not_specified_definition_variant		json;
	_not_specified_adr_spr					json;
	_target_variable						integer[];
	_res_indicator_label					text;
	_res_indicator_description				text;
	_res_state_or_change_label				text;
	_res_state_or_change_description		text;
	_res_unit_label							text;
	_res_unit_description					text;
	_check_area_domain						integer[];
	_res_area_domain_attribute_json			json;
	_check_sub_population					integer[];
	_res_sub_population_attribute_json		json;
	--_check_core_ldsities					integer;
	_check_division_ldsities				integer;
	_res_core_json							json;
	_res_division_json						json;	
	_res									json;
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata_denominator: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata_denominator: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom) -- column for DENOMINATOR
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable;

	if array_length(_target_variable,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	_not_specified_label := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_label'::character varying))::text;
	_not_specified_description := (nfiesta_results.fn_get_gui_header(_jlang,'not_specified_description'::character varying))::text;
	_without_distinction_label := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_label'::character varying))::text;
	_without_distinction_description := (nfiesta_results.fn_get_gui_header(_jlang,'without_distinction_description'::character varying))::text;
	-----------------------------------------------------------------
	_not_specified_definition_variant := json_agg(json_build_object('label',_not_specified_label,'description',_not_specified_description));
	_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label',_not_specified_label,'description',_not_specified_description),'restriction',json_build_object('label',_not_specified_label,'description',_not_specified_description)));
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - denominator
	execute
	'
	select
			((ctv.metadata->'''||_lang_label||''')->''indicator'')->>''label'' as label,		
			((ctv.metadata->'''||_lang_label||''')->''indicator'')->>''description'' as description
	from
			nfiesta_results.c_target_variable as ctv
	where
			ctv.id = $1
	'
	using _target_variable[1]
	into _res_indicator_label, _res_indicator_description;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - denominator
	execute
	'
	select
			((ctv.metadata->'''||_lang_label||''')->''state_or_change'')->>''label'' as label,		
			((ctv.metadata->'''||_lang_label||''')->''state_or_change'')->>''description'' as description
	from
			nfiesta_results.c_target_variable as ctv
	where
			ctv.id = $1
	'
	using _target_variable[1]
	into _res_state_or_change_label, _res_state_or_change_description;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - denominator
	execute
	'
	select
			((ctv.metadata->'''||_lang_label||''')->''unit'')->>''label'' as label,		
			((ctv.metadata->'''||_lang_label||''')->''unit'')->>''description'' as description
	from
			nfiesta_results.c_target_variable as ctv
	where
			ctv.id = $1
	'
	using _target_variable[1]
	into _res_unit_label, _res_unit_description;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - denominator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain_denom is null then 0 else trg.area_domain_denom end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 04: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		_res_area_domain_attribute_json := json_agg(json_build_object('label',_without_distinction_label,'description',_without_distinction_description));
	else
		if _lang_label = 'en'::varchar(2)
		then
			execute
			'
			with
			w1 as	(
					select
							string_to_array(cad.label_en,'';'') as label,
							string_to_array(cad.description_en,'';'') as description
					from
							nfiesta_results.c_area_domain as cad
					where
							cad.id = $1
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_area_domain[1]
			into _res_area_domain_attribute_json;
		else
			execute
			'
			with
			w1 as	(
					select
							string_to_array(cadl.label,'';'') as label,
							string_to_array(cadl.description,'';'') as description
					from
							nfiesta_results.c_area_domain_language as cadl
					where
							cadl.area_domain = $1
					and
							cadl.language = $2
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_area_domain[1], _lang
			into _res_area_domain_attribute_json;		
		end if;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - denominator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			and
					trg.web = true
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 05: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		_res_sub_population_attribute_json := json_agg(json_build_object('label',_without_distinction_label,'description',_without_distinction_description));
	else
		if _lang_label = 'en'::varchar(2)
		then
			execute
			'
			with
			w1 as	(
					select
							string_to_array(csp.label_en,'';'') as label,
							string_to_array(csp.description_en,'';'') as description
					from
							nfiesta_results.c_sub_population as csp
					where
							csp.id = $1
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_sub_population[1]
			into _res_sub_population_attribute_json;
		else
			execute
			'
			with
			w1 as	(
					select
							string_to_array(cspl.label,'';'') as label,
							string_to_array(cspl.description,'';'') as description
					from
							nfiesta_results.c_sub_population_language as cspl
					where
							cspl.sub_population = $1
					and
							cspl.language = $2
					)
			,w2 as	(
					select
							unnest(w1.label) as label,
							unnest(w1.description) as description
					from
							w1
					)
			select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
			'
			using _check_sub_population[1], _lang
			into _res_sub_population_attribute_json;		
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local_densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'core'
	into _check_core_ldsities;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- CORE JSON
	execute
	'
	with
	w1 as	(
			select
					(ctv.metadata->'''||_lang_label||''')->''local_densities'' as local_densities
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)			
	,w3 as	(
			select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''core''
			)
	,w4 as	(
			select
					row_number() over() as new_id,
					(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
					(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
					(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
					(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
					(w3.local_densities->''ldsity'')->>''label'' as label,
					(w3.local_densities->''ldsity'')->>''description'' as description,	
					w3.local_densities->>''use_negative'' as use_negative,
					(w3.local_densities->''version'')->>''label'' as version_label,
					(w3.local_densities->''version'')->>''description'' as version_description,
					w3.local_densities->''definition_variant'' as definition_variant,
					w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
					w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
			from
					w3
			)
	,w5 as	(
			select
					w4.new_id,
					w4.object_type_label,
					w4.object_type_description,
					w4.object_label,
					w4.object_description,
					w4.label,
					w4.description,
					w4.use_negative,
					case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
					case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
					case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
					case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
					case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
			from
					w4
			)
	,w6 as	(
			select
					w5.new_id,
					json_build_object
						(
						''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
						''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
						''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
						''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
						''definition_variant'',w5.definition_variant,
						''area_domain_restrictions'',w5.area_domain_restrictions,
						''sub_population_restrictions'',w5.sub_population_restrictions,
						''use_negative'',w5.use_negative
						) as  local_densities
			from
					w5
			)
	select
			json_agg(w6.local_densities order by w6.new_id) as local_densities
	from
			w6
	'
	using _target_variable[1], _not_specified_label, _not_specified_description, _not_specified_definition_variant, _not_specified_adr_spr
	into _res_core_json;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- DIVISION JSON
	if _check_division_ldsities = 0
	then
		_res_division_json := null::json;
	else
		execute
		'
		with
		w1 as	(
				select
						(ctv.metadata->'''||_lang_label||''')->''local_densities'' as local_densities
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		,w2 as	(
				select json_array_elements(w1.local_densities) as local_densities from w1 
				)				
		,w3 as	(
				select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w4 as	(
				select
						row_number() over() as new_id,
						(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w3.local_densities->''ldsity'')->>''label'' as label,
						(w3.local_densities->''ldsity'')->>''description'' as description,	
						w3.local_densities->>''use_negative'' as use_negative,
						(w3.local_densities->''version'')->>''label'' as version_label,
						(w3.local_densities->''version'')->>''description'' as version_description,
						w3.local_densities->''definition_variant'' as definition_variant,
						w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w3
				)
		,w5 as	(
				select
						w4.new_id,
						w4.object_type_label,
						w4.object_type_description,
						w4.object_label,
						w4.object_description,
						w4.label,
						w4.description,
						w4.use_negative,
						case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
						case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
						case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
						case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
						case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
				from
						w4
				)
		,w6 as	(
				select
						w5.new_id,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
							''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
							''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
							''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
							''definition_variant'',w5.definition_variant,
							''area_domain_restrictions'',w5.area_domain_restrictions,
							''sub_population_restrictions'',w5.sub_population_restrictions,
							''use_negative'',w5.use_negative
							) as  local_densities
				from
						w5
				)
		select
				json_agg(w6.local_densities order by w6.new_id) as local_densities
		from
				w6
		'
		using _target_variable[1], _not_specified_label, _not_specified_description, _not_specified_definition_variant, _not_specified_adr_spr
		into _res_division_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities = 0
	then
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json
		))
		into _res;
	else
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json,
			'division_contributions',_res_division_json
		))
		into _res;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) is
'The function returns metadata (description) for given input ID (that represents target variable for denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata" schema="nfiesta_results" src="functions/fn_get_user_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang										integer;
	_lang_label									varchar;
	_estimate_type								integer;
	_target_variable							integer;
	_target_variable_denom						integer;
	_estimation_period							integer;
	_panel_refyearset_group						integer;
	_phase_estimate_type						integer;
	_phase_estimate_type_denom					integer;
	_estimation_cell_collection					integer;
	_area_domain								integer;
	_area_domain_denom							integer;
	_sub_population								integer;
	_sub_population_denom						integer;
	--_target_variable_num						integer[];
	--_target_variable_denom_check				integer;
	--_target_variable_denom					integer;
	_cond4topic_num								text;
	_cond4topic_denom							text;
	_res_topic_label							text;
	_res_topic_description						text;
	_res_estimation_period_label				text;
	_res_estimation_period_description			text;
	_res_estimation_cell_collection_label		text;
	_res_estimation_cell_collection_description	text;
	_res_estimate_type_label					text;
	_res_estimate_type_description				text;
	_res_unit_num_label							text;
	_res_unit_num_description					text;
	_res_unit_of_measurement_label				text;
	_res_unit_of_measurement_description		text;
	_res										json;

	_estimation_cell_from_hierarchy_for_ecc_select_by_user			integer;
	_estimation_cell_from_hierarchy_for_ecc_select_by_user_superior	integer;
	_heighest_estimation_cell_select_by_user						integer;
	_heighest_estimation_cell_collection_select_by_user				integer;
	_estimation_cell_collection_array								integer[];
	_test_occurence_result_group									integer;	
begin
	-----------------------------------------------------------------
	_lang := nfiesta_results.fn_get_language_id(_jlang);
	_lang_label := (select cl.label from nfiesta_results.c_language as cl where cl.id = _lang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_query: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select
			trg.estimate_type,
			trg.target_variable,
			trg.target_variable_denom,
			trg.estimation_period,
			trg.panel_refyearset_group,
			trg.phase_estimate_type,
			trg.phase_estimate_type_denom,
			trg.estimation_cell_collection,
			trg.area_domain,
			trg.area_domain_denom,
			trg.sub_population,
			trg.sub_population_denom
	from
			nfiesta_results.t_result_group as trg
	where
			trg.id = _id_group[1]
	and
			trg.web = true
	into
			_estimate_type,
			_target_variable,
			_target_variable_denom,
			_estimation_period,
			_panel_refyearset_group,
			_phase_estimate_type,
			_phase_estimate_type_denom,
			_estimation_cell_collection,
			_area_domain,
			_area_domain_denom,
			_sub_population,
			_sub_population_denom;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	select count(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom_check;

	if _target_variable_denom_check is distinct from 0 -- target variable denom exists
	then
		if _target_variable_denom_check > 1
		then
			raise exception 'Error: 03: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
		end if;

		select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
		where trg.id in (select unnest(_id_group))
		and trg.web = true
		into _target_variable_denom;		
	else
		_target_variable_denom := null::integer;
	end if;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	if _lang_label = 'en'::varchar(2)
	then
		execute
		'
		with
		w1 as	(
				select crt.topic from nfiesta_results.cm_result2topic as crt
				where ' || _cond4topic_num ||'
				and ' || _cond4topic_denom ||'
				)
		,w2 as	(
				select
						id,
						label_en as label,
						description_en as description
				from
						nfiesta_results.c_topic
				where
						id in (select distinct w1.topic from w1)
				)
		,w3 as	(
				select
						array_agg(w2.label order by w2.description) as label,
						array_agg(w2.description order by w2.description) as description
				from
						w2
				)
		select
				array_to_string(w3.label, ''; ''),
				array_to_string(w3.description, ''; '')
		from
				w3
		'
		using _target_variable, _target_variable_denom
		into _res_topic_label, _res_topic_description;
	else
		execute
		'
		with
		w1 as	(
				select crt.topic from nfiesta_results.cm_result2topic as crt
				where ' || _cond4topic_num ||'
				and ' || _cond4topic_denom ||'
				)
		,w2 as	(
				select
						topic as id,
						label,
						description
				from
						nfiesta_results.c_topic_language
				where
						topic in (select distinct w1.topic from w1)
				and
						language = $3
				)
		,w3 as	(
				select
						array_agg(w2.label order by w2.description) as label,
						array_agg(w2.description order by w2.description) as description
				from
						w2
				)
		select
				array_to_string(w3.label, ''; ''),
				array_to_string(w3.description, ''; '')
		from
				w3
		'
		using _target_variable, _target_variable_denom, _lang
		into _res_topic_label, _res_topic_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	if _lang_label = 'en'::varchar(2)
	then
		execute
		'
		select
				cep.label_en,
				cep.description_en
		from
				nfiesta_results.c_estimation_period as cep
		where
				cep.id =	(
							select distinct trg.estimation_period
							from nfiesta_results.t_result_group trg
							where trg.id in (select unnest($1))
							and trg.web = true
							)
		'
		using _id_group
		into _res_estimation_period_label, _res_estimation_period_description;
	else
		execute
		'
		select
				cepl.label,
				cepl.description
		from
				nfiesta_results.c_estimation_period_language as cepl
		where
				cepl.estimation_period =	(
											select distinct trg.estimation_period
											from nfiesta_results.t_result_group trg
											where trg.id in (select unnest($1))
											and trg.web = true
											)
		and		cepl.language = $2
		'
		using _id_group, _lang
		into _res_estimation_period_label, _res_estimation_period_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	select tech.cell from nfiesta_results.t_estimation_cell_hierarchy as tech
	where tech.cell in	(
						select cec.id from nfiesta_results.c_estimation_cell as cec
						where cec.estimation_cell_collection =	_estimation_cell_collection
						)
	order by cell limit 1
	into _estimation_cell_from_hierarchy_for_ecc_select_by_user;

	if _estimation_cell_from_hierarchy_for_ecc_select_by_user is null
	then
		-- selected estimation cell collection is maybe the heighest
		select count(tech.cell_superior) from nfiesta_results.t_estimation_cell_hierarchy as tech
		where tech.cell_superior in	(
									select cec.id from nfiesta_results.c_estimation_cell as cec
									where cec.estimation_cell_collection = _estimation_cell_collection
									)
		into _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior;

		if _estimation_cell_from_hierarchy_for_ecc_select_by_user_superior = 0
		then
			raise exception 'Error: 04: fn_get_user_metadata: For user selected estimation_cell_collection = % not exists any estimation cell in t_estimation_cell_hierarchy table!',_estimation_cell_collection;
		end if;

		-- user selected the heighest estimation cell collection => test for occurrence in t_result_group table is not needed
		_estimation_cell_collection_array := array[_estimation_cell_collection];		
	else
		with
		w1 as	(
				select * from nfiesta_results.fn_get_heighest_estimation_cell(array[_estimation_cell_from_hierarchy_for_ecc_select_by_user]) as res
				)
		select w1.res[1] from w1
		into _heighest_estimation_cell_select_by_user; -- the heighest estimation cell from hierarchy

		select cec.estimation_cell_collection from nfiesta_results.c_estimation_cell as cec
		where cec.id = _heighest_estimation_cell_select_by_user
		into _heighest_estimation_cell_collection_select_by_user; -- the heighest estimation cell collection from hierarchy

		-- test for occurrence in t_result_group
		select id from nfiesta_results.t_result_group
		where estimate_type = _estimate_type
		and target_variable = _target_variable
		and coalesce(target_variable_denom,0) = coalesce(_target_variable_denom,0)
		and estimation_period = _estimation_period
		and phase_estimate_type = _phase_estimate_type
		and coalesce(phase_estimate_type_denom,0) = coalesce(_phase_estimate_type_denom,0)
		and estimation_cell_collection = _heighest_estimation_cell_collection_select_by_user
		and coalesce(area_domain,0) = coalesce(_area_domain,0)
		and coalesce(area_domain_denom,0) = coalesce(_area_domain_denom,0)
		and coalesce(sub_population,0) = coalesce(_sub_population,0)
		and coalesce(sub_population_denom,0) = coalesce(_sub_population_denom,0)
		and web = true
		into _test_occurence_result_group;

		if _test_occurence_result_group is null
		then
			_estimation_cell_collection_array := array[_estimation_cell_collection];
		else
			_estimation_cell_collection_array := array[_estimation_cell_collection] || array[_heighest_estimation_cell_collection_select_by_user];
		end if;
	end if;	

	if _lang_label = 'en'::varchar(2)
	then
		execute
		'
		with
		w1 as	(
				select
						cecc.id,
						cecc.label_en as label,
						cecc.description_en as description
				from
						nfiesta_results.c_estimation_cell_collection as cecc
				where
						cecc.id in (select unnest($1))
				)
		,w2 as	(
				select
						array_agg(w1.label order by w1.id) as label,
						array_agg(w1.description order by w1.id) as description
				from
						w1
				)
		select
				array_to_string(w2.label,''; '') as label,
				array_to_string(w2.description,''; '') as description
		from
			w2
		'
		using _estimation_cell_collection_array
		into _res_estimation_cell_collection_label, _res_estimation_cell_collection_description;
	else
		execute
		'
		with
		w1 as	(
				select
						ceccl.estimation_cell_collection as id,
						ceccl.label,
						ceccl.description
				from
						nfiesta_results.c_estimation_cell_collection_language as ceccl
				where
						ceccl.estimation_cell_collection in (select unnest($1))
				and
						ceccl.language = $2
				)
		,w2 as	(
				select
						array_agg(w1.label order by w1.id) as label,
						array_agg(w1.description order by w1.id) as description
				from
						w1
				)
		select
				array_to_string(w2.label,''; '') as label,
				array_to_string(w2.description,''; '') as description
		from
			w2
		'
		using _estimation_cell_collection_array, _lang
		into _res_estimation_cell_collection_label, _res_estimation_cell_collection_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATE TYPE
	if _target_variable_denom is null
	then
		execute
		'
		select
				cet.label,
				cet.description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 1
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;
	else
		execute
		'
		select
				cet.label,
				cet.description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 2
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	select
			((ctv.metadata->'''||_lang_label||''')->''unit'')->>''label'' as label,		
			((ctv.metadata->'''||_lang_label||''')->''unit'')->>''description'' as description
	from
			nfiesta_results.c_target_variable as ctv
	where
			ctv.id = $1
	'
	using _target_variable
	into _res_unit_num_label, _res_unit_num_description;		
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASUREMENT
	if _target_variable_denom is null
	then
		_res_unit_of_measurement_label := _res_unit_num_label;
		_res_unit_of_measurement_description := _res_unit_num_description;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_label_num,
						((w2.metadata->'''||_lang_label||''')->''unit'')->>''label'' as unit_label_denom,					
						((w1.metadata->'''||_lang_label||''')->''unit'')->>''description'' as unit_description_num,
						((w2.metadata->'''||_lang_label||''')->''unit'')->>''description'' as unit_description_denom
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.*,
						case
							when (w3.target_variable_num = w3.target_variable_denom) or (w3.unit_label_num = w3.unit_label_denom) then ''%''
							else concat(w3.unit_label_num,'' / '',w3.unit_label_denom)
						end as label4user,						
						case
							when (w3.target_variable_num = w3.target_variable_denom) or (w3.unit_description_num = w3.unit_description_denom) then ''%''
							else concat(w3.unit_description_num,'' / '',w3.unit_description_denom)
						end as description4user
				from
						w3
				)
		select
				w4.label4user,
				w4.description4user
		from
				w4
		'
		using _target_variable, _target_variable_denom
		into _res_unit_of_measurement_label, _res_unit_of_measurement_description;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _target_variable_denom is null
	then
		-- basic metadata + numerator metadata
		select json_build_object
		(
			'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
			'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
			'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
			'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
			'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
			'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator(_jlang,_id_group))
		)
		into _res;
	else
		-- basic metadata + numerator metadata + denominator metadata
		select json_build_object
		(
			'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
			'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
			'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
			'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
			'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
			'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator(_jlang,_id_group)),
			'denominator',(select * from nfiesta_results.fn_get_user_metadata_denominator(_jlang,_id_group))
		)
		into _res;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) is
'The function returns metadata for given input ID (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_num_denom_variables" schema="nfiesta_results" src="functions/fn_get_num_denom_variables.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta_results.fn_get_num_denom_variables.sql(integer, integer)
--DROP FUNCTION nfiesta_results.fn_get_num_denom_variables.sql(integer, integer);
CREATE OR REPLACE FUNCTION nfiesta_results.fn_get_num_denom_variables(
		_num_var integer[], _denom_var integer[] DEFAULT NULL::int[])
RETURNS TABLE (
variable_numerator 	integer,
variable_denominator 	integer
)
AS
$function$
DECLARE
_denom_var4query	integer[];
BEGIN
	IF _num_var IS NULL
	THEN
		RAISE EXCEPTION 'Given array of numerator variables is NULL!';
	END IF;

	IF (SELECT count(*) FROM nfiesta_results.t_variable WHERE array[id] <@ _num_var) != array_length(_num_var,1)
	THEN
		RAISE EXCEPTION 'Not all of numerator variables are present in table t_variable.';
	END IF;

	IF (SELECT count(*) FROM nfiesta_results.t_variable WHERE array[id] <@ _denom_var) != array_length(_denom_var,1)
	THEN
		RAISE EXCEPTION 'Not all of denominator variables are present in table t_variable.';
	END IF;

	IF array_length(_num_var,1) < array_length(_denom_var,1)
	THEN
		RAISE EXCEPTION 'Given array of denominator variables must be shorter (or equal) then given array of numerator variables.';
	END IF;

	IF _denom_var IS NULL
	THEN _denom_var4query := _num_var;
	ELSE _denom_var4query := _denom_var;
	END IF;

	RETURN QUERY EXECUTE
	'
	WITH RECURSIVE w_var AS (
		SELECT 1 AS iter, variable_superior AS variable_start, variable_superior, variable
		FROM nfiesta_results.t_variable_hierarchy
		WHERE variable_superior = ANY($1)
		UNION ALL
		SELECT iter + 1 AS iter, t1.variable_start, t2.variable_superior, t2.variable
		FROM w_var AS t1
		INNER JOIN nfiesta_results.t_variable_hierarchy AS t2
		ON t1.variable = t2.variable_superior
		WHERE NOT ARRAY[t1.variable] <@ $1
	), w_all AS (
		SELECT distinct iter, t1.variable_start, t1.variable_superior, t1.variable, t3.label_en AS lab_start, t7.label_en AS lab_sup, t5.label_en AS lab_var
		FROM w_var AS t1
		INNER JOIN nfiesta_results.t_variable AS t2
		ON t1.variable_start = t2.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t3
		ON t2.sub_population_category = t3.id
		INNER JOIN nfiesta_results.t_variable AS t4
		ON t1.variable = t4.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t5
		ON t4.sub_population_category = t5.id
		INNER JOIN nfiesta_results.t_variable AS t6
		ON t1.variable_superior = t6.id
		LEFT JOIN nfiesta_results.c_sub_population_category AS t7
		ON t6.sub_population_category = t7.id
		WHERE variable = ANY($2)
		ORDER BY variable_start, variable
	), w_iter AS(
		SELECT 	variable_start, variable_superior, variable, lab_start, --lab_sup, 
			lab_var,
			iter, min(iter) OVER (partition by variable) AS min_iter
		FROM w_all
	), w_final AS (
		SELECT variable_start AS denom, variable AS num
		FROM w_iter WHERE iter = min_iter AND
			NOT ARRAY[variable] <@ $1
		UNION ALL
		SELECT t1.var AS denom, t1.var AS num
		FROM unnest($1) AS t1(var)
	)
	SELECT DISTINCT t1.num, CASE WHEN $3 IS NOT NULL THEN t1.denom ELSE NULL::int END AS denom --, t3.label AS lab_denom, t5.label AS lab_num
	FROM w_final AS t1
	INNER JOIN nfiesta_results.t_variable AS t2
	ON t1.denom = t2.id
	LEFT JOIN nfiesta_results.c_sub_population_category AS t3
	ON t2.sub_population_category = t3.id
	INNER JOIN nfiesta_results.t_variable AS t4
	ON t1.num = t4.id
	LEFT JOIN nfiesta_results.c_sub_population_category AS t5
	ON t4.sub_population_category = t5.id
	ORDER BY denom, t1.num;
	'
	USING _denom_var4query, _num_var, _denom_var;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta_results.fn_get_num_denom_variables(integer[],integer[]) IS 'Function returns table with assigned denominator variables to numerator variables.';
-- </function>
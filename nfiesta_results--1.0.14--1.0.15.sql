--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_area_domain', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_area_domain_category', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_area_domain_category_language', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_area_domain_language', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimate_type', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_cell', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_cell_collection', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_cell_collection_language', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_cell_language', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_period', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_period_language', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_gui_header_language', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_language', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_panel_refyearset_group', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_panel_refyearset_group_language', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_phase_estimate_type', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_sub_population', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_sub_population_category', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_sub_population_category_language', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_sub_population_language', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_target_variable', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_topic', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_topic_language', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.cm_area_domain_category', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.cm_result2topic', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.cm_sub_population_category', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.f_a_cell', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_estimate_conf', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_estimation_cell_hierarchy', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_result', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_result_group', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_total_estimate_conf', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_variable', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_variable_hierarchy', '');
---------------------------------------------------------------------------------------------------
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_area_domain_category_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_area_domain_category_language_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_area_domain_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_area_domain_language_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimate_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_cell_collection_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_cell_collection_language_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_cell_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_cell_language_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_period_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_estimation_period_language_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_gui_header_language_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_language_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_panel_refyearset_group_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_panel_refyearset_group_language_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_phase_estimate_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_sub_population_category_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_sub_population_category_language_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_sub_population_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_sub_population_language_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_target_variable_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_topic_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.c_topic_language_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.cm_area_domain_category_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.cm_result2topic_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.cm_sub_population_category_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.f_a_cell_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_estimate_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_estimation_cell_hierarchy_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_result_group_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_result_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_total_estimate_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_variable_hierarchy_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('nfiesta_results.t_variable_id_seq', '');
---------------------------------------------------------------------------------------------------
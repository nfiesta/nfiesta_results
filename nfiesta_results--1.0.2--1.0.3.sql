--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------


drop function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean);
-- <function name="fn_get_user_options_numerator" schema="nfiesta_results" src="functions/fn_get_user_options_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, /*boolean, boolean, boolean, boolean,*/ boolean);

create or replace function nfiesta_results.fn_get_user_options_numerator
(
	_jlang character varying,
	_topic integer default null::integer,
	_nom_estimation_period integer default null::integer,
	_nom_estimation_cell_collection integer default null::integer,
	_nom_id_group integer[] default null::integer[],
	_nom_indicator boolean default false,
	_nom_state boolean default false,
	_nom_unit_of_measure boolean default false,
	--_nom_ldsity boolean default false,
	--_nom_definition_variant boolean default false,
	--_nom_area_domain boolean default false,
	--_nom_population boolean default false,
	_unit_of_measure boolean default false
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[]
)
language plpgsql
stable
as
$$
declare
	_lang_suffix			character varying(3);
	_lang_covariate			text;
	_check_nom_id_group		integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	-- TOPIC --
	-----------------------------------------------------------------
	if _topic is null
	then
		return query execute
		'
		with
		w1 as	(
				select
						distinct
						target_variable,
						case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom
				from
						nfiesta_results.t_result_group where web = true
				)
		,w2 as	(
				select
						target_variable,
						case when denominator is null then 0 else denominator end as denominator,
						topic
				from
						nfiesta_results.cm_result2topic
				)
		,w3 as	(
				select distinct w2.topic from w1 inner join w2
				on w1.target_variable = w2.target_variable
				and w1.target_variable_denom = w2.denominator
				)
		,w4 as	(
				select
						id,
						label'||_lang_suffix||' AS label
				from
						nfiesta_results.c_topic
				where
						id in (select w3.topic from w3)
				)
		select
				w4.id as res_id,
				w4.label::text as res_label,
				null::integer[] as res_id_group
		from
				w4 order by w4.label;
		';
	else
		-----------------------------------------------------------------
		-- ESTIMATION_PERIOD --
		-----------------------------------------------------------------
		if _nom_estimation_period is null
		then
			return query execute
			'
			with
			w1 as	(
					select
							target_variable,
							case when denominator is null then 0 else denominator end as denominator
					from
							nfiesta_results.cm_result2topic where topic = $1
					)
			,w2 as	(
					select
							target_variable,
							case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
							estimation_period
					from
							nfiesta_results.t_result_group where web = true
					)
			,w3 as	(
					select distinct w2.estimation_period from w2 inner join w1
					on w2.target_variable = w1.target_variable
					and w2.target_variable_denom = w1.denominator
					)
			,w4 as	(
					select
							id,
							label'||_lang_suffix||' AS label
					from
							nfiesta_results.c_estimation_period
					where
							id in (select w3.estimation_period from w3)
					)
			select
					w4.id as res_id,
					w4.label::text as res_label,
					null::integer[] as res_id_group
			from
					w4 order by w4.label;				
			'
			using _topic;
		else
			-----------------------------------------------------------------
			-- ESTIMATION_CELL_COLLECTION --
			-----------------------------------------------------------------
			if _nom_estimation_cell_collection is null
			then
				return query execute
				'			
				with
				w1 as	(
						select
								target_variable,
								case when denominator is null then 0 else denominator end as denominator
						from
								nfiesta_results.cm_result2topic where topic = $1
						)
				,w2 as	(
						select
								target_variable,
								case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
								estimation_period,
								estimation_cell_collection
						from
								nfiesta_results.t_result_group where web = true
						)
				,w3 as	(
						select * from w2 inner join w1
						on w2.target_variable = w1.target_variable
						and w2.target_variable_denom = w1.denominator
						)
				,w4 as	(
						select distinct w3.estimation_cell_collection from w3 where w3.estimation_period = $2
						)
				,w5 as	(
						select
								id,
								label'||_lang_suffix||' AS label
						from
								nfiesta_results.c_estimation_cell_collection
						where
								id in (select w4.estimation_cell_collection from w4)
						)
				select
						w5.id as res_id,
						w5.label::text as res_label,
						null::integer[] as res_id_group
				from
						w5 order by w5.label;
				'
				using _topic, _nom_estimation_period;
			else
				-----------------------------------------------------------------
				-- NOM_INDICATOR --
				-----------------------------------------------------------------
				if _nom_indicator = false
				then
					return query execute
					'
					with
					w1 as	(
							select
									target_variable,
									case when denominator is null then 0 else denominator end as denominator
							from
									nfiesta_results.cm_result2topic where topic = $1
							)
					,w2 as	(
							select
									id,
									target_variable,
									case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
									estimation_period,
									estimation_cell_collection
							from
									nfiesta_results.t_result_group where web = true
							)
					,w3 as	(
							select w2.* from w2 inner join w1
							on w2.target_variable = w1.target_variable
							and w2.target_variable_denom = w1.denominator
							)
					,w4 as	(
							select w3.* from w3
							where w3.estimation_period = $2
							and w3.estimation_cell_collection = $3
							)
					,w5 as	(
							select
									target_variable,
									array_agg(w4.id order by w4.id) as id
							from
									w4 group by target_variable
							)
					,w6 as	(
							select
									w5.*,
									((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
									((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
							from
									w5
									inner join nfiesta_results.c_target_variable as ctv
									on w5.target_variable = ctv.id
							)
					,w7 as	(
							select
									w6.id,
									label'||_lang_suffix||' AS label
							from
									w6
							)
					,w8 as	(
							select
									w7.label,
									unnest(w7.id) as id
							from
									w7
							)
					,w9 as	(
							select w8.label, array_agg(w8.id) as id from w8 group by w8.label
							)
					select
							null::integer as res_id,
							w9.label as res_label,
							w9.id as res_id_group
					from
							w9 order by w9.label;
					'
					using _topic, _nom_estimation_period, _nom_estimation_cell_collection;
				else
					-----------------------------------------------------------------
					-- NOM_STATE --
					-----------------------------------------------------------------
					if _nom_state = false
					then
						return query execute
						'
						with
						w1 as	(
								select * from nfiesta_results.t_result_group as trg
								where trg.id in (select unnest($1))
								and trg.web = true
								)
						,w2 as	(
								select
										target_variable,
										array_agg(w1.id order by w1.id) as id
								from
										w1 group by target_variable
								)
						,w3 as	(
								select
										w2.*,
										((ctv.metadata->''cs'')->''state or change'')->>''label'' as label,
										((ctv.metadata->''en'')->''state or change'')->>''label'' as label_en
								from
										w2
										inner join nfiesta_results.c_target_variable as ctv
										on w2.target_variable = ctv.id
								)
						,w4 as	(
								select
										w3.id,
										label'||_lang_suffix||' AS label
								from
										w3
								)
						,w5 as	(
								select
										w4.label,
										unnest(w4.id) as id
								from
										w4
								)
						,w6 as	(
								select w5.label, array_agg(w5.id) as id from w5 group by w5.label
								)
						select
								null::integer as res_id,
								w6.label as res_label,
								w6.id as res_id_group
						from
								w6 order by w6.label;
						'
						using _nom_id_group;					
					else
						-----------------------------------------------------------------
						-- NOM_UNIT_OF_MEASURE --
						-----------------------------------------------------------------
						if _nom_unit_of_measure = false
						then
							return query execute
							'
							with
							w1 as	(
									select * from nfiesta_results.t_result_group as trg
									where trg.id in (select unnest($1))
									and trg.web = true
									)
							,w2 as	(
									select
											target_variable,
											array_agg(w1.id order by w1.id) as id
									from
											w1 group by target_variable
									)
							,w3 as	(
									select
											w2.*,
											((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
											((ctv.metadata->''en'')->''unit'')->>''label'' as label_en
									from
											w2
											inner join nfiesta_results.c_target_variable as ctv
											on w2.target_variable = ctv.id
									)
							,w4 as	(
									select
											w3.id,
											label'||_lang_suffix||' AS label
									from
											w3
									)
							,w5 as	(
									select
											w4.label,
											unnest(w4.id) as id
									from
											w4
									)
							,w6 as	(
									select w5.label, array_agg(w5.id) as id from w5 group by w5.label
									)
							select
									null::integer as res_id,
									w6.label as res_label,
									w6.id as res_id_group
							from
									w6 order by w6.label;
							'
							using _nom_id_group;
						else
							/*
							-----------------------------------------------------------------
							-- NOM_LDSITY --
							-----------------------------------------------------------------
							if _nom_ldsity = false
							then
								return query execute
								'
								with
								w1 as	(
										select * from nfiesta_results.t_result_group as trg
										where trg.id in (select unnest($1))
										and trg.web = true
										)
								,w2 as	(
										select
												target_variable,
												array_agg(w1.id order by w1.id) as id
										from
												w1 group by target_variable
										)
								,w3 as	(
										select
												t.target_variable,
												t.id,
												t.label'||_lang_suffix||' AS label
										from
												(
												select
														w2.*,
														((ctv.metadata->''cs'')->''local densities'') as label,
														((ctv.metadata->''en'')->''local densities'') as label_en
												from
														w2
														inner join nfiesta_results.c_target_variable as ctv
														on w2.target_variable = ctv.id
												) as t
										)
								,w4 as	(
										select
												w3.target_variable,
												json_array_elements(w3.label) as label
										from
												w3
										)
								,w5 as	(
										select
												row_number() over () as new_id,
												w4.target_variable,
												w4.label->>''label'' as label
										from w4
										)
								,w6 as	(
										select
												w5.target_variable,
												array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
										from
												w5 group by w5.target_variable
										)
								,w7 as	(
										select w3.*, w6.label4user from w3 inner join w6
										on w3.target_variable = w6.target_variable
										)
								,w8 as	(
										select
												w7.label4user as label,
												unnest(w7.id) as id
										from
												w7
										)
								,w9 as	(
										select w8.label, array_agg(w8.id) as id from w8 group by w8.label
										)
								select
										null::integer as res_id,
										w9.label as res_label,
										w9.id as res_id_group
								from
										w9 order by w9.label;
								'
								using _nom_id_group;
							else
								-----------------------------------------------------------------
								-- NOM_DEFINITION_VARIANT --
								-----------------------------------------------------------------
								if _nom_definition_variant = false
								then
									return query execute
									'
									with
									w1 as	(
											select * from nfiesta_results.t_result_group as trg
											where trg.id in (select unnest($1))
											and trg.web = true
											)
									,w2 as	(
											select
													target_variable,
													array_agg(w1.id order by w1.id) as id
											from
													w1 group by target_variable
											)
									,w3 as	(
											select
													t.target_variable,
													t.id,
													t.label'||_lang_suffix||' AS label
											from
													(
													select
															w2.*,
															((ctv.metadata->''cs'')->''local densities'') as label,
															((ctv.metadata->''en'')->''local densities'') as label_en
													from
															w2
															inner join nfiesta_results.c_target_variable as ctv
															on w2.target_variable = ctv.id
													) as t
											)
									,w4 as	(
											select
													w3.target_variable,
													json_array_elements(w3.label) as label
											from
													w3
											)
									,w5 as	(
											select
													row_number() over () as new_id,
													w4.target_variable,
													coalesce((w4.label->''definition variant'')->>''label'',''null''::text) as label
											from w4
											)										
									,w6 as	(
										select
												w5.target_variable,
												array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
										from
												w5 group by w5.target_variable
											)
									,w7 as	(
											select w3.*, w6.label4user from w3 inner join w6
											on w3.target_variable = w6.target_variable
											)
									,w8 as	(
											select
													w7.label4user as label,
													unnest(w7.id) as id
											from
													w7
											)
									,w9 as	(
											select w8.label, array_agg(w8.id) as id from w8 group by w8.label
											)
									select
											null::integer as res_id,
											w9.label as res_label,
											w9.id as res_id_group
									from
											w9 order by w9.label;
									'
									using _nom_id_group;									
								else
									-----------------------------------------------------------------
									-- NOM_AREA_DOMAIN --
									-----------------------------------------------------------------
									if _nom_area_domain = false
									then
										return query execute
										'										
										with
										w1 as	(
												select * from nfiesta_results.t_result_group as trg
												where trg.id in (select unnest($1))
												and trg.web = true
												)
										,w2 as	(
												select
														target_variable,
														array_agg(w1.id order by w1.id) as id
												from
														w1 group by target_variable
												)
										,w3 as	(
												select
														t.target_variable,
														t.id,
														t.label'||_lang_suffix||' AS label
												from
														(
														select
																w2.*,
																((ctv.metadata->''cs'')->''local densities'') as label,
																((ctv.metadata->''en'')->''local densities'') as label_en
														from
																w2
																inner join nfiesta_results.c_target_variable as ctv
																on w2.target_variable = ctv.id
														) as t
												)
										,w4 as	(
												select
														w3.target_variable,
														json_array_elements(w3.label) as label
												from
														w3
												)
										,w5 as	(
												select
														row_number() over () as new_id,
														w4.target_variable,
														replace(
														replace(
														replace(
														replace(
														coalesce((w4.label->''area domain'')->>''label'',''null''::text)
														,''", "'',''; '')
														,''","'',''; '')
														,''["'','''')
														,''"]'','''')													
														as label
												from w4
												)
										,w6 as	(
											select
													w5.target_variable,
													array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
											from
													w5 group by w5.target_variable
												)
										,w7 as	(
												select w3.*, w6.label4user from w3 inner join w6
												on w3.target_variable = w6.target_variable
												)
										,w8 as	(
												select
														w7.label4user as label,
														unnest(w7.id) as id
												from
														w7
												)
										,w9 as	(
												select w8.label, array_agg(w8.id) as id from w8 group by w8.label
												)
										select
												null::integer as res_id,
												w9.label as res_label,
												w9.id as res_id_group
										from
												w9 order by w9.label;
										'
										using _nom_id_group;
									else
										-----------------------------------------------------------------
										-- NOM_POPULATION --
										-----------------------------------------------------------------
										if _nom_population = false
										then
											return query execute
											'	
											with
											w1 as	(
													select * from nfiesta_results.t_result_group as trg
													where trg.id in (select unnest($1))
													and trg.web = true
													)
											,w2 as	(
													select
															target_variable,
															array_agg(w1.id order by w1.id) as id
													from
															w1 group by target_variable
													)
											,w3 as	(
													select
															t.target_variable,
															t.id,
															t.label'||_lang_suffix||' AS label
													from
															(
															select
																	w2.*,
																	((ctv.metadata->''cs'')->''local densities'') as label,
																	((ctv.metadata->''en'')->''local densities'') as label_en
															from
																	w2
																	inner join nfiesta_results.c_target_variable as ctv
																	on w2.target_variable = ctv.id
															) as t
													)
											,w4 as	(
													select
															w3.target_variable,
															json_array_elements(w3.label) as label
													from
															w3
													)
											,w5 as	(
													select
															row_number() over () as new_id,
															w4.target_variable,
															replace(
															replace(
															replace(
															replace(
															coalesce((w4.label->''population'')->>''label'',''null''::text)
															,''", "'',''; '')
															,''","'',''; '')
															,''["'','''')
															,''"]'','''')													
															as label
													from w4
													)
											,w6 as	(
												select
														w5.target_variable,
														array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
												from
														w5 group by w5.target_variable
													)
											,w7 as	(
													select w3.*, w6.label4user from w3 inner join w6
													on w3.target_variable = w6.target_variable
													)
											,w8 as	(
													select
															w7.label4user as label,
															unnest(w7.id) as id
													from
															w7
													)
											,w9 as	(
													select w8.label, array_agg(w8.id) as id from w8 group by w8.label
													)
											select
													null::integer as res_id,
													w9.label as res_label,
													w9.id as res_id_group
											from
													w9 order by w9.label;
											'
											using _nom_id_group;
										else
							*/
											-----------------------------------------------------------------
											-- UNIT_OF_MEASURE --
											-----------------------------------------------------------------
											if _unit_of_measure = false
											then			
												select
														count(t.target_variable)
												from
														(
														select distinct trg.target_variable from nfiesta_results.t_result_group as trg
														where trg.id in (select unnest(_nom_id_group))
														) as t
												into _check_nom_id_group;

												if _check_nom_id_group is distinct from 1
												then
													raise exception 'Error 01: fn_get_user_options_numerator: The values in input argument _nom_id_group = % must only be for the same target variable!',_nom_id_group;
												end if;

												return query execute
												'
												with
												w1 as	(
														select
																id,
																target_variable,
																coalesce(target_variable_denom,0) as target_variable_denom 
														from
																nfiesta_results.t_result_group
														where
																id in (select unnest($1))
														and
																web = true
														)
												,w2 as	(
														select
																w1.target_variable,
																w1.target_variable_denom,
																array_agg(w1.id order by w1.id) as id
														from
																w1 group by w1.target_variable, w1.target_variable_denom
														)
												,w3 as	(
														select
																w2.*,
																((ctv1.metadata->''cs'')->''unit'')->>''label'' as unit_nom,
																((ctv2.metadata->''cs'')->''unit'')->>''label'' as unit_denom,
																((ctv1.metadata->''en'')->''unit'')->>''label'' as unit_nom_en,
																((ctv2.metadata->''en'')->''unit'')->>''label'' as unit_denom_en
														from
																w2
																inner join nfiesta_results.c_target_variable as ctv1 on w2.target_variable = ctv1.id
																left join nfiesta_results.c_target_variable as ctv2 on w2.target_variable_denom = ctv2.id
														)
												,w4 as	(
														select
																w3.target_variable,
																w3.target_variable_denom,
																w3.id,
																w3.unit_nom'||_lang_suffix||' AS unit_nom,
																w3.unit_denom'||_lang_suffix||' AS unit_denom
														from
																w3
														)
												,w5 as	(
														select
																w4.*,
																case
																	when (w4.target_variable = w4.target_variable_denom) or (w4.unit_nom = w4.unit_denom) then ''%''
																	when w4.target_variable_denom = 0 then w4.unit_nom
																	else concat(w4.unit_nom,'' / '',w4.unit_denom)
																end as label4user,
																------------------
																case
																	when w4.target_variable_denom = 0 then 0
																	else 1
																end as res_id
														from
																w4
														)
												,w6 as	(
														select
																w5.res_id,
																w5.label4user as label,
																unnest(w5.id) as id
														from
																w5
														)
												,w7 as	(
														select
																w6.res_id,
																w6.label,
																array_agg(w6.id) as id
														from
																w6 group by w6.res_id, w6.label
														)
												select
														w7.res_id,
														w7.label as res_label,
														w7.id as res_id_group
												from
														w7 order by w7.label;
												'
												using _nom_id_group;																	
											else
												raise exception 'Error 02: fn_get_user_options_numerator: This variant of function is not implemented !';
											end if;
										--end if;
									--end if;
								--end if;
							--end if;
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, /*boolean, boolean, boolean, boolean,*/ boolean) is
'The function returns list of available IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, /*boolean, boolean, boolean, boolean,*/ boolean) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_ldsity" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_ldsity.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_ldsity
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_ldsity(character varying, integer[], text[], text[], text[], text[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_ldsity
(
	_jlang				character varying,
	_id_group			integer[],
	_ldsity				text[] default null::text[],
	_variant			text[] default null::text[],
	_area_domain		text[] default null::text[],
	_population			text[] default null::text[]
)
returns table
(
	res_id				integer,
	res_label			text,
	res_id_group		integer[],
	res_ldsity			text[],
	res_variant			text[],
	res_area_domain		text[],
	res_population		text[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix			character varying(3);
		_cond_ldsity			text;
		_cond_variant			text;
		_cond_area_domain		text;
		_cond_population		text;
		_undivided_text			text;
		_check_query			integer;
begin	
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_numerator_ldsity: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _ldsity is null
		then
			_cond_ldsity := 'w6.label_ldsity is not null';
		else
			_cond_ldsity := 'w6.label_ldsity @> $2 and w6.ldsity not in (select unnest($2))';
		end if;

		if _variant is null
		then
			_cond_variant := 'w6.label_variant is not null';
		else
			_cond_variant := 'w6.label_variant @> $3';
		end if;
	
		if _area_domain is null
		then
			_cond_area_domain := 'w6.label_area_domain is not null';
		else
			_cond_area_domain := 'w6.label_area_domain @> $4';
		end if;
	
		if _population is null
		then
			_cond_population := 'w6.label_population is not null';
		else
			_cond_population := 'w6.label_population @> $5';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ' then _undivided_text := 'bez dalšího příspěvku lokální hustoty'; end if;
		if _jlang = 'en-GB' then _undivided_text := 'without next contribution of local density'; end if;
		-----------------------------------------------------------------
		execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.label) as label
				from
						w3
				)
		,w5 as	(
				select
						row_number() over () as new_id,
						w4.target_variable,
						w4.label->>''object type label'' as object_type_label,
						w4.label->>''label'' as ldsity,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''definition variant'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as variant,					
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''area domain'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as area_domain,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''population'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as population
				from w4
				)
		,w6 as	(
				select
						w5.*,
						t.label_ldsity,
						case when nfiesta_results.fn_array_compare(t.label_ldsity,$2) = true then 0 else 1 end as ldsity_end,
						t.label_variant,
						case when nfiesta_results.fn_array_compare(t.label_variant,$3) = true then 0 else 1 end as variant_end,
						t.label_area_domain,
						case when nfiesta_results.fn_array_compare(t.label_area_domain,$4) = true then 0 else 1 end as area_domain_end,
						t.label_population,
						case when nfiesta_results.fn_array_compare(t.label_population,$5) = true then 0 else 1 end as population_end
				from
						w5
						inner join
							(
							select
									w5.target_variable,
									array_agg(w5.ldsity order by w5.new_id) as label_ldsity,
									array_agg(w5.variant order by w5.new_id) as label_variant,
									array_agg(w5.area_domain order by w5.new_id) as label_area_domain,
									array_agg(w5.population order by w5.new_id) as label_population
							from
									w5 group by w5.target_variable
							) as t
				on w5.target_variable = t.target_variable
				)
				-----------------------------------------------------
				-----------------------------------------------------
		,w7 as	(
				select w6.* from w6
				where w6.ldsity_end > 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8 as	(
				select
						w7.*,
						w3.id as res_id_group
				from
						w7 inner join w3 on w7.target_variable = w3.target_variable
				)
		,w9 as	(
				select
						w8.ldsity_end as res_id,
						w8.ldsity as res_label,
						unnest(w8.res_id_group) as res_id_group
				from
						w8
				)
		,w10 as	(
				select
						w9.res_id,
						w9.res_label,
						array_agg(w9.res_id_group order by w9.res_id_group) as res_id_group
				from
						w9 group by w9.res_id, w9.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w7a as	(
				select w6.* from w6
				where w6.ldsity_end = 0
				and w6.variant_end = 0
				and w6.area_domain_end = 0
				and w6.population_end = 0
				)
		,w8a as	(
				select
						w7a.*,
						w3.id as res_id_group
				from
						w7a inner join w3 on w7a.target_variable = w3.target_variable
				)
		,w9a as	(
				select
						w8a.ldsity_end as res_id,
						$6 as res_label,
						unnest(w8a.res_id_group) as res_id_group
				from
						w8a
				)
		,w10a as	(
				select
						w9a.res_id,
						w9a.res_label,
						array_agg(distinct w9a.res_id_group order by w9a.res_id_group) as res_id_group
				from
						w9a group by w9a.res_id, w9a.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w11 as	(
				select
						w10.res_id,
						w10.res_label,
						w10.res_id_group,
						$2 || array[w10.res_label] as res_ldsity,	-- input value + next user selection
						$3 as res_variant,							-- input value
						$4 as res_area_domain,						-- input value
						$5 as res_population						-- input value
				from
						w10
				-----------------------------------------------------
				union all
				-----------------------------------------------------
				select
						w10a.res_id,
						w10a.res_label,
						w10a.res_id_group,
						$2 as res_ldsity,		-- input value
						$3 as res_variant,		-- input value
						$4 as res_area_domain,	-- input value
						$5 as res_population	-- input value
				from
						w10a
				)
		-----------------------------------------------------
		-----------------------------------------------------
		select
				count(w11.*)
		from
				w11
		'
		using _id_group, _ldsity, _variant, _area_domain, _population, _undivided_text
		into _check_query;
		
		if _check_query = 0
		then
			raise exception 'Error 02: fn_get_user_options_numerator_ldsity: Return query execute must not be zero rows!';
		else
			return query execute
			'
			with
			w1 as	(
					select * from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							target_variable,
							array_agg(w1.id order by w1.id) as id
					from
							w1 group by target_variable
					)
			,w3 as	(
					select
							t.target_variable,
							t.id,
							t.label'||_lang_suffix||' AS label
					from
							(
							select
									w2.*,
									((ctv.metadata->''cs'')->''local densities'') as label,
									((ctv.metadata->''en'')->''local densities'') as label_en
							from
									w2
									inner join nfiesta_results.c_target_variable as ctv
									on w2.target_variable = ctv.id
							) as t
					)
			,w4 as	(
					select
							w3.target_variable,
							json_array_elements(w3.label) as label
					from
							w3
					)
			,w5 as	(
					select
							row_number() over () as new_id,
							w4.target_variable,
							w4.label->>''object type label'' as object_type_label,
							w4.label->>''label'' as ldsity,
							replace(
							replace(
							replace(
							replace(
							coalesce((w4.label->''definition variant'')->>''label'',''null''::text)
							,''", "'',''; '')
							,''","'',''; '')
							,''["'','''')
							,''"]'','''')													
							as variant,					
							replace(
							replace(
							replace(
							replace(
							coalesce((w4.label->''area domain'')->>''label'',''null''::text)
							,''", "'',''; '')
							,''","'',''; '')
							,''["'','''')
							,''"]'','''')													
							as area_domain,
							replace(
							replace(
							replace(
							replace(
							coalesce((w4.label->''population'')->>''label'',''null''::text)
							,''", "'',''; '')
							,''","'',''; '')
							,''["'','''')
							,''"]'','''')													
							as population
					from w4
					)
			,w6 as	(
					select
							w5.*,
							t.label_ldsity,
							case when nfiesta_results.fn_array_compare(t.label_ldsity,$2) = true then 0 else 1 end as ldsity_end,
							t.label_variant,
							case when nfiesta_results.fn_array_compare(t.label_variant,$3) = true then 0 else 1 end as variant_end,
							t.label_area_domain,
							case when nfiesta_results.fn_array_compare(t.label_area_domain,$4) = true then 0 else 1 end as area_domain_end,
							t.label_population,
							case when nfiesta_results.fn_array_compare(t.label_population,$5) = true then 0 else 1 end as population_end
					from
							w5
							inner join
								(
								select
										w5.target_variable,
										array_agg(w5.ldsity order by w5.new_id) as label_ldsity,
										array_agg(w5.variant order by w5.new_id) as label_variant,
										array_agg(w5.area_domain order by w5.new_id) as label_area_domain,
										array_agg(w5.population order by w5.new_id) as label_population
								from
										w5 group by w5.target_variable
								) as t
					on w5.target_variable = t.target_variable
					)
					-----------------------------------------------------
					-----------------------------------------------------
			,w7 as	(
					select w6.* from w6
					where w6.ldsity_end > 0
					and '||_cond_ldsity||'
					and '||_cond_variant||'
					and '||_cond_area_domain||'
					and '||_cond_population||'
					)
			,w8 as	(
					select
							w7.*,
							w3.id as res_id_group
					from
							w7 inner join w3 on w7.target_variable = w3.target_variable
					)
			,w9 as	(
					select
							w8.ldsity_end as res_id,
							w8.ldsity as res_label,
							unnest(w8.res_id_group) as res_id_group
					from
							w8
					)
			,w10 as	(
					select
							w9.res_id,
							w9.res_label,
							array_agg(w9.res_id_group order by w9.res_id_group) as res_id_group
					from
							w9 group by w9.res_id, w9.res_label
					)
			-----------------------------------------------------
			-----------------------------------------------------
			,w7a as	(
					select w6.* from w6
					where w6.ldsity_end = 0
					and w6.variant_end = 0
					and w6.area_domain_end = 0
					and w6.population_end = 0
					)
			,w8a as	(
					select
							w7a.*,
							w3.id as res_id_group
					from
							w7a inner join w3 on w7a.target_variable = w3.target_variable
					)
			,w9a as	(
					select
							w8a.ldsity_end as res_id,
							$6 as res_label,
							unnest(w8a.res_id_group) as res_id_group
					from
							w8a
					)
			,w10a as	(
					select
							w9a.res_id,
							w9a.res_label,
							array_agg(distinct w9a.res_id_group order by w9a.res_id_group) as res_id_group
					from
							w9a group by w9a.res_id, w9a.res_label
					)
			-----------------------------------------------------
			-----------------------------------------------------
			,w11 as	(
					select
							w10.res_id,
							w10.res_label,
							w10.res_id_group,
							$2 || array[w10.res_label] as res_ldsity,	-- input value + next user selection
							$3 as res_variant,							-- input value
							$4 as res_area_domain,						-- input value
							$5 as res_population						-- input value
					from
							w10
					-----------------------------------------------------
					union all
					-----------------------------------------------------
					select
							w10a.res_id,
							w10a.res_label,
							w10a.res_id_group,
							$2 as res_ldsity,		-- input value
							$3 as res_variant,		-- input value
							$4 as res_area_domain,	-- input value
							$5 as res_population	-- input value
					from
							w10a
					)
			-----------------------------------------------------
			-----------------------------------------------------
			select
					w11.res_id,
					w11.res_label,
					w11.res_id_group,
					w11.res_ldsity,
					w11.res_variant,
					w11.res_area_domain,
					w11.res_population
			from
					w11
			order
					by w11.res_id, w11.res_label;
			'
			using _id_group, _ldsity, _variant, _area_domain, _population, _undivided_text;
		end if;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_ldsity(character varying, integer[], text[], text[], text[], text[]) is
'The function returns list of available labels of local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_ldsity(character varying, integer[], text[], text[], text[], text[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_variant" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_variant.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_variant
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_variant(character varying, integer[], text[], text[], text[], text[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_variant
(
	_jlang				character varying,
	_id_group			integer[],
	_ldsity				text[],
	_variant			text[] default null::text[],
	_area_domain		text[] default null::text[],
	_population			text[] default null::text[]
)
returns table
(
	res_id				integer,
	res_label			text,
	res_id_group		integer[],
	res_ldsity			text[],
	res_variant			text[],
	res_area_domain		text[],
	res_population		text[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix		character varying(3);
		_cond_ldsity		text;
		_cond_variant		text;
		_cond_area_domain	text;
		_cond_population	text;
		_undivided_text		text;
		_without_limit_text	text;
begin
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_numerator_variant: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _ldsity is null
		then
			raise exception 'Error 02: fn_get_user_options_numerator_variant: Input argument _ldsity must not be null!';
		end if;
		-----------------------------------------------------------------
		_cond_ldsity := 'w6.label_ldsity @> $2 and w6.ldsity = $2[array_length($2,1)]';
	
		if _variant is null
		then
			_cond_variant := 'w6.label_variant is not null';
		else
			_cond_variant := 'w6.label_variant @> $3';
		end if;
	
		if _area_domain is null
		then
			_cond_area_domain := 'w6.label_area_domain is not null';
		else
			_cond_area_domain := 'w6.label_area_domain @> $4';
		end if;
	
		if _population is null
		then
			_cond_population := 'w6.label_population is not null';
		else
			_cond_population := 'w6.label_population @> $5';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ' then _undivided_text := 'bez dalšího příspěvku lokální hustoty'; end if;
		if _jlang = 'en-GB' then _undivided_text := 'without next contribution of local density'; end if;
		if _jlang = 'cs-CZ' then _without_limit_text := 'bez omezení'; end if;
		if _jlang = 'en-GB' then _without_limit_text := 'without limits'; end if;
		-----------------------------------------------------------------	
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.label) as label
				from
						w3
				)
		,w5 as	(
				select
						row_number() over () as new_id,
						w4.target_variable,
						w4.label->>''object type label'' as object_type_label,
						w4.label->>''label'' as ldsity,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''definition variant'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as variant,					
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''area domain'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as area_domain,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''population'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as population
				from w4
				)
		,w6 as	(
				select
						w5.*,
						t.label_ldsity,
						case when nfiesta_results.fn_array_compare(t.label_ldsity,$2) = true then 0 else 1 end as ldsity_end,
						t.label_variant,
						case when nfiesta_results.fn_array_compare(t.label_variant,$3) = true then 0 else 1 end as variant_end,
						t.label_area_domain,
						case when nfiesta_results.fn_array_compare(t.label_area_domain,$4) = true then 0 else 1 end as area_domain_end,
						t.label_population,
						case when nfiesta_results.fn_array_compare(t.label_population,$5) = true then 0 else 1 end as population_end
				from
						w5
						inner join
							(
							select
									w5.target_variable,
									array_agg(w5.ldsity order by w5.new_id) as label_ldsity,
									array_agg(w5.variant order by w5.new_id) as label_variant,
									array_agg(w5.area_domain order by w5.new_id) as label_area_domain,
									array_agg(w5.population order by w5.new_id) as label_population
							from
									w5 group by w5.target_variable
							) as t
				on w5.target_variable = t.target_variable
				)
				-----------------------------------------------------
				-----------------------------------------------------
		,w7 as	(
				select w6.* from w6
				where w6.variant_end > 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8 as	(
				select
						w7.*,
						w3.id as res_id_group
				from
						w7 inner join w3 on w7.target_variable = w3.target_variable
				)
		,w9 as	(
				select
						w8.variant_end as res_id,
						w8.variant as res_label,
						unnest(w8.res_id_group) as res_id_group
				from
						w8
				)
		,w10 as	(
				select
						w9.res_id,
						w9.res_label,
						array_agg(distinct w9.res_id_group order by w9.res_id_group) as res_id_group
				from
						w9 group by w9.res_id, w9.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w7a as	(
				select w6.* from w6
				where w6.variant_end = 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8a as	(
				select
						w7a.*,
						w3.id as res_id_group
				from
						w7a inner join w3 on w7a.target_variable = w3.target_variable
				)
		,w9a as	(
				select
						w8a.variant_end as res_id,
						$6 as res_label,
						unnest(w8a.res_id_group) as res_id_group
				from
						w8a
				)
		,w10a as	(
				select
						w9a.res_id,
						w9a.res_label,
						array_agg(distinct w9a.res_id_group order by w9a.res_id_group) as res_id_group
				from
						w9a group by w9a.res_id, w9a.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w11 as	(
				select
						w10.res_id,
						w10.res_label,
						w10.res_id_group,
						$2 as res_ldsity,							-- input value
						$3 || array[w10.res_label] as res_variant,	-- input value + next user selection
						$4 as res_area_domain,						-- input value
						$5 as res_population						-- input value
				from
						w10
				-----------------------------------------------------
				union all
				-----------------------------------------------------
				select
						w10a.res_id,
						w10a.res_label,
						w10a.res_id_group,
						$2 as res_ldsity,		-- input value
						$3 as res_variant,		-- input value
						$4 as res_area_domain,	-- input value
						$5 as res_population	-- input value
				from
						w10a
				)
		-----------------------------------------------------
		-----------------------------------------------------
		select
				w11.res_id,
				case when w11.res_label = ''null'' then $7 else w11.res_label end as res_label,
				w11.res_id_group,
				w11.res_ldsity,
				w11.res_variant,
				w11.res_area_domain,
				w11.res_population
		from
				w11
		order
				by w11.res_id, w11.res_label;
		'
		using _id_group, _ldsity, _variant, _area_domain, _population, _undivided_text, _without_limit_text;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_variant(character varying, integer[], text[], text[], text[], text[]) is
'The function returns list of available labels of variants of local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_variant(character varying, integer[], text[], text[], text[], text[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_area_domain" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_area_domain.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_area_domain
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_area_domain(character varying, integer[], text[], text[], text[], text[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_area_domain
(
	_jlang				character varying,
	_id_group			integer[],
	_ldsity				text[],
	_variant			text[],
	_area_domain		text[] default null::text[],
	_population			text[] default null::text[]
)
returns table
(
	res_id				integer,
	res_label			text,
	res_id_group		integer[],
	res_ldsity			text[],
	res_variant			text[],
	res_area_domain		text[],
	res_population		text[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix		character varying(3);
		_cond_ldsity		text;
		_cond_variant		text;
		_cond_area_domain	text;
		_cond_population	text;
		_undivided_text		text;
		_without_limit_text	text;
begin
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_numerator_area_domain: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _ldsity is null
		then
			raise exception 'Error 02: fn_get_user_options_numerator_area_domain: Input argument _ldsity must not be null!';
		end if;
		-----------------------------------------------------------------
		if _variant is null
		then
			raise exception 'Error 03: fn_get_user_options_numerator_area_domain: Input argument _variant must not be null!';
		end if;
		-----------------------------------------------------------------		
		_cond_ldsity := 'w6.label_ldsity @> $2 and w6.ldsity = $2[array_length($2,1)]';
		_cond_variant := 'w6.label_variant @> $3 and w6.variant = $3[array_length($3,1)]';
	
		if _area_domain is null
		then
			_cond_area_domain := 'w6.label_area_domain is not null';
		else
			_cond_area_domain := 'w6.label_area_domain @> $4';
		end if;
	
		if _population is null
		then
			_cond_population := 'w6.label_population is not null';
		else
			_cond_population := 'w6.label_population @> $5';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ' then _undivided_text := 'bez dalšího příspěvku lokální hustoty'; end if;
		if _jlang = 'en-GB' then _undivided_text := 'without next contribution of local density'; end if;
		if _jlang = 'cs-CZ' then _without_limit_text := 'bez omezení'; end if;
		if _jlang = 'en-GB' then _without_limit_text := 'without limits'; end if;	
		-----------------------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.label) as label
				from
						w3
				)
		,w5 as	(
				select
						row_number() over () as new_id,
						w4.target_variable,
						w4.label->>''object type label'' as object_type_label,
						w4.label->>''label'' as ldsity,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''definition variant'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as variant,					
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''area domain'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as area_domain,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''population'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as population
				from w4
				)
		,w6 as	(
				select
						w5.*,
						t.label_ldsity,
						case when nfiesta_results.fn_array_compare(t.label_ldsity,$2) = true then 0 else 1 end as ldsity_end,
						t.label_variant,
						case when nfiesta_results.fn_array_compare(t.label_variant,$3) = true then 0 else 1 end as variant_end,
						t.label_area_domain,
						case when nfiesta_results.fn_array_compare(t.label_area_domain,$4) = true then 0 else 1 end as area_domain_end,
						t.label_population,
						case when nfiesta_results.fn_array_compare(t.label_population,$5) = true then 0 else 1 end as population_end
				from
						w5
						inner join
							(
							select
									w5.target_variable,
									array_agg(w5.ldsity order by w5.new_id) as label_ldsity,
									array_agg(w5.variant order by w5.new_id) as label_variant,
									array_agg(w5.area_domain order by w5.new_id) as label_area_domain,
									array_agg(w5.population order by w5.new_id) as label_population
							from
									w5 group by w5.target_variable
							) as t
				on w5.target_variable = t.target_variable
				)
				-----------------------------------------------------
				-----------------------------------------------------
		,w7 as	(
				select w6.* from w6
				where w6.area_domain_end > 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8 as	(
				select
						w7.*,
						w3.id as res_id_group
				from
						w7 inner join w3 on w7.target_variable = w3.target_variable
				)
		,w9 as	(
				select
						w8.area_domain_end as res_id,
						w8.area_domain as res_label,
						unnest(w8.res_id_group) as res_id_group
				from
						w8
				)
		,w10 as	(
				select
						w9.res_id,
						w9.res_label,
						array_agg(distinct w9.res_id_group order by w9.res_id_group) as res_id_group
				from
						w9 group by w9.res_id, w9.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w7a as	(
				select w6.* from w6
				where w6.area_domain_end = 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8a as	(
				select
						w7a.*,
						w3.id as res_id_group
				from
						w7a inner join w3 on w7a.target_variable = w3.target_variable
				)
		,w9a as	(
				select
						w8a.area_domain_end as res_id,
						$6 as res_label,
						unnest(w8a.res_id_group) as res_id_group
				from
						w8a
				)
		,w10a as	(
				select
						w9a.res_id,
						w9a.res_label,
						array_agg(distinct w9a.res_id_group order by w9a.res_id_group) as res_id_group
				from
						w9a group by w9a.res_id, w9a.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w11 as	(
				select
						w10.res_id,
						w10.res_label,
						w10.res_id_group,
						$2 as res_ldsity,								-- input value
						$3 as res_variant,								-- input value
						$4 || array[w10.res_label] as res_area_domain,	-- input value + next user selection
						$5 as res_population							-- input value
				from
						w10
				-----------------------------------------------------
				union all
				-----------------------------------------------------
				select
						w10a.res_id,
						w10a.res_label,
						w10a.res_id_group,
						$2 as res_ldsity,		-- input value
						$3 as res_variant,		-- input value
						$4 as res_area_domain,	-- input value
						$5 as res_population	-- input value
				from
						w10a
				)
		-----------------------------------------------------
		-----------------------------------------------------
		select
				w11.res_id,
				case when w11.res_label = ''null'' then $7 else w11.res_label end as res_label,
				w11.res_id_group,
				w11.res_ldsity,
				w11.res_variant,
				w11.res_area_domain,
				w11.res_population
		from
				w11
		order
				by w11.res_id, w11.res_label;
		'
		using _id_group, _ldsity, _variant, _area_domain, _population, _undivided_text, _without_limit_text;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_area_domain(character varying, integer[], text[], text[], text[], text[]) is
'The function returns list of available labels of area domains of local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_area_domain(character varying, integer[], text[], text[], text[], text[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_population" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_population
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_population(character varying, integer[], text[], text[], text[], text[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_population
(
	_jlang				character varying,
	_id_group			integer[],
	_ldsity				text[],
	_variant			text[],
	_area_domain		text[],
	_population			text[] default null::text[]
)
returns table
(
	res_id				integer,
	res_label			text,
	res_id_group		integer[],
	res_ldsity			text[],
	res_variant			text[],
	res_area_domain		text[],
	res_population		text[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix		character varying(3);
		_cond_ldsity		text;
		_cond_variant		text;
		_cond_area_domain	text;
		_cond_population	text;
		_undivided_text		text;
		_without_limit_text	text;
begin
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_numerator_population: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _ldsity is null
		then
			raise exception 'Error 02: fn_get_user_options_numerator_population: Input argument _ldsity must not be null!';
		end if;
		-----------------------------------------------------------------
		if _variant is null
		then
			raise exception 'Error 03: fn_get_user_options_numerator_population: Input argument _variant must not be null!';
		end if;
		-----------------------------------------------------------------
		if _area_domain is null
		then
			raise exception 'Error 04: fn_get_user_options_numerator_population: Input argument _area_domain must not be null!';
		end if;
		-----------------------------------------------------------------		
		_cond_ldsity := 'w6.label_ldsity @> $2 and w6.ldsity = $2[array_length($2,1)]';
		_cond_variant := 'w6.label_variant @> $3 and w6.variant = $3[array_length($3,1)]';
		_cond_area_domain := 'w6.label_area_domain @> $4 and w6.area_domain = $4[array_length($4,1)]';
	
		if _population is null
		then
			_cond_population := 'w6.label_population is not null';
		else
			_cond_population := 'w6.label_population @> $5';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ' then _undivided_text := 'bez dalšího příspěvku lokální hustoty'; end if;
		if _jlang = 'en-GB' then _undivided_text := 'without next contribution of local density'; end if;
		if _jlang = 'cs-CZ' then _without_limit_text := 'bez omezení'; end if;
		if _jlang = 'en-GB' then _without_limit_text := 'without limits'; end if;			
		-----------------------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.label) as label
				from
						w3
				)
		,w5 as	(
				select
						row_number() over () as new_id,
						w4.target_variable,
						w4.label->>''object type label'' as object_type_label,
						w4.label->>''label'' as ldsity,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''definition variant'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as variant,					
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''area domain'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as area_domain,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''population'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as population
				from w4
				)
		,w6 as	(
				select
						w5.*,
						t.label_ldsity,
						case when nfiesta_results.fn_array_compare(t.label_ldsity,$2) = true then 0 else 1 end as ldsity_end,
						t.label_variant,
						case when nfiesta_results.fn_array_compare(t.label_variant,$3) = true then 0 else 1 end as variant_end,
						t.label_area_domain,
						case when nfiesta_results.fn_array_compare(t.label_area_domain,$4) = true then 0 else 1 end as area_domain_end,
						t.label_population,
						case when nfiesta_results.fn_array_compare(t.label_population,$5) = true then 0 else 1 end as population_end
				from
						w5
						inner join
							(
							select
									w5.target_variable,
									array_agg(w5.ldsity order by w5.new_id) as label_ldsity,
									array_agg(w5.variant order by w5.new_id) as label_variant,
									array_agg(w5.area_domain order by w5.new_id) as label_area_domain,
									array_agg(w5.population order by w5.new_id) as label_population
							from
									w5 group by w5.target_variable
							) as t
				on w5.target_variable = t.target_variable
				)
				-----------------------------------------------------
				-----------------------------------------------------
		,w7 as	(
				select w6.* from w6
				where w6.population_end > 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8 as	(
				select
						w7.*,
						w3.id as res_id_group
				from
						w7 inner join w3 on w7.target_variable = w3.target_variable
				)
		,w9 as	(
				select
						w8.population_end as res_id,
						w8.population as res_label,
						unnest(w8.res_id_group) as res_id_group
				from
						w8
				)
		,w10 as	(
				select
						w9.res_id,
						w9.res_label,
						array_agg(distinct w9.res_id_group order by w9.res_id_group) as res_id_group
				from
						w9 group by w9.res_id, w9.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w7a as	(
				select w6.* from w6
				where w6.population_end = 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8a as	(
				select
						w7a.*,
						w3.id as res_id_group
				from
						w7a inner join w3 on w7a.target_variable = w3.target_variable
				)
		,w9a as	(
				select
						w8a.population_end as res_id,
						$6 as res_label,
						unnest(w8a.res_id_group) as res_id_group
				from
						w8a
				)
		,w10a as	(
				select
						w9a.res_id,
						w9a.res_label,
						array_agg(distinct w9a.res_id_group order by w9a.res_id_group) as res_id_group
				from
						w9a group by w9a.res_id, w9a.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w11 as	(
				select
						w10.res_id,
						w10.res_label,
						w10.res_id_group,
						$2 as res_ldsity,								-- input value
						$3 as res_variant,								-- input value
						$4 as res_area_domain,							-- input value
						$5 || array[w10.res_label] as res_population	-- input value + next user selection
				from
						w10
				-----------------------------------------------------
				union all
				-----------------------------------------------------
				select
						w10a.res_id,
						w10a.res_label,
						w10a.res_id_group,
						$2 as res_ldsity,		-- input value
						$3 as res_variant,		-- input value
						$4 as res_area_domain,	-- input value
						$5 as res_population	-- input value
				from
						w10a
				)
		-----------------------------------------------------
		-----------------------------------------------------
		select
				w11.res_id,
				case when w11.res_label = ''null'' then $7 else w11.res_label end as res_label,
				w11.res_id_group,
				w11.res_ldsity,
				w11.res_variant,
				w11.res_area_domain,
				w11.res_population
		from
				w11
		order
				by w11.res_id, w11.res_label;
		'
		using _id_group, _ldsity, _variant, _area_domain, _population, _undivided_text, _without_limit_text;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_population(character varying, integer[], text[], text[], text[], text[]) is
'The function returns list of available labels of populations of local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_population(character varying, integer[], text[], text[], text[], text[]) to public;
-- </function>




drop function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean, boolean, boolean, boolean, boolean);
-- <function name="fn_get_user_options_denominator" schema="nfiesta_results" src="functions/fn_get_user_options_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean);

create or replace function nfiesta_results.fn_get_user_options_denominator
(
	_jlang character varying,
	_denom_id_group integer[],
	_denom_indicator boolean DEFAULT false,
	_denom_state boolean DEFAULT false
	--_denom_ldsity boolean DEFAULT false,
	--_denom_definition_variant boolean DEFAULT false,
	--_denom_area_domain boolean DEFAULT false,
	--_denom_population boolean DEFAULT false
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[]
)
language plpgsql
stable
as
$$
declare
	_lang_suffix			character varying(3);
	_lang_covariate			text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _denom_id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_denominator: Input argument _denom_id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	-- DENOM_INDICATOR --
	-----------------------------------------------------------------
	if _denom_indicator = false
	then
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group
				where id in (select unnest($1))
				and web = true
				)
		,w2 as	(
				select
						w1.target_variable_denom,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						w2.*,
						((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
						((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
				from
						w2
						inner join nfiesta_results.c_target_variable as ctv
						on w2.target_variable_denom = ctv.id
				)
		,w4 as	(
				select
						w3.id,
						label'||_lang_suffix||' AS label
				from
						w3
				)
		,w5 as	(
				select
						w4.label,
						unnest(w4.id) as id
				from
						w4
				)
		,w6 as	(
				select w5.label, array_agg(w5.id) as id from w5 group by w5.label
				)
		select
				1 as res_id,
				w6.label as res_label,
				w6.id as res_id_group
		from
				w6 order by w6.label;
		'
		using _denom_id_group;
	else
		-----------------------------------------------------------------
		-- DENOM_STATE --
		-----------------------------------------------------------------
		if _denom_state = false
		then
			return query execute
			'
			with
			w1 as	(
					select * from nfiesta_results.t_result_group
					where id in (select unnest($1))
					and web = true
					)
			,w2 as	(
					select
							w1.target_variable_denom,
							array_agg(w1.id order by w1.id) as id
					from
							w1 group by target_variable_denom
					)
			,w3 as	(
					select
							w2.*,
							((ctv.metadata->''cs'')->''state or change'')->>''label'' as label,
							((ctv.metadata->''en'')->''state or change'')->>''label'' as label_en
					from
							w2
							inner join nfiesta_results.c_target_variable as ctv
							on w2.target_variable_denom = ctv.id
					)
			,w4 as	(
					select
							w3.id,
							label'||_lang_suffix||' AS label
					from
							w3
					)
			,w5 as	(
					select
							w4.label,
							unnest(w4.id) as id
					from
							w4
					)
			,w6 as	(
					select w5.label, array_agg(w5.id) as id from w5 group by w5.label
					)
			select
					1 as res_id,
					w6.label as res_label,
					w6.id as res_id_group
			from
					w6 order by w6.label;
			'
			using _denom_id_group;
		else
			/*
			-----------------------------------------------------------------
			-- DENOM_LDSITY --
			-----------------------------------------------------------------
			if _denom_ldsity = false
			then
				return query execute
				'
				with
				w1 as	(
						select * from nfiesta_results.t_result_group
						where id in (select unnest($1))
						and web = true
						)
				,w2 as	(
						select
								w1.target_variable_denom,
								array_agg(w1.id order by w1.id) as id
						from
								w1 group by target_variable_denom
						)
				,w3 as	(
						select
								t.target_variable_denom,
								t.id,
								t.label'||_lang_suffix||' AS label
						from
								(
								select
										w2.*,
										((ctv.metadata->''cs'')->''local densities'') as label,
										((ctv.metadata->''en'')->''local densities'') as label_en
								from
										w2
										inner join nfiesta_results.c_target_variable as ctv
										on w2.target_variable_denom = ctv.id
								) as t
						)
				,w4 as	(
						select
								w3.target_variable_denom,
								json_array_elements(w3.label) as label
						from
								w3
						)
				,w5 as	(
						select
								row_number() over () as new_id,
								w4.target_variable_denom,
								w4.label->>''label'' as label
						from w4
						)
				,w6 as	(
						select
								w5.target_variable_denom,
								array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
						from
								w5 group by w5.target_variable_denom
						)
				,w7 as	(
						select w3.*, w6.label4user from w3 inner join w6
						on w3.target_variable_denom = w6.target_variable_denom
						)
				,w8 as	(
						select
								w7.label4user as label,
								unnest(w7.id) as id
						from
								w7
						)
				,w9 as	(
						select w8.label, array_agg(w8.id) as id from w8 group by w8.label
						)
				select
						1 as res_id,
						w9.label as res_label,
						w9.id as res_id_group
				from
						w9 order by w9.label;
				'
				using _denom_id_group;
			else
				-----------------------------------------------------------------
				-- DENOM_DEFINITION_VARIANT --
				-----------------------------------------------------------------
				if _denom_definition_variant = false
				then
					return query execute
					'
					with
					w1 as	(
							select * from nfiesta_results.t_result_group
							where id in (select unnest($1))
							and web = true
							)
					,w2 as	(
							select
									w1.target_variable_denom,
									array_agg(w1.id order by w1.id) as id
							from
									w1 group by target_variable_denom
							)
					,w3 as	(
							select
									t.target_variable_denom,
									t.id,
									t.label'||_lang_suffix||' AS label
							from
									(
									select
											w2.*,
											((ctv.metadata->''cs'')->''local densities'') as label,
											((ctv.metadata->''en'')->''local densities'') as label_en
									from
											w2
											inner join nfiesta_results.c_target_variable as ctv
											on w2.target_variable_denom = ctv.id
									) as t
							)
					,w4 as	(
							select
									w3.target_variable_denom,
									json_array_elements(w3.label) as label
							from
									w3
							)
					,w5 as	(
							select
									row_number() over () as new_id,
									w4.target_variable_denom,
									coalesce((w4.label->''definition variant'')->>''label'',''null''::text) as label
							from w4
							)										
					,w6 as	(
						select
								w5.target_variable_denom,
								array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
						from
								w5 group by w5.target_variable_denom
							)
					,w7 as	(
							select w3.*, w6.label4user from w3 inner join w6
							on w3.target_variable_denom = w6.target_variable_denom
							)
					,w8 as	(
							select
									w7.label4user as label,
									unnest(w7.id) as id
							from
									w7
							)
					,w9 as	(
							select w8.label, array_agg(w8.id) as id from w8 group by w8.label
							)
					select
							1 as res_id,
							w9.label as res_label,
							w9.id as res_id_group
					from
							w9 order by w9.label;
					'
					using _denom_id_group;
				else
					-----------------------------------------------------------------
					-- DENOM_AREA_DOMAIN --
					-----------------------------------------------------------------
					if _denom_area_domain = false
					then
						return query execute
						'
						with
						w1 as	(
								select * from nfiesta_results.t_result_group
								where id in (select unnest($1))
								and web = true
								)
						,w2 as	(
								select
										w1.target_variable_denom,
										array_agg(w1.id order by w1.id) as id
								from
										w1 group by target_variable_denom
								)
						,w3 as	(
								select
										t.target_variable_denom,
										t.id,
										t.label'||_lang_suffix||' AS label
								from
										(
										select
												w2.*,
												((ctv.metadata->''cs'')->''local densities'') as label,
												((ctv.metadata->''en'')->''local densities'') as label_en
										from
												w2
												inner join nfiesta_results.c_target_variable as ctv
												on w2.target_variable_denom = ctv.id
										) as t
								)
						,w4 as	(
								select
										w3.target_variable_denom,
										json_array_elements(w3.label) as label
								from
										w3
								)
						,w5 as	(
								select
										row_number() over () as new_id,
										w4.target_variable_denom,
										replace(
										replace(
										replace(
										replace(
										coalesce((w4.label->''area domain'')->>''label'',''null''::text)
										,''", "'',''; '')
										,''","'',''; '')
										,''["'','''')
										,''"]'','''')													
										as label
								from w4
								)
						,w6 as	(
							select
									w5.target_variable_denom,
									array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
							from
									w5 group by w5.target_variable_denom
								)
						,w7 as	(
								select w3.*, w6.label4user from w3 inner join w6
								on w3.target_variable_denom = w6.target_variable_denom
								)
						,w8 as	(
								select
										w7.label4user as label,
										unnest(w7.id) as id
								from
										w7
								)
						,w9 as	(
								select w8.label, array_agg(w8.id) as id from w8 group by w8.label
								)
						select
								1 as res_id,
								w9.label as res_label,
								w9.id as res_id_group
						from
								w9 order by w9.label;
						'
						using _denom_id_group;
					else
						-----------------------------------------------------------------
						-- DENOM_POPULATION --
						-----------------------------------------------------------------
						if _denom_population = false
						then
							return query execute
							'
							with
							w1 as	(
									select * from nfiesta_results.t_result_group
									where id in (select unnest($1))
									and web = true
									)
							,w2 as	(
									select
											w1.target_variable_denom,
											array_agg(w1.id order by w1.id) as id
									from
											w1 group by target_variable_denom
									)
							,w3 as	(
									select
											t.target_variable_denom,
											t.id,
											t.label'||_lang_suffix||' AS label
									from
											(
											select
													w2.*,
													((ctv.metadata->''cs'')->''local densities'') as label,
													((ctv.metadata->''en'')->''local densities'') as label_en
											from
													w2
													inner join nfiesta_results.c_target_variable as ctv
													on w2.target_variable_denom = ctv.id
											) as t
									)
							,w4 as	(
									select
											w3.target_variable_denom,
											json_array_elements(w3.label) as label
									from
											w3
									)
							,w5 as	(
									select
											row_number() over () as new_id,
											w4.target_variable_denom,
											replace(
											replace(
											replace(
											replace(
											coalesce((w4.label->''population'')->>''label'',''null''::text)
											,''", "'',''; '')
											,''","'',''; '')
											,''["'','''')
											,''"]'','''')													
											as label
									from w4
									)
							,w6 as	(
								select
										w5.target_variable_denom,
										array_to_string(array_agg(w5.label order by w5.new_id),'' | '') as label4user
								from
										w5 group by w5.target_variable_denom
									)
							,w7 as	(
									select w3.*, w6.label4user from w3 inner join w6
									on w3.target_variable_denom = w6.target_variable_denom
									)
							,w8 as	(
									select
											w7.label4user as label,
											unnest(w7.id) as id
									from
											w7
									)
							,w9 as	(
									select w8.label, array_agg(w8.id) as id from w8 group by w8.label
									)
							select
									0 as res_id,
									w9.label as res_label,
									w9.id as res_id_group
							from
									w9 order by w9.label;
							'
							using _denom_id_group; 
						else
							raise exception 'Error 02: fn_get_user_options_denominator: This variant of function is not implemented !';
						end if;
					end if;
				end if;
			end if;
			*/
			raise exception 'Error 02: fn_get_user_options_denominator: This variant of function is not implemented !';
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean) is
'The function returns list of available IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_ldsity" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_ldsity.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_ldsity
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_ldsity(character varying, integer[], text[], text[], text[], text[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_ldsity
(
	_jlang				character varying,
	_id_group			integer[],
	_ldsity				text[] default null::text[],
	_variant			text[] default null::text[],
	_area_domain		text[] default null::text[],
	_population			text[] default null::text[]
)
returns table
(
	res_id				integer,
	res_label			text,
	res_id_group		integer[],
	res_ldsity			text[],
	res_variant			text[],
	res_area_domain		text[],
	res_population		text[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix			character varying(3);
		_cond_ldsity			text;
		_cond_variant			text;
		_cond_area_domain		text;
		_cond_population		text;
		_undivided_text			text;
		_check_query			integer;
begin	
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_denominator_ldsity: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _ldsity is null
		then
			_cond_ldsity := 'w6.label_ldsity is not null';
		else
			_cond_ldsity := 'w6.label_ldsity @> $2 and w6.ldsity not in (select unnest($2))';
		end if;

		if _variant is null
		then
			_cond_variant := 'w6.label_variant is not null';
		else
			_cond_variant := 'w6.label_variant @> $3';
		end if;
	
		if _area_domain is null
		then
			_cond_area_domain := 'w6.label_area_domain is not null';
		else
			_cond_area_domain := 'w6.label_area_domain @> $4';
		end if;
	
		if _population is null
		then
			_cond_population := 'w6.label_population is not null';
		else
			_cond_population := 'w6.label_population @> $5';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ' then _undivided_text := 'bez dalšího příspěvku lokální hustoty'; end if;
		if _jlang = 'en-GB' then _undivided_text := 'without next contribution of local density'; end if;
		-----------------------------------------------------------------
		execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable_denom as target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.label) as label
				from
						w3
				)
		,w5 as	(
				select
						row_number() over () as new_id,
						w4.target_variable,
						w4.label->>''object type label'' as object_type_label,
						w4.label->>''label'' as ldsity,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''definition variant'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as variant,					
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''area domain'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as area_domain,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''population'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as population
				from w4
				)
		,w6 as	(
				select
						w5.*,
						t.label_ldsity,
						case when nfiesta_results.fn_array_compare(t.label_ldsity,$2) = true then 0 else 1 end as ldsity_end,
						t.label_variant,
						case when nfiesta_results.fn_array_compare(t.label_variant,$3) = true then 0 else 1 end as variant_end,
						t.label_area_domain,
						case when nfiesta_results.fn_array_compare(t.label_area_domain,$4) = true then 0 else 1 end as area_domain_end,
						t.label_population,
						case when nfiesta_results.fn_array_compare(t.label_population,$5) = true then 0 else 1 end as population_end
				from
						w5
						inner join
							(
							select
									w5.target_variable,
									array_agg(w5.ldsity order by w5.new_id) as label_ldsity,
									array_agg(w5.variant order by w5.new_id) as label_variant,
									array_agg(w5.area_domain order by w5.new_id) as label_area_domain,
									array_agg(w5.population order by w5.new_id) as label_population
							from
									w5 group by w5.target_variable
							) as t
				on w5.target_variable = t.target_variable
				)
				-----------------------------------------------------
				-----------------------------------------------------
		,w7 as	(
				select w6.* from w6
				where w6.ldsity_end > 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8 as	(
				select
						w7.*,
						w3.id as res_id_group
				from
						w7 inner join w3 on w7.target_variable = w3.target_variable
				)
		,w9 as	(
				select
						w8.ldsity_end as res_id,
						w8.ldsity as res_label,
						unnest(w8.res_id_group) as res_id_group
				from
						w8
				)
		,w10 as	(
				select
						w9.res_id,
						w9.res_label,
						array_agg(w9.res_id_group order by w9.res_id_group) as res_id_group
				from
						w9 group by w9.res_id, w9.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w7a as	(
				select w6.* from w6
				where w6.ldsity_end = 0
				and w6.variant_end = 0
				and w6.area_domain_end = 0
				and w6.population_end = 0
				)
		,w8a as	(
				select
						w7a.*,
						w3.id as res_id_group
				from
						w7a inner join w3 on w7a.target_variable = w3.target_variable
				)
		,w9a as	(
				select
						w8a.ldsity_end as res_id,
						$6 as res_label,
						unnest(w8a.res_id_group) as res_id_group
				from
						w8a
				)
		,w10a as	(
				select
						w9a.res_id,
						w9a.res_label,
						array_agg(distinct w9a.res_id_group order by w9a.res_id_group) as res_id_group
				from
						w9a group by w9a.res_id, w9a.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w11 as	(
				select
						w10.res_id,
						w10.res_label,
						w10.res_id_group,
						$2 || array[w10.res_label] as res_ldsity,	-- input value + next user selection
						$3 as res_variant,							-- input value
						$4 as res_area_domain,						-- input value
						$5 as res_population						-- input value
				from
						w10
				-----------------------------------------------------
				union all
				-----------------------------------------------------
				select
						w10a.res_id,
						w10a.res_label,
						w10a.res_id_group,
						$2 as res_ldsity,		-- input value
						$3 as res_variant,		-- input value
						$4 as res_area_domain,	-- input value
						$5 as res_population	-- input value
				from
						w10a
				)
		-----------------------------------------------------
		-----------------------------------------------------
		select
				count(w11.*)
		from
				w11
		'
		using _id_group, _ldsity, _variant, _area_domain, _population, _undivided_text
		into _check_query;
		
		if _check_query = 0
		then
			raise exception 'Error 02: fn_get_user_options_denominator_ldsity: Return query execute must not be zero rows!';
		else
			return query execute
			'
			with
			w1 as	(
					select * from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							target_variable_denom as target_variable,
							array_agg(w1.id order by w1.id) as id
					from
							w1 group by target_variable_denom
					)
			,w3 as	(
					select
							t.target_variable,
							t.id,
							t.label'||_lang_suffix||' AS label
					from
							(
							select
									w2.*,
									((ctv.metadata->''cs'')->''local densities'') as label,
									((ctv.metadata->''en'')->''local densities'') as label_en
							from
									w2
									inner join nfiesta_results.c_target_variable as ctv
									on w2.target_variable = ctv.id
							) as t
					)
			,w4 as	(
					select
							w3.target_variable,
							json_array_elements(w3.label) as label
					from
							w3
					)
			,w5 as	(
					select
							row_number() over () as new_id,
							w4.target_variable,
							w4.label->>''object type label'' as object_type_label,
							w4.label->>''label'' as ldsity,
							replace(
							replace(
							replace(
							replace(
							coalesce((w4.label->''definition variant'')->>''label'',''null''::text)
							,''", "'',''; '')
							,''","'',''; '')
							,''["'','''')
							,''"]'','''')													
							as variant,					
							replace(
							replace(
							replace(
							replace(
							coalesce((w4.label->''area domain'')->>''label'',''null''::text)
							,''", "'',''; '')
							,''","'',''; '')
							,''["'','''')
							,''"]'','''')													
							as area_domain,
							replace(
							replace(
							replace(
							replace(
							coalesce((w4.label->''population'')->>''label'',''null''::text)
							,''", "'',''; '')
							,''","'',''; '')
							,''["'','''')
							,''"]'','''')													
							as population
					from w4
					)
			,w6 as	(
					select
							w5.*,
							t.label_ldsity,
							case when nfiesta_results.fn_array_compare(t.label_ldsity,$2) = true then 0 else 1 end as ldsity_end,
							t.label_variant,
							case when nfiesta_results.fn_array_compare(t.label_variant,$3) = true then 0 else 1 end as variant_end,
							t.label_area_domain,
							case when nfiesta_results.fn_array_compare(t.label_area_domain,$4) = true then 0 else 1 end as area_domain_end,
							t.label_population,
							case when nfiesta_results.fn_array_compare(t.label_population,$5) = true then 0 else 1 end as population_end
					from
							w5
							inner join
								(
								select
										w5.target_variable,
										array_agg(w5.ldsity order by w5.new_id) as label_ldsity,
										array_agg(w5.variant order by w5.new_id) as label_variant,
										array_agg(w5.area_domain order by w5.new_id) as label_area_domain,
										array_agg(w5.population order by w5.new_id) as label_population
								from
										w5 group by w5.target_variable
								) as t
					on w5.target_variable = t.target_variable
					)
					-----------------------------------------------------
					-----------------------------------------------------
			,w7 as	(
					select w6.* from w6
					where w6.ldsity_end > 0
					and '||_cond_ldsity||'
					and '||_cond_variant||'
					and '||_cond_area_domain||'
					and '||_cond_population||'
					)
			,w8 as	(
					select
							w7.*,
							w3.id as res_id_group
					from
							w7 inner join w3 on w7.target_variable = w3.target_variable
					)
			,w9 as	(
					select
							w8.ldsity_end as res_id,
							w8.ldsity as res_label,
							unnest(w8.res_id_group) as res_id_group
					from
							w8
					)
			,w10 as	(
					select
							w9.res_id,
							w9.res_label,
							array_agg(w9.res_id_group order by w9.res_id_group) as res_id_group
					from
							w9 group by w9.res_id, w9.res_label
					)
			-----------------------------------------------------
			-----------------------------------------------------
			,w7a as	(
					select w6.* from w6
					where w6.ldsity_end = 0
					and w6.variant_end = 0
					and w6.area_domain_end = 0
					and w6.population_end = 0
					)
			,w8a as	(
					select
							w7a.*,
							w3.id as res_id_group
					from
							w7a inner join w3 on w7a.target_variable = w3.target_variable
					)
			,w9a as	(
					select
							w8a.ldsity_end as res_id,
							$6 as res_label,
							unnest(w8a.res_id_group) as res_id_group
					from
							w8a
					)
			,w10a as	(
					select
							w9a.res_id,
							w9a.res_label,
							array_agg(distinct w9a.res_id_group order by w9a.res_id_group) as res_id_group
					from
							w9a group by w9a.res_id, w9a.res_label
					)
			-----------------------------------------------------
			-----------------------------------------------------
			,w11 as	(
					select
							w10.res_id,
							w10.res_label,
							w10.res_id_group,
							$2 || array[w10.res_label] as res_ldsity,	-- input value + next user selection
							$3 as res_variant,							-- input value
							$4 as res_area_domain,						-- input value
							$5 as res_population						-- input value
					from
							w10
					-----------------------------------------------------
					union all
					-----------------------------------------------------
					select
							w10a.res_id,
							w10a.res_label,
							w10a.res_id_group,
							$2 as res_ldsity,		-- input value
							$3 as res_variant,		-- input value
							$4 as res_area_domain,	-- input value
							$5 as res_population	-- input value
					from
							w10a
					)
			-----------------------------------------------------
			-----------------------------------------------------
			select
					w11.res_id,
					w11.res_label,
					w11.res_id_group,
					w11.res_ldsity,
					w11.res_variant,
					w11.res_area_domain,
					w11.res_population
			from
					w11
			order
					by w11.res_id, w11.res_label;
			'
			using _id_group, _ldsity, _variant, _area_domain, _population, _undivided_text;
		end if;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator_ldsity(character varying, integer[], text[], text[], text[], text[]) is
'The function returns list of available labels of local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_ldsity(character varying, integer[], text[], text[], text[], text[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_variant" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_variant.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_variant
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_variant(character varying, integer[], text[], text[], text[], text[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_variant
(
	_jlang				character varying,
	_id_group			integer[],
	_ldsity				text[],
	_variant			text[] default null::text[],
	_area_domain		text[] default null::text[],
	_population			text[] default null::text[]
)
returns table
(
	res_id				integer,
	res_label			text,
	res_id_group		integer[],
	res_ldsity			text[],
	res_variant			text[],
	res_area_domain		text[],
	res_population		text[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix		character varying(3);
		_cond_ldsity		text;
		_cond_variant		text;
		_cond_area_domain	text;
		_cond_population	text;
		_undivided_text		text;
		_without_limit_text	text;
begin
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_denominator_variant: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _ldsity is null
		then
			raise exception 'Error 02: fn_get_user_options_denominator_variant: Input argument _ldsity must not be null!';
		end if;
		-----------------------------------------------------------------
		_cond_ldsity := 'w6.label_ldsity @> $2 and w6.ldsity = $2[array_length($2,1)]';
	
		if _variant is null
		then
			_cond_variant := 'w6.label_variant is not null';
		else
			_cond_variant := 'w6.label_variant @> $3';
		end if;
	
		if _area_domain is null
		then
			_cond_area_domain := 'w6.label_area_domain is not null';
		else
			_cond_area_domain := 'w6.label_area_domain @> $4';
		end if;
	
		if _population is null
		then
			_cond_population := 'w6.label_population is not null';
		else
			_cond_population := 'w6.label_population @> $5';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ' then _undivided_text := 'bez dalšího příspěvku lokální hustoty'; end if;
		if _jlang = 'en-GB' then _undivided_text := 'without next contribution of local density'; end if;
		if _jlang = 'cs-CZ' then _without_limit_text := 'bez omezení'; end if;
		if _jlang = 'en-GB' then _without_limit_text := 'without limits'; end if;		
		-----------------------------------------------------------------	
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable_denom as target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.label) as label
				from
						w3
				)
		,w5 as	(
				select
						row_number() over () as new_id,
						w4.target_variable,
						w4.label->>''object type label'' as object_type_label,
						w4.label->>''label'' as ldsity,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''definition variant'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as variant,					
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''area domain'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as area_domain,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''population'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as population
				from w4
				)
		,w6 as	(
				select
						w5.*,
						t.label_ldsity,
						case when nfiesta_results.fn_array_compare(t.label_ldsity,$2) = true then 0 else 1 end as ldsity_end,
						t.label_variant,
						case when nfiesta_results.fn_array_compare(t.label_variant,$3) = true then 0 else 1 end as variant_end,
						t.label_area_domain,
						case when nfiesta_results.fn_array_compare(t.label_area_domain,$4) = true then 0 else 1 end as area_domain_end,
						t.label_population,
						case when nfiesta_results.fn_array_compare(t.label_population,$5) = true then 0 else 1 end as population_end
				from
						w5
						inner join
							(
							select
									w5.target_variable,
									array_agg(w5.ldsity order by w5.new_id) as label_ldsity,
									array_agg(w5.variant order by w5.new_id) as label_variant,
									array_agg(w5.area_domain order by w5.new_id) as label_area_domain,
									array_agg(w5.population order by w5.new_id) as label_population
							from
									w5 group by w5.target_variable
							) as t
				on w5.target_variable = t.target_variable
				)
				-----------------------------------------------------
				-----------------------------------------------------
		,w7 as	(
				select w6.* from w6
				where w6.variant_end > 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8 as	(
				select
						w7.*,
						w3.id as res_id_group
				from
						w7 inner join w3 on w7.target_variable = w3.target_variable
				)
		,w9 as	(
				select
						w8.variant_end as res_id,
						w8.variant as res_label,
						unnest(w8.res_id_group) as res_id_group
				from
						w8
				)
		,w10 as	(
				select
						w9.res_id,
						w9.res_label,
						array_agg(distinct w9.res_id_group order by w9.res_id_group) as res_id_group
				from
						w9 group by w9.res_id, w9.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w7a as	(
				select w6.* from w6
				where w6.variant_end = 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8a as	(
				select
						w7a.*,
						w3.id as res_id_group
				from
						w7a inner join w3 on w7a.target_variable = w3.target_variable
				)
		,w9a as	(
				select
						w8a.variant_end as res_id,
						$6 as res_label,
						unnest(w8a.res_id_group) as res_id_group
				from
						w8a
				)
		,w10a as	(
				select
						w9a.res_id,
						w9a.res_label,
						array_agg(distinct w9a.res_id_group order by w9a.res_id_group) as res_id_group
				from
						w9a group by w9a.res_id, w9a.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w11 as	(
				select
						w10.res_id,
						w10.res_label,
						w10.res_id_group,
						$2 as res_ldsity,							-- input value
						$3 || array[w10.res_label] as res_variant,	-- input value + next user selection
						$4 as res_area_domain,						-- input value
						$5 as res_population						-- input value
				from
						w10
				-----------------------------------------------------
				union all
				-----------------------------------------------------
				select
						w10a.res_id,
						w10a.res_label,
						w10a.res_id_group,
						$2 as res_ldsity,		-- input value
						$3 as res_variant,		-- input value
						$4 as res_area_domain,	-- input value
						$5 as res_population	-- input value
				from
						w10a
				)
		-----------------------------------------------------
		-----------------------------------------------------
		select
				w11.res_id,
				case when w11.res_label = ''null'' then $7 else w11.res_label end as res_label,
				w11.res_id_group,
				w11.res_ldsity,
				w11.res_variant,
				w11.res_area_domain,
				w11.res_population
		from
				w11
		order
				by w11.res_id, w11.res_label;
		'
		using _id_group, _ldsity, _variant, _area_domain, _population, _undivided_text, _without_limit_text;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator_variant(character varying, integer[], text[], text[], text[], text[]) is
'The function returns list of available labels of variants of local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_variant(character varying, integer[], text[], text[], text[], text[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_area_domain" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_area_domain.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_area_domain
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_area_domain(character varying, integer[], text[], text[], text[], text[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_area_domain
(
	_jlang				character varying,
	_id_group			integer[],
	_ldsity				text[],
	_variant			text[],
	_area_domain		text[] default null::text[],
	_population			text[] default null::text[]
)
returns table
(
	res_id				integer,
	res_label			text,
	res_id_group		integer[],
	res_ldsity			text[],
	res_variant			text[],
	res_area_domain		text[],
	res_population		text[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix		character varying(3);
		_cond_ldsity		text;
		_cond_variant		text;
		_cond_area_domain	text;
		_cond_population	text;
		_undivided_text		text;
		_without_limit_text	text;
begin
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_denominator_area_domain: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _ldsity is null
		then
			raise exception 'Error 02: fn_get_user_options_denominator_area_domain: Input argument _ldsity must not be null!';
		end if;
		-----------------------------------------------------------------
		if _variant is null
		then
			raise exception 'Error 03: fn_get_user_options_denominator_area_domain: Input argument _variant must not be null!';
		end if;
		-----------------------------------------------------------------		
		_cond_ldsity := 'w6.label_ldsity @> $2 and w6.ldsity = $2[array_length($2,1)]';
		_cond_variant := 'w6.label_variant @> $3 and w6.variant = $3[array_length($3,1)]';
	
		if _area_domain is null
		then
			_cond_area_domain := 'w6.label_area_domain is not null';
		else
			_cond_area_domain := 'w6.label_area_domain @> $4';
		end if;
	
		if _population is null
		then
			_cond_population := 'w6.label_population is not null';
		else
			_cond_population := 'w6.label_population @> $5';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ' then _undivided_text := 'bez dalšího příspěvku lokální hustoty'; end if;
		if _jlang = 'en-GB' then _undivided_text := 'without next contribution of local density'; end if;
		if _jlang = 'cs-CZ' then _without_limit_text := 'bez omezení'; end if;
		if _jlang = 'en-GB' then _without_limit_text := 'without limits'; end if;		
		-----------------------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable_denom as target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.label) as label
				from
						w3
				)
		,w5 as	(
				select
						row_number() over () as new_id,
						w4.target_variable,
						w4.label->>''object type label'' as object_type_label,
						w4.label->>''label'' as ldsity,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''definition variant'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as variant,					
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''area domain'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as area_domain,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''population'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as population
				from w4
				)
		,w6 as	(
				select
						w5.*,
						t.label_ldsity,
						case when nfiesta_results.fn_array_compare(t.label_ldsity,$2) = true then 0 else 1 end as ldsity_end,
						t.label_variant,
						case when nfiesta_results.fn_array_compare(t.label_variant,$3) = true then 0 else 1 end as variant_end,
						t.label_area_domain,
						case when nfiesta_results.fn_array_compare(t.label_area_domain,$4) = true then 0 else 1 end as area_domain_end,
						t.label_population,
						case when nfiesta_results.fn_array_compare(t.label_population,$5) = true then 0 else 1 end as population_end
				from
						w5
						inner join
							(
							select
									w5.target_variable,
									array_agg(w5.ldsity order by w5.new_id) as label_ldsity,
									array_agg(w5.variant order by w5.new_id) as label_variant,
									array_agg(w5.area_domain order by w5.new_id) as label_area_domain,
									array_agg(w5.population order by w5.new_id) as label_population
							from
									w5 group by w5.target_variable
							) as t
				on w5.target_variable = t.target_variable
				)
				-----------------------------------------------------
				-----------------------------------------------------
		,w7 as	(
				select w6.* from w6
				where w6.area_domain_end > 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8 as	(
				select
						w7.*,
						w3.id as res_id_group
				from
						w7 inner join w3 on w7.target_variable = w3.target_variable
				)
		,w9 as	(
				select
						w8.area_domain_end as res_id,
						w8.area_domain as res_label,
						unnest(w8.res_id_group) as res_id_group
				from
						w8
				)
		,w10 as	(
				select
						w9.res_id,
						w9.res_label,
						array_agg(distinct w9.res_id_group order by w9.res_id_group) as res_id_group
				from
						w9 group by w9.res_id, w9.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w7a as	(
				select w6.* from w6
				where w6.area_domain_end = 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8a as	(
				select
						w7a.*,
						w3.id as res_id_group
				from
						w7a inner join w3 on w7a.target_variable = w3.target_variable
				)
		,w9a as	(
				select
						w8a.area_domain_end as res_id,
						$6 as res_label,
						unnest(w8a.res_id_group) as res_id_group
				from
						w8a
				)
		,w10a as	(
				select
						w9a.res_id,
						w9a.res_label,
						array_agg(distinct w9a.res_id_group order by w9a.res_id_group) as res_id_group
				from
						w9a group by w9a.res_id, w9a.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w11 as	(
				select
						w10.res_id,
						w10.res_label,
						w10.res_id_group,
						$2 as res_ldsity,								-- input value
						$3 as res_variant,								-- input value
						$4 || array[w10.res_label] as res_area_domain,	-- input value + next user selection
						$5 as res_population							-- input value
				from
						w10
				-----------------------------------------------------
				union all
				-----------------------------------------------------
				select
						w10a.res_id,
						w10a.res_label,
						w10a.res_id_group,
						$2 as res_ldsity,		-- input value
						$3 as res_variant,		-- input value
						$4 as res_area_domain,	-- input value
						$5 as res_population	-- input value
				from
						w10a
				)
		-----------------------------------------------------
		-----------------------------------------------------
		select
				w11.res_id,
				case when w11.res_label = ''null'' then $7 else w11.res_label end as res_label,
				w11.res_id_group,
				w11.res_ldsity,
				w11.res_variant,
				w11.res_area_domain,
				w11.res_population
		from
				w11
		order
				by w11.res_id, w11.res_label;
		'
		using _id_group, _ldsity, _variant, _area_domain, _population, _undivided_text, _without_limit_text;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator_area_domain(character varying, integer[], text[], text[], text[], text[]) is
'The function returns list of available labels of area domains of local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_area_domain(character varying, integer[], text[], text[], text[], text[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_population" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_population
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_population(character varying, integer[], text[], text[], text[], text[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_population
(
	_jlang				character varying,
	_id_group			integer[],
	_ldsity				text[],
	_variant			text[],
	_area_domain		text[],
	_population			text[] default null::text[]
)
returns table
(
	res_id				integer,
	res_label			text,
	res_id_group		integer[],
	res_ldsity			text[],
	res_variant			text[],
	res_area_domain		text[],
	res_population		text[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix		character varying(3);
		_cond_ldsity		text;
		_cond_variant		text;
		_cond_area_domain	text;
		_cond_population	text;
		_undivided_text		text;
		_without_limit_text	text;
begin
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_denominator_population: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _ldsity is null
		then
			raise exception 'Error 02: fn_get_user_options_denominator_population: Input argument _ldsity must not be null!';
		end if;
		-----------------------------------------------------------------
		if _variant is null
		then
			raise exception 'Error 03: fn_get_user_options_denominator_population: Input argument _variant must not be null!';
		end if;
		-----------------------------------------------------------------
		if _area_domain is null
		then
			raise exception 'Error 04: fn_get_user_options_denominator_population: Input argument _area_domain must not be null!';
		end if;
		-----------------------------------------------------------------		
		_cond_ldsity := 'w6.label_ldsity @> $2 and w6.ldsity = $2[array_length($2,1)]';
		_cond_variant := 'w6.label_variant @> $3 and w6.variant = $3[array_length($3,1)]';
		_cond_area_domain := 'w6.label_area_domain @> $4 and w6.area_domain = $4[array_length($4,1)]';
	
		if _population is null
		then
			_cond_population := 'w6.label_population is not null';
		else
			_cond_population := 'w6.label_population @> $5';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ' then _undivided_text := 'bez dalšího příspěvku lokální hustoty'; end if;
		if _jlang = 'en-GB' then _undivided_text := 'without next contribution of local density'; end if;
		if _jlang = 'cs-CZ' then _without_limit_text := 'bez omezení'; end if;
		if _jlang = 'en-GB' then _without_limit_text := 'without limits'; end if;	
		-----------------------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable_denom as target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.label) as label
				from
						w3
				)
		,w5 as	(
				select
						row_number() over () as new_id,
						w4.target_variable,
						w4.label->>''object type label'' as object_type_label,
						w4.label->>''label'' as ldsity,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''definition variant'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as variant,					
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''area domain'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as area_domain,
						replace(
						replace(
						replace(
						replace(
						coalesce((w4.label->''population'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as population
				from w4
				)
		,w6 as	(
				select
						w5.*,
						t.label_ldsity,
						case when nfiesta_results.fn_array_compare(t.label_ldsity,$2) = true then 0 else 1 end as ldsity_end,
						t.label_variant,
						case when nfiesta_results.fn_array_compare(t.label_variant,$3) = true then 0 else 1 end as variant_end,
						t.label_area_domain,
						case when nfiesta_results.fn_array_compare(t.label_area_domain,$4) = true then 0 else 1 end as area_domain_end,
						t.label_population,
						case when nfiesta_results.fn_array_compare(t.label_population,$5) = true then 0 else 1 end as population_end
				from
						w5
						inner join
							(
							select
									w5.target_variable,
									array_agg(w5.ldsity order by w5.new_id) as label_ldsity,
									array_agg(w5.variant order by w5.new_id) as label_variant,
									array_agg(w5.area_domain order by w5.new_id) as label_area_domain,
									array_agg(w5.population order by w5.new_id) as label_population
							from
									w5 group by w5.target_variable
							) as t
				on w5.target_variable = t.target_variable
				)
				-----------------------------------------------------
				-----------------------------------------------------
		,w7 as	(
				select w6.* from w6
				where w6.population_end > 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8 as	(
				select
						w7.*,
						w3.id as res_id_group
				from
						w7 inner join w3 on w7.target_variable = w3.target_variable
				)
		,w9 as	(
				select
						w8.population_end as res_id,
						w8.population as res_label,
						unnest(w8.res_id_group) as res_id_group
				from
						w8
				)
		,w10 as	(
				select
						w9.res_id,
						w9.res_label,
						array_agg(distinct w9.res_id_group order by w9.res_id_group) as res_id_group
				from
						w9 group by w9.res_id, w9.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w7a as	(
				select w6.* from w6
				where w6.population_end = 0
				and '||_cond_ldsity||'
				and '||_cond_variant||'
				and '||_cond_area_domain||'
				and '||_cond_population||'
				)
		,w8a as	(
				select
						w7a.*,
						w3.id as res_id_group
				from
						w7a inner join w3 on w7a.target_variable = w3.target_variable
				)
		,w9a as	(
				select
						w8a.population_end as res_id,
						$6 as res_label,
						unnest(w8a.res_id_group) as res_id_group
				from
						w8a
				)
		,w10a as	(
				select
						w9a.res_id,
						w9a.res_label,
						array_agg(distinct w9a.res_id_group order by w9a.res_id_group) as res_id_group
				from
						w9a group by w9a.res_id, w9a.res_label
				)
		-----------------------------------------------------
		-----------------------------------------------------
		,w11 as	(
				select
						w10.res_id,
						w10.res_label,
						w10.res_id_group,
						$2 as res_ldsity,								-- input value
						$3 as res_variant,								-- input value
						$4 as res_area_domain,							-- input value
						$5 || array[w10.res_label] as res_population	-- input value + next user selection
				from
						w10
				-----------------------------------------------------
				union all
				-----------------------------------------------------
				select
						w10a.res_id,
						w10a.res_label,
						w10a.res_id_group,
						$2 as res_ldsity,		-- input value
						$3 as res_variant,		-- input value
						$4 as res_area_domain,	-- input value
						$5 as res_population	-- input value
				from
						w10a
				)
		-----------------------------------------------------
		-----------------------------------------------------
		select
				w11.res_id,
				case when w11.res_label = ''null'' then $7 else w11.res_label end as res_label,
				w11.res_id_group,
				w11.res_ldsity,
				w11.res_variant,
				w11.res_area_domain,
				w11.res_population
		from
				w11
		order
				by w11.res_id, w11.res_label;
		'
		using _id_group, _ldsity, _variant, _area_domain, _population, _undivided_text, _without_limit_text;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator_population(character varying, integer[], text[], text[], text[], text[]) is
'The function returns list of available labels of populations of local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_population(character varying, integer[], text[], text[], text[], text[]) to public;
-- </function>




-- <function name="fn_get_user_options_area_domain_numerator" schema="nfiesta_results" src="functions/fn_get_user_options_area_domain_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_area_domain_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_area_domain_numerator(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_area_domain_numerator
(
	_jlang character varying,
	_id_group integer[],
	_area_domain integer[] default null::integer[]
)
returns table
(
	res_estimate_type integer,
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_area_domain integer[]
)
as
$$
declare
	_lang_suffix			character varying(3);
	_target_variable_num	integer[];
	_estimate_type			integer;
	_check_records			integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_area_domain_numerator: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_area_domain_numerator: The values in input argument _id_group = % must only be for the same target variable!',_id_group;
	end if;	
	-----------------------------------------------------------------
	select distinct estimate_type from nfiesta_results.t_result_group
	where id in (select unnest(_id_group))
	and web = true
	into _estimate_type;
	-----------------------------------------------------------------
	if _area_domain is null
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as csp
						on w3.area_domain = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmspc
				where cmspc.area_domain_category in
					(
					select cspc.id from nfiesta_results.c_area_domain_category as cspc
					where cspc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.area_domain as area_domain,
						cspc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
						inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)	
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
				)
		select
				$2 as res_estimate_type,
				w16.atomic_type as res_id,
				(w16.label'||_lang_suffix||')::text AS res_label,
				w16.id as res_id_group,
				array[w16.atomic_type] as res_area_domain
		from
				w16;
		'
		using _id_group, _estimate_type;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as csp
						on w3.area_domain = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmspc
				where cmspc.area_domain_category in
					(
					select cspc.id from nfiesta_results.c_area_domain_category as cspc
					where cspc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.area_domain as area_domain,
						cspc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
						inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as area_domain,
								t.id
						from
								(
								select w11.area_domain, w11.id, w11.atomic_type from w11 union all
								select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						$3 as res_estimate_type,
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_area_domain
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _area_domain, _estimate_type
		into _check_records;	
		
		if _check_records = 0
		then
			if _area_domain = array[0]
			then
				return query
				select
						_estimate_type as res_estimate_type,
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						_area_domain as _res_area_domain;
			else
				return query
				select
						_estimate_type as res_estimate_type,
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						array_remove(_area_domain,0) as _res_area_domain;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.area_domain] as area_domain,
							t.id
					from
							(
							select w1.area_domain, array_agg(w1.id order by w1.id) as id
							from w1 where w1.area_domain = 0
							group by w1.area_domain
							) as t
					)
			,w3 as	(
					select w1.area_domain, array_agg(w1.id order by w1.id) as id
					from w1 where w1.area_domain > 0
					group by w1.area_domain
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_area_domain as csp
							on w3.area_domain = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_area_domain_category as cmspc
					where cmspc.area_domain_category in
						(
						select cspc.id from nfiesta_results.c_area_domain_category as cspc
						where cspc.area_domain in (select w6.area_domain from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.area_domain as area_domain,
							cspc2.area_domain as atomic_type
					from
							w7
							inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
							inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.area_domain from w8
					)
			,w10 as (
					select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.area_domain
					)
			,w11 as (
					select
							w6.area_domain,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.area_domain = w10.area_domain
					)
			,w12 as	(
					select
							w11.area_domain,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.area_domain,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.area_domain,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
							from	(
									select distinct a1.atomic_type, a1.area_domain
									from
										(
										select w13.atomic_type, w13.area_domain from w13
										union all
										select w5.area_domain as atomic_type, w5.area_domain from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as area_domain,
									null::integer[] as area_domain,
									t.id
							from
									(
									select w11.area_domain, w11.id, w11.atomic_type from w11 union all
									select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
					)
					select
							$3 as res_estimate_type,
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_area_domain
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _area_domain, _estimate_type;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_area_domain_numerator(character varying, integer[], integer[]) is
'The function returns list of available area domains for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of area domains.';

grant execute on function nfiesta_results.fn_get_user_options_area_domain_numerator(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_area_domain_denominator" schema="nfiesta_results" src="functions/fn_get_user_options_area_domain_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_area_domain_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_area_domain_denominator(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_area_domain_denominator
(
	_jlang character varying,
	_id_group integer[],
	_area_domain integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_area_domain integer[]
)
as
$$
declare
	_lang_suffix			character varying(3);
	_target_variable_denom	integer[];
	_estimate_type			integer;
	_check_records			integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_area_domain_denominator: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom;

	if array_length(_target_variable_denom,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_area_domain_denominator: The values in input argument _id_group = % must only be for the same target variable denominator!',_id_group;
	end if;		
	-----------------------------------------------------------------
	select distinct estimate_type from nfiesta_results.t_result_group
	where id in (select unnest(_id_group))
	and web = true
	into _estimate_type;
	-----------------------------------------------------------------
	if _area_domain is null
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.area_domain_denom is null then 0 else trg.area_domain_denom end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as csp
						on w3.area_domain = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmspc
				where cmspc.area_domain_category in
					(
					select cspc.id from nfiesta_results.c_area_domain_category as cspc
					where cspc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.area_domain as area_domain,
						cspc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
						inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)	
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
				)
		select
				w16.atomic_type as res_id,
				(w16.label'||_lang_suffix||')::text AS res_label,
				w16.id as res_id_group,
				array[w16.atomic_type] as res_area_domain
		from
				w16;
		'
		using _id_group;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.area_domain_denom is null then 0 else trg.area_domain_denom end as area_domain
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.area_domain] as area_domain,
						t.id
				from
						(
						select w1.area_domain, array_agg(w1.id order by w1.id) as id
						from w1 where w1.area_domain = 0
						group by w1.area_domain
						) as t
				)
		,w3 as	(
				select w1.area_domain, array_agg(w1.id order by w1.id) as id
				from w1 where w1.area_domain > 0
				group by w1.area_domain
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_area_domain as csp
						on w3.area_domain = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_area_domain_category as cmspc
				where cmspc.area_domain_category in
					(
					select cspc.id from nfiesta_results.c_area_domain_category as cspc
					where cspc.area_domain in (select w6.area_domain from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.area_domain as area_domain,
						cspc2.area_domain as atomic_type
				from
						w7
						inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
						inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.area_domain from w8
				)
		,w10 as (
				select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.area_domain
				)
		,w11 as (
				select
						w6.area_domain,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.area_domain = w10.area_domain
				)
		,w12 as	(
				select
						w11.area_domain,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.area_domain,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.area_domain,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
						from	(
								select distinct a1.atomic_type, a1.area_domain
								from
									(
									select w13.atomic_type, w13.area_domain from w13
									union all
									select w5.area_domain as atomic_type, w5.area_domain from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as area_domain,
								t.id
						from
								(
								select w11.area_domain, w11.id, w11.atomic_type from w11 union all
								select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_area_domain
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _area_domain
		into _check_records;	
		
		if _check_records = 0
		then
			if _area_domain = array[0]
			then
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						_area_domain as _res_area_domain;
			else
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						array_remove(_area_domain,0) as _res_area_domain;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.area_domain] as area_domain,
							t.id
					from
							(
							select w1.area_domain, array_agg(w1.id order by w1.id) as id
							from w1 where w1.area_domain = 0
							group by w1.area_domain
							) as t
					)
			,w3 as	(
					select w1.area_domain, array_agg(w1.id order by w1.id) as id
					from w1 where w1.area_domain > 0
					group by w1.area_domain
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_area_domain as csp
							on w3.area_domain = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.area_domain, array[w5.area_domain] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_area_domain_category as cmspc
					where cmspc.area_domain_category in
						(
						select cspc.id from nfiesta_results.c_area_domain_category as cspc
						where cspc.area_domain in (select w6.area_domain from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.area_domain as area_domain,
							cspc2.area_domain as atomic_type
					from
							w7
							inner join nfiesta_results.c_area_domain_category as cspc1 on w7.area_domain_category = cspc1.id
							inner join nfiesta_results.c_area_domain_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.area_domain from w8
					)
			,w10 as (
					select w9.area_domain, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.area_domain
					)
			,w11 as (
					select
							w6.area_domain,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.area_domain = w10.area_domain
					)
			,w12 as	(
					select
							w11.area_domain,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.area_domain,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.area_domain,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.area_domain order by a2.area_domain) as area_domain
							from	(
									select distinct a1.atomic_type, a1.area_domain
									from
										(
										select w13.atomic_type, w13.area_domain from w13
										union all
										select w5.area_domain as atomic_type, w5.area_domain from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.area_domain as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as area_domain,
									null::integer[] as area_domain,
									t.id
							from
									(
									select w11.area_domain, w11.id, w11.atomic_type from w11 union all
									select w5a.area_domain, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_area_domain as csp on w15.atomic_type = csp.id
					)
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_area_domain
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _area_domain;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_area_domain_denominator(character varying, integer[], integer[]) is
'The function returns list of available area domains for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of area domains.';

grant execute on function nfiesta_results.fn_get_user_options_area_domain_denominator(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_sub_population_numerator" schema="nfiesta_results" src="functions/fn_get_user_options_sub_population_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_sub_population_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_sub_population_numerator(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_sub_population_numerator
(
	_jlang character varying,
	_id_group integer[],
	_sub_population integer[] default null::integer[]
)
returns table
(
	res_estimate_type integer,
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_sub_population integer[]
)
as
$$
declare
	_lang_suffix			character varying(3);
	_target_variable_num	integer[];
	_estimate_type			integer;
	_check_records			integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_sub_population_numerator: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_sub_population_numerator: The values in input argument _id_group = % must only be for the same target variable!',_id_group;
	end if;	
	-----------------------------------------------------------------
	select distinct estimate_type from nfiesta_results.t_result_group
	where id in (select unnest(_id_group))
	and web = true
	into _estimate_type;
	-----------------------------------------------------------------
	if _sub_population is null
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)	
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		select
				$2 as res_estimate_type,
				w16.atomic_type as res_id,
				(w16.label'||_lang_suffix||')::text AS res_label,
				w16.id as res_id_group,
				array[w16.atomic_type] as res_sub_population
		from
				w16;
		'
		using _id_group, _estimate_type;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as sub_population,
								t.id
						from
								(
								select w11.sub_population, w11.id, w11.atomic_type from w11 union all
								select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						$3 as res_estimate_type,
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_sub_population
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _sub_population, _estimate_type
		into _check_records;	
		
		if _check_records = 0
		then
			if _estimate_type = 1
			then
				if	array_length(_id_group,1) is distinct from 1
				then
					raise exception 'Error 02: fn_get_user_options_sub_population_numerator: Output argument _res_id_group contains more elements then one!';
				end if;
			end if;
		
			if _sub_population = array[0]
			then
				return query
				select
						_estimate_type as res_estimate_type,
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						_sub_population as _res_sub_population;
			else
				return query
				select
						_estimate_type as res_estimate_type,
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						array_remove(_sub_population,0) as _res_sub_population;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.sub_population] as sub_population,
							t.id
					from
							(
							select w1.sub_population, array_agg(w1.id order by w1.id) as id
							from w1 where w1.sub_population = 0
							group by w1.sub_population
							) as t
					)
			,w3 as	(
					select w1.sub_population, array_agg(w1.id order by w1.id) as id
					from w1 where w1.sub_population > 0
					group by w1.sub_population
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_sub_population as csp
							on w3.sub_population = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_sub_population_category as cmspc
					where cmspc.sub_population_category in
						(
						select cspc.id from nfiesta_results.c_sub_population_category as cspc
						where cspc.sub_population in (select w6.sub_population from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.sub_population as sub_population,
							cspc2.sub_population as atomic_type
					from
							w7
							inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
							inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.sub_population from w8
					)
			,w10 as (
					select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.sub_population
					)
			,w11 as (
					select
							w6.sub_population,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.sub_population = w10.sub_population
					)
			,w12 as	(
					select
							w11.sub_population,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.sub_population,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.sub_population,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
							from	(
									select distinct a1.atomic_type, a1.sub_population
									from
										(
										select w13.atomic_type, w13.sub_population from w13
										union all
										select w5.sub_population as atomic_type, w5.sub_population from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as sub_population,
									null::integer[] as sub_population,
									t.id
							from
									(
									select w11.sub_population, w11.id, w11.atomic_type from w11 union all
									select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
					)
					select
							$3 as res_estimate_type,
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_sub_population
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _sub_population, _estimate_type;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_sub_population_numerator(character varying, integer[], integer[]) is
'The function returns list of available sub populations for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of sub populations.';

grant execute on function nfiesta_results.fn_get_user_options_sub_population_numerator(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_sub_population_denominator" schema="nfiesta_results" src="functions/fn_get_user_options_sub_population_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_sub_population_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_sub_population_denominator(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_sub_population_denominator
(
	_jlang character varying,
	_id_group integer[],
	_sub_population integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_sub_population integer[]
)
as
$$
declare
	_lang_suffix			character varying(3);
	_target_variable_denom	integer[];
	_estimate_type			integer;
	_check_records			integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_sub_population_denominator: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom;

	if array_length(_target_variable_denom,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_sub_population_denominator: The values in input argument _id_group = % must only be for the same target variable denominator!',_id_group;
	end if;	
	-----------------------------------------------------------------
	select distinct estimate_type from nfiesta_results.t_result_group
	where id in (select unnest(_id_group))
	and web = true
	into _estimate_type;
	-----------------------------------------------------------------
	if _sub_population is null
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)	
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		select
				w16.atomic_type as res_id,
				(w16.label'||_lang_suffix||')::text AS res_label,
				w16.id as res_id_group,
				array[w16.atomic_type] as res_sub_population
		from
				w16;
		'
		using _id_group;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as sub_population,
								t.id
						from
								(
								select w11.sub_population, w11.id, w11.atomic_type from w11 union all
								select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_sub_population
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _sub_population
		into _check_records;	
		
		if _check_records = 0 -- uz neexistuje dalsi
		then
			if	array_length(_id_group,1) is distinct from 1
			then
				raise exception 'Error 02: fn_get_user_options_sub_population_denominator: Output argument _res_id_group contains more elements then one!';
			end if;
		
			if _sub_population = array[0] -- na vstupu je hned bezrozliseni
			then
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						_sub_population as _res_sub_population;
			else
				-- vyberem doslo na konec
				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as _res_id_group,
						array_remove(_sub_population,0) as _res_sub_population;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.sub_population] as sub_population,
							t.id
					from
							(
							select w1.sub_population, array_agg(w1.id order by w1.id) as id
							from w1 where w1.sub_population = 0
							group by w1.sub_population
							) as t
					)
			,w3 as	(
					select w1.sub_population, array_agg(w1.id order by w1.id) as id
					from w1 where w1.sub_population > 0
					group by w1.sub_population
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_sub_population as csp
							on w3.sub_population = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_sub_population_category as cmspc
					where cmspc.sub_population_category in
						(
						select cspc.id from nfiesta_results.c_sub_population_category as cspc
						where cspc.sub_population in (select w6.sub_population from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.sub_population as sub_population,
							cspc2.sub_population as atomic_type
					from
							w7
							inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
							inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.sub_population from w8
					)
			,w10 as (
					select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.sub_population
					)
			,w11 as (
					select
							w6.sub_population,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.sub_population = w10.sub_population
					)
			,w12 as	(
					select
							w11.sub_population,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.sub_population,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.sub_population,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
							from	(
									select distinct a1.atomic_type, a1.sub_population
									from
										(
										select w13.atomic_type, w13.sub_population from w13
										union all
										select w5.sub_population as atomic_type, w5.sub_population from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as sub_population,
									null::integer[] as sub_population,
									t.id
							from
									(
									select w11.sub_population, w11.id, w11.atomic_type from w11 union all
									select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
					)
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_sub_population
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _sub_population;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_sub_population_denominator(character varying, integer[], integer[]) is
'The function returns list of available sub populations for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of sub populations.';

grant execute on function nfiesta_results.fn_get_user_options_sub_population_denominator(character varying, integer[], integer[]) to public;
-- </function>




drop function nfiesta_results.fn_get_user_options_metadata(character varying, integer[]);
-- <function name="fn_get_user_options_metadata" schema="nfiesta_results" src="functions/fn_get_user_options_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	res_metadata_type_num		text,
	res_metadata_label_num		text,
	res_metadata_type_denom		text,
	res_metadata_label_denom	text
)
language plpgsql
stable
as
$$
declare
	_lang_suffix					character varying(3);
	_lang							text;
	_target_variable_num			integer[];
	_target_variable_denom_check	integer;
	_target_variable_denom			integer;

	_cond4topic_num					text;
	_cond4topic_denom				text;

	_res_topic						text;
	_res_estimation_period			text;
	_res_estimation_cell_collection	text;
	_res_indicator_num				text;
	_res_indicator_denom			text;
	_res_state_or_change_num		text;
	_res_state_or_change_denom		text;
	_res_unit_num					text;
	_res_unit_denom					text;
	_res_unit_of_measure			text;

	_check_ldsity_contributions_num		integer;
	_check_ldsity_contributions_denom	integer;
	_query_ldsity_attributes_num_1		text;
	_query_ldsity_attributes_num_2		text;
	_query_ldsity_attributes_num		text;
	_query_ldsity_attributes_denom_1	text;
	_query_ldsity_attributes_denom_2	text;
	_query_ldsity_attributes_denom		text;
	_query_ldsity_attributes			text;

	_res								text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	select count(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom_check;

	if _target_variable_denom_check is distinct from 0 -- target variable denom exists
	then
		if _target_variable_denom_check > 1
		then
			raise exception 'Error: 03: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
		end if;

		select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
		where trg.id in (select unnest(_id_group))
		and trg.web = true
		into _target_variable_denom;		
	else
		_target_variable_denom := null::integer;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	execute
	'
	with
	w1 as	(
			select crt.topic from nfiesta_results.cm_result2topic as crt
			where ' || _cond4topic_num ||'
			and ' || _cond4topic_denom ||'
			)
	,w2 as	(
			select
					label'||_lang_suffix||' AS label
			from
					nfiesta_results.c_topic
			where
					id in (select distinct w1.topic from w1)
			)
	,w3 as	(
			select array_agg(w2.label order by w2.label) as label
			from w2
			)
			select array_to_string(w3.label, ''; '') from w3
	'
	using _target_variable_num[1], _target_variable_denom
	into _res_topic;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	execute
	'
	select
			cep.label'||_lang_suffix||' AS label
	from
			nfiesta_results.c_estimation_period as cep
	where
			cep.id =	(
						select distinct trg.estimation_period
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_period;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	execute
	'
	select
			cecc.label'||_lang_suffix||' AS label
	from
			nfiesta_results.c_estimation_cell_collection as cecc
	where
			cecc.id =	(
						select distinct trg.estimation_cell_collection
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_cell_collection;
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
					((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label
	from
			w1
	'
	using _target_variable_num[1]
	into _res_indicator_num;
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - denominator
	if _target_variable_denom is null
	then
		_res_indicator_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
						((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.label'||_lang_suffix||' AS label
		from
				w1
		'
		using _target_variable_denom
		into _res_indicator_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state or change'')->>''label'' as label,
					((ctv.metadata->''en'')->''state or change'')->>''label'' as label_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label
	from
			w1
	'
	using _target_variable_num[1]
	into _res_state_or_change_num;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - denominator
	if _target_variable_denom is null
	then
		_res_state_or_change_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''state or change'')->>''label'' as label,
						((ctv.metadata->''en'')->''state or change'')->>''label'' as label_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.label'||_lang_suffix||' AS label
		from
				w1
		'
		using _target_variable_denom
		into _res_state_or_change_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label
	from
			w1
	'
	using _target_variable_num[1]
	into _res_unit_num;
	-----------------------------------------------------------------
	-- METADATA OF UNIT - denominator
	if _target_variable_denom is null
	then
		_res_unit_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
						((ctv.metadata->''en'')->''unit'')->>''label'' as label_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.label'||_lang_suffix||' AS label
		from
				w1
		'
		using _target_variable_denom
		into _res_unit_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA LDSITY - numerator
	-- check => how many ldsity contributions?
	execute
	'
	with			
	w1 as	(
			select
					t.id as target_variable,
					t.label'||_lang_suffix||' AS label
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as label,
							((ctv.metadata->''en'')->''local densities'') as label_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = $1
					) as t
			)									
	,w2 as	(
			select
					w1.target_variable,
					json_array_elements(w1.label) as label
			from
					w1
			)
	select count(w2.*) from w2
	'
	using _target_variable_num[1]
	into _check_ldsity_contributions_num;

	--raise notice '_check_ldsity_contributions_num: %',_check_ldsity_contributions_num;
			
	if _check_ldsity_contributions_num = 0
	then
		raise exception 'Error: 04: fn_get_user_options_metadata: For target variable [ID = %] numerator not exists any ldsity contribution!',_target_variable_num[1];
	end if;

	_query_ldsity_attributes_num_1 := concat(
	'
	with			
	w1a as	(
			select
					t.id as target_variable,
					t.label',_lang_suffix,' AS label
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as label,
							((ctv.metadata->''en'')->''local densities'') as label_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = ',_target_variable_num[1],'
					) as t
			)									
	,w2a as	(
			select
					w1a.target_variable,
					json_array_elements(w1a.label) as label
			from
					w1a
			)
	,w3a as	(
			select
					row_number() over () as new_id,
					w2a.target_variable,
					w2a.label->>''object type label'' as object_type_label,
					w2a.label->>''label'' as ldsity,
					replace(
					replace(
					replace(
					replace(
					coalesce((w2a.label->''definition variant'')->>''label'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as variant,					
					replace(
					replace(
					replace(
					replace(
					coalesce((w2a.label->''area domain'')->>''label'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as area_domain,
					replace(
					replace(
					replace(
					replace(
					coalesce((w2a.label->''population'')->>''label'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as population
			from
					w2a
			)
	');

	for i in 1.._check_ldsity_contributions_num
	loop
		if _lang = 'cs'
		then
			if i = 1
			then
				_query_ldsity_attributes_num_2 :=
				concat(
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Varianta:'' as res_metadata_type,
						w3a.variant as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Plošná doména:'' as res_metadata_type,
						w3a.area_domain as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Populace:'' as res_metadata_type,
						w3a.population as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			else
				_query_ldsity_attributes_num_2 := 
				concat(_query_ldsity_attributes_num_2,' union all ',
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Varianta:'' as res_metadata_type,
						w3a.variant as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Plošná doména:'' as res_metadata_type,
						w3a.area_domain as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Populace:'' as res_metadata_type,
						w3a.population as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			end if;
		else
			if i = 1
			then
				_query_ldsity_attributes_num_2 :=
				concat(
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Variant:'' as res_metadata_type,
						w3a.variant as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Area domain:'' as res_metadata_type,
						w3a.area_domain as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Population:'' as res_metadata_type,
						w3a.population as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			else
				_query_ldsity_attributes_num_2 := 
				concat(_query_ldsity_attributes_num_2,' union all ',
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Variant:'' as res_metadata_type,
						w3a.variant as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Area domain:'' as res_metadata_type,
						w3a.area_domain as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Population:'' as res_metadata_type,
						w3a.population as res_metadata_label
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			end if;
		end if;
	end loop;

	_query_ldsity_attributes_num := concat(_query_ldsity_attributes_num_1,' ',_query_ldsity_attributes_num_2);
	-----------------------------------------------------------------
	-- METADATA LDSITY - denominator
	if _target_variable_denom is null
	then
		if _lang = 'cs'
		then
			_query_ldsity_attributes_denom :=
			'
			select
					0 as id_ldsity,
					0 as id_attribute,
					null::text as res_metadata_type,
					null::text as res_metadata_label
			';
		else
		end if;
	else
		-- check => how many ldsity contributions?
		execute
		'
		with			
		w1 as	(
				select
						t.id as target_variable,
						t.label'||_lang_suffix||' AS label
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = $1
						) as t
				)									
		,w2 as	(
				select
						w1.target_variable,
						json_array_elements(w1.label) as label
				from
						w1
				)
		select count(w2.*) from w2
		'
		using _target_variable_denom
		into _check_ldsity_contributions_denom;
				
		if _check_ldsity_contributions_denom = 0
		then
			raise exception 'Error: 05: fn_get_user_options_metadata: For target variable [ID = %] denominator not exists any ldsity contribution!',_target_variable_denom;
		end if;

		_query_ldsity_attributes_denom_1 := concat(
		'
		with			
		w1b as	(
				select
						t.id as target_variable,
						t.label',_lang_suffix,' AS label
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as label,
								((ctv.metadata->''en'')->''local densities'') as label_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = ',_target_variable_denom,'
						) as t
				)									
		,w2b as	(
				select
						w1b.target_variable,
						json_array_elements(w1b.label) as label
				from
						w1b
				)
		,w3b as	(
				select
						row_number() over () as new_id,
						w2b.target_variable,
						w2b.label->>''object type label'' as object_type_label,
						w2b.label->>''label'' as ldsity,
						replace(
						replace(
						replace(
						replace(
						coalesce((w2b.label->''definition variant'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as variant,					
						replace(
						replace(
						replace(
						replace(
						coalesce((w2b.label->''area domain'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as area_domain,
						replace(
						replace(
						replace(
						replace(
						coalesce((w2b.label->''population'')->>''label'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as population
				from
						w2b
				)
		');

		for i in 1.._check_ldsity_contributions_denom
		loop
			if _lang = 'cs'
			then
				if i = 1
				then
					_query_ldsity_attributes_denom_2 :=
					concat(
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Varianta:'' as res_metadata_type,
							w3b.variant as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Plošná doména:'' as res_metadata_type,
							w3b.area_domain as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Populace:'' as res_metadata_type,
							w3b.population as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				else
					_query_ldsity_attributes_denom_2 := 
					concat(_query_ldsity_attributes_denom_2,' union all ',
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Varianta:'' as res_metadata_type,
							w3b.variant as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Plošná doména:'' as res_metadata_type,
							w3b.area_domain as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Populace:'' as res_metadata_type,
							w3b.population as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				end if;
			else
				if i = 1
				then
					_query_ldsity_attributes_denom_2 :=
					concat(
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Variant:'' as res_metadata_type,
							w3b.variant as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Area domain:'' as res_metadata_type,
							w3b.area_domain as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Population:'' as res_metadata_type,
							w3b.population as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				else
					_query_ldsity_attributes_denom_2 := 
					concat(_query_ldsity_attributes_denom_2,' union all ',
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Variant:'' as res_metadata_type,
							w3b.variant as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Area domain:'' as res_metadata_type,
							w3b.area_domain as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Population:'' as res_metadata_type,
							w3b.population as res_metadata_label
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				end if;
			end if;
		end loop;

		_query_ldsity_attributes_denom := concat(_query_ldsity_attributes_denom_1,' ',_query_ldsity_attributes_denom_2);

	end if;
	-----------------------------------------------------------------
	if _target_variable_denom is null
	then
		_query_ldsity_attributes := concat(
		',w4a as	(',_query_ldsity_attributes_num,')
		,w4ab as	(
					select
							w4a.res_metadata_type as res_metadata_type_num,
							w4a.res_metadata_label as res_metadata_label_num,
							null::text as res_metadata_type_denom,
							null::text as res_metadata_label_denom
					from
							w4a
					)
		');
	else
		if _check_ldsity_contributions_num >= _check_ldsity_contributions_denom
		then
			_query_ldsity_attributes := concat(
			',w4a as	(',_query_ldsity_attributes_num,')
			,w4b as	(',_query_ldsity_attributes_denom,')
			,w4ab as	(
						select
								w4a.res_metadata_type as res_metadata_type_num,
								w4a.res_metadata_label as res_metadata_label_num,
								w4b.res_metadata_type as res_metadata_type_denom,
								w4b.res_metadata_label as res_metadata_label_denom
						from
								w4a left join w4b
						on
								w4a.id_ldsity = w4b.id_ldsity and
								w4a.id_attribute = w4b.id_attribute
						)
			');
		else
			_query_ldsity_attributes := concat(
			',w4a as	(',_query_ldsity_attributes_num,')
			,w4b as	(',_query_ldsity_attributes_denom,')
			,w4ab as	(
						select
								w4a.res_metadata_type as res_metadata_type_num,
								w4a.res_metadata_label as res_metadata_label_num,
								w4b.res_metadata_type as res_metadata_type_denom,
								w4b.res_metadata_label as res_metadata_label_denom
						from
								w4a right join w4b
						on
								w4a.id_ldsity = w4b.id_ldsity and
								w4a.id_attribute = w4b.id_attribute
						)
			');
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASURE
	if _target_variable_denom is null
	then
		_res_unit_of_measure := _res_unit_num;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''label'' as unit_num,
						((w2.metadata->''cs'')->''unit'')->>''label'' as unit_denom,
						((w1.metadata->''en'')->''unit'')->>''label'' as unit_num_en,
						((w2.metadata->''en'')->''unit'')->>''label'' as unit_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_num'||_lang_suffix||' AS unit_num,
						w3.unit_denom'||_lang_suffix||' AS unit_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_num = w4.unit_denom) then ''%''
							else concat(w4.unit_num,'' / '',w4.unit_denom)
						end as label4user
				from
						w4
				)
		select w5.label4user from w5
		'
		using _target_variable_num[1], _target_variable_denom
		into _res_unit_of_measure;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _lang_suffix = ''
	then
		_res := concat(
		'
		with
		w01 as	(
				select
						''Tematický okruh:'' as res_metadata_type_num,
						''',_res_topic,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						null::text as res_metadata_label_denom
				union all
				select
						''Období:'' as res_metadata_type_num,
						''',_res_estimation_period,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						null::text as res_metadata_label_denom
				union all
				select
						''Geografické členění:'' as res_metadata_type_num,
						''',_res_estimation_cell_collection,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						null::text as res_metadata_label_denom
				union all
				select
						''Indikátor:'' as res_metadata_type_num,
						''',_res_indicator_num,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						case when ''',_res_indicator_denom,''' = '''' then null::text else ''',_res_indicator_denom,''' end as res_metadata_label_denom
				union all
				select
						''Stav nebo změna:'' as res_metadata_type_num,
						''',_res_state_or_change_num,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						case when ''',_res_state_or_change_denom,''' = '''' then null::text else ''',_res_state_or_change_denom,''' end as res_metadata_label_denom
				union all
				select
						''Jednotka indikátoru:'' as res_metadata_type_num,
						''',_res_unit_num,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						case when ''',_res_unit_denom,'''  = '''' then null::text else ''',_res_unit_denom,''' end as res_metadata_label_denom
				union all
				select
						null::text as res_metadata_type_num,
						null::text as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						null::text as res_metadata_label_denom
				union all
				select
						''Příspěvky:'' as res_metadata_type_num,
						null::text as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						null::text as res_metadata_label_denom
				)
		',_query_ldsity_attributes,
		',w02 as	(
					select
							null::text as res_metadata_type_num,
							null::text as res_metadata_label_num,
							null::text as res_metadata_type_denom,
							null::text as res_metadata_label_denom
					union all
					select
							''Jednotka výstupu:'' as res_metadata_type_num,
							''',_res_unit_of_measure,''' as res_metadata_label_num,
							null::text as res_metadata_type_denom,
							null::text as res_metadata_label_denom
					)
		select
				w01.res_metadata_type_num,
				w01.res_metadata_label_num,
				w01.res_metadata_type_denom,
				w01.res_metadata_label_denom
		from
				w01
		union all
		select
				w4ab.res_metadata_type_num,
				case when w4ab.res_metadata_label_num = ''null'' then null::text else w4ab.res_metadata_label_num end as res_metadata_label_num,
				w4ab.res_metadata_type_denom,
				case when w4ab.res_metadata_label_denom = ''null'' then null::text else w4ab.res_metadata_label_denom end as res_metadata_label_denom
		from
				w4ab
		union all
		select
				w02.res_metadata_type_num,
				w02.res_metadata_label_num,
				w02.res_metadata_type_denom,
				w02.res_metadata_label_denom
		from
				w02;
		');
	else
		_res := concat(
		'
		with
		w01 as	(
				select
						''Topic:'' as res_metadata_type_num,
						''',_res_topic,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						null::text as res_metadata_label_denom
				union all
				select
						''Period:'' as res_metadata_type_num,
						''',_res_estimation_period,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						null::text as res_metadata_label_denom
				union all
				select
						''Geographic region:'' as res_metadata_type_num,
						''',_res_estimation_cell_collection,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						null::text as res_metadata_label_denom
				union all
				select
						''Indicator:'' as res_metadata_type_num,
						''',_res_indicator_num,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						case when ''',_res_indicator_denom,''' = '''' then null::text else ''',_res_indicator_denom,''' end as res_metadata_label_denom
				union all
				select
						''State or change:'' as res_metadata_type_num,
						''',_res_state_or_change_num,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						case when ''',_res_state_or_change_denom,''' = '''' then null::text else ''',_res_state_or_change_denom,''' end as res_metadata_label_denom
				union all
				select
						''Unit of indicator:'' as res_metadata_type_num,
						''',_res_unit_num,''' as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						case when ''',_res_unit_denom,'''  = '''' then null::text else ''',_res_unit_denom,''' end as res_metadata_label_denom
				union all
				select
						null::text as res_metadata_type_num,
						null::text as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						null::text as res_metadata_label_denom
				union all
				select
						''Contributions:'' as res_metadata_type_num,
						null::text as res_metadata_label_num,
						null::text as res_metadata_type_denom,
						null::text as res_metadata_label_denom
				)
		',_query_ldsity_attributes,
		',w02 as	(
					select
							null::text as res_metadata_type_num,
							null::text as res_metadata_label_num,
							null::text as res_metadata_type_denom,
							null::text as res_metadata_label_denom
					union all
					select
							''Unit of measurement:'' as res_metadata_type_num,
							''',_res_unit_of_measure,''' as res_metadata_label_num,
							null::text as res_metadata_type_denom,
							null::text as res_metadata_label_denom
					)
		select
				w01.res_metadata_type_num,
				w01.res_metadata_label_num,
				w01.res_metadata_type_denom,
				w01.res_metadata_label_denom
		from
				w01
		union all
		select
				w4ab.res_metadata_type_num,
				case when w4ab.res_metadata_label_num = ''null'' then null::text else w4ab.res_metadata_label_num end as res_metadata_label_num,
				w4ab.res_metadata_type_denom,
				case when w4ab.res_metadata_label_denom = ''null'' then null::text else w4ab.res_metadata_label_denom end as res_metadata_label_denom
		from
				w4ab
		union all
		select
				w02.res_metadata_type_num,
				w02.res_metadata_label_num,
				w02.res_metadata_type_denom,
				w02.res_metadata_label_denom
		from
				w02;
		');
	end if;

	return query execute ''||_res||'';
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_metadata(character varying, integer[]) is
'The function returns options metadata for given input IDs (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_options_metadata(character varying, integer[]) to public;
-- </function>



drop function nfiesta_results.fn_get_user_query(character varying, integer[]);
-- <function name="fn_get_user_query" schema="nfiesta_results" src="functions/fn_get_user_query.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_query
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_query(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_query
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	estimation_cell					text,
	variable_area_domain_num		text,
	variable_sub_population_num		text,
	variable_area_domain_denom		text,
	variable_sub_population_denom	text,	
	point_estimate					numeric,
	standard_deviation				numeric,
	variation_coeficient			numeric,
	--sample_size					integer,
	--min_sample_size				integer,
	interval_estimation				numeric
)
language plpgsql
stable
as
$$
declare
	_lang_suffix					character varying(3);
	_estimate_type					integer;
	_target_variable				integer;
	_target_variable_denom			integer;
	_estimation_period				integer;
	_panel_refyearset_group			integer;
	_phase_estimate_type			integer;
	_phase_estimate_type_denom		integer;
	_estimation_cell_collection		integer;
	_area_domain					integer;
	_area_domain_denom				integer;
	_sub_population					integer;
	_sub_population_denom			integer;
	_cond_area_domain				text;
	_cond_sub_population			text;
	_cond_area_domain_denom			text;
	_cond_sub_population_denom		text;
	_check4unit						integer;
	_coeficient4ratio				numeric;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_query: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_query: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select
			trg.estimate_type,
			trg.target_variable,
			trg.target_variable_denom,
			trg.estimation_period,
			trg.panel_refyearset_group,
			trg.phase_estimate_type,
			trg.phase_estimate_type_denom,
			trg.estimation_cell_collection,
			trg.area_domain,
			trg.area_domain_denom,
			trg.sub_population,
			trg.sub_population_denom
	from
			nfiesta_results.t_result_group as trg
	where
			trg.id = _id_group[1]
	into
			_estimate_type,
			_target_variable,
			_target_variable_denom,
			_estimation_period,
			_panel_refyearset_group,
			_phase_estimate_type,
			_phase_estimate_type_denom,
			_estimation_cell_collection,
			_area_domain,
			_area_domain_denom,
			_sub_population,
			_sub_population_denom;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _estimate_type = 1 -- TOTAL
	then
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$2';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$3';
		end if;
		---------------------------------------------------	
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					null::integer,
					null::integer,
					null::integer
					)
				)
		,w2 as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $4
				and ttec.panel_refyearset_group = $5
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $6)
				and ttec.phase_estimate_type = $7
				)
		,w3 as	(
				select
						w2.id as id_t_total_estimate_conf,
						w2.estimation_cell,
						w2.variable,
						w2.phase_estimate_type,
						w2.force_synthetic,
						w2.estimation_period,
						w2.panel_refyearset_group,
						w1.nominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,
						tv.area_domain_category,
						tv.sub_population_category
				from
						w2
						inner join w1 on w2.variable = w1.nominator_variable
						inner join nfiesta_results.t_variable as tv on w2.variable = tv.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf
				from
						nfiesta_results.t_estimate_conf as tec 
				where
						tec.total_estimate_conf in (select w3.id_t_total_estimate_conf from w3)
				and
						tec.estimate_type = $8
				)
		,w5 as	(
				select
						w4.*,
						w3.*
				from
						w4 inner join w3 on w4.total_estimate_conf = w3.id_t_total_estimate_conf
				)
		,w6 as	(
				select
						tr.*,
						w5.*
				from
						nfiesta_results.t_result as tr
						inner join w5 on tr.estimate_conf = w5.id_t_estimate_conf
				where
						tr.is_latest = true
				)
		,w7 as	(
				select
						cec.id as estimation_cell_id,
						cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w6.area_domain_category is null then 0
							else cadc.id
						end
							as variable_area_domain_id,	
						---------------------------------
						case
							when w6.area_domain_category is null then ''bez rozliseni''
							else replace(cadc.label,'';'',''; '')
						end
							as variable_area_domain,
						---------------------------------
						case
							when w6.area_domain_category is null then ''without distinction''
							else replace(cadc.label_en,'';'',''; '')
						end
							as variable_area_domain_en,
						-----------------------------------------------------
						case
							when w6.sub_population_category is null then 0
							else cspc.id
						end
							as variable_sub_population_id,
						---------------------------------
						case
							when w6.sub_population_category is null then ''bez rozliseni''
							else replace(cspc.label,'';'',''; '')
						end
							as variable_sub_population,
						---------------------------------
						case
							when w6.sub_population_category is null then ''without distinction''
							else replace(cspc.label_en,'';'',''; '')
						end
							as variable_sub_population_en,
						-----------------------------------------------------
						w6.point as point_estimate,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else sqrt(w6.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null or w6.point = 0.0::numeric then null::numeric
							else (sqrt(w6.var) / w6.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						--w6.act_ssize as sample_size,
						--w6.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w6.var < 0.0 or w6.var is null then null::numeric
							else (1.96 * sqrt(w6.var))
						end
							as interval_estimation
				from
						w6
						inner join nfiesta_results.c_estimation_cell as cec on w6.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc on w6.area_domain_category = cadc.id
						left join nfiesta_results.c_sub_population_category as cspc on w6.sub_population_category = cspc.id
				)
		select
				w7.estimation_cell'||_lang_suffix||' as estimation_cell,
				w7.variable_area_domain'||_lang_suffix||' as variable_area_domain_num,
				w7.variable_sub_population'||_lang_suffix||' as variable_sub_population_num,
				null::text as variable_area_domain_denom,
				null::text as variable_sub_population_denom,
				round(w7.point_estimate::numeric,2) as point_estimate,
				round(w7.standard_deviation::numeric,2) as standard_deviation,
				round(w7.variation_coeficient::numeric,2) as variation_coeficient,
				--w7.sample_size::integer,
				--(ceil(w7.min_sample_size::numeric))::integer as min_sample_size,
				round(w7.interval_estimation::numeric,2) as interval_estimation
		from
				w7 order by w7.estimation_cell_id, variable_area_domain_id, variable_sub_population_id;
		'
		using
				_target_variable,
				_area_domain,
				_sub_population,
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection,
				_phase_estimate_type,
				_estimate_type;
				
	else -- RATIO
		---------------------------------------------------
		if _area_domain is null
		then
			_cond_area_domain := 'null::integer';
		else
			_cond_area_domain := '$3';
		end if;
		---------------------------------------------------
		if _sub_population is null
		then
			_cond_sub_population := 'null::integer';
		else
			_cond_sub_population := '$4';
		end if;
		---------------------------------------------------
		if _area_domain_denom is null
		then
			_cond_area_domain_denom := 'null::integer';
		else
			_cond_area_domain_denom := '$5';
		end if;
		---------------------------------------------------
		if _sub_population_denom is null
		then
			_cond_sub_population_denom := 'null::integer';
		else
			_cond_sub_population_denom := '$6';
		end if;
		---------------------------------------------------
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''label'' as unit_num,
						((w2.metadata->''cs'')->''unit'')->>''label'' as unit_denom,
						((w1.metadata->''en'')->''unit'')->>''label'' as unit_num_en,
						((w2.metadata->''en'')->''unit'')->>''label'' as unit_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_num'||_lang_suffix||' AS unit_num,
						w3.unit_denom'||_lang_suffix||' AS unit_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_num = w4.unit_denom) then 0
							else 1
						end as check4unit
				from
						w4
				)
		select w5.check4unit from w5
		'
		using _target_variable, _target_variable_denom
		into _check4unit;		
		---------------------------------------------------
		if _check4unit = 0
		then	
			_coeficient4ratio := 100.0::numeric;
		else
			_coeficient4ratio := 1.0::numeric;
		end if;
		---------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.fn_get_attribute_categories4target_variable
					(
					$1,
					'||_cond_area_domain||',
					'||_cond_sub_population||',
					$2,
					'||_cond_area_domain_denom||',
					'||_cond_sub_population_denom||'
					)
				)
		,w2a as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.nominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.phase_estimate_type = $10
				)
		,w2b as	(
				select ttec.* from nfiesta_results.t_total_estimate_conf as ttec
				where ttec.variable in (select w1.denominator_variable from w1)
				and ttec.estimation_period = $7
				and ttec.panel_refyearset_group = $8
				and ttec.estimation_cell in (select cec.id from nfiesta_results.c_estimation_cell as cec where cec.estimation_cell_collection = $9)
				and ttec.phase_estimate_type = $10
				)
		,w3 as	(
				select
						w2a.id as id_t_total_estimate_conf_nom,
						w2b.id as id_t_total_estimate_conf_denom,
						w2a.estimation_cell,
						w2a.variable,
						w2a.phase_estimate_type,
						w2a.force_synthetic,
						w2a.estimation_period,
						w2a.panel_refyearset_group,
						w1.nominator_variable,
						w1.denominator_variable,
						w1.label as fce_label,
						w1.description as fce_description,
						w1.label_en as fce_label_en,
						w1.description_en as fce_description_en,	
						tv1.area_domain_category as area_domain_category_nom,
						tv2.area_domain_category as area_domain_category_denom,
						tv1.sub_population_category as sub_population_category_nom,
						tv2.sub_population_category as sub_population_category_denom
				from
						w2a
						inner join w1 on w2a.variable = w1.nominator_variable
						inner join w2b on w2b.variable = w1.denominator_variable
						inner join nfiesta_results.t_variable as tv1 on w2a.variable = tv1.id
						inner join nfiesta_results.t_variable as tv2 on w2b.variable = tv2.id
				)
		,w4 as	(
				select
						tec.id as id_t_estimate_conf,
						tec.estimate_type,
						tec.total_estimate_conf,
						tec.denominator,
						w3.*
				from
						nfiesta_results.t_estimate_conf as tec
						
						inner join w3
						on tec.total_estimate_conf = w3.id_t_total_estimate_conf_nom
						and tec.denominator = w3.id_t_total_estimate_conf_denom
				where
						tec.estimate_type = $11
				)
		,w5 as	(
				select
						tr.id,
						tr.estimate_conf,
						tr.point * $12 as point,
						tr.var * $12 as var,
						--tr.min_ssize,
						--tr.act_ssize,
						w4.*
				from
						nfiesta_results.t_result as tr
						inner join w4 on tr.estimate_conf = w4.id_t_estimate_conf
				where
						tr.is_latest = true
				)			
		,w6 as	(
				select
						cec.id as estimation_cell_id,
						cec.description as estimation_cell,
						cec.description_en as estimation_cell_en,
						-----------------------------------------------------
						case
							when w5.area_domain_category_nom is null then 0
							else cadc1.id
						end
							as variable_area_domain_id_nom,	
						---------------------------------
						case
							when w5.area_domain_category_denom is null then 0
							else cadc2.id
						end
							as variable_area_domain_id_denom,	
						---------------------------------
						case
							when w5.area_domain_category_nom is null then ''bez rozliseni''
							else replace(cadc1.label,'';'',''; '')
						end
							as variable_area_domain_nom,
						---------------------------------
						case
							when w5.area_domain_category_denom is null then ''bez rozliseni''
							else replace(cadc2.label,'';'',''; '')
						end
							as variable_area_domain_denom,
						---------------------------------
						case
							when w5.area_domain_category_nom is null then ''without distinction''
							else replace(cadc1.label_en,'';'',''; '')
						end
							as variable_area_domain_nom_en,
						---------------------------------
						case
							when w5.area_domain_category_denom is null then ''without distinction''
							else replace(cadc2.label_en,'';'',''; '')
						end
							as variable_area_domain_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						case
							when w5.sub_population_category_nom is null then 0
							else cspc1.id
						end
							as variable_sub_population_id_nom,	
						---------------------------------
						case
							when w5.sub_population_category_denom is null then 0
							else cspc2.id
						end
							as variable_sub_population_id_denom,	
						---------------------------------
						case
							when w5.sub_population_category_nom is null then ''bez rozliseni''
							else replace(cspc1.label,'';'',''; '')
						end
							as variable_sub_population_nom,
						---------------------------------
						case
							when w5.sub_population_category_denom is null then ''bez rozliseni''
							else replace(cspc2.label,'';'',''; '')
						end
							as variable_sub_population_denom,
						---------------------------------
						case
							when w5.sub_population_category_nom is null then ''without distinction''
							else replace(cspc1.label_en,'';'',''; '')
						end
							as variable_sub_population_nom_en,
						---------------------------------
						case
							when w5.sub_population_category_denom is null then ''without distinction''
							else replace(cspc2.label_en,'';'',''; '')
						end
							as variable_sub_population_denom_en,
						-----------------------------------------------------
						-----------------------------------------------------
						w5.point as point_estimate,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else sqrt(w5.var)
						end
							as standard_deviation,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null or w5.point = 0.0::numeric then null::numeric
							else (sqrt(w5.var) / w5.point * 100.0)
						end
							as variation_coeficient,
						-----------------------------------------------------
						--w5.act_ssize as sample_size,
						--w5.min_ssize as min_sample_size,
						-----------------------------------------------------
						case
							when w5.var < 0.0 or w5.var is null then null::numeric
							else (1.96 * sqrt(w5.var))
						end
							as interval_estimation
				from
						w5
						inner join nfiesta_results.c_estimation_cell as cec on w5.estimation_cell = cec.id
						left join nfiesta_results.c_area_domain_category as cadc1 on w5.area_domain_category_nom = cadc1.id
						left join nfiesta_results.c_area_domain_category as cadc2 on w5.area_domain_category_denom = cadc2.id
						left join nfiesta_results.c_sub_population_category as cspc1 on w5.sub_population_category_nom = cspc1.id
						left join nfiesta_results.c_sub_population_category as cspc2 on w5.sub_population_category_denom = cspc2.id
				)
		select
				w6.estimation_cell'||_lang_suffix||' as estimation_cell,
				--concat(w6.variable_area_domain_nom'||_lang_suffix||','' / '',w6.variable_area_domain_denom'||_lang_suffix||') as variable_area_domain,
				--concat(w6.variable_sub_population_nom'||_lang_suffix||','' / '',w6.variable_sub_population_denom'||_lang_suffix||') as variable_sub_population,
				w6.variable_area_domain_nom'||_lang_suffix||' as variable_area_domain_num,
				w6.variable_sub_population_nom'||_lang_suffix||' as variable_sub_population_num,
				w6.variable_area_domain_denom'||_lang_suffix||' as variable_area_domain_denom,				
				w6.variable_sub_population_denom'||_lang_suffix||' as variable_sub_population_denom,				
				round(w6.point_estimate::numeric,2) as point_estimate,
				round(w6.standard_deviation::numeric,2) as standard_deviation,
				round(w6.variation_coeficient::numeric,2) as variation_coeficient,
				--w6.sample_size::integer,
				--(ceil(w6.min_sample_size::numeric))::integer as min_sample_size,
				round(w6.interval_estimation::numeric,2) as interval_estimation
		from
				w6
		order
				by	w6.estimation_cell_id,
					w6.variable_area_domain_id_nom,
					w6.variable_area_domain_id_denom,
					w6.variable_sub_population_id_nom,
					w6.variable_sub_population_id_denom;
		'
		using
				_target_variable,
				_target_variable_denom,
				_area_domain,
				_sub_population,
				_area_domain_denom,
				_sub_population_denom,				
				_estimation_period,
				_panel_refyearset_group,
				_estimation_cell_collection,
				_phase_estimate_type,
				_estimate_type,
				_coeficient4ratio;	
	end if;

end;
$$
;

comment on function nfiesta_results.fn_get_user_query(character varying, integer[]) is
'The function returns result estimations for gived group of result and attribute variables from t_result table.';

grant execute on function nfiesta_results.fn_get_user_query(character varying, integer[]) to public;
-- </function>




drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);
-- <function name="fn_get_user_metadata" schema="nfiesta_results" src="functions/fn_get_user_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	res_metadata_type			text,
	res_metadata_description	text
)
language plpgsql
stable
as
$$
declare
	_lang_suffix					character varying(3);
	_lang							text;
	_target_variable_num			integer[];
	_target_variable_denom_check	integer;
	_target_variable_denom			integer;

	_cond4topic_num					text;
	_cond4topic_denom				text;

	_res_topic						text;
	_res_estimation_period			text;
	_res_estimation_cell_collection	text;
	_res_indicator_num				text;
	_res_indicator_denom			text;
	_res_state_or_change_num		text;
	_res_state_or_change_denom		text;
	_res_unit_num					text;
	_res_unit_denom					text;
	_res_unit_of_measure			text;

	_check_ldsity_contributions_num		integer;
	_check_ldsity_contributions_denom	integer;
	_query_ldsity_attributes_num_1		text;
	_query_ldsity_attributes_num_2		text;
	_query_ldsity_attributes_num		text;
	_query_ldsity_attributes_denom_1	text;
	_query_ldsity_attributes_denom_2	text;
	_query_ldsity_attributes_denom		text;
	_query_ldsity_attributes			text;

	_res								text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	select count(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom_check;

	if _target_variable_denom_check is distinct from 0 -- target variable denom exists
	then
		if _target_variable_denom_check > 1
		then
			raise exception 'Error: 03: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
		end if;

		select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
		where trg.id in (select unnest(_id_group))
		and trg.web = true
		into _target_variable_denom;		
	else
		_target_variable_denom := null::integer;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	execute
	'
	with
	w1 as	(
			select crt.topic from nfiesta_results.cm_result2topic as crt
			where ' || _cond4topic_num ||'
			and ' || _cond4topic_denom ||'
			)
	,w2 as	(
			select
					description'||_lang_suffix||' AS description
			from
					nfiesta_results.c_topic
			where
					id in (select distinct w1.topic from w1)
			)
	,w3 as	(
			select array_agg(w2.description order by w2.description) as description
			from w2
			)
			select array_to_string(w3.description, ''; '') from w3
	'
	using _target_variable_num[1], _target_variable_denom
	into _res_topic;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	execute
	'
	select
			cep.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_period as cep
	where
			cep.id =	(
						select distinct trg.estimation_period
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_period;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	execute
	'
	select
			cecc.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_cell_collection as cecc
	where
			cecc.id =	(
						select distinct trg.estimation_cell_collection
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_cell_collection;
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
					((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num[1]
	into _res_indicator_num;
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - denominator
	if _target_variable_denom is null
	then
		_res_indicator_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
						((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.description'||_lang_suffix||' AS description
		from
				w1
		'
		using _target_variable_denom
		into _res_indicator_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state or change'')->>''description'' as description,
					((ctv.metadata->''en'')->''state or change'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num[1]
	into _res_state_or_change_num;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - denominator
	if _target_variable_denom is null
	then
		_res_state_or_change_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''state or change'')->>''description'' as description,
						((ctv.metadata->''en'')->''state or change'')->>''description'' as description_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.description'||_lang_suffix||' AS description
		from
				w1
		'
		using _target_variable_denom
		into _res_state_or_change_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num[1]
	into _res_unit_num;
	-----------------------------------------------------------------
	-- METADATA OF UNIT - denominator
	if _target_variable_denom is null
	then
		_res_unit_denom := null::text;
	else
		execute
		'
		with
		w1 as	(
				select
						((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
						((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		select
				w1.description'||_lang_suffix||' AS description
		from
				w1
		'
		using _target_variable_denom
		into _res_unit_denom;
	end if;
	-----------------------------------------------------------------
	-- METADATA LDSITY - numerator
	-- check => how many ldsity contributions?
	execute
	'
	with			
	w1 as	(
			select
					t.id as target_variable,
					t.description'||_lang_suffix||' AS description
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as description,
							((ctv.metadata->''en'')->''local densities'') as description_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = $1
					) as t
			)									
	,w2 as	(
			select
					w1.target_variable,
					json_array_elements(w1.description) as description
			from
					w1
			)
	select count(w2.*) from w2
	'
	using _target_variable_num[1]
	into _check_ldsity_contributions_num;

	--raise notice '_check_ldsity_contributions_num: %',_check_ldsity_contributions_num;
			
	if _check_ldsity_contributions_num = 0
	then
		raise exception 'Error: 04: fn_get_user_options_metadata: For target variable [ID = %] numerator not exists any ldsity contribution!',_target_variable_num[1];
	end if;

	_query_ldsity_attributes_num_1 := concat(
	'
	with			
	w1a as	(
			select
					t.id as target_variable,
					t.description',_lang_suffix,' AS description
			from
					(
					select
							ctv.id,
							((ctv.metadata->''cs'')->''local densities'') as description,
							((ctv.metadata->''en'')->''local densities'') as description_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = ',_target_variable_num[1],'
					) as t
			)									
	,w2a as	(
			select
					w1a.target_variable,
					json_array_elements(w1a.description) as description
			from
					w1a
			)
	,w3a as	(
			select
					row_number() over () as new_id,
					w2a.target_variable,
					w2a.description->>''object type label'' as object_type_label,
					w2a.description->>''object type description'' as object_type_description,
					w2a.description->>''description'' as ldsity,
					replace(
					replace(
					replace(
					replace(
					coalesce((w2a.description->''definition variant'')->>''description'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as variant,					
					replace(
					replace(
					replace(
					replace(
					coalesce((w2a.description->''area domain'')->>''description'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as area_domain,
					replace(
					replace(
					replace(
					replace(
					coalesce((w2a.description->''population'')->>''description'',''null''::text)
					,''", "'',''; '')
					,''","'',''; '')
					,''["'','''')
					,''"]'','''')													
					as population
			from
					w2a
			)
	');

	for i in 1.._check_ldsity_contributions_num
	loop
		if _lang = 'cs'
		then
			if i = 1
			then
				_query_ldsity_attributes_num_2 :=
				concat(
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Varianta:'' as res_metadata_type,
						w3a.variant as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Plošná doména:'' as res_metadata_type,
						w3a.area_domain as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Populace:'' as res_metadata_type,
						w3a.population as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			else
				_query_ldsity_attributes_num_2 := 
				concat(_query_ldsity_attributes_num_2,' union all ',
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Varianta:'' as res_metadata_type,
						w3a.variant as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Plošná doména:'' as res_metadata_type,
						w3a.area_domain as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Populace:'' as res_metadata_type,
						w3a.population as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			end if;
		else
			if i = 1
			then
				_query_ldsity_attributes_num_2 :=
				concat(
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Variant:'' as res_metadata_type,
						w3a.variant as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Area domain:'' as res_metadata_type,
						w3a.area_domain as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Population:'' as res_metadata_type,
						w3a.population as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			else
				_query_ldsity_attributes_num_2 := 
				concat(_query_ldsity_attributes_num_2,' union all ',
				'select
						',i,' as id_ldsity,
						1 as id_attribute,
						case when w3a.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
						w3a.ldsity as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all
				select
						',i,' as id_ldsity,
						2 as id_attribute,
						''     Variant:'' as res_metadata_type,
						w3a.variant as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all			
				select
						',i,' as id_ldsity,
						3 as id_attribute,
						''     Area domain:'' as res_metadata_type,
						w3a.area_domain as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				union all	
				select
						',i,' as id_ldsity,
						4 as id_attribute,
						''     Population:'' as res_metadata_type,
						w3a.population as res_metadata_description
				from
						w3a where w3a.new_id = ',i,'
				'
				);
			end if;
		end if;
	end loop;

	_query_ldsity_attributes_num := concat(_query_ldsity_attributes_num_1,' ',_query_ldsity_attributes_num_2);
	-----------------------------------------------------------------
	-- METADATA LDSITY - denominator
	if _target_variable_denom is null
	then
		if _lang = 'cs'
		then
			_query_ldsity_attributes_denom :=
			'
			select
					0 as id_ldsity,
					0 as id_attribute,
					null::text as res_metadata_type,
					null::text as res_metadata_description
			';
		else
		end if;
	else
		-- check => how many ldsity contributions?
		execute
		'
		with			
		w1 as	(
				select
						t.id as target_variable,
						t.description'||_lang_suffix||' AS description
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as description,
								((ctv.metadata->''en'')->''local densities'') as description_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = $1
						) as t
				)									
		,w2 as	(
				select
						w1.target_variable,
						json_array_elements(w1.description) as description
				from
						w1
				)
		select count(w2.*) from w2
		'
		using _target_variable_denom
		into _check_ldsity_contributions_denom;
				
		if _check_ldsity_contributions_denom = 0
		then
			raise exception 'Error: 05: fn_get_user_options_metadata: For target variable [ID = %] denominator not exists any ldsity contribution!',_target_variable_denom;
		end if;

		_query_ldsity_attributes_denom_1 := concat(
		'
		with			
		w1b as	(
				select
						t.id as target_variable,
						t.description',_lang_suffix,' AS description
				from
						(
						select
								ctv.id,
								((ctv.metadata->''cs'')->''local densities'') as description,
								((ctv.metadata->''en'')->''local densities'') as description_en
						from
								nfiesta_results.c_target_variable as ctv
						where
								ctv.id = ',_target_variable_denom,'
						) as t
				)									
		,w2b as	(
				select
						w1b.target_variable,
						json_array_elements(w1b.description) as description
				from
						w1b
				)
		,w3b as	(
				select
						row_number() over () as new_id,
						w2b.target_variable,
						w2b.description->>''object type label'' as object_type_label,
						w2b.description->>''object type description'' as object_type_description,
						w2b.description->>''description'' as ldsity,
						replace(
						replace(
						replace(
						replace(
						coalesce((w2b.description->''definition variant'')->>''description'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as variant,					
						replace(
						replace(
						replace(
						replace(
						coalesce((w2b.description->''area domain'')->>''description'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as area_domain,
						replace(
						replace(
						replace(
						replace(
						coalesce((w2b.description->''population'')->>''description'',''null''::text)
						,''", "'',''; '')
						,''","'',''; '')
						,''["'','''')
						,''"]'','''')													
						as population
				from
						w2b
				)
		');

		for i in 1.._check_ldsity_contributions_denom
		loop
			if _lang = 'cs'
			then
				if i = 1
				then
					_query_ldsity_attributes_denom_2 :=
					concat(
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Varianta:'' as res_metadata_type,
							w3b.variant as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Plošná doména:'' as res_metadata_type,
							w3b.area_domain as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Populace:'' as res_metadata_type,
							w3b.population as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				else
					_query_ldsity_attributes_denom_2 := 
					concat(_query_ldsity_attributes_denom_2,' union all ',
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Příspěvek lokální hustoty:'' else ''Členící příspěvek:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Varianta:'' as res_metadata_type,
							w3b.variant as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Plošná doména:'' as res_metadata_type,
							w3b.area_domain as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Populace:'' as res_metadata_type,
							w3b.population as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				end if;
			else
				if i = 1
				then
					_query_ldsity_attributes_denom_2 :=
					concat(
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Variant:'' as res_metadata_type,
							w3b.variant as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Area domain:'' as res_metadata_type,
							w3b.area_domain as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Population:'' as res_metadata_type,
							w3b.population as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				else
					_query_ldsity_attributes_denom_2 := 
					concat(_query_ldsity_attributes_denom_2,' union all ',
					'select
							',i,' as id_ldsity,
							1 as id_attribute,
							case when w3b.object_type_label = ''core'' then ''Local density contribution:'' else ''Devided contribution:'' end as res_metadata_type,
							w3b.ldsity as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all
					select
							',i,' as id_ldsity,
							2 as id_attribute,
							''     Variant:'' as res_metadata_type,
							w3b.variant as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all			
					select
							',i,' as id_ldsity,
							3 as id_attribute,
							''     Area domain:'' as res_metadata_type,
							w3b.area_domain as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					union all	
					select
							',i,' as id_ldsity,
							4 as id_attribute,
							''     Population:'' as res_metadata_type,
							w3b.population as res_metadata_description
					from
							w3b where w3b.new_id = ',i,'
					'
					);
				end if;
			end if;
		end loop;

		_query_ldsity_attributes_denom := concat(_query_ldsity_attributes_denom_1,' ',_query_ldsity_attributes_denom_2);

	end if;
	-----------------------------------------------------------------
	if _target_variable_denom is null
	then
		_query_ldsity_attributes := concat(
		',w4a as	(',_query_ldsity_attributes_num,')
		,w4ab as	(
					select
							w4a.res_metadata_type as res_metadata_type_num,
							w4a.res_metadata_description as res_metadata_description_num,
							null::text as res_metadata_type_denom,
							null::text as res_metadata_description_denom
					from
							w4a
					)
		');
	else
		if _check_ldsity_contributions_num >= _check_ldsity_contributions_denom
		then
			_query_ldsity_attributes := concat(
			',w4a as	(',_query_ldsity_attributes_num,')
			,w4b as	(',_query_ldsity_attributes_denom,')
			,w4ab as	(
						select
								w4a.res_metadata_type as res_metadata_type_num,
								w4a.res_metadata_description as res_metadata_description_num,
								w4b.res_metadata_type as res_metadata_type_denom,
								w4b.res_metadata_description as res_metadata_description_denom
						from
								w4a left join w4b
						on
								w4a.id_ldsity = w4b.id_ldsity and
								w4a.id_attribute = w4b.id_attribute
						)
			');
		else
			_query_ldsity_attributes := concat(
			',w4a as	(',_query_ldsity_attributes_num,')
			,w4b as	(',_query_ldsity_attributes_denom,')
			,w4ab as	(
						select
								w4a.res_metadata_type as res_metadata_type_num,
								w4a.res_metadata_description as res_metadata_description_num,
								w4b.res_metadata_type as res_metadata_type_denom,
								w4b.res_metadata_description as res_metadata_description_denom
						from
								w4a right join w4b
						on
								w4a.id_ldsity = w4b.id_ldsity and
								w4a.id_attribute = w4b.id_attribute
						)
			');
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASURE
	if _target_variable_denom is null
	then
		_res_unit_of_measure := _res_unit_num;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''description'' as unit_num,
						((w2.metadata->''cs'')->''unit'')->>''description'' as unit_denom,
						((w1.metadata->''en'')->''unit'')->>''description'' as unit_num_en,
						((w2.metadata->''en'')->''unit'')->>''description'' as unit_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_num'||_lang_suffix||' AS unit_num,
						w3.unit_denom'||_lang_suffix||' AS unit_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_num = w4.unit_denom) then ''%''
							else concat(w4.unit_num,'' / '',w4.unit_denom)
						end as description4user
				from
						w4
				)
		select w5.description4user from w5
		'
		using _target_variable_num[1], _target_variable_denom
		into _res_unit_of_measure;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _lang_suffix = ''
	then
		if _target_variable_denom is null
		then
			_res := concat(
			'
			with
			w01 as	(
					select
							''Tematický okruh:'' as res_metadata_type,
							''',_res_topic,''' as res_metadata_description
					union all
					select
							''Období:'' as res_metadata_type,
							''',_res_estimation_period,''' as res_metadata_description
					union all
					select
							''Geografické členění:'' as res_metadata_type,
							''',_res_estimation_cell_collection,''' as res_metadata_description
					union all
					select
							''Jednotka výstupu:'' as res_metadata_type,
							''',_res_unit_of_measure,''' as res_metadata_description
					union all
					select
							''Indikátor:'' as res_metadata_type,
							''',_res_indicator_num,''' as res_metadata_description
					union all
					select
							''Stav nebo změna:'' as res_metadata_type,
							''',_res_state_or_change_num,''' as res_metadata_description
					union all
					select
							''Jednotka indikátoru:'' as res_metadata_type,
							''',_res_unit_num,''' as res_metadata_description
					)
			',_query_ldsity_attributes,
			'select
					w01.res_metadata_type,
					w01.res_metadata_description
			from
					w01
			union all
			select
					''Příspěvky:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_num as res_metadata_type,
					case when w4ab.res_metadata_description_num = ''null'' then null::text else w4ab.res_metadata_description_num end as res_metadata_description
			from
					w4ab;
			');
		else
			_res := concat(
			'
			with
			w01 as	(
					select
							''Tematický okruh:'' as res_metadata_type,
							''',_res_topic,''' as res_metadata_description
					union all
					select
							''Období:'' as res_metadata_type,
							''',_res_estimation_period,''' as res_metadata_description
					union all
					select
							''Geografické členění:'' as res_metadata_type,
							''',_res_estimation_cell_collection,''' as res_metadata_description
					union all
					select
							''Jednotka výstupu:'' as res_metadata_type,
							''',_res_unit_of_measure,''' as res_metadata_description
					union all
					select
							null::text as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Informace čitatele:'' as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Indikátor:'' as res_metadata_type,
							''',_res_indicator_num,''' as res_metadata_description
					union all
					select
							''Stav nebo změna:'' as res_metadata_type,
							''',_res_state_or_change_num,''' as res_metadata_description
					union all
					select
							''Jednotka indikátoru:'' as res_metadata_type,
							''',_res_unit_num,''' as res_metadata_description
					)
			,w02 as	(
					select
							null::text as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Informace jmenovatele:'' as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Indikátor:'' as res_metadata_type,
							''',_res_indicator_denom,''' as res_metadata_description
					union all
					select
							''Stav nebo změna:'' as res_metadata_type,
							''',_res_state_or_change_denom,''' as res_metadata_description
					union all
					select
							''Jednotka indikátoru:'' as res_metadata_type,
							''',_res_unit_denom,''' as res_metadata_description
					)
			',_query_ldsity_attributes,
			'select
					w01.res_metadata_type,
					w01.res_metadata_description
			from
					w01
			union all
			select
					''Příspěvky:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_num as res_metadata_type,
					case when w4ab.res_metadata_description_num = ''null'' then null::text else w4ab.res_metadata_description_num end as res_metadata_description
			from
					w4ab where w4ab.res_metadata_type_num is not null
			union all
			select
					w02.res_metadata_type,
					w02.res_metadata_description
			from
					w02
			union all
			select
					''Příspěvky:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_denom as res_metadata_type,
					case when w4ab.res_metadata_description_denom = ''null'' then null::text else w4ab.res_metadata_description_denom end as res_metadata_description
			from
					w4ab where w4ab.res_metadata_type_denom is not null
			');		
		end if;			
	else
		if _target_variable_denom is null
		then
			_res := concat(
			'
			with
			w01 as	(
					select
							''Topic:'' as res_metadata_type,
							''',_res_topic,''' as res_metadata_description
					union all
					select
							''Period:'' as res_metadata_type,
							''',_res_estimation_period,''' as res_metadata_description
					union all
					select
							''Geographic region:'' as res_metadata_type,
							''',_res_estimation_cell_collection,''' as res_metadata_description
					union all
					select
							''Unit of measurement:'' as res_metadata_type,
							''',_res_unit_of_measure,''' as res_metadata_description
					union all
					select
							''Indicator:'' as res_metadata_type,
							''',_res_indicator_num,''' as res_metadata_description
					union all
					select
							''State or change:'' as res_metadata_type,
							''',_res_state_or_change_num,''' as res_metadata_description
					union all
					select
							''Unit of indicator:'' as res_metadata_type,
							''',_res_unit_num,''' as res_metadata_description
					)
			',_query_ldsity_attributes,
			'select
					w01.res_metadata_type,
					w01.res_metadata_description
			from
					w01
			union all
			select
					''Contributions:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_num as res_metadata_type,
					case when w4ab.res_metadata_description_num = ''null'' then null::text else w4ab.res_metadata_description_num end as res_metadata_description
			from
					w4ab;
			');
		else
			_res := concat(
			'
			with
			w01 as	(
					select
							''Topic:'' as res_metadata_type,
							''',_res_topic,''' as res_metadata_description
					union all
					select
							''Period:'' as res_metadata_type,
							''',_res_estimation_period,''' as res_metadata_description
					union all
					select
							''Geographic region:'' as res_metadata_type,
							''',_res_estimation_cell_collection,''' as res_metadata_description
					union all
					select
							''Unit of measurement:'' as res_metadata_type,
							''',_res_unit_of_measure,''' as res_metadata_description
					union all
					select
							null::text as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Numerator informations:'' as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Indicator:'' as res_metadata_type,
							''',_res_indicator_num,''' as res_metadata_description
					union all
					select
							''State or change:'' as res_metadata_type,
							''',_res_state_or_change_num,''' as res_metadata_description
					union all
					select
							''Unit of indicator:'' as res_metadata_type,
							''',_res_unit_num,''' as res_metadata_description
					)
			,w02 as	(
					select
							null::text as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Denominator informations:'' as res_metadata_type,
							null::text as res_metadata_description
					union all
					select
							''Indicator:'' as res_metadata_type,
							''',_res_indicator_denom,''' as res_metadata_description
					union all
					select
							''State or change:'' as res_metadata_type,
							''',_res_state_or_change_denom,''' as res_metadata_description
					union all
					select
							''Unit of indicator:'' as res_metadata_type,
							''',_res_unit_denom,''' as res_metadata_description
					)
			',_query_ldsity_attributes,
			'select
					w01.res_metadata_type,
					w01.res_metadata_description
			from
					w01
			union all
			select
					''Contributions:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_num as res_metadata_type,
					case when w4ab.res_metadata_description_num = ''null'' then null::text else w4ab.res_metadata_description_num end as res_metadata_description
			from
					w4ab where w4ab.res_metadata_type_num is not null
			union all
			select
					w02.res_metadata_type,
					w02.res_metadata_description
			from
					w02
			union all
			select
					''Contributions:'' as res_metadata_type,
					null::text as res_metadata_description
			union all
			select
					w4ab.res_metadata_type_denom as res_metadata_type,
					case when w4ab.res_metadata_description_denom = ''null'' then null::text else w4ab.res_metadata_description_denom end as res_metadata_description
			from
					w4ab where w4ab.res_metadata_type_denom is not null
			');		
		end if;
	end if;

	return query execute ''||_res||'';
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) is
'The function returns metadata for given input ID (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) to public;
-- </function>
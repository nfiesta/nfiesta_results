--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------



-- <function name="fn_get_user_options_numerator_ldsity_core" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_ldsity_core.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_ldsity_core
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_ldsity_core
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix						character varying(3);
		_not_specified_version_label		text;
		_not_specified_version_description	text;
		_not_specified_defintion_variant	json;
		_not_specified_adr_spr				json;
begin	
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_numerator_ldsity_core: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ'
		then
			_not_specified_version_label = 'neuvedeno'::text;
			_not_specified_version_description = 'Neuvedeno.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
		end if;
	
		if _jlang = 'en-GB'
		then
			_not_specified_version_label := 'not specified'::text;
			_not_specified_version_description = 'Not specified.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
		end if;
		-----------------------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.local_densities'||_lang_suffix||' AS local_densities
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as local_densities,
								((ctv.metadata->''en'')->''local densities'') as local_densities_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where w4.local_densities->>''object type label'' = ''core''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						w5.local_densities->>''object type label'' as object_type_label,
						w5.local_densities->>''object type description'' as object_type_description,
						w5.local_densities->>''object label'' as object_label,
						w5.local_densities->>''object description'' as object_description,
						w5.local_densities->>''label'' as label,
						w5.local_densities->>''description'' as description,
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition variant'' as definition_variant,
						w5.local_densities->''area domain restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub population restrictions'' as sub_population_restrictions
				from
						w5
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''object type label'',w7.object_type_label,
							''object type description'',w7.object_type_description,
							''object label'',w7.object_label,
							''object description'',w7.object_description,
							''label'',w7.label,
							''description'',w7.description,
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition variant'',w7.definition_variant,
							''area domain restrictions'',w7.area_domain_restrictions,
							''sub population restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						w8.new_id,
						json_agg(w8.local_densities) as local_densities
				from
						w8 group by w8.target_variable, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]) is
'The function returns list of available labels of standart local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_ldsity_division" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_ldsity_division.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_ldsity_division
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_ldsity_division(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_ldsity_division
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix						character varying(3);
		_not_specified_version_label		text;
		_not_specified_version_description	text;
		_not_specified_defintion_variant	json;
		_not_specified_adr_spr				json;		
begin	
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_numerator_ldsity_division: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ'
		then
			_not_specified_version_label = 'neuvedeno'::text;
			_not_specified_version_description = 'Neuvedeno.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
		end if;
	
		if _jlang = 'en-GB'
		then
			_not_specified_version_label := 'not specified'::text;
			_not_specified_version_description = 'Not specified.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
		end if;
		-----------------------------------------------------------------		
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.local_densities'||_lang_suffix||' AS local_densities
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as local_densities,
								((ctv.metadata->''en'')->''local densities'') as local_densities_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where w4.local_densities->>''object type label'' = ''division''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						w5.local_densities->>''object type label'' as object_type_label,
						w5.local_densities->>''object type description'' as object_type_description,
						w5.local_densities->>''object label'' as object_label,
						w5.local_densities->>''object description'' as object_description,
						w5.local_densities->>''label'' as label,
						w5.local_densities->>''description'' as description,
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition variant'' as definition_variant,
						w5.local_densities->''area domain restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub population restrictions'' as sub_population_restrictions
				from
						w5
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''object type label'',w7.object_type_label,
							''object type description'',w7.object_type_description,
							''object label'',w7.object_label,
							''object description'',w7.object_description,
							''label'',w7.label,
							''description'',w7.description,
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition variant'',w7.definition_variant,
							''area domain restrictions'',w7.area_domain_restrictions,
							''sub population restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						w8.new_id,
						json_agg(w8.local_densities) as local_densities
				from
						w8 group by w8.target_variable, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_ldsity_division(character varying, integer[]) is
'The function returns list of available labels of division local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_ldsity_division(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_ldsity_core" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_ldsity_core.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_ldsity_core
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_ldsity_core(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_ldsity_core
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix						character varying(3);
		_not_specified_version_label		text;
		_not_specified_version_description	text;
		_not_specified_defintion_variant	json;
		_not_specified_adr_spr				json;		
begin	
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_denominator_ldsity_core: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ'
		then
			_not_specified_version_label = 'neuvedeno'::text;
			_not_specified_version_description = 'Neuvedeno.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
		end if;
	
		if _jlang = 'en-GB'
		then
			_not_specified_version_label := 'not specified'::text;
			_not_specified_version_description = 'Not specified.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
		end if;
		-----------------------------------------------------------------		
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable_denom as target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.local_densities'||_lang_suffix||' AS local_densities
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as local_densities,
								((ctv.metadata->''en'')->''local densities'') as local_densities_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where w4.local_densities->>''object type label'' = ''core''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						w5.local_densities->>''object type label'' as object_type_label,
						w5.local_densities->>''object type description'' as object_type_description,
						w5.local_densities->>''object label'' as object_label,
						w5.local_densities->>''object description'' as object_description,
						w5.local_densities->>''label'' as label,
						w5.local_densities->>''description'' as description,
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition variant'' as definition_variant,
						w5.local_densities->''area domain restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub population restrictions'' as sub_population_restrictions
				from
						w5
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''object type label'',w7.object_type_label,
							''object type description'',w7.object_type_description,
							''object label'',w7.object_label,
							''object description'',w7.object_description,
							''label'',w7.label,
							''description'',w7.description,
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition variant'',w7.definition_variant,
							''area domain restrictions'',w7.area_domain_restrictions,
							''sub population restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						w8.new_id,
						json_agg(w8.local_densities) as local_densities
				from
						w8 group by w8.target_variable, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator_ldsity_core(character varying, integer[]) is
'The function returns list of available labels of standart local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_ldsity_core(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_ldsity_division" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_ldsity_division.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_ldsity_division
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_ldsity_division(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_ldsity_division
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix						character varying(3);
		_not_specified_version_label		text;
		_not_specified_version_description	text;
		_not_specified_defintion_variant	json;
		_not_specified_adr_spr				json;
begin	
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_denominator_ldsity_division: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ'
		then
			_not_specified_version_label = 'neuvedeno'::text;
			_not_specified_version_description = 'Neuvedeno.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
		end if;
	
		if _jlang = 'en-GB'
		then
			_not_specified_version_label := 'not specified'::text;
			_not_specified_version_description = 'Not specified.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
		end if;
		-----------------------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable_denom as target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.local_densities'||_lang_suffix||' AS local_densities
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local densities'') as local_densities,
								((ctv.metadata->''en'')->''local densities'') as local_densities_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where w4.local_densities->>''object type label'' = ''division''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						w5.local_densities->>''object type label'' as object_type_label,
						w5.local_densities->>''object type description'' as object_type_description,
						w5.local_densities->>''object label'' as object_label,
						w5.local_densities->>''object description'' as object_description,
						w5.local_densities->>''label'' as label,
						w5.local_densities->>''description'' as description,
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition variant'' as definition_variant,
						w5.local_densities->''area domain restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub population restrictions'' as sub_population_restrictions
				from
						w5
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''object type label'',w7.object_type_label,
							''object type description'',w7.object_type_description,
							''object label'',w7.object_label,
							''object description'',w7.object_description,
							''label'',w7.label,
							''description'',w7.description,
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition variant'',w7.definition_variant,
							''area domain restrictions'',w7.area_domain_restrictions,
							''sub population restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						w8.new_id,
						json_agg(w8.local_densities) as local_densities
				from
						w8 group by w8.target_variable, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator_ldsity_division(character varying, integer[]) is
'The function returns list of available labels of division local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_ldsity_division(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata_numerator" schema="nfiesta_results" src="functions/fn_get_user_metadata_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata_numerator
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	column_1	text,
	column_2	text,
	column_3	text
)
language plpgsql
stable
as
$$
declare
	_lang_suffix					character varying(3);
	_lang							text;
	_target_variable_num			integer[];
	_res_indicator_num				text;
	_res_state_or_change_num		text;
	_res_unit_num					text;
	_check_area_domain				integer[];
	_res_area_domain_attribute		text;
	_check_sub_population			integer[];
	_res_sub_population_attribute	text;
	_check_core_ldsities			integer;
	_check_division_ldsities		integer;
	_main_query						text;
	_main_query_core				text;
	_main_query_division			text;
	_example_for_i_core				text;
	_example_for_i_division			text;
	_query_core_i					text;
	_query_core						text;
	_query_division_i				text;
	_query_division					text;
	_query_core_and_division		text;
	_query_local_densities			text;
	_res							text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata_numerator: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata_numerator: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;	
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
					((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num[1]
	into _res_indicator_num;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state or change'')->>''description'' as description,
					((ctv.metadata->''en'')->''state or change'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num[1]
	into _res_state_or_change_num;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_num[1]
	into _res_unit_num;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 04: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_area_domain_attribute := 'Bez rozlišení.';
		else
			_res_area_domain_attribute := 'Without distinction.';
		end if;
	else
		execute
		'
		select
				cad.description'||_lang_suffix||' as description
		from
				nfiesta_results.c_area_domain as cad
		where
				cad.id = $1
		'
		using _check_area_domain[1]
		into _res_area_domain_attribute;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 05: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_sub_population_attribute := 'Bez rozlišení.';
		else
			_res_sub_population_attribute := 'Without distinction.';
		end if;
	else
		execute
		'
		select
				csp.description'||_lang_suffix||' as description
		from
				nfiesta_results.c_sub_population as csp
		where
				csp.id = $1
		'
		using _check_sub_population[1]
		into _res_sub_population_attribute;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable_num[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2 where w2.local_densities->>'object type label' = 'core'
	into _check_core_ldsities;
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable_num[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2 where w2.local_densities->>'object type label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	_main_query := concat
	(
	'
	,w1 as	(
			select
					t.local_densities'||_lang_suffix||' as local_densities
			from
					(select
							(ctv.metadata->''cs'')->''local densities'' as local_densities,
							(ctv.metadata->''en'')->''local densities'' as local_densities_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = ',_target_variable_num[1],'
					) as t
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	'
	);
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	_main_query_core :=
	'
	,w3_core as	(
				select
						row_number () over () as new_id,
						w2.*
				from
						w2 where w2.local_densities->>''object type label'' = ''core''
				)		
	';
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	_main_query_division :=
	'
	,w3_division as	(
					select
							row_number () over () as new_id,
							w2.*
					from
							w2 where w2.local_densities->>''object type label'' = ''division''
					)		
	';	
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- EXAMPLE for i-CORE
	if _lang_suffix = '' -- CS --
	then
		_example_for_i_core :=
		'
		--------------------------------------------------------------------------
		,w4_core_#I_CORE# as	(
								select ''   základní:'' as column_1, w3_core.local_densities->>''description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      objekt:'' as column_1, w3_core.local_densities->>''object description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      verze:'' as column_1, (w3_core.local_densities->''version'')->>''description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      použití záporu:'' as column_1, w3_core.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE#
								)
		--------------------------------------------------------------------------
		,w5_core_#I_CORE# as	(	
								select
										case when t3.new_id_dv = 1 then ''      definiční varianty:'' else null::text end as column_1,
										t3.dv as column_2,
										null::text as column_3
								from
										(
										select
												row_number () over () as new_id_dv,
												t2.dv->>''description'' as dv
										from
												(
												select json_array_elements(t1.dv) as dv from 
												(select case when (w3_core.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''neuvedeno'',''description'',''Neuvedeno.'')))
												else (w3_core.local_densities->''definition variant'') end as dv from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w6_core_#I_CORE# as	(
								select null::text as column_1, ''objekt:'' as column_2, ''omezení:'' as column_3
								)
		--------------------------------------------------------------------------
		,w7_core_#I_CORE# as	(
								select
										case when t3.new_id_adr = 1 then ''      plošná omezení:'' else null::text end as column_1,
										t3.adr_object as column_2,
										t3.adr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_adr,
												(t2.adr->''object'')->>''description'' as adr_object,
												(t2.adr->''restriction'')->>''description'' as adr_restriction
										from
												(
												select json_array_elements(t1.adr) as adr from 
												(select case when (w3_core.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Neuvedeno.''),''restriction'',json_build_object(''description'',''Neuvedeno.''))))
												else (w3_core.local_densities->''area domain restrictions'') end as adr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w8_core_#I_CORE# as	(
								select
										case when t3.new_id_spr = 1 then ''      sub-populační omezení:'' else null::text end as column_1,
										t3.spr_object as column_2,
										t3.spr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_spr,
												(t2.spr->''object'')->>''description'' as spr_object,
												(t2.spr->''restriction'')->>''description'' as spr_restriction
										from
												(
												select json_array_elements(t1.spr) as spr from 
												(select case when (w3_core.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Neuvedeno.''),''restriction'',json_build_object(''description'',''Neuvedeno.''))))
												else (w3_core.local_densities->''sub population restrictions'') end as spr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)			
		--------------------------------------------------------------------------
		,w9_core_#I_CORE# as	(
								select * from w4_core_#I_CORE# union all
								select * from w5_core_#I_CORE# union all
								select * from w6_core_#I_CORE# union all
								select * from w7_core_#I_CORE# union all
								select * from w8_core_#I_CORE#
								)		
		';
	else
		_example_for_i_core :=
		'
		--------------------------------------------------------------------------
		,w4_core_#I_CORE# as	(
								select ''   main:'' as column_1, w3_core.local_densities->>''description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      object:'' as column_1, w3_core.local_densities->>''object description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      version:'' as column_1, (w3_core.local_densities->''version'')->>''description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      use negative:'' as column_1, w3_core.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE#
								)
		--------------------------------------------------------------------------
		,w5_core_#I_CORE# as	(	
								select
										case when t3.new_id_dv = 1 then ''      definition variants:'' else null::text end as column_1,
										t3.dv as column_2,
										null::text as column_3
								from
										(
										select
												row_number () over () as new_id_dv,
												t2.dv->>''description'' as dv
										from
												(
												select json_array_elements(t1.dv) as dv from 
												(select case when (w3_core.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''not specified'',''description'',''Not specified.'')))
												else (w3_core.local_densities->''definition variant'') end as dv from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w6_core_#I_CORE# as	(
								select null::text as column_1, ''object:'' as column_2, ''restriction:'' as column_3
								)
		--------------------------------------------------------------------------
		,w7_core_#I_CORE# as	(
								select
										case when t3.new_id_adr = 1 then ''      area domain restrictions:'' else null::text end as column_1,
										t3.adr_object as column_2,
										t3.adr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_adr,
												(t2.adr->''object'')->>''description'' as adr_object,
												(t2.adr->''restriction'')->>''description'' as adr_restriction
										from
												(
												select json_array_elements(t1.adr) as adr from 
												(select case when (w3_core.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Not specified.''),''restriction'',json_build_object(''description'',''Not specified.''))))
												else (w3_core.local_densities->''area domain restrictions'') end as adr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w8_core_#I_CORE# as	(
								select
										case when t3.new_id_spr = 1 then ''      sub population restrictions:'' else null::text end as column_1,
										t3.spr_object as column_2,
										t3.spr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_spr,
												(t2.spr->''object'')->>''description'' as spr_object,
												(t2.spr->''restriction'')->>''description'' as spr_restriction
										from
												(
												select json_array_elements(t1.spr) as spr from 
												(select case when (w3_core.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Not specified.''),''restriction'',json_build_object(''description'',''Not specified.''))))
												else (w3_core.local_densities->''sub population restrictions'') end as spr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)			
		--------------------------------------------------------------------------
		,w9_core_#I_CORE# as	(
								select * from w4_core_#I_CORE# union all
								select * from w5_core_#I_CORE# union all
								select * from w6_core_#I_CORE# union all
								select * from w7_core_#I_CORE# union all
								select * from w8_core_#I_CORE#
								)		
		';	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- EXAMPLE for i-DIVISION
	if _lang_suffix = '' -- CS --
	then
		_example_for_i_division :=
		'
		--------------------------------------------------------------------------
		,w4_division_#I_DIVISION# as	(
										select ''   členící:'' as column_1, w3_division.local_densities->>''description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      objekt:'' as column_1, w3_division.local_densities->>''object description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      verze:'' as column_1, (w3_division.local_densities->''version'')->>''description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      použití záporu:'' as column_1, w3_division.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION#
										)
		--------------------------------------------------------------------------
		,w5_division_#I_DIVISION# as	(	
										select
												case when t3.new_id_dv = 1 then ''      definiční varianty:'' else null::text end as column_1,
												t3.dv as column_2,
												null::text as column_3
										from
												(
												select
														row_number () over () as new_id_dv,
														t2.dv->>''description'' as dv
												from
														(
														select json_array_elements(t1.dv) as dv from 
														(select case when (w3_division.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''neuvedeno'',''description'',''Neuvedeno.'')))
														else (w3_division.local_densities->''definition variant'') end as dv from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w6_division_#I_DIVISION# as	(
										select null::text as column_1, ''objekt:'' as column_2, ''omezení:'' as column_3
										)
		--------------------------------------------------------------------------
		,w7_division_#I_DIVISION# as	(
										select
												case when t3.new_id_adr = 1 then ''      plošná omezení:'' else null::text end as column_1,
												t3.adr_object as column_2,
												t3.adr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_adr,
														(t2.adr->''object'')->>''description'' as adr_object,
														(t2.adr->''restriction'')->>''description'' as adr_restriction
												from
														(
														select json_array_elements(t1.adr) as adr from 
														(select case when (w3_division.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Neuvedeno.''),''restriction'',json_build_object(''description'',''Neuvedeno.''))))
														else (w3_division.local_densities->''area domain restrictions'') end as adr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w8_division_#I_DIVISION# as	(
										select
												case when t3.new_id_spr = 1 then ''      sub-populační omezení:'' else null::text end as column_1,
												t3.spr_object as column_2,
												t3.spr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_spr,
														(t2.spr->''object'')->>''description'' as spr_object,
														(t2.spr->''restriction'')->>''description'' as spr_restriction
												from
														(
														select json_array_elements(t1.spr) as spr from 
														(select case when (w3_division.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Neuvedeno.''),''restriction'',json_build_object(''description'',''Neuvedeno.''))))
														else (w3_division.local_densities->''sub population restrictions'') end as spr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)			
		--------------------------------------------------------------------------
		,w9_division_#I_DIVISION# as	(
										select * from w4_division_#I_DIVISION# union all
										select * from w5_division_#I_DIVISION# union all
										select * from w6_division_#I_DIVISION# union all
										select * from w7_division_#I_DIVISION# union all
										select * from w8_division_#I_DIVISION#
										)		
		';
	else
		_example_for_i_division :=
		'
		--------------------------------------------------------------------------
		,w4_division_#I_DIVISION# as	(
										select ''   separated:'' as column_1, w3_division.local_densities->>''description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      object:'' as column_1, w3_division.local_densities->>''object description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      version:'' as column_1, (w3_division.local_densities->''version'')->>''description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      use negative:'' as column_1, w3_division.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION#
										)
		--------------------------------------------------------------------------
		,w5_division_#I_DIVISION# as	(	
										select
												case when t3.new_id_dv = 1 then ''      definition variants:'' else null::text end as column_1,
												t3.dv as column_2,
												null::text as column_3
										from
												(
												select
														row_number () over () as new_id_dv,
														t2.dv->>''description'' as dv
												from
														(
														select json_array_elements(t1.dv) as dv from 
														(select case when (w3_division.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''not specified'',''description'',''Not specified.'')))
														else (w3_division.local_densities->''definition variant'') end as dv from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w6_division_#I_DIVISION# as	(
										select null::text as column_1, ''object:'' as column_2, ''restriction:'' as column_3
										)
		--------------------------------------------------------------------------
		,w7_division_#I_DIVISION# as	(
										select
												case when t3.new_id_adr = 1 then ''      area domain restrictions:'' else null::text end as column_1,
												t3.adr_object as column_2,
												t3.adr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_adr,
														(t2.adr->''object'')->>''description'' as adr_object,
														(t2.adr->''restriction'')->>''description'' as adr_restriction
												from
														(
														select json_array_elements(t1.adr) as adr from 
														(select case when (w3_division.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Not specified.''),''restriction'',json_build_object(''description'',''Not specified.''))))
														else (w3_division.local_densities->''area domain restrictions'') end as adr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w8_division_#I_DIVISION# as	(
										select
												case when t3.new_id_spr = 1 then ''      sub population restrictions:'' else null::text end as column_1,
												t3.spr_object as column_2,
												t3.spr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_spr,
														(t2.spr->''object'')->>''description'' as spr_object,
														(t2.spr->''restriction'')->>''description'' as spr_restriction
												from
														(
														select json_array_elements(t1.spr) as spr from 
														(select case when (w3_division.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Not specified.''),''restriction'',json_build_object(''description'',''Not specified.''))))
														else (w3_division.local_densities->''sub population restrictions'') end as spr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)			
		--------------------------------------------------------------------------
		,w9_division_#I_DIVISION# as	(
										select * from w4_division_#I_DIVISION# union all
										select * from w5_division_#I_DIVISION# union all
										select * from w6_division_#I_DIVISION# union all
										select * from w7_division_#I_DIVISION# union all
										select * from w8_division_#I_DIVISION#
										)		
		';	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	for i_core in 1.._check_core_ldsities
	loop
		if i_core = 1
		then
			_query_core_i := replace(_example_for_i_core,'#I_CORE#'::text,i_core::text);
			_query_core := concat('select * from w9_core_',i_core);
		else
			_query_core_i := _query_core_i || replace(_example_for_i_core,'#I_CORE#'::text,i_core::text);
			_query_core := _query_core || concat(' union all select null::text as column_1, null::text as column_2, null::text as column_3 union all select * from w9_core_',i_core);
		end if;
	end loop;

	_query_core := concat(',w10_core as (',_query_core,')');
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities > 0
	then
		for i_division in 1.._check_division_ldsities
		loop
			if i_division = 1
			then
				_query_division_i := replace(_example_for_i_division,'#I_DIVISION#'::text,i_division::text);
				_query_division := concat('select * from w9_division_',i_division);
			else
				_query_division_i := _query_division_i || replace(_example_for_i_division,'#I_DIVISION#'::text,i_division::text);
				_query_division := _query_division || concat(' union all select null::text as column_1, null::text as column_2, null::text as column_3 union all select * from w9_division_',i_division);
			end if;
		end loop;

		_query_division := concat(',w10_division as (',_query_division,')');
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities > 0
	then
		_query_core_and_division :=
		'
		,w02 as	(
				select * from w10_core union all
				select null::text as column_1, null::text as column_2, null::text as column_3 union all
				select * from w10_division
				)';
	else
		_query_core_and_division := ',w02 as (select * from w10_core)';
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities > 0
	then
		_query_local_densities := concat
			(
			_main_query,
			_main_query_core,
			_main_query_division,
			_query_core_i,
			_query_core,
			_query_division_i,
			_query_division,
			_query_core_and_division
			);
	else
		_query_local_densities := concat
			(
			_main_query,
			_main_query_core,
			_query_core_i,
			_query_core,
			_query_core_and_division
			);	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _lang_suffix = '' -- CS
	then
		_res := concat(
		'
		with
		w01 as	(
				select
						''Indikátor:'' as column_1,
						''',_res_indicator_num,''' as column_2,
						null::text as column_3
				union all
				select
						''Stav nebo změna:'' as column_1,
						''',_res_state_or_change_num,''' as column_2,
						null::text as column_3
				union all
				select
						''Jednotka indikátoru:'' as column_1,
						''',_res_unit_num,''' as column_2,
						null::text as column_3
				union all
				select
						''Plošná atributová členění:'' as column_1,
						''',_res_area_domain_attribute,''' as column_2,
						null::text as column
				union all
				select
						''Populační atributová členění:'' as column_1,
						''',_res_sub_population_attribute,''' as column_2,
						null::text as column
				union all
				select
						null::text as column_1,
						null::text as column_2,
						null::text as column_3
				)
		',_query_local_densities,
		'select
				w01.column_1,
				w01.column_2,
				w01.column_3
		from
				w01
		union all
		select
				''Příspěvky:'' as column_1,
				null::text as column_2,
				null::text as column_3
		union all
		select
				w02.column_1,
				w02.column_2,
				w02.column_3
		from
				w02;
		');		
	else -- EN
		_res := concat(
		'
		with
		w01 as	(
				select
						''Indicator:'' as column_1,
						''',_res_indicator_num,''' as column_2,
						null::text as column_3
				union all
				select
						''State or change:'' as column_1,
						''',_res_state_or_change_num,''' as column_2,
						null::text as column_3
				union all
				select
						''Unit of indicator:'' as column_1,
						''',_res_unit_num,''' as column_2,
						null::text as column_3
				union all
				select
						''Area domain classifactions:'' as column_1,
						''',_res_area_domain_attribute,''' as column_2,
						null::text as column
				union all
				select
						''Sub population classifications:'' as column_1,
						''',_res_sub_population_attribute,''' as column_2,
						null::text as column
				union all
				select
						null::text as column_1,
						null::text as column_2,
						null::text as column_3
				)
		',_query_local_densities,
		'select
				w01.column_1,
				w01.column_2,
				w01.column_3
		from
				w01
		union all
		select
				''Local densities:'' as column_1,
				null::text as column_2,
				null::text as column_3
		union all
		select
				w02.column_1,
				w02.column_2,
				w02.column_3
		from
				w02;
		');
	end if;

	return query execute ''||_res||'';
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]) is
'The function returns metadata (description) for given input ID (that represents target variable for nominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata_denominator" schema="nfiesta_results" src="functions/fn_get_user_metadata_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata_denominator
(
	_jlang character varying,
	_id_group integer[]
)
returns table
(
	column_1	text,
	column_2	text,
	column_3	text
)
language plpgsql
stable
as
$$
declare
	_lang_suffix					character varying(3);
	_lang							text;
	_target_variable_denom			integer[];
	_res_indicator_denom			text;
	_res_state_or_change_denom		text;
	_res_unit_denom					text;
	_check_area_domain				integer[];
	_res_area_domain_attribute		text;
	_check_sub_population			integer[];
	_res_sub_population_attribute	text;
	_check_core_ldsities			integer;
	_check_division_ldsities		integer;
	_main_query						text;
	_main_query_core				text;
	_main_query_division			text;
	_example_for_i_core				text;
	_example_for_i_division			text;
	_query_core_i					text;
	_query_core						text;
	_query_division_i				text;
	_query_division					text;
	_query_core_and_division		text;
	_query_local_densities			text;
	_res							text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata_denominator: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata_denominator: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom;

	if array_length(_target_variable_denom,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
	end if;

	if _target_variable_denom = array[null::int]
	then
		raise exception 'Error: 04: fn_get_user_metadata_denominator: For input argument _id_group = % not exists any target variable denom in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - denominator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
					((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_denom[1]
	into _res_indicator_denom;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - denominator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state or change'')->>''description'' as description,
					((ctv.metadata->''en'')->''state or change'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_denom[1]
	into _res_state_or_change_denom;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - denominator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable_denom[1]
	into _res_unit_denom;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - denominator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 05: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_area_domain_attribute := 'Bez rozlišení.';
		else
			_res_area_domain_attribute := 'Without distinction.';
		end if;
	else
		execute
		'
		select
				cad.description'||_lang_suffix||' as description
		from
				nfiesta_results.c_area_domain as cad
		where
				cad.id = $1
		'
		using _check_area_domain[1]
		into _res_area_domain_attribute;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - denominator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 06: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_sub_population_attribute := 'Bez rozlišení.';
		else
			_res_sub_population_attribute := 'Without distinction.';
		end if;
	else
		execute
		'
		select
				csp.description'||_lang_suffix||' as description
		from
				nfiesta_results.c_sub_population as csp
		where
				csp.id = $1
		'
		using _check_sub_population[1]
		into _res_sub_population_attribute;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable_denom[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2 where w2.local_densities->>'object type label' = 'core'
	into _check_core_ldsities;
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable_denom[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2 where w2.local_densities->>'object type label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	_main_query := concat
	(
	'
	,w1 as	(
			select
					t.local_densities'||_lang_suffix||' as local_densities
			from
					(select
							(ctv.metadata->''cs'')->''local densities'' as local_densities,
							(ctv.metadata->''en'')->''local densities'' as local_densities_en
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id = ',_target_variable_denom[1],'
					) as t
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	'
	);
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	_main_query_core :=
	'
	,w3_core as	(
				select
						row_number () over () as new_id,
						w2.*
				from
						w2 where w2.local_densities->>''object type label'' = ''core''
				)		
	';
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	_main_query_division :=
	'
	,w3_division as	(
					select
							row_number () over () as new_id,
							w2.*
					from
							w2 where w2.local_densities->>''object type label'' = ''division''
					)		
	';	
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- EXAMPLE for i-CORE
	if _lang_suffix = '' -- CS --
	then
		_example_for_i_core :=
		'
		--------------------------------------------------------------------------
		,w4_core_#I_CORE# as	(
								select ''   základní:'' as column_1, w3_core.local_densities->>''description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      objekt:'' as column_1, w3_core.local_densities->>''object description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      verze:'' as column_1, (w3_core.local_densities->''version'')->>''description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      použití záporu:'' as column_1, w3_core.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE#
								)
		--------------------------------------------------------------------------
		,w5_core_#I_CORE# as	(	
								select
										case when t3.new_id_dv = 1 then ''      definiční varianty:'' else null::text end as column_1,
										t3.dv as column_2,
										null::text as column_3
								from
										(
										select
												row_number () over () as new_id_dv,
												t2.dv->>''description'' as dv
										from
												(
												select json_array_elements(t1.dv) as dv from 
												(select case when (w3_core.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''neuvedeno'',''description'',''Neuvedeno.'')))
												else (w3_core.local_densities->''definition variant'') end as dv from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w6_core_#I_CORE# as	(
								select null::text as column_1, ''objekt:'' as column_2, ''omezení:'' as column_3
								)
		--------------------------------------------------------------------------
		,w7_core_#I_CORE# as	(
								select
										case when t3.new_id_adr = 1 then ''      plošná omezení:'' else null::text end as column_1,
										t3.adr_object as column_2,
										t3.adr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_adr,
												(t2.adr->''object'')->>''description'' as adr_object,
												(t2.adr->''restriction'')->>''description'' as adr_restriction
										from
												(
												select json_array_elements(t1.adr) as adr from 
												(select case when (w3_core.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Neuvedeno.''),''restriction'',json_build_object(''description'',''Neuvedeno.''))))
												else (w3_core.local_densities->''area domain restrictions'') end as adr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w8_core_#I_CORE# as	(
								select
										case when t3.new_id_spr = 1 then ''      sub-populační omezení:'' else null::text end as column_1,
										t3.spr_object as column_2,
										t3.spr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_spr,
												(t2.spr->''object'')->>''description'' as spr_object,
												(t2.spr->''restriction'')->>''description'' as spr_restriction
										from
												(
												select json_array_elements(t1.spr) as spr from 
												(select case when (w3_core.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Neuvedeno.''),''restriction'',json_build_object(''description'',''Neuvedeno.''))))
												else (w3_core.local_densities->''sub population restrictions'') end as spr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)			
		--------------------------------------------------------------------------
		,w9_core_#I_CORE# as	(
								select * from w4_core_#I_CORE# union all
								select * from w5_core_#I_CORE# union all
								select * from w6_core_#I_CORE# union all
								select * from w7_core_#I_CORE# union all
								select * from w8_core_#I_CORE#
								)		
		';
	else
		_example_for_i_core :=
		'
		--------------------------------------------------------------------------
		,w4_core_#I_CORE# as	(
								select ''   main:'' as column_1, w3_core.local_densities->>''description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      object:'' as column_1, w3_core.local_densities->>''object description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      version:'' as column_1, (w3_core.local_densities->''version'')->>''description'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE# union all
								select ''      use negative:'' as column_1, w3_core.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_core where w3_core.new_id = #I_CORE#
								)
		--------------------------------------------------------------------------
		,w5_core_#I_CORE# as	(	
								select
										case when t3.new_id_dv = 1 then ''      definition variants:'' else null::text end as column_1,
										t3.dv as column_2,
										null::text as column_3
								from
										(
										select
												row_number () over () as new_id_dv,
												t2.dv->>''description'' as dv
										from
												(
												select json_array_elements(t1.dv) as dv from 
												(select case when (w3_core.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''not specified'',''description'',''Not specified.'')))
												else (w3_core.local_densities->''definition variant'') end as dv from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w6_core_#I_CORE# as	(
								select null::text as column_1, ''object:'' as column_2, ''restriction:'' as column_3
								)
		--------------------------------------------------------------------------
		,w7_core_#I_CORE# as	(
								select
										case when t3.new_id_adr = 1 then ''      area domain restrictions:'' else null::text end as column_1,
										t3.adr_object as column_2,
										t3.adr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_adr,
												(t2.adr->''object'')->>''description'' as adr_object,
												(t2.adr->''restriction'')->>''description'' as adr_restriction
										from
												(
												select json_array_elements(t1.adr) as adr from 
												(select case when (w3_core.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Not specified.''),''restriction'',json_build_object(''description'',''Not specified.''))))
												else (w3_core.local_densities->''area domain restrictions'') end as adr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)
		--------------------------------------------------------------------------
		,w8_core_#I_CORE# as	(
								select
										case when t3.new_id_spr = 1 then ''      sub population restrictions:'' else null::text end as column_1,
										t3.spr_object as column_2,
										t3.spr_restriction as column_3
								from
										(
										select
												row_number () over () as new_id_spr,
												(t2.spr->''object'')->>''description'' as spr_object,
												(t2.spr->''restriction'')->>''description'' as spr_restriction
										from
												(
												select json_array_elements(t1.spr) as spr from 
												(select case when (w3_core.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Not specified.''),''restriction'',json_build_object(''description'',''Not specified.''))))
												else (w3_core.local_densities->''sub population restrictions'') end as spr from w3_core where w3_core.new_id = #I_CORE#) as t1
												) as t2
										) as t3
								)			
		--------------------------------------------------------------------------
		,w9_core_#I_CORE# as	(
								select * from w4_core_#I_CORE# union all
								select * from w5_core_#I_CORE# union all
								select * from w6_core_#I_CORE# union all
								select * from w7_core_#I_CORE# union all
								select * from w8_core_#I_CORE#
								)		
		';	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- EXAMPLE for i-DIVISION
	if _lang_suffix = '' -- CS --
	then
		_example_for_i_division :=
		'
		--------------------------------------------------------------------------
		,w4_division_#I_DIVISION# as	(
										select ''   členící:'' as column_1, w3_division.local_densities->>''description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      objekt:'' as column_1, w3_division.local_densities->>''object description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      verze:'' as column_1, (w3_division.local_densities->''version'')->>''description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      použití záporu:'' as column_1, w3_division.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION#
										)
		--------------------------------------------------------------------------
		,w5_division_#I_DIVISION# as	(	
										select
												case when t3.new_id_dv = 1 then ''      definiční varianty:'' else null::text end as column_1,
												t3.dv as column_2,
												null::text as column_3
										from
												(
												select
														row_number () over () as new_id_dv,
														t2.dv->>''description'' as dv
												from
														(
														select json_array_elements(t1.dv) as dv from 
														(select case when (w3_division.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''neuvedeno.'',''description'',''Neuvedeno.'')))
														else (w3_division.local_densities->''definition variant'') end as dv from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w6_division_#I_DIVISION# as	(
										select null::text as column_1, ''objekt:'' as column_2, ''omezení:'' as column_3
										)
		--------------------------------------------------------------------------
		,w7_division_#I_DIVISION# as	(
										select
												case when t3.new_id_adr = 1 then ''      plošná omezení:'' else null::text end as column_1,
												t3.adr_object as column_2,
												t3.adr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_adr,
														(t2.adr->''object'')->>''description'' as adr_object,
														(t2.adr->''restriction'')->>''description'' as adr_restriction
												from
														(
														select json_array_elements(t1.adr) as adr from 
														(select case when (w3_division.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Neuvedeno.''),''restriction'',json_build_object(''description'',''Neuvedeno.''))))
														else (w3_division.local_densities->''area domain restrictions'') end as adr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w8_division_#I_DIVISION# as	(
										select
												case when t3.new_id_spr = 1 then ''      sub-populační omezení:'' else null::text end as column_1,
												t3.spr_object as column_2,
												t3.spr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_spr,
														(t2.spr->''object'')->>''description'' as spr_object,
														(t2.spr->''restriction'')->>''description'' as spr_restriction
												from
														(
														select json_array_elements(t1.spr) as spr from 
														(select case when (w3_division.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Neuvedeno.''),''restriction'',json_build_object(''description'',''Neuvedeno.''))))
														else (w3_division.local_densities->''sub population restrictions'') end as spr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)			
		--------------------------------------------------------------------------
		,w9_division_#I_DIVISION# as	(
										select * from w4_division_#I_DIVISION# union all
										select * from w5_division_#I_DIVISION# union all
										select * from w6_division_#I_DIVISION# union all
										select * from w7_division_#I_DIVISION# union all
										select * from w8_division_#I_DIVISION#
										)		
		';
	else
		_example_for_i_division :=
		'
		--------------------------------------------------------------------------
		,w4_division_#I_DIVISION# as	(
										select ''   separated:'' as column_1, w3_division.local_densities->>''description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      object:'' as column_1, w3_division.local_densities->>''object description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      version:'' as column_1, (w3_division.local_densities->''version'')->>''description'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION# union all
										select ''      use negative:'' as column_1, w3_division.local_densities->>''use_negative'' as column_2, null::text as column_3 from w3_division where w3_division.new_id = #I_DIVISION#
										)
		--------------------------------------------------------------------------
		,w5_division_#I_DIVISION# as	(	
										select
												case when t3.new_id_dv = 1 then ''      definition variants:'' else null::text end as column_1,
												t3.dv as column_2,
												null::text as column_3
										from
												(
												select
														row_number () over () as new_id_dv,
														t2.dv->>''description'' as dv
												from
														(
														select json_array_elements(t1.dv) as dv from 
														(select case when (w3_division.local_densities->''definition variant'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''label'',''not specified'',''description'',''Not specified.'')))
														else (w3_division.local_densities->''definition variant'') end as dv from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w6_division_#I_DIVISION# as	(
										select null::text as column_1, ''object:'' as column_2, ''restriction:'' as column_3
										)
		--------------------------------------------------------------------------
		,w7_division_#I_DIVISION# as	(
										select
												case when t3.new_id_adr = 1 then ''      area domain restrictions:'' else null::text end as column_1,
												t3.adr_object as column_2,
												t3.adr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_adr,
														(t2.adr->''object'')->>''description'' as adr_object,
														(t2.adr->''restriction'')->>''description'' as adr_restriction
												from
														(
														select json_array_elements(t1.adr) as adr from 
														(select case when (w3_division.local_densities->''area domain restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Not specified.''),''restriction'',json_build_object(''description'',''Not specified.''))))
														else (w3_division.local_densities->''area domain restrictions'') end as adr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)
		--------------------------------------------------------------------------
		,w8_division_#I_DIVISION# as	(
										select
												case when t3.new_id_spr = 1 then ''      sub population restrictions:'' else null::text end as column_1,
												t3.spr_object as column_2,
												t3.spr_restriction as column_3
										from
												(
												select
														row_number () over () as new_id_spr,
														(t2.spr->''object'')->>''description'' as spr_object,
														(t2.spr->''restriction'')->>''description'' as spr_restriction
												from
														(
														select json_array_elements(t1.spr) as spr from 
														(select case when (w3_division.local_densities->''sub population restrictions'')::jsonb = ''null''::jsonb then (select json_agg(json_build_object(''object'',json_build_object(''description'',''Not specified.''),''restriction'',json_build_object(''description'',''Not specified.''))))
														else (w3_division.local_densities->''sub population restrictions'') end as spr from w3_division where w3_division.new_id = #I_DIVISION#) as t1
														) as t2
												) as t3
										)			
		--------------------------------------------------------------------------
		,w9_division_#I_DIVISION# as	(
										select * from w4_division_#I_DIVISION# union all
										select * from w5_division_#I_DIVISION# union all
										select * from w6_division_#I_DIVISION# union all
										select * from w7_division_#I_DIVISION# union all
										select * from w8_division_#I_DIVISION#
										)		
		';	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	for i_core in 1.._check_core_ldsities
	loop
		if i_core = 1
		then
			_query_core_i := replace(_example_for_i_core,'#I_CORE#'::text,i_core::text);
			_query_core := concat('select * from w9_core_',i_core);
		else
			_query_core_i := _query_core_i || replace(_example_for_i_core,'#I_CORE#'::text,i_core::text);
			_query_core := _query_core || concat(' union all select null::text as column_1, null::text as column_2, null::text as column_3 union all select * from w9_core_',i_core);
		end if;
	end loop;

	_query_core := concat(',w10_core as (',_query_core,')');
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities > 0
	then
		for i_division in 1.._check_division_ldsities
		loop
			if i_division = 1
			then
				_query_division_i := replace(_example_for_i_division,'#I_DIVISION#'::text,i_division::text);
				_query_division := concat('select * from w9_division_',i_division);
			else
				_query_division_i := _query_division_i || replace(_example_for_i_division,'#I_DIVISION#'::text,i_division::text);
				_query_division := _query_division || concat(' union all select null::text as column_1, null::text as column_2, null::text as column_3 union all select * from w9_division_',i_division);
			end if;
		end loop;

		_query_division := concat(',w10_division as (',_query_division,')');
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities > 0
	then
		_query_core_and_division :=
		'
		,w02 as	(
				select * from w10_core union all
				select null::text as column_1, null::text as column_2, null::text as column_3 union all
				select * from w10_division
				)';
	else
		_query_core_and_division := ',w02 as (select * from w10_core)';
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities > 0
	then
		_query_local_densities := concat
			(
			_main_query,
			_main_query_core,
			_main_query_division,
			_query_core_i,
			_query_core,
			_query_division_i,
			_query_division,
			_query_core_and_division
			);
	else
		_query_local_densities := concat
			(
			_main_query,
			_main_query_core,
			_query_core_i,
			_query_core,
			_query_core_and_division
			);	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _lang_suffix = '' -- CS
	then
		_res := concat(
		'
		with
		w01 as	(
				select
						''Indikátor:'' as column_1,
						''',_res_indicator_denom,''' as column_2,
						null::text as column_3
				union all
				select
						''Stav nebo změna:'' as column_1,
						''',_res_state_or_change_denom,''' as column_2,
						null::text as column_3
				union all
				select
						''Jednotka indikátoru:'' as column_1,
						''',_res_unit_denom,''' as column_2,
						null::text as column_3
				union all				
				select
						''Plošná atributová členění:'' as column_1,
						''',_res_area_domain_attribute,''' as column_2,
						null::text as column
				union all
				select
						''Populační atributová členění:'' as column_1,
						''',_res_sub_population_attribute,''' as column_2,
						null::text as column
				union all
				select
						null::text as column_1,
						null::text as column_2,
						null::text as column_3
				)
		',_query_local_densities,
		'select
				w01.column_1,
				w01.column_2,
				w01.column_3
		from
				w01
		union all
		select
				''Příspěvky:'' as column_1,
				null::text as column_2,
				null::text as column_3
		union all
		select
				w02.column_1,
				w02.column_2,
				w02.column_3
		from
				w02;
		');		
	else -- EN
		_res := concat(
		'
		with
		w01 as	(
				select
						''Indicator:'' as column_1,
						''',_res_indicator_denom,''' as column_2,
						null::text as column_3
				union all
				select
						''State or change:'' as column_1,
						''',_res_state_or_change_denom,''' as column_2,
						null::text as column_3
				union all
				select
						''Unit of indicator:'' as column_1,
						''',_res_unit_denom,''' as column_2,
						null::text as column_3
				union all					
				select
						''Area domain classifactions:'' as column_1,
						''',_res_area_domain_attribute,''' as column_2,
						null::text as column
				union all
				select
						''Sub population classifications:'' as column_1,
						''',_res_sub_population_attribute,''' as column_2,
						null::text as column
				union all
				select
						null::text as column_1,
						null::text as column_2,
						null::text as column_3
				)
		',_query_local_densities,
		'select
				w01.column_1,
				w01.column_2,
				w01.column_3
		from
				w01
		union all
		select
				''Local densities:'' as column_1,
				null::text as column_2,
				null::text as column_3
		union all
		select
				w02.column_1,
				w02.column_2,
				w02.column_3
		from
				w02;
		');
	end if;

	return query execute ''||_res||'';
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) is
'The function returns metadata (description) for given input ID (that represents target variable for denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) to public;
-- </function>



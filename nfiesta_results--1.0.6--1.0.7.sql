--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------



-------------------------------------------------------------------------------
--	c_gui_header
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_gui_header;

create table nfiesta_results.c_gui_header
(
	id serial not null,
	"header" varchar(100) not null,
	label_en varchar(250) not NULL,
	constraint pkey__c_gui_header primary key (id),
	constraint ukey__c_gui_header__header unique (header),
	constraint ukey__c_gui_header__label_en unique (label_en)
);

comment on table nfiesta_results.c_gui_header is 'Table of gui headers.';
comment on column nfiesta_results.c_gui_header.id is 'Identifier of gui header, primary key.';
comment on column nfiesta_results.c_gui_header.header is 'Name of gui header.';
comment on column nfiesta_results.c_gui_header.label_en is 'English label of gui header.';

comment on constraint pkey__c_gui_header on nfiesta_results.c_gui_header is 'Database identifier, primary key.';
comment on constraint ukey__c_gui_header__header on nfiesta_results.c_gui_header is 'Unique key on column header.';
comment on constraint ukey__c_gui_header__label_en on nfiesta_results.c_gui_header is 'Unique key on column label_en.';

alter table nfiesta_results.c_gui_header owner to adm_nfiesta;
grant all on table nfiesta_results.c_gui_header to adm_nfiesta;

create index idx__c_gui_header__header on nfiesta_results.c_gui_header using btree (header);
comment on index nfiesta_results.idx__c_gui_header__header is
'BTree index on column header';

insert into nfiesta_results.c_gui_header(header,label_en) values
('indicator','indicator'),
('state_or_change','state or change'),
('unit','unit'),
('ldsity_type', 'type of local density contribution'),
('ldsity_object', 'object of local density contribution'),
('ldsity', 'local density contribution'),
('version','version'),
('definition_variant','definition variant'),
('area_domain','area domain'),
('population','population'),
('use_negative','use negative'),
('object','object'),
('restriction','restriction'),
('metadata','metadata'),
('topic','topic'),
('period','period'),
('geographic_region','geographic region'),
('estimate_type','estimate type'),
('unit_of_measurement','unit of measurement'),
('numerator','numerator'),
('denominator','denominator'),
('area_adomain_attribute','area domain attribute'),
('sub_population_attribute','sub-population attribute'),
('core_contributions','core contributions'),
('division_contributions','division contributions'),
('area_domain_restrictions','area domain restrictions'),
('sub_population_restrictions','sub-population restrictions');
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
--	c_gui_header_language
-------------------------------------------------------------------------------

drop table if exists nfiesta_results.c_gui_header_language;

create table nfiesta_results.c_gui_header_language
(
	id serial not null,
	language integer not null,
	gui_header integer NOT NULL,
	label varchar(250) not NULL,
	constraint pkey__c_gui_header_language primary key (id),
	constraint ukey__c_gui_header_language unique (LANGUAGE,gui_header,label)
);

comment on table nfiesta_results.c_gui_header_language is 'Table of gui headers for more foreing languages.';
comment on column nfiesta_results.c_gui_header_language.id is 'Identifier of foreign language gui header, primary key.';
comment on column nfiesta_results.c_gui_header_language.language is 'Foreign key to c_language table.';
comment on column nfiesta_results.c_gui_header_language.gui_header is 'Foreign key to c_gui_header table.';
comment on column nfiesta_results.c_gui_header_language.label is 'Label of gui header.';

comment on constraint pkey__c_gui_header_language on nfiesta_results.c_gui_header_language is 'Database identifier, primary key.';
comment on constraint ukey__c_gui_header_language on nfiesta_results.c_gui_header_language is 'Unique key on columns language, gui_header and label.';


ALTER table nfiesta_results.c_gui_header_language
  ADD CONSTRAINT fkey__c_gui_header_language__c_language FOREIGN KEY (language)
      REFERENCES nfiesta_results.c_language (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__c_gui_header_language__c_language ON nfiesta_results.c_gui_header_language IS 'Foreign key on c_language table.';

ALTER table nfiesta_results.c_gui_header_language
  ADD CONSTRAINT fkey__c_gui_header_language__c_gui_header FOREIGN KEY (gui_header)
      REFERENCES nfiesta_results.c_gui_header (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__c_gui_header_language__c_gui_header ON nfiesta_results.c_gui_header_language IS 'Foreign key on c_gui_header table.';

CREATE INDEX fki__c_gui_header_language__c_language
  ON nfiesta_results.c_gui_header_language
  USING btree
  (language);
COMMENT ON INDEX nfiesta_results.fki__c_gui_header_language__c_language
  IS 'Index over foreign key fkey__c_gui_header_language__c_language';

CREATE INDEX fki__c_gui_header_language__c_gui_header
  ON nfiesta_results.c_gui_header_language
  USING btree
  (gui_header);
COMMENT ON INDEX nfiesta_results.fki__c_gui_header_language__c_gui_header
  IS 'Index over foreign key fkey__c_gui_header_language__c_gui_header';

alter table nfiesta_results.c_gui_header_language owner to adm_nfiesta;
grant all on table nfiesta_results.c_gui_header_language to adm_nfiesta;
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- new columns label_en and description_en in c_estimate_type and c_phase_estimate_type
-------------------------------------------------------------------------------
ALTER TABLE nfiesta_results.c_estimate_type ADD COLUMN label_en character varying(120);
ALTER TABLE nfiesta_results.c_estimate_type ALTER COLUMN label_en SET NOT NULL;
COMMENT ON COLUMN nfiesta_results.c_estimate_type.label_en IS 'English label of estimate type.';

ALTER TABLE nfiesta_results.c_estimate_type ADD COLUMN description_en character varying(120);
ALTER TABLE nfiesta_results.c_estimate_type ALTER COLUMN description_en SET NOT NULL;
COMMENT ON COLUMN nfiesta_results.c_estimate_type.description_en IS 'English description of estimate type.';

ALTER TABLE nfiesta_results.c_phase_estimate_type ADD COLUMN label_en character varying(120);
ALTER TABLE nfiesta_results.c_phase_estimate_type ALTER COLUMN label_en SET NOT NULL;
COMMENT ON COLUMN nfiesta_results.c_phase_estimate_type.label_en IS 'English label of phase estimate type.';

ALTER TABLE nfiesta_results.c_phase_estimate_type ADD COLUMN description_en character varying(120);
ALTER TABLE nfiesta_results.c_phase_estimate_type ALTER COLUMN description_en SET NOT NULL;
COMMENT ON COLUMN nfiesta_results.c_phase_estimate_type.description_en IS 'English description of pahse estimate type.';

ALTER TABLE nfiesta_results.c_estimate_type
ADD CONSTRAINT ukey__c_estimate_type__label_en UNIQUE(label_en);
COMMENT ON CONSTRAINT ukey__c_estimate_type__label_en ON nfiesta_results.c_estimate_type IS 'Unique key on column label_en';

ALTER TABLE nfiesta_results.c_phase_estimate_type
ADD CONSTRAINT ukey__c_phase_estimate_type__label_en UNIQUE(label_en);
COMMENT ON CONSTRAINT ukey__c_phase_estimate_type__label_en ON nfiesta_results.c_phase_estimate_type IS 'Unique key on column label_en';
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------



-- <function name="fn_get_gui_headers" schema="nfiesta_results" src="functions/fn_get_gui_headers.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_gui_headers
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_gui_headers(character varying);

create or replace function nfiesta_results.fn_get_gui_headers(_jlang character varying)
  returns json as
$$
declare
	_clang			integer;
	_res			json;
begin
	----------------------------------
	if _jlang is null
	then
		raise exception 'Error 01: fn_get_gui_headers: Input argument _jlang must not be NULL !';
	end if;
	----------------------------------
	select count(id) from nfiesta_results.c_language where label = _jlang
	into _clang;
	----------------------------------
	if _clang = 0
	then
		raise exception 'Error 02: fn_get_gui_headers: For input argument _jlang = % not exist any record in nfi_results4web.c_language table !', _jlang;
	end if;
	----------------------------------
	if _clang > 1
	then
		raise exception 'Error 03: fn_get_gui_headers: For input argument _jlang = % exists more records then one in nfi_results4web.c_language table !', _jlang;
	end if;

	----------------------------------
	if _jlang = 'en-GB'
	then
		with
		w1 as	(
				select id, "header", label_en from nfiesta_results.c_gui_header
				)
		select json_agg(json_build_object('gui_header',w1.header,'label',w1.label_en)) from w1
		into _res;				
	else
		with
		w1 as	(
				select id, "header", label_en from nfiesta_results.c_gui_header 
				)
		,w2 as	(
				select * from nfiesta_results.c_gui_header_language
				where language = (select id from nfiesta_results.c_language where label = _jlang)
				)
		,w3 as	(
				select w1.id, w1.header, w1.label_en, w2.label from w1 inner join w2
				on w1.id = w2.gui_header
				)
		,w4 as	(
				select w3.id, w3.header, w3.label as label from w3
				)
		select json_agg(json_build_object('gui_header',w4.header,'label',w4.label)) from w4
		into _res;
	end if;
	----------------------------------
	if _res is null
	then
		raise exception 'Error 04: fn_get_gui_headers: Output argument _res is NULL! Not exists any gui header!';
	end if;	
	----------------------------------
	
	return _res;

end;
$$
  language plpgsql stable
  cost 100;

comment on function nfiesta_results.fn_get_gui_headers(character varying) is
'The function returns gui headers for given input language.';

grant execute on function nfiesta_results.fn_get_gui_headers(character varying) to public;
-- </function>



-- <function name="fn_get_user_options_numerator" schema="nfiesta_results" src="functions/fn_get_user_options_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean);

create or replace function nfiesta_results.fn_get_user_options_numerator
(
	_jlang character varying,
	_topic integer default null::integer,
	_num_estimation_period integer default null::integer,
	_num_estimation_cell_collection integer default null::integer,
	_num_id_group integer[] default null::integer[],
	_num_indicator boolean default false,
	_num_state boolean default false,
	_num_unit_of_measure boolean default false,
	_unit_of_measurement boolean default false
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[]
)
language plpgsql
stable
as
$$
declare
	_lang_suffix			character varying(3);
	_lang_covariate			text;
	_check_num_id_group		integer;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	-- TOPIC --
	-----------------------------------------------------------------
	if _topic is null
	then
		return query execute
		'
		with
		w1 as	(
				select
						distinct
						target_variable,
						case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom
				from
						nfiesta_results.t_result_group where web = true
				)
		,w2 as	(
				select
						target_variable,
						case when denominator is null then 0 else denominator end as denominator,
						topic
				from
						nfiesta_results.cm_result2topic
				)
		,w3 as	(
				select distinct w2.topic from w1 inner join w2
				on w1.target_variable = w2.target_variable
				and w1.target_variable_denom = w2.denominator
				)
		,w4 as	(
				select
						id,
						label'||_lang_suffix||' AS label
				from
						nfiesta_results.c_topic
				where
						id in (select w3.topic from w3)
				)
		select
				w4.id as res_id,
				w4.label::text as res_label,
				null::integer[] as res_id_group
		from
				w4 order by w4.label;
		';
	else
		-----------------------------------------------------------------
		-- ESTIMATION_PERIOD --
		-----------------------------------------------------------------
		if _num_estimation_period is null
		then
			return query execute
			'
			with
			w1 as	(
					select
							target_variable,
							case when denominator is null then 0 else denominator end as denominator
					from
							nfiesta_results.cm_result2topic where topic = $1
					)
			,w2 as	(
					select
							target_variable,
							case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
							estimation_period
					from
							nfiesta_results.t_result_group where web = true
					)
			,w3 as	(
					select distinct w2.estimation_period from w2 inner join w1
					on w2.target_variable = w1.target_variable
					and w2.target_variable_denom = w1.denominator
					)
			,w4 as	(
					select
							id,
							label'||_lang_suffix||' AS label
					from
							nfiesta_results.c_estimation_period
					where
							id in (select w3.estimation_period from w3)
					)
			select
					w4.id as res_id,
					w4.label::text as res_label,
					null::integer[] as res_id_group
			from
					w4 order by w4.label;				
			'
			using _topic;
		else
			-----------------------------------------------------------------
			-- ESTIMATION_CELL_COLLECTION --
			-----------------------------------------------------------------
			if _num_estimation_cell_collection is null
			then
				return query execute
				'			
				with
				w1 as	(
						select
								target_variable,
								case when denominator is null then 0 else denominator end as denominator
						from
								nfiesta_results.cm_result2topic where topic = $1
						)
				,w2 as	(
						select
								target_variable,
								case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
								estimation_period,
								estimation_cell_collection
						from
								nfiesta_results.t_result_group where web = true
						)
				,w3 as	(
						select * from w2 inner join w1
						on w2.target_variable = w1.target_variable
						and w2.target_variable_denom = w1.denominator
						)
				,w4 as	(
						select distinct w3.estimation_cell_collection from w3 where w3.estimation_period = $2
						)
				,w5 as	(
						select
								id,
								label'||_lang_suffix||' AS label
						from
								nfiesta_results.c_estimation_cell_collection
						where
								id in (select w4.estimation_cell_collection from w4)
						)
				select
						w5.id as res_id,
						w5.label::text as res_label,
						null::integer[] as res_id_group
				from
						w5 order by w5.label;
				'
				using _topic, _num_estimation_period;
			else
				-----------------------------------------------------------------
				-- INDICATOR --
				-----------------------------------------------------------------
				if _num_indicator = false
				then
					return query execute
					'
					with
					w1 as	(
							select
									target_variable,
									case when denominator is null then 0 else denominator end as denominator
							from
									nfiesta_results.cm_result2topic where topic = $1
							)
					,w2 as	(
							select
									id,
									target_variable,
									case when target_variable_denom is null then 0 else target_variable_denom end as target_variable_denom,
									estimation_period,
									estimation_cell_collection
							from
									nfiesta_results.t_result_group where web = true
							)
					,w3 as	(
							select w2.* from w2 inner join w1
							on w2.target_variable = w1.target_variable
							and w2.target_variable_denom = w1.denominator
							)
					,w4 as	(
							select w3.* from w3
							where w3.estimation_period = $2
							and w3.estimation_cell_collection = $3
							)
					,w5 as	(
							select
									target_variable,
									array_agg(w4.id order by w4.id) as id
							from
									w4 group by target_variable
							)
					,w6 as	(
							select
									w5.*,
									((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
									((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
							from
									w5
									inner join nfiesta_results.c_target_variable as ctv
									on w5.target_variable = ctv.id
							)
					,w7 as	(
							select
									w6.id,
									label'||_lang_suffix||' AS label
							from
									w6
							)
					,w8 as	(
							select
									w7.label,
									unnest(w7.id) as id
							from
									w7
							)
					,w9 as	(
							select w8.label, array_agg(w8.id) as id from w8 group by w8.label
							)
					select
							null::integer as res_id,
							w9.label as res_label,
							w9.id as res_id_group
					from
							w9 order by w9.label;
					'
					using _topic, _num_estimation_period, _num_estimation_cell_collection;
				else
					-----------------------------------------------------------------
					-- NUM_STATE --
					-----------------------------------------------------------------
					if _num_state = false
					then
						return query execute
						'
						with
						w1 as	(
								select * from nfiesta_results.t_result_group as trg
								where trg.id in (select unnest($1))
								and trg.web = true
								)
						,w2 as	(
								select
										target_variable,
										array_agg(w1.id order by w1.id) as id
								from
										w1 group by target_variable
								)
						,w3 as	(
								select
										w2.*,
										((ctv.metadata->''cs'')->''state_or_change'')->>''label'' as label,
										((ctv.metadata->''en'')->''state_or_change'')->>''label'' as label_en
								from
										w2
										inner join nfiesta_results.c_target_variable as ctv
										on w2.target_variable = ctv.id
								)
						,w4 as	(
								select
										w3.id,
										label'||_lang_suffix||' AS label
								from
										w3
								)
						,w5 as	(
								select
										w4.label,
										unnest(w4.id) as id
								from
										w4
								)
						,w6 as	(
								select w5.label, array_agg(w5.id) as id from w5 group by w5.label
								)
						select
								null::integer as res_id,
								w6.label as res_label,
								w6.id as res_id_group
						from
								w6 order by w6.label;
						'
						using _num_id_group;					
					else
						-----------------------------------------------------------------
						-- NUM_UNIT_OF_MEASURE --
						-----------------------------------------------------------------
						if _num_unit_of_measure = false
						then
							return query execute
							'
							with
							w1 as	(
									select * from nfiesta_results.t_result_group as trg
									where trg.id in (select unnest($1))
									and trg.web = true
									)
							,w2 as	(
									select
											target_variable,
											array_agg(w1.id order by w1.id) as id
									from
											w1 group by target_variable
									)
							,w3 as	(
									select
											w2.*,
											((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
											((ctv.metadata->''en'')->''unit'')->>''label'' as label_en
									from
											w2
											inner join nfiesta_results.c_target_variable as ctv
											on w2.target_variable = ctv.id
									)
							,w4 as	(
									select
											w3.id,
											label'||_lang_suffix||' AS label
									from
											w3
									)
							,w5 as	(
									select
											w4.label,
											unnest(w4.id) as id
									from
											w4
									)
							,w6 as	(
									select w5.label, array_agg(w5.id) as id from w5 group by w5.label
									)
							select
									null::integer as res_id,
									w6.label as res_label,
									w6.id as res_id_group
							from
									w6 order by w6.label;
							'
							using _num_id_group;
						else
							-----------------------------------------------------------------
							-- UNIT_OF_MEASUREMENT --
							-----------------------------------------------------------------
							if _unit_of_measurement = false
							then			
								select
										count(t.target_variable)
								from
										(
										select distinct trg.target_variable from nfiesta_results.t_result_group as trg
										where trg.id in (select unnest(_num_id_group))
										) as t
								into _check_num_id_group;

								if _check_num_id_group is distinct from 1
								then
									raise exception 'Error 01: fn_get_user_options_numerator: The values in input argument _num_id_group = % must only be for the same target variable!',_num_id_group;
								end if;

								return query execute
								'
								with
								w1 as	(
										select
												id,
												target_variable,
												coalesce(target_variable_denom,0) as target_variable_denom 
										from
												nfiesta_results.t_result_group
										where
												id in (select unnest($1))
										and
												web = true
										)
								,w2 as	(
										select
												w1.target_variable,
												w1.target_variable_denom,
												array_agg(w1.id order by w1.id) as id
										from
												w1 group by w1.target_variable, w1.target_variable_denom
										)
								,w3 as	(
										select
												w2.*,
												((ctv1.metadata->''cs'')->''unit'')->>''label'' as unit_nom,
												((ctv2.metadata->''cs'')->''unit'')->>''label'' as unit_denom,
												((ctv1.metadata->''en'')->''unit'')->>''label'' as unit_nom_en,
												((ctv2.metadata->''en'')->''unit'')->>''label'' as unit_denom_en
										from
												w2
												inner join nfiesta_results.c_target_variable as ctv1 on w2.target_variable = ctv1.id
												left join nfiesta_results.c_target_variable as ctv2 on w2.target_variable_denom = ctv2.id
										)
								,w4 as	(
										select
												w3.target_variable,
												w3.target_variable_denom,
												w3.id,
												w3.unit_nom'||_lang_suffix||' AS unit_nom,
												w3.unit_denom'||_lang_suffix||' AS unit_denom
										from
												w3
										)
								,w5 as	(
										select
												w4.*,
												case
													when (w4.target_variable = w4.target_variable_denom) or (w4.unit_nom = w4.unit_denom) then ''%''
													when w4.target_variable_denom = 0 then w4.unit_nom
													else concat(w4.unit_nom,'' / '',w4.unit_denom)
												end as label4user,
												------------------
												case
													when w4.target_variable_denom = 0 then 0
													else 1
												end as res_id
										from
												w4
										)
								,w6 as	(
										select
												w5.res_id,
												w5.label4user as label,
												unnest(w5.id) as id
										from
												w5
										)
								,w7 as	(
										select
												w6.res_id,
												w6.label,
												array_agg(w6.id) as id
										from
												w6 group by w6.res_id, w6.label
										)
								select
										w7.res_id,
										w7.label as res_label,
										w7.id as res_id_group
								from
										w7 order by w7.label;
								'
								using _num_id_group;																	
							else
								raise exception 'Error 02: fn_get_user_options_numerator: This variant of function is not implemented !';
							end if;
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean) is
'The function returns list of available IDs and labels of topic, estimation period, estimation cell collection, indicator, state or change, unit of indicator and unit of measurement from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator(character varying, integer, integer, integer, integer[], boolean, boolean, boolean, boolean) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_ldsity_core" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_ldsity_core.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_ldsity_core
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_ldsity_core
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix						character varying(3);
		_not_specified_version_label		text;
		_not_specified_version_description	text;
		_not_specified_defintion_variant	json;
		_not_specified_adr_spr				json;
begin	
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_numerator_ldsity_core: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ'
		then
			_not_specified_version_label = 'neuvedeno'::text;
			_not_specified_version_description = 'Neuvedeno.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
		end if;
	
		if _jlang = 'en-GB'
		then
			_not_specified_version_label := 'not specified'::text;
			_not_specified_version_description = 'Not specified.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
		end if;
		-----------------------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.local_densities'||_lang_suffix||' AS local_densities
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local_densities'') as local_densities,
								((ctv.metadata->''en'')->''local_densities'') as local_densities_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where (w4.local_densities->''ldsity_type'')->>''label'' = ''core''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						(w5.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w5.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w5.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w5.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w5.local_densities->''ldsity'')->>''label'' as label,
						(w5.local_densities->''ldsity'')->>''description'' as description,	
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition_variant'' as definition_variant,
						w5.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w5
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w7.object_type_label,''description'',w7.object_type_description),
							''ldsity_object'',json_build_object(''label'',w7.object_label,''description'',w7.object_description),
							''ldsity'',json_build_object(''label'',w7.label,''description'',w7.description),
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition_variant'',w7.definition_variant,
							''area_domain_restrictions'',w7.area_domain_restrictions,
							''sub_population_restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						--w8.new_id,
						json_agg(w8.local_densities order by w8.new_id) as local_densities
				from
						w8 group by w8.target_variable --, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]) is
'The function returns list of available labels of standart local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_ldsity_core(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_sub_population" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_sub_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_sub_population
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_sub_population(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_sub_population
(
	_jlang character varying,
	_id_group integer[],
	_sub_population integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_sub_population integer[],
	res_division boolean
)
as
$$
declare
	_lang_suffix							character varying(3);
	_target_variable_num					integer[];
	_check_records							integer;

	_count_target_variable					integer;
	_count_target_variable_with_core		integer;
	_count_target_variable_with_division	integer;
	_res_division							boolean;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_numerator_sub_population: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same target variable numerator!',_id_group;
	end if;
	-----------------------------------------------------------------
	if _sub_population is null -- first calling of function
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)	
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17a as	(
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							array[w16.atomic_type] as res_sub_population
					from
							w16
					)
		,w17 as	(
				select
						w17a.res_id,
						w17a.res_label,
						w17a.res_id_group,
						w17a.res_sub_population,
						null::boolean as res_division
				from
						w17a
				)
		-----------------------------------------
		,w18 as	(
				select
						a.*,
						b.target_variable
				from
						(
						select
								w17.res_id,
								w17.res_label,
								w17.res_sub_population,
								unnest(w17.res_id_group) as res_id_group
						from
								w17 where w17.res_id = 0
						) as a
				inner join nfiesta_results.t_result_group as b on a.res_id_group = b.id
				)
				--select * from w18;
		,w19 as	(
				select
						ctv.id,
						(ctv.metadata->''en'')->''local_densities'' as local_densities
				from
						nfiesta_results.c_target_variable as ctv where ctv.id in (select distinct w18.target_variable from w18)
				)
		,w20 as	(
				select
						w19.id,
						json_array_elements(w19.local_densities) as local_densities
				from
						w19
				)
		,w21 as	(
				select
						w20.id,
						(w20.local_densities->''ldsity_type'')->>''label'' as object_type_label
				from
						w20
				)
		,w22 as	(
				select w18.* from w18 where w18.target_variable in (select w21.id from w21 where w21.object_type_label = ''division'')
				)
		,w23 as	(
				select w18.* from w18 where w18.target_variable not in (select distinct w22.target_variable from w22) -- TARGET VARIABLE WITHOUT DIVISION
				)
		,w24a as	(
					select
							w23.res_id,
							w23.res_label,
							w23.res_sub_population,
							array_agg(res_id_group) as res_id_group
					from
							w23 group by w23.res_id, w23.res_label, w23.res_sub_population
					)
		,w24 as	(
				select
						w24a.res_id,
						w24a.res_label,
						w24a.res_sub_population,
						w24a.res_id_group,
						null::boolean as res_division
				from
						w24a
				)
				select w24.res_id, w24.res_label, w24.res_id_group, w24.res_sub_population, w24.res_division from w24 union all
				select w17.res_id, w17.res_label, w17.res_id_group, w17.res_sub_population, w17.res_division from w17 where w17.res_id is distinct from 0;
		'
		using _id_group;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as sub_population,
								t.id
						from
								(
								select w11.sub_population, w11.id, w11.atomic_type from w11 union all
								select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_sub_population
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _sub_population
		into _check_records;	
		
		if _check_records = 0
		then
			-------------------------------------
			-- input _id_group must be for the same group of target variables:
			-- variant 1 => target variables without division ldsity object(s)
			-- variant 2 => target variables with core ldsity object and with division ldsity object(s)
			-------------------------------------
			with
			w1 as	(
					select
							ctv.id,
							(ctv.metadata->'en')->'local_densities' as local_densities
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id in (select target_variable from nfiesta_results.t_result_group trg where id in (select unnest(_id_group)))
					)
			,w2 as	(
					select
							w1.id,
							json_array_elements(w1.local_densities) as local_densities
					from
							w1
					)
			,w3 as	(
					select
							w2.id,
							(w2.local_densities->'ldsity_type')->>'label' as object_type_label
					from
							w2
					)
			-------------------------------
			,w4a as	(select distinct w3.id from w3)
			,w4b as	(select distinct w3.id from w3 where w3.object_type_label = 'core')
			,w4c as	(select distinct w3.id from w3 where w3.object_type_label = 'division')
			-------------------------------
			,w5a as	(select 1 as id, count(w4a.*) as count_target_variable from w4a)
			,w5b as	(select 1 as id, count(w4b.*) as count_target_variable_with_core from w4b)
			,w5c as	(select 1 as id, count(w4c.*) as count_target_variable_with_division from w4c)
			-------------------------------
			select
					w5a.count_target_variable,
					w5b.count_target_variable_with_core,
					w5c.count_target_variable_with_division
			from
					w5a
					inner join w5b on w5a.id = w5b.id
					inner join w5c on w5a.id = w5c.id
			into
					_count_target_variable,
					_count_target_variable_with_core,
					_count_target_variable_with_division;
			-------------------------------------
			-------------------------------------
			if _sub_population = array[0] -- first user selection was without distinction
			then
				if _count_target_variable_with_division is distinct from 0
				then
					raise exception 'Error: 03: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then the values in input argument _id_group = % must only be for target variables that are without any division local density!',_id_group;
				end if;

				-- variant 1
				if _count_target_variable is distinct from _count_target_variable_with_core
				then
					raise exception 'Error: 04: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						_sub_population as res_sub_population,
						false as res_division; 
			else
				-- second and more user selections

				if _count_target_variable_with_division = 0
				then
					-- variant 1 => not exists any target variable with division
					if _count_target_variable is distinct from _count_target_variable_with_core
					then
						raise exception 'Error: 05: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
					end if;

					_res_division := false;
				else
					-- variant 2 => exist target variable with any division
					if	(_count_target_variable is distinct from _count_target_variable_with_division)
						or
						(_count_target_variable_with_core is distinct from _count_target_variable_with_division)
					then
						raise exception 'Error: 06: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables must contains some division local density!',_id_group;
					end if;

					_res_division := true;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						array_remove(_sub_population,0) as res_sub_population,
						_res_division as res_division;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.sub_population] as sub_population,
							t.id
					from
							(
							select w1.sub_population, array_agg(w1.id order by w1.id) as id
							from w1 where w1.sub_population = 0
							group by w1.sub_population
							) as t
					)
			,w3 as	(
					select w1.sub_population, array_agg(w1.id order by w1.id) as id
					from w1 where w1.sub_population > 0
					group by w1.sub_population
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_sub_population as csp
							on w3.sub_population = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_sub_population_category as cmspc
					where cmspc.sub_population_category in
						(
						select cspc.id from nfiesta_results.c_sub_population_category as cspc
						where cspc.sub_population in (select w6.sub_population from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.sub_population as sub_population,
							cspc2.sub_population as atomic_type
					from
							w7
							inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
							inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.sub_population from w8
					)
			,w10 as (
					select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.sub_population
					)
			,w11 as (
					select
							w6.sub_population,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.sub_population = w10.sub_population
					)
			,w12 as	(
					select
							w11.sub_population,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.sub_population,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.sub_population,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
							from	(
									select distinct a1.atomic_type, a1.sub_population
									from
										(
										select w13.atomic_type, w13.sub_population from w13
										union all
										select w5.sub_population as atomic_type, w5.sub_population from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as sub_population,
									null::integer[] as sub_population,
									t.id
							from
									(
									select w11.sub_population, w11.id, w11.atomic_type from w11 union all
									select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
					)
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_sub_population,
							null::boolean as res_division
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _sub_population;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_numerator_sub_population(character varying, integer[], integer[]) is
'The function returns list of available sub populations for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of sub populations.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_sub_population(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_numerator_ldsity_division" schema="nfiesta_results" src="functions/fn_get_user_options_numerator_ldsity_division.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_numerator_ldsity_division
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_numerator_ldsity_division(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_numerator_ldsity_division
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix						character varying(3);
		_not_specified_version_label		text;
		_not_specified_version_description	text;
		_not_specified_defintion_variant	json;
		_not_specified_adr_spr				json;		
begin	
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_numerator_ldsity_division: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ'
		then
			_not_specified_version_label = 'neuvedeno'::text;
			_not_specified_version_description = 'Neuvedeno.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
		end if;
	
		if _jlang = 'en-GB'
		then
			_not_specified_version_label := 'not specified'::text;
			_not_specified_version_description = 'Not specified.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
		end if;
		-----------------------------------------------------------------		
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.local_densities'||_lang_suffix||' AS local_densities
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local_densities'') as local_densities,
								((ctv.metadata->''en'')->''local_densities'') as local_densities_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where (w4.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						(w5.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w5.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w5.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w5.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w5.local_densities->''ldsity'')->>''label'' as label,
						(w5.local_densities->''ldsity'')->>''description'' as description,	
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition_variant'' as definition_variant,
						w5.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w5						
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w7.object_type_label,''description'',w7.object_type_description),
							''ldsity_object'',json_build_object(''label'',w7.object_label,''description'',w7.object_description),
							''ldsity'',json_build_object(''label'',w7.label,''description'',w7.description),							
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition_variant'',w7.definition_variant,
							''area_domain_restrictions'',w7.area_domain_restrictions,
							''sub_population_restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						--w8.new_id,
						json_agg(w8.local_densities order by w8.new_id) as local_densities
				from
						w8 group by w8.target_variable --, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_numerator_ldsity_division(character varying, integer[]) is
'The function returns list of available labels of division local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_numerator_ldsity_division(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_ldsity_core" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_ldsity_core.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_ldsity_core
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_ldsity_core(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_ldsity_core
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix						character varying(3);
		_not_specified_version_label		text;
		_not_specified_version_description	text;
		_not_specified_defintion_variant	json;
		_not_specified_adr_spr				json;		
begin	
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_denominator_ldsity_core: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ'
		then
			_not_specified_version_label = 'neuvedeno'::text;
			_not_specified_version_description = 'Neuvedeno.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
		end if;
	
		if _jlang = 'en-GB'
		then
			_not_specified_version_label := 'not specified'::text;
			_not_specified_version_description = 'Not specified.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
		end if;
		-----------------------------------------------------------------		
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable_denom as target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.local_densities'||_lang_suffix||' AS local_densities
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local_densities'') as local_densities,
								((ctv.metadata->''en'')->''local_densities'') as local_densities_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where (w4.local_densities->''ldsity_type'')->>''label'' = ''core''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						(w5.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w5.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w5.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w5.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w5.local_densities->''ldsity'')->>''label'' as label,
						(w5.local_densities->''ldsity'')->>''description'' as description,	
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition_variant'' as definition_variant,
						w5.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w5						
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w7.object_type_label,''description'',w7.object_type_description),
							''ldsity_object'',json_build_object(''label'',w7.object_label,''description'',w7.object_description),
							''ldsity'',json_build_object(''label'',w7.label,''description'',w7.description),							
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition_variant'',w7.definition_variant,
							''area_domain_restrictions'',w7.area_domain_restrictions,
							''sub_population_restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						--w8.new_id,
						json_agg(w8.local_densities order by w8.new_id) as local_densities
				from
						w8 group by w8.target_variable --, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator_ldsity_core(character varying, integer[]) is
'The function returns list of available labels of standart local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_ldsity_core(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_sub_population" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_sub_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_sub_population
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_sub_population
(
	_jlang character varying,
	_id_group integer[],
	_sub_population integer[] default null::integer[]
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[],
	res_sub_population integer[],
	res_division boolean
)
as
$$
declare
	_lang_suffix							character varying(3);
	_target_variable_denom					integer[];
	_check_records							integer;

	_count_target_variable					integer;
	_count_target_variable_with_core		integer;
	_count_target_variable_with_division	integer;
	_res_division							boolean;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_denominator_sub_population: Input argument _id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom;

	if array_length(_target_variable_denom,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_denominator_sub_population: The values in input argument _id_group = % must only be for the same target variable denominator!',_id_group;
	end if;
	-----------------------------------------------------------------
	if _sub_population is null -- first calling of function
	then
		return query execute
		'
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)	
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17a as	(
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							array[w16.atomic_type] as res_sub_population
					from
							w16
					)
		,w17 as	(
				select
						w17a.res_id,
						w17a.res_label,
						w17a.res_id_group,
						w17a.res_sub_population,
						null::boolean as res_division
				from
						w17a
				)
		-----------------------------------------
		,w18 as	(
				select
						a.*,
						b.target_variable
				from
						(
						select
								w17.res_id,
								w17.res_label,
								w17.res_sub_population,
								unnest(w17.res_id_group) as res_id_group
						from
								w17 where w17.res_id = 0
						) as a
				inner join nfiesta_results.t_result_group as b on a.res_id_group = b.id
				)
		,w19 as	(
				select
						ctv.id,
						(ctv.metadata->''en'')->''local_densities'' as local_densities
				from
						nfiesta_results.c_target_variable as ctv where ctv.id in (select distinct w18.target_variable from w18)
				)
		,w20 as	(
				select
						w19.id,
						json_array_elements(w19.local_densities) as local_densities
				from
						w19
				)
		,w21 as	(
				select
						w20.id,
						(w20.local_densities->''ldsity_type'')->>''label'' as object_type_label
				from
						w20
				)
		,w22 as	(
				select w18.* from w18 where w18.target_variable in (select w21.id from w21 where w21.object_type_label = ''division'')
				)
		,w23 as	(
				select w18.* from w18 where w18.target_variable not in (select distinct w22.target_variable from w22) -- TARGET VARIABLE WITHOUT DIVISION
				)
		,w24a as	(
					select
							w23.res_id,
							w23.res_label,
							w23.res_sub_population,
							array_agg(res_id_group) as res_id_group
					from
							w23 group by w23.res_id, w23.res_label, w23.res_sub_population
					)
		,w24 as	(
				select
						w24a.res_id,
						w24a.res_label,
						w24a.res_sub_population,
						w24a.res_id_group,
						null::boolean as res_division
				from
						w24a
				)
				select w24.res_id, w24.res_label, w24.res_id_group, w24.res_sub_population, w24.res_division from w24 union all
				select w17.res_id, w17.res_label, w17.res_id_group, w17.res_sub_population, w17.res_division from w17 where w17.res_id is distinct from 0;
		'
		using _id_group;
	else
		execute
		'		
		with
		w1 as	(
				select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
				from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						0 as atomic_type,
						array[t.sub_population] as sub_population,
						t.id
				from
						(
						select w1.sub_population, array_agg(w1.id order by w1.id) as id
						from w1 where w1.sub_population = 0
						group by w1.sub_population
						) as t
				)
		,w3 as	(
				select w1.sub_population, array_agg(w1.id order by w1.id) as id
				from w1 where w1.sub_population > 0
				group by w1.sub_population
				)
		,w4 as	(
				select
						w3.*,
						csp.atomic
				from 
						w3
						inner join nfiesta_results.c_sub_population as csp
						on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.*
				from
						w4 where w4.atomic = true  
				)
				,w5a as	(
						select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
						)
		,w6 as	(
				select w4.* from w4 where w4.atomic = false
				)
		,w7 as	(
				select * from nfiesta_results.cm_sub_population_category as cmspc
				where cmspc.sub_population_category in
					(
					select cspc.id from nfiesta_results.c_sub_population_category as cspc
					where cspc.sub_population in (select w6.sub_population from w6)
					)
				)
		,w8 as	(
				select
						w7.*,
						cspc1.sub_population as sub_population,
						cspc2.sub_population as atomic_type
				from
						w7
						inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
						inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
				)
		,w9 as	(
				select distinct w8.atomic_type, w8.sub_population from w8
				)
		,w10 as (
				select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
				from w9 group by w9.sub_population
				)
		,w11 as (
				select
						w6.sub_population,
						w6.id,
						w10.atomic_type
				from
						w6 inner join w10 on w6.sub_population = w10.sub_population
				)
		,w12 as	(
				select
						w11.sub_population,
						unnest(w11.atomic_type) as atomic_type,
						w11.id
				from
						w11
				)
		,w13 as	(
				select
						w12.sub_population,
						w12.atomic_type,
						unnest(w12.id) as id
				from
						w12
				)
		,w14 as	(
				select
						t1.atomic_type,
						t1.sub_population,
						t2.id
				from		
						(
						select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
						from	(
								select distinct a1.atomic_type, a1.sub_population
								from
									(
									select w13.atomic_type, w13.sub_population from w13
									union all
									select w5.sub_population as atomic_type, w5.sub_population from w5
									) as a1
								) as a2
						group by a2.atomic_type
						) as t1
				inner join
						(
						select b3.atomic_type, array_agg(b3.id order by b3.id) as id
						from	(
								select distinct b2.atomic_type, b2.id
								from
									(
									select w13.atomic_type, w13.id from w13 union all
									select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
									) as b2
								) as b3
						group by b3.atomic_type
						) as t2
				on t1.atomic_type = t2.atomic_type
				)
				,w_0 as	(
						-----------------------------------------------------
						select
								0 as atomic_type,
								null::integer[] as sub_population,
								t.id
						from
								(
								select w11.sub_population, w11.id, w11.atomic_type from w11 union all
								select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
								) as t
						where nfiesta_results.fn_array_compare(t.atomic_type,$2)
						-----------------------------------------------------
						)
		,w15 as	(
				select w2.* from w2 union all
				select w14.* from w14 union all
				select w_0.* from w_0
				)
		,w16 as	(
				select
						w15.*,
						case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
						case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
				from
						w15
						left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
				)
		,w17 as	(
				select
						w16.atomic_type as res_id,
						(w16.label'||_lang_suffix||')::text AS res_label,
						w16.id as res_id_group,
						$2 || array[w16.atomic_type] as res_sub_population
				from
						w16
						
						where w16.atomic_type not in (select unnest($2))
				)
		select count(w17.*) from w17
		'
		using _id_group, _sub_population
		into _check_records;	
		
		if _check_records = 0
		then
			-------------------------------------
			-- input _id_group must be for the same group of target variables:
			-- variant 1 => target variables without division ldsity object(s)
			-- variant 2 => target variables with core ldsity object and with division ldsity object(s)
			-------------------------------------
			with
			w1 as	(
					select
							ctv.id,
							(ctv.metadata->'en')->'local_densities' as local_densities
					from
							nfiesta_results.c_target_variable as ctv
					where
							ctv.id in (select target_variable from nfiesta_results.t_result_group trg where id in (select unnest(_id_group)))
					)
			,w2 as	(
					select
							w1.id,
							json_array_elements(w1.local_densities) as local_densities
					from
							w1
					)
			,w3 as	(
					select
							w2.id,
							(w2.local_densities->'ldsity_type')->>'label' as object_type_label
					from
							w2
					)
			-------------------------------
			,w4a as	(select distinct w3.id from w3)
			,w4b as	(select distinct w3.id from w3 where w3.object_type_label = 'core')
			,w4c as	(select distinct w3.id from w3 where w3.object_type_label = 'division')
			-------------------------------
			,w5a as	(select 1 as id, count(w4a.*) as count_target_variable from w4a)
			,w5b as	(select 1 as id, count(w4b.*) as count_target_variable_with_core from w4b)
			,w5c as	(select 1 as id, count(w4c.*) as count_target_variable_with_division from w4c)
			-------------------------------
			select
					w5a.count_target_variable,
					w5b.count_target_variable_with_core,
					w5c.count_target_variable_with_division
			from
					w5a
					inner join w5b on w5a.id = w5b.id
					inner join w5c on w5a.id = w5c.id
			into
					_count_target_variable,
					_count_target_variable_with_core,
					_count_target_variable_with_division;
			-------------------------------------
			-------------------------------------
			if _sub_population = array[0] -- first user selection was without distinction
			then
				if _count_target_variable_with_division is distinct from 0
				then
					raise exception 'Error: 03: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then the values in input argument _id_group = % must only be for target variables that are without any division local density!',_id_group;
				end if;

				-- variant 1
				if _count_target_variable is distinct from _count_target_variable_with_core
				then
					raise exception 'Error: 04: fn_get_user_options_numerator_sub_population: If first user selection is without distinction then The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						_sub_population as res_sub_population,
						false as res_division; 
			else
				-- second and more user selections

				if _count_target_variable_with_division = 0
				then
					-- variant 1 => not exists any target variable with division
					if _count_target_variable is distinct from _count_target_variable_with_core
					then
						raise exception 'Error: 05: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables without any division local density!',_id_group;
					end if;

					_res_division := false;
				else
					-- variant 2 => exist target variable with any division
					if	(_count_target_variable is distinct from _count_target_variable_with_division)
						or
						(_count_target_variable_with_core is distinct from _count_target_variable_with_division)
					then
						raise exception 'Error: 06: fn_get_user_options_numerator_sub_population: The values in input argument _id_group = % must only be for the same group of target variables => all target variables must contains some division local density!',_id_group;
					end if;

					_res_division := true;
				end if;

				return query
				select
						null::integer as res_id,
						null::text as res_label,
						_id_group as res_id_group,
						array_remove(_sub_population,0) as res_sub_population,
						_res_division as res_division;
			end if;
		else
			return query execute
			'		
			with
			w1 as	(
					select trg.id, case when trg.sub_population_denom is null then 0 else trg.sub_population_denom end as sub_population
					from nfiesta_results.t_result_group as trg
					where trg.id in (select unnest($1))
					and trg.web = true
					)
			,w2 as	(
					select
							0 as atomic_type,
							array[t.sub_population] as sub_population,
							t.id
					from
							(
							select w1.sub_population, array_agg(w1.id order by w1.id) as id
							from w1 where w1.sub_population = 0
							group by w1.sub_population
							) as t
					)
			,w3 as	(
					select w1.sub_population, array_agg(w1.id order by w1.id) as id
					from w1 where w1.sub_population > 0
					group by w1.sub_population
					)
			,w4 as	(
					select
							w3.*,
							csp.atomic
					from 
							w3
							inner join nfiesta_results.c_sub_population as csp
							on w3.sub_population = csp.id
					)
			,w5 as	(
					select
							w4.*
					from
							w4 where w4.atomic = true  
					)
					,w5a as	(
							select w5.sub_population, array[w5.sub_population] as atomic_type, w5.id from w5
							)
			,w6 as	(
					select w4.* from w4 where w4.atomic = false
					)
			,w7 as	(
					select * from nfiesta_results.cm_sub_population_category as cmspc
					where cmspc.sub_population_category in
						(
						select cspc.id from nfiesta_results.c_sub_population_category as cspc
						where cspc.sub_population in (select w6.sub_population from w6)
						)
					)
			,w8 as	(
					select
							w7.*,
							cspc1.sub_population as sub_population,
							cspc2.sub_population as atomic_type
					from
							w7
							inner join nfiesta_results.c_sub_population_category as cspc1 on w7.sub_population_category = cspc1.id
							inner join nfiesta_results.c_sub_population_category as cspc2 on w7.atomic_category = cspc2.id
					)
			,w9 as	(
					select distinct w8.atomic_type, w8.sub_population from w8
					)
			,w10 as (
					select w9.sub_population, array_agg(w9.atomic_type order by w9.atomic_type) as atomic_type
					from w9 group by w9.sub_population
					)
			,w11 as (
					select
							w6.sub_population,
							w6.id,
							w10.atomic_type
					from
							w6 inner join w10 on w6.sub_population = w10.sub_population
					)
			,w12 as	(
					select
							w11.sub_population,
							unnest(w11.atomic_type) as atomic_type,
							w11.id
					from
							w11
					)
			,w13 as	(
					select
							w12.sub_population,
							w12.atomic_type,
							unnest(w12.id) as id
					from
							w12
					)
			,w14 as	(
					select
							t1.atomic_type,
							t1.sub_population,
							t2.id
					from		
							(
							select a2.atomic_type, array_agg(a2.sub_population order by a2.sub_population) as sub_population
							from	(
									select distinct a1.atomic_type, a1.sub_population
									from
										(
										select w13.atomic_type, w13.sub_population from w13
										union all
										select w5.sub_population as atomic_type, w5.sub_population from w5
										) as a1
									) as a2
							group by a2.atomic_type
							) as t1
					inner join
							(
							select b3.atomic_type, array_agg(b3.id order by b3.id) as id
							from	(
									select distinct b2.atomic_type, b2.id
									from
										(
										select w13.atomic_type, w13.id from w13 union all
										select b1.atomic_type, b1.id from (select w5.sub_population as atomic_type, unnest(w5.id) as id from w5) as b1
										) as b2
									) as b3
							group by b3.atomic_type
							) as t2
					on t1.atomic_type = t2.atomic_type
					)
					,w_0 as	(
							-----------------------------------------------------
							select
									0 as atomic_type,
									--t.atomic_type as sub_population,
									null::integer[] as sub_population,
									t.id
							from
									(
									select w11.sub_population, w11.id, w11.atomic_type from w11 union all
									select w5a.sub_population, w5a.id, w5a.atomic_type from w5a
									) as t
							where nfiesta_results.fn_array_compare(t.atomic_type,$2)
							-----------------------------------------------------
							)
			,w15 as	(
					select w2.* from w2 union all
					select w14.* from w14 union all
					select w_0.* from w_0
					)
			,w16 as	(
					select
							w15.*,
							case when csp.label is null then ''bez rozliseni'' else csp.label end as label,
							case when csp.label_en is null then ''without distinction'' else csp.label_en end as label_en
					from
							w15
							left join nfiesta_results.c_sub_population as csp on w15.atomic_type = csp.id
					)
					select
							w16.atomic_type as res_id,
							(w16.label'||_lang_suffix||')::text AS res_label,
							w16.id as res_id_group,
							$2 || array[w16.atomic_type] as res_sub_population,
							null::boolean as res_division
					from
							w16
							
							where w16.atomic_type not in (select unnest($2));
			'
			using _id_group, _sub_population;		
		end if;
	end if;
end;
$$
language plpgsql stable
cost 100
rows 1000;

comment on function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]) is
'The function returns list of available sub populations for given input arguments. The first input argument is an indentifator of language mutation. A possible variants are "cs-CZ" or "en-GB". The second input argument is a list of IDs from t_result_group table. The third input argument is a list of sub populations.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_sub_population(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator_ldsity_division" schema="nfiesta_results" src="functions/fn_get_user_options_denominator_ldsity_division.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator_ldsity_division
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator_ldsity_division(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_options_denominator_ldsity_division
(
	_jlang							character varying,
	_id_group						integer[]
)
returns table
(
	res_id								integer,
	res_label							json,
	res_id_group						integer[]
)
language plpgsql
stable
as
$$
declare
		_lang_suffix						character varying(3);
		_not_specified_version_label		text;
		_not_specified_version_description	text;
		_not_specified_defintion_variant	json;
		_not_specified_adr_spr				json;
begin	
		-----------------------------------------------------------------
		_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
		-----------------------------------------------------------------
		if _id_group is null
		then
			raise exception 'Error 01: fn_get_user_options_denominator_ldsity_division: Input argument _id_group must not be null!';
		end if;
		-----------------------------------------------------------------
		if _jlang = 'cs-CZ'
		then
			_not_specified_version_label = 'neuvedeno'::text;
			_not_specified_version_description = 'Neuvedeno.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
		end if;
	
		if _jlang = 'en-GB'
		then
			_not_specified_version_label := 'not specified'::text;
			_not_specified_version_description = 'Not specified.'::text;
			_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
			_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
		end if;
		-----------------------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group as trg
				where trg.id in (select unnest($1))
				and trg.web = true
				)
		,w2 as	(
				select
						target_variable_denom as target_variable,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						t.target_variable,
						t.id,
						t.local_densities'||_lang_suffix||' AS local_densities
				from
						(
						select
								w2.*,
								((ctv.metadata->''cs'')->''local_densities'') as local_densities,
								((ctv.metadata->''en'')->''local_densities'') as local_densities_en
						from
								w2
								inner join nfiesta_results.c_target_variable as ctv
								on w2.target_variable = ctv.id
						) as t
				)
		,w4 as	(
				select
						w3.target_variable,
						json_array_elements(w3.local_densities) as local_densities
				from
						w3
				)
		,w5 as	(
				select * from w4 where (w4.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w6 as	(
				select
						row_number() over() as new_id,
						w5.target_variable,
						(w5.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w5.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w5.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w5.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w5.local_densities->''ldsity'')->>''label'' as label,
						(w5.local_densities->''ldsity'')->>''description'' as description,	
						w5.local_densities->>''use_negative'' as use_negative,
						(w5.local_densities->''version'')->>''label'' as version_label,
						(w5.local_densities->''version'')->>''description'' as version_description,
						w5.local_densities->''definition_variant'' as definition_variant,
						w5.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w5.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w5	
				)
		,w7 as	(
				select
						w6.new_id,
						w6.target_variable,
						w6.object_type_label,
						w6.object_type_description,
						w6.object_label,
						w6.object_description,
						w6.label,
						w6.description,
						w6.use_negative,
						case when (w6.version_label = ''null'' or w6.version_label is null) then $2 else w6.version_label end as version_label,
						case when (w6.version_description = ''null'' or w6.version_description is null) then $3 else w6.version_description end as version_description,
						case when w6.definition_variant::jsonb = ''null''::jsonb then $4 else w6.definition_variant end as definition_variant,
						case when w6.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w6.area_domain_restrictions end as area_domain_restrictions,
						case when w6.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w6.sub_population_restrictions end as sub_population_restrictions
				from
						w6
				)
		,w8 as	(
				select
						w7.new_id,
						w7.target_variable,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w7.object_type_label,''description'',w7.object_type_description),
							''ldsity_object'',json_build_object(''label'',w7.object_label,''description'',w7.object_description),
							''ldsity'',json_build_object(''label'',w7.label,''description'',w7.description),
							''version'',json_build_object(''label'',w7.version_label,''description'',w7.version_description),
							''definition_variant'',w7.definition_variant,
							''area_domain_restrictions'',w7.area_domain_restrictions,
							''sub_population_restrictions'',w7.sub_population_restrictions,
							''use_negative'',w7.use_negative
							) as  local_densities
				from
						w7
				)
		,w9 as	(
				select
						w8.target_variable,
						--w8.new_id,
						json_agg(w8.local_densities order by w8.new_id) as local_densities
				from
						w8 group by w8.target_variable --, w8.new_id
				)
		select
				null::integer as res_id,
				w9.local_densities as res_label,
				w3.id as res_id_group
		from
				w9 inner join w3 on w9.target_variable = w3.target_variable;
		'	
		using _id_group, _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr;
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator_ldsity_division(character varying, integer[]) is
'The function returns list of available labels of division local densities with IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator_ldsity_division(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_options_denominator" schema="nfiesta_results" src="functions/fn_get_user_options_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_options_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean);

create or replace function nfiesta_results.fn_get_user_options_denominator
(
	_jlang character varying,
	_denom_id_group integer[],
	_denom_indicator boolean DEFAULT false,
	_denom_state boolean DEFAULT false
)
returns table
(
	res_id integer,
	res_label text,
	res_id_group integer[]
)
language plpgsql
stable
as
$$
declare
	_lang_suffix			character varying(3);
	_lang_covariate			text;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);
	-----------------------------------------------------------------
	if _denom_id_group is null
	then
		raise exception 'Error 01: fn_get_user_options_denominator: Input argument _denom_id_group must not be NULL !';
	end if;
	-----------------------------------------------------------------
	-- DENOM_INDICATOR --
	-----------------------------------------------------------------
	if _denom_indicator = false
	then
		return query execute
		'
		with
		w1 as	(
				select * from nfiesta_results.t_result_group
				where id in (select unnest($1))
				and web = true
				)
		,w2 as	(
				select
						w1.target_variable_denom,
						array_agg(w1.id order by w1.id) as id
				from
						w1 group by target_variable_denom
				)
		,w3 as	(
				select
						w2.*,
						((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
						((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en
				from
						w2
						inner join nfiesta_results.c_target_variable as ctv
						on w2.target_variable_denom = ctv.id
				)
		,w4 as	(
				select
						w3.id,
						label'||_lang_suffix||' AS label
				from
						w3
				)
		,w5 as	(
				select
						w4.label,
						unnest(w4.id) as id
				from
						w4
				)
		,w6 as	(
				select w5.label, array_agg(w5.id) as id from w5 group by w5.label
				)
		select
				1 as res_id,
				w6.label as res_label,
				w6.id as res_id_group
		from
				w6 order by w6.label;
		'
		using _denom_id_group;
	else
		-----------------------------------------------------------------
		-- DENOM_STATE --
		-----------------------------------------------------------------
		if _denom_state = false
		then
			return query execute
			'
			with
			w1 as	(
					select * from nfiesta_results.t_result_group
					where id in (select unnest($1))
					and web = true
					)
			,w2 as	(
					select
							w1.target_variable_denom,
							array_agg(w1.id order by w1.id) as id
					from
							w1 group by target_variable_denom
					)
			,w3 as	(
					select
							w2.*,
							((ctv.metadata->''cs'')->''state_or_change'')->>''label'' as label,
							((ctv.metadata->''en'')->''state_or_change'')->>''label'' as label_en
					from
							w2
							inner join nfiesta_results.c_target_variable as ctv
							on w2.target_variable_denom = ctv.id
					)
			,w4 as	(
					select
							w3.id,
							label'||_lang_suffix||' AS label
					from
							w3
					)
			,w5 as	(
					select
							w4.label,
							unnest(w4.id) as id
					from
							w4
					)
			,w6 as	(
					select w5.label, array_agg(w5.id) as id from w5 group by w5.label
					)
			select
					1 as res_id,
					w6.label as res_label,
					w6.id as res_id_group
			from
					w6 order by w6.label;
			'
			using _denom_id_group;
		else
			raise exception 'Error 02: fn_get_user_options_denominator: This variant of function is not implemented !';
		end if;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
end;
$$
;

comment on function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean) is
'The function returns list of available IDs from t_result_group table for given input arguments.';

grant execute on function nfiesta_results.fn_get_user_options_denominator(character varying, integer[], boolean, boolean) to public;
-- </function>



drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);
drop function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]);
drop function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]);



-- <function name="fn_get_user_metadata" schema="nfiesta_results" src="functions/fn_get_user_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang_suffix								character varying(3);
	_lang										text;
	_target_variable_num						integer[];
	_target_variable_denom_check				integer;
	_target_variable_denom						integer;
	_cond4topic_num								text;
	_cond4topic_denom							text;
	_res_topic_label							text;
	_res_topic_description						text;
	_res_estimation_period_label				text;
	_res_estimation_period_description			text;
	_res_estimation_cell_collection_label		text;
	_res_estimation_cell_collection_description	text;
	_res_estimate_type_label					text;
	_res_estimate_type_description				text;
	_res_unit_num_label							text;
	_res_unit_num_description					text;
	_res_unit_of_measurement_label				text;
	_res_unit_of_measurement_description		text;
	_res										json;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_options_metadata: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable)
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_num;

	if array_length(_target_variable_num,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	select count(t.target_variable_denom)
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable_denom_check;

	if _target_variable_denom_check is distinct from 0 -- target variable denom exists
	then
		if _target_variable_denom_check > 1
		then
			raise exception 'Error: 03: fn_get_user_options_metadata: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
		end if;

		select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
		where trg.id in (select unnest(_id_group))
		and trg.web = true
		into _target_variable_denom;		
	else
		_target_variable_denom := null::integer;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF TOPIC
	if _target_variable_denom is null
	then 
		_cond4topic_denom := 'crt.denominator is null';
	else
		_cond4topic_denom := 'crt.denominator = $2';
	end if;

	_cond4topic_num := 'crt.target_variable = $1';

	execute
	'
	with
	w1 as	(
			select crt.topic from nfiesta_results.cm_result2topic as crt
			where ' || _cond4topic_num ||'
			and ' || _cond4topic_denom ||'
			)
	,w2 as	(
			select
					label'||_lang_suffix||' AS label,
					description'||_lang_suffix||' AS description
			from
					nfiesta_results.c_topic
			where
					id in (select distinct w1.topic from w1)
			)
	,w3 as	(
			select
					array_agg(w2.label order by w2.description) as label,
					array_agg(w2.description order by w2.description) as description
			from
					w2
			)
	select
			array_to_string(w3.label, ''; ''),
			array_to_string(w3.description, ''; '')
	from
			w3
	'
	using _target_variable_num[1], _target_variable_denom
	into _res_topic_label, _res_topic_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION PERIOD
	execute
	'
	select
			cep.label'||_lang_suffix||' AS label,
			cep.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_period as cep
	where
			cep.id =	(
						select distinct trg.estimation_period
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_period_label, _res_estimation_period_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATION CELL COLLECTION
	execute
	'
	select
			cecc.label'||_lang_suffix||' AS label,
			cecc.description'||_lang_suffix||' AS description
	from
			nfiesta_results.c_estimation_cell_collection as cecc
	where
			cecc.id =	(
						select distinct trg.estimation_cell_collection
						from nfiesta_results.t_result_group trg
						where trg.id in (select unnest($1))
						)
	'
	using _id_group
	into _res_estimation_cell_collection_label, _res_estimation_cell_collection_description;
	-----------------------------------------------------------------
	-- METADATA OF ESTIMATE TYPE
	if _target_variable_denom is null
	then
		execute
		'
		select
				cet.label'||_lang_suffix||' AS label,
				cet.description'||_lang_suffix||' AS description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 1
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;
	else
		execute
		'
		select
				cet.label'||_lang_suffix||' AS label,
				cet.description'||_lang_suffix||' AS description
		from
				nfiesta_results.c_estimate_type as cet where cet.id = 2
		'
		into
				_res_estimate_type_label, _res_estimate_type_description;	
	end if;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF MEASUREMENT
	if _target_variable_denom is null
	then
		_res_unit_of_measurement_label := _res_unit_num_label;
		_res_unit_of_measurement_description := _res_unit_num_description;
	else
		execute
		'
		with
		w1 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $1
				)
		,w2 as	(
				select 1 as id, id as target_variable, metadata from nfiesta_results.c_target_variable ctv where ctv.id = $2
				)
		,w3 as	(
				select
						w1.target_variable as target_variable_num,
						w2.target_variable as target_variable_denom,
						((w1.metadata->''cs'')->''unit'')->>''label'' as unit_label_num,
						((w2.metadata->''cs'')->''unit'')->>''label'' as unit_label_denom,
						((w1.metadata->''en'')->''unit'')->>''label'' as unit_label_num_en,
						((w2.metadata->''en'')->''unit'')->>''label'' as unit_label_denom_en,						
						((w1.metadata->''cs'')->''unit'')->>''description'' as unit_description_num,
						((w2.metadata->''cs'')->''unit'')->>''description'' as unit_description_denom,
						((w1.metadata->''en'')->''unit'')->>''description'' as unit_description_num_en,
						((w2.metadata->''en'')->''unit'')->>''description'' as unit_description_denom_en
				from
						w1 inner join w2 on w1.id = w1.id
				)
		,w4 as	(
				select
						w3.target_variable_num,
						w3.target_variable_denom,
						w3.unit_label_num'||_lang_suffix||' AS unit_label_num,
						w3.unit_label_denom'||_lang_suffix||' AS unit_label_denom,						
						w3.unit_description_num'||_lang_suffix||' AS unit_description_num,
						w3.unit_description_denom'||_lang_suffix||' AS unit_description_denom
				from
						w3
				)
		,w5 as	(
				select
						w4.*,
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_label_num = w4.unit_label_denom) then ''%''
							else concat(w4.unit_label_num,'' / '',w4.unit_label_denom)
						end as label4user,						
						case
							when (w4.target_variable_num = w4.target_variable_denom) or (w4.unit_description_num = w4.unit_description_denom) then ''%''
							else concat(w4.unit_description_num,'' / '',w4.unit_description_denom)
						end as description4user
				from
						w4
				)
		select
				w5.label4user,
				w5.description4user
		from
				w5
		'
		using _target_variable_num[1], _target_variable_denom
		into _res_unit_of_measurement_label, _res_unit_of_measurement_description;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _target_variable_denom is null
	then
		-- basic metadata + numerator metadata
		if _lang_suffix = ''
		then
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('cs-CZ',_id_group))
			)
			into _res;
		else
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('en-GB',_id_group))
			)
			into _res;		
		end if;
	else
		-- basic metadata + numerator metadata + denominator metadata
		if _lang_suffix = ''
		then
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('cs-CZ',_id_group)),
				'denominator',(select * from nfiesta_results.fn_get_user_metadata_denominator('cs-CZ',_id_group))
			)
			into _res;	
		else
			select json_build_object
			(
				'topic',json_build_object('label',_res_topic_label,'description',_res_topic_description),
				'period',json_build_object('label',_res_estimation_period_label,'description',_res_estimation_period_description),
				'geographic_region',json_build_object('label',_res_estimation_cell_collection_label,'description',_res_estimation_cell_collection_description),
				'estimate_type',json_build_object('label',_res_estimate_type_label,'description',_res_estimate_type_description),
				'unit_of_measurement',json_build_object('label',_res_unit_of_measurement_label,'description',_res_unit_of_measurement_description),
				'numerator',(select * from nfiesta_results.fn_get_user_metadata_numerator('en-GB',_id_group)),
				'denominator',(select * from nfiesta_results.fn_get_user_metadata_denominator('en-GB',_id_group))
			)
			into _res;			
		end if;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) is
'The function returns metadata for given input ID (that represents target variable for nominator or nominator and denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata_numerator" schema="nfiesta_results" src="functions/fn_get_user_metadata_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata_numerator
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang_suffix							character varying(3);
	_lang									text;
	_target_variable						integer[];
	_res_indicator_label					text;
	_res_indicator_description				text;
	_res_state_or_change_label				text;
	_res_state_or_change_description		text;
	_res_unit_label							text;
	_res_unit_description					text;
	_check_area_domain						integer[];
	_res_area_domain_attribute_json			json;
	_check_sub_population					integer[];
	_res_sub_population_attribute_json		json;
	--_check_core_ldsities					integer;
	_check_division_ldsities				integer;
	_not_specified_version_label			text;
	_not_specified_version_description		text;
	_not_specified_defintion_variant		json;
	_not_specified_adr_spr					json;
	_res_core_json							json;
	_res_division_json						json;	
	_res									json;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata_numerator: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata_numerator: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable) -- column for NUMERATOR
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable;

	if array_length(_target_variable,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	if _lang_suffix = ''
	then
		_not_specified_version_label = 'neuvedeno'::text;
		_not_specified_version_description = 'Neuvedeno.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
	else
		_not_specified_version_label := 'not specified'::text;
		_not_specified_version_description = 'Not specified.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
					((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en,		
					((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
					((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_indicator_label, _res_indicator_description;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state_or_change'')->>''label'' as label,
					((ctv.metadata->''en'')->''state_or_change'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''state_or_change'')->>''description'' as description,
					((ctv.metadata->''en'')->''state_or_change'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_state_or_change_label, _res_state_or_change_description;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_unit_label, _res_unit_description;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 04: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_area_domain_attribute_json := json_agg(json_build_object('label','bez rozlišení','desription','Bez rozlišení.'));
		else
			_res_area_domain_attribute_json := json_agg(json_build_object('label','without distinction','desription','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(cad.label'||_lang_suffix||','';'') as label,
						string_to_array(cad.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_area_domain as cad
				where
						cad.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_area_domain[1]
		into _res_area_domain_attribute_json;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 05: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_sub_population_attribute_json := json_agg(json_build_object('label','bez rozlišení','desription','Bez rozlišení.'));
		else
			_res_sub_population_attribute_json := json_agg(json_build_object('label','without distinction','desription','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(csp.label'||_lang_suffix||','';'') as label,
						string_to_array(csp.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_sub_population as csp
				where
						csp.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_sub_population[1]
		into _res_sub_population_attribute_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local_densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'core'
	into _check_core_ldsities;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- CORE JSON
	execute
	'
	with
	w1 as	(
			select
					(ctv.metadata->''cs'')->''local_densities'' as local_densities,
					(ctv.metadata->''en'')->''local_densities'' as local_densities_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	,w2 as	(
			select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
			)
	,w3 as	(
			select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''core''
			)
	,w4 as	(
			select
					row_number() over() as new_id,
					(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
					(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
					(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
					(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
					(w3.local_densities->''ldsity'')->>''label'' as label,
					(w3.local_densities->''ldsity'')->>''description'' as description,	
					w3.local_densities->>''use_negative'' as use_negative,
					(w3.local_densities->''version'')->>''label'' as version_label,
					(w3.local_densities->''version'')->>''description'' as version_description,
					w3.local_densities->''definition_variant'' as definition_variant,
					w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
					w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
			from
					w3
			)
	,w5 as	(
			select
					w4.new_id,
					w4.object_type_label,
					w4.object_type_description,
					w4.object_label,
					w4.object_description,
					w4.label,
					w4.description,
					w4.use_negative,
					case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
					case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
					case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
					case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
					case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
			from
					w4
			)
	,w6 as	(
			select
					w5.new_id,
					json_build_object
						(
						''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
						''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
						''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
						''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
						''definition_variant'',w5.definition_variant,
						''area_domain_restrictions'',w5.area_domain_restrictions,
						''sub_population_restrictions'',w5.sub_population_restrictions,
						''use_negative'',w5.use_negative
						) as  local_densities
			from
					w5
			)
	select
			json_agg(w6.local_densities order by w6.new_id) as local_densities
	from
			w6
	'
	using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
	into _res_core_json;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- DIVISION JSON
	if _check_division_ldsities = 0
	then
		_res_division_json := null::json;
	else
		execute
		'
		with
		w1 as	(
				select
						(ctv.metadata->''cs'')->''local_densities'' as local_densities,
						(ctv.metadata->''en'')->''local_densities'' as local_densities_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		,w2 as	(
				select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
				)
		,w3 as	(
				select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w4 as	(
				select
						row_number() over() as new_id,
						(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w3.local_densities->''ldsity'')->>''label'' as label,
						(w3.local_densities->''ldsity'')->>''description'' as description,	
						w3.local_densities->>''use_negative'' as use_negative,
						(w3.local_densities->''version'')->>''label'' as version_label,
						(w3.local_densities->''version'')->>''description'' as version_description,
						w3.local_densities->''definition_variant'' as definition_variant,
						w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w3
				)
		,w5 as	(
				select
						w4.new_id,
						w4.object_type_label,
						w4.object_type_description,
						w4.object_label,
						w4.object_description,
						w4.label,
						w4.description,
						w4.use_negative,
						case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
						case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
						case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
						case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
						case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
				from
						w4
				)
		,w6 as	(
				select
						w5.new_id,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
							''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
							''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
							''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
							''definition_variant'',w5.definition_variant,
							''area_domain_restrictions'',w5.area_domain_restrictions,
							''sub_population_restrictions'',w5.sub_population_restrictions,
							''use_negative'',w5.use_negative
							) as  local_densities
				from
						w5
				)
		select
				json_agg(w6.local_densities order by w6.new_id) as local_densities
		from
				w6
		'
		using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
		into _res_division_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities = 0
	then
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json
		))
		into _res;
	else
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json,
			'division_contributions',_res_division_json
		))
		into _res;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]) is
'The function returns metadata (description) for given input ID (that represents target variable for nominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata_denominator" schema="nfiesta_results" src="functions/fn_get_user_metadata_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata_denominator
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang_suffix							character varying(3);
	_lang									text;
	_target_variable						integer[];
	_res_indicator_label					text;
	_res_indicator_description				text;
	_res_state_or_change_label				text;
	_res_state_or_change_description		text;
	_res_unit_label							text;
	_res_unit_description					text;
	_check_area_domain						integer[];
	_res_area_domain_attribute_json			json;
	_check_sub_population					integer[];
	_res_sub_population_attribute_json		json;
	--_check_core_ldsities					integer;
	_check_division_ldsities				integer;
	_not_specified_version_label			text;
	_not_specified_version_description		text;
	_not_specified_defintion_variant		json;
	_not_specified_adr_spr					json;
	_res_core_json							json;
	_res_division_json						json;	
	_res									json;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata_denominator: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata_denominator: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom) -- column for DENOMINATOR
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable;

	if array_length(_target_variable,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	if _lang_suffix = ''
	then
		_not_specified_version_label = 'neuvedeno'::text;
		_not_specified_version_description = 'Neuvedeno.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
	else
		_not_specified_version_label := 'not specified'::text;
		_not_specified_version_description = 'Not specified.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
					((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en,		
					((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
					((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_indicator_label, _res_indicator_description;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state_or_change'')->>''label'' as label,
					((ctv.metadata->''en'')->''state_or_change'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''state_or_change'')->>''description'' as description,
					((ctv.metadata->''en'')->''state_or_change'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_state_or_change_label, _res_state_or_change_description;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_unit_label, _res_unit_description;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 04: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_area_domain_attribute_json := json_agg(json_build_object('label','bez rozlišení','desription','Bez rozlišení.'));
		else
			_res_area_domain_attribute_json := json_agg(json_build_object('label','without distinction','desription','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(cad.label'||_lang_suffix||','';'') as label,
						string_to_array(cad.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_area_domain as cad
				where
						cad.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_area_domain[1]
		into _res_area_domain_attribute_json;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 05: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_sub_population_attribute_json := json_agg(json_build_object('label','bez rozlišení','desription','Bez rozlišení.'));
		else
			_res_sub_population_attribute_json := json_agg(json_build_object('label','without distinction','desription','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(csp.label'||_lang_suffix||','';'') as label,
						string_to_array(csp.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_sub_population as csp
				where
						csp.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_sub_population[1]
		into _res_sub_population_attribute_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local_densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'core'
	into _check_core_ldsities;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- CORE JSON
	execute
	'
	with
	w1 as	(
			select
					(ctv.metadata->''cs'')->''local_densities'' as local_densities,
					(ctv.metadata->''en'')->''local_densities'' as local_densities_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	,w2 as	(
			select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
			)
	,w3 as	(
			select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''core''
			)
	,w4 as	(
			select
					row_number() over() as new_id,
					(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
					(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
					(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
					(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
					(w3.local_densities->''ldsity'')->>''label'' as label,
					(w3.local_densities->''ldsity'')->>''description'' as description,	
					w3.local_densities->>''use_negative'' as use_negative,
					(w3.local_densities->''version'')->>''label'' as version_label,
					(w3.local_densities->''version'')->>''description'' as version_description,
					w3.local_densities->''definition_variant'' as definition_variant,
					w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
					w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
			from
					w3
			)
	,w5 as	(
			select
					w4.new_id,
					w4.object_type_label,
					w4.object_type_description,
					w4.object_label,
					w4.object_description,
					w4.label,
					w4.description,
					w4.use_negative,
					case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
					case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
					case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
					case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
					case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
			from
					w4
			)
	,w6 as	(
			select
					w5.new_id,
					json_build_object
						(
						''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
						''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
						''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
						''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
						''definition_variant'',w5.definition_variant,
						''area_domain_restrictions'',w5.area_domain_restrictions,
						''sub_population_restrictions'',w5.sub_population_restrictions,
						''use_negative'',w5.use_negative
						) as  local_densities
			from
					w5
			)
	select
			json_agg(w6.local_densities order by w6.new_id) as local_densities
	from
			w6
	'
	using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
	into _res_core_json;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- DIVISION JSON
	if _check_division_ldsities = 0
	then
		_res_division_json := null::json;
	else
		execute
		'
		with
		w1 as	(
				select
						(ctv.metadata->''cs'')->''local_densities'' as local_densities,
						(ctv.metadata->''en'')->''local_densities'' as local_densities_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		,w2 as	(
				select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
				)
		,w3 as	(
				select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w4 as	(
				select
						row_number() over() as new_id,
						(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w3.local_densities->''ldsity'')->>''label'' as label,
						(w3.local_densities->''ldsity'')->>''description'' as description,	
						w3.local_densities->>''use_negative'' as use_negative,
						(w3.local_densities->''version'')->>''label'' as version_label,
						(w3.local_densities->''version'')->>''description'' as version_description,
						w3.local_densities->''definition_variant'' as definition_variant,
						w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w3
				)
		,w5 as	(
				select
						w4.new_id,
						w4.object_type_label,
						w4.object_type_description,
						w4.object_label,
						w4.object_description,
						w4.label,
						w4.description,
						w4.use_negative,
						case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
						case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
						case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
						case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
						case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
				from
						w4
				)
		,w6 as	(
				select
						w5.new_id,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
							''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
							''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
							''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
							''definition_variant'',w5.definition_variant,
							''area_domain_restrictions'',w5.area_domain_restrictions,
							''sub_population_restrictions'',w5.sub_population_restrictions,
							''use_negative'',w5.use_negative
							) as  local_densities
				from
						w5
				)
		select
				json_agg(w6.local_densities order by w6.new_id) as local_densities
		from
				w6
		'
		using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
		into _res_division_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities = 0
	then
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json
		))
		into _res;
	else
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json,
			'division_contributions',_res_division_json
		))
		into _res;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) is
'The function returns metadata (description) for given input ID (that represents target variable for denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) to public;
-- </function>
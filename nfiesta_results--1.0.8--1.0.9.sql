--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------



update nfiesta_results.c_gui_header set header = 'area_domain_attribute' where header = 'area_adomain_attribute';



-- <function name="fn_get_user_metadata_numerator" schema="nfiesta_results" src="functions/fn_get_user_metadata_numerator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata_numerator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata_numerator
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang_suffix							character varying(3);
	_lang									text;
	_target_variable						integer[];
	_res_indicator_label					text;
	_res_indicator_description				text;
	_res_state_or_change_label				text;
	_res_state_or_change_description		text;
	_res_unit_label							text;
	_res_unit_description					text;
	_check_area_domain						integer[];
	_res_area_domain_attribute_json			json;
	_check_sub_population					integer[];
	_res_sub_population_attribute_json		json;
	--_check_core_ldsities					integer;
	_check_division_ldsities				integer;
	_not_specified_version_label			text;
	_not_specified_version_description		text;
	_not_specified_defintion_variant		json;
	_not_specified_adr_spr					json;
	_res_core_json							json;
	_res_division_json						json;	
	_res									json;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata_numerator: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata_numerator: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable) -- column for NUMERATOR
	from	(
			select distinct trg.target_variable from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable;

	if array_length(_target_variable,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one target variable in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	if _lang_suffix = ''
	then
		_not_specified_version_label = 'neuvedeno'::text;
		_not_specified_version_description = 'Neuvedeno.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
	else
		_not_specified_version_label := 'not specified'::text;
		_not_specified_version_description = 'Not specified.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
					((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en,		
					((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
					((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_indicator_label, _res_indicator_description;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state_or_change'')->>''label'' as label,
					((ctv.metadata->''en'')->''state_or_change'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''state_or_change'')->>''description'' as description,
					((ctv.metadata->''en'')->''state_or_change'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_state_or_change_label, _res_state_or_change_description;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_unit_label, _res_unit_description;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 04: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_area_domain_attribute_json := json_agg(json_build_object('label','bez rozlišení','description','Bez rozlišení.'));
		else
			_res_area_domain_attribute_json := json_agg(json_build_object('label','without distinction','description','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(cad.label'||_lang_suffix||','';'') as label,
						string_to_array(cad.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_area_domain as cad
				where
						cad.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_area_domain[1]
		into _res_area_domain_attribute_json;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 05: fn_get_user_metadata_numerator: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_sub_population_attribute_json := json_agg(json_build_object('label','bez rozlišení','description','Bez rozlišení.'));
		else
			_res_sub_population_attribute_json := json_agg(json_build_object('label','without distinction','description','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(csp.label'||_lang_suffix||','';'') as label,
						string_to_array(csp.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_sub_population as csp
				where
						csp.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_sub_population[1]
		into _res_sub_population_attribute_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local_densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'core'
	into _check_core_ldsities;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- CORE JSON
	execute
	'
	with
	w1 as	(
			select
					(ctv.metadata->''cs'')->''local_densities'' as local_densities,
					(ctv.metadata->''en'')->''local_densities'' as local_densities_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	,w2 as	(
			select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
			)
	,w3 as	(
			select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''core''
			)
	,w4 as	(
			select
					row_number() over() as new_id,
					(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
					(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
					(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
					(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
					(w3.local_densities->''ldsity'')->>''label'' as label,
					(w3.local_densities->''ldsity'')->>''description'' as description,	
					w3.local_densities->>''use_negative'' as use_negative,
					(w3.local_densities->''version'')->>''label'' as version_label,
					(w3.local_densities->''version'')->>''description'' as version_description,
					w3.local_densities->''definition_variant'' as definition_variant,
					w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
					w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
			from
					w3
			)
	,w5 as	(
			select
					w4.new_id,
					w4.object_type_label,
					w4.object_type_description,
					w4.object_label,
					w4.object_description,
					w4.label,
					w4.description,
					w4.use_negative,
					case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
					case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
					case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
					case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
					case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
			from
					w4
			)
	,w6 as	(
			select
					w5.new_id,
					json_build_object
						(
						''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
						''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
						''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
						''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
						''definition_variant'',w5.definition_variant,
						''area_domain_restrictions'',w5.area_domain_restrictions,
						''sub_population_restrictions'',w5.sub_population_restrictions,
						''use_negative'',w5.use_negative
						) as  local_densities
			from
					w5
			)
	select
			json_agg(w6.local_densities order by w6.new_id) as local_densities
	from
			w6
	'
	using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
	into _res_core_json;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- DIVISION JSON
	if _check_division_ldsities = 0
	then
		_res_division_json := null::json;
	else
		execute
		'
		with
		w1 as	(
				select
						(ctv.metadata->''cs'')->''local_densities'' as local_densities,
						(ctv.metadata->''en'')->''local_densities'' as local_densities_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		,w2 as	(
				select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
				)
		,w3 as	(
				select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w4 as	(
				select
						row_number() over() as new_id,
						(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w3.local_densities->''ldsity'')->>''label'' as label,
						(w3.local_densities->''ldsity'')->>''description'' as description,	
						w3.local_densities->>''use_negative'' as use_negative,
						(w3.local_densities->''version'')->>''label'' as version_label,
						(w3.local_densities->''version'')->>''description'' as version_description,
						w3.local_densities->''definition_variant'' as definition_variant,
						w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w3
				)
		,w5 as	(
				select
						w4.new_id,
						w4.object_type_label,
						w4.object_type_description,
						w4.object_label,
						w4.object_description,
						w4.label,
						w4.description,
						w4.use_negative,
						case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
						case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
						case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
						case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
						case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
				from
						w4
				)
		,w6 as	(
				select
						w5.new_id,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
							''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
							''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
							''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
							''definition_variant'',w5.definition_variant,
							''area_domain_restrictions'',w5.area_domain_restrictions,
							''sub_population_restrictions'',w5.sub_population_restrictions,
							''use_negative'',w5.use_negative
							) as  local_densities
				from
						w5
				)
		select
				json_agg(w6.local_densities order by w6.new_id) as local_densities
		from
				w6
		'
		using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
		into _res_division_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities = 0
	then
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json
		))
		into _res;
	else
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json,
			'division_contributions',_res_division_json
		))
		into _res;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]) is
'The function returns metadata (description) for given input ID (that represents target variable for nominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata_numerator(character varying, integer[]) to public;
-- </function>



-- <function name="fn_get_user_metadata_denominator" schema="nfiesta_results" src="functions/fn_get_user_metadata_denominator.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_get_user_metadata_denominator
---------------------------------------------------------------------------------------------------

-- drop function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]);

create or replace function nfiesta_results.fn_get_user_metadata_denominator
(
	_jlang character varying,
	_id_group integer[]
)
returns json
language plpgsql
stable
as
$$
declare
	_lang_suffix							character varying(3);
	_lang									text;
	_target_variable						integer[];
	_res_indicator_label					text;
	_res_indicator_description				text;
	_res_state_or_change_label				text;
	_res_state_or_change_description		text;
	_res_unit_label							text;
	_res_unit_description					text;
	_check_area_domain						integer[];
	_res_area_domain_attribute_json			json;
	_check_sub_population					integer[];
	_res_sub_population_attribute_json		json;
	--_check_core_ldsities					integer;
	_check_division_ldsities				integer;
	_not_specified_version_label			text;
	_not_specified_version_description		text;
	_not_specified_defintion_variant		json;
	_not_specified_adr_spr					json;
	_res_core_json							json;
	_res_division_json						json;	
	_res									json;
begin
	-----------------------------------------------------------------
	_lang_suffix := nfiesta_results.fn_get_language4query(_jlang);

	if _lang_suffix = ''
	then
		_lang := 'cs';
	else
		_lang := replace(_lang_suffix,'_','');
	end if;
	-----------------------------------------------------------------
	if _id_group is null
	then
		raise exception 'Error: 01: fn_get_user_metadata_denominator: Input argument _id_group must not be NULL!';
	end if;
	-----------------------------------------------------------------
	if array_length(_id_group,1) is distinct from 1
	then
		raise exception 'Error: 02: fn_get_user_metadata_denominator: Count of elements in input argument _id_group must be one!';
	end if;
	-----------------------------------------------------------------
	select array_agg(t.target_variable_denom) -- column for DENOMINATOR
	from	(
			select distinct trg.target_variable_denom from nfiesta_results.t_result_group as trg
			where trg.id in (select unnest(_id_group))
			and trg.web = true
			) as t
	into _target_variable;

	if array_length(_target_variable,1) is distinct from 1
	then
		raise exception 'Error: 03: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one target variable denom in t_result_group table!',_id_group;
	end if;
	-----------------------------------------------------------------
	if _lang_suffix = ''
	then
		_not_specified_version_label = 'neuvedeno'::text;
		_not_specified_version_description = 'Neuvedeno.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','neuvedeno','description','Neuvedeno.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','neuvedeno','description','Neuvedeno.'),'restriction',json_build_object('label','neuvedeno','description','Neuvedeno.')));
	else
		_not_specified_version_label := 'not specified'::text;
		_not_specified_version_description = 'Not specified.'::text;
		_not_specified_defintion_variant := json_agg(json_build_object('label','not specified','description','Not specified.'));
		_not_specified_adr_spr := json_agg(json_build_object('object',json_build_object('label','not specified','description','Not specified.'),'restriction',json_build_object('label','not specified','description','Not specified.')));
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- METADATA OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''indicator'')->>''label'' as label,
					((ctv.metadata->''en'')->''indicator'')->>''label'' as label_en,		
					((ctv.metadata->''cs'')->''indicator'')->>''description'' as description,
					((ctv.metadata->''en'')->''indicator'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_indicator_label, _res_indicator_description;
	-----------------------------------------------------------------
	-- METADATA OF STATE OR CHANGE - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''state_or_change'')->>''label'' as label,
					((ctv.metadata->''en'')->''state_or_change'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''state_or_change'')->>''description'' as description,
					((ctv.metadata->''en'')->''state_or_change'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_state_or_change_label, _res_state_or_change_description;
	-----------------------------------------------------------------
	-- METADATA OF UNIT OF INDICATOR - numerator
	execute
	'
	with
	w1 as	(
			select
					((ctv.metadata->''cs'')->''unit'')->>''label'' as label,
					((ctv.metadata->''en'')->''unit'')->>''label'' as label_en,			
					((ctv.metadata->''cs'')->''unit'')->>''description'' as description,
					((ctv.metadata->''en'')->''unit'')->>''description'' as description_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	select
			w1.label'||_lang_suffix||' AS label,
			w1.description'||_lang_suffix||' AS description
	from
			w1
	'
	using _target_variable[1]
	into _res_unit_label, _res_unit_description;
	-----------------------------------------------------------------
	-- METADATA OF AREA DOMAIN ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.area_domain is null then 0 else trg.area_domain end as area_domain
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.area_domain) from (select distinct w1.area_domain from w1) as t
	into _check_area_domain;

	if array_length(_check_area_domain,1) is distinct from 1
	then
		raise exception 'Error: 04: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one area domain attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_area_domain[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_area_domain_attribute_json := json_agg(json_build_object('label','bez rozlišení','description','Bez rozlišení.'));
		else
			_res_area_domain_attribute_json := json_agg(json_build_object('label','without distinction','description','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(cad.label'||_lang_suffix||','';'') as label,
						string_to_array(cad.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_area_domain as cad
				where
						cad.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_area_domain[1]
		into _res_area_domain_attribute_json;
	end if;
	-----------------------------------------------------------------
	-- METADATA OF SUB POPULATION ATTRIBUTES - numerator
	with
	w1 as	(
			select
					trg.id,
					case when trg.sub_population is null then 0 else trg.sub_population end as sub_population
			from
					nfiesta_results.t_result_group as trg
			where
					trg.id in (select unnest(_id_group))
			)
	select array_agg(t.sub_population) from (select distinct w1.sub_population from w1) as t
	into _check_sub_population;

	if array_length(_check_sub_population,1) is distinct from 1
	then
		raise exception 'Error: 05: fn_get_user_metadata_denominator: For input argument _id_group = % exists more then one sub population attribute classification in t_result_group table!',_id_group;
	end if;

	if _check_sub_population[1] = 0
	then
		if _lang_suffix = ''
		then
			_res_sub_population_attribute_json := json_agg(json_build_object('label','bez rozlišení','description','Bez rozlišení.'));
		else
			_res_sub_population_attribute_json := json_agg(json_build_object('label','without distinction','description','Without distinction.'));
		end if;
	else
		execute
		'
		with
		w1 as	(
				select
						string_to_array(csp.label'||_lang_suffix||','';'') as label,
						string_to_array(csp.description'||_lang_suffix||','';'') as description
				from
						nfiesta_results.c_sub_population as csp
				where
						csp.id = $1
				)
		,w2 as	(
				select
						unnest(w1.label) as label,
						unnest(w1.description) as description
				from
						w1
				)
		select json_agg(json_build_object(''label'',w2.label,''description'',w2.description)) from w2
		'
		using _check_sub_population[1]
		into _res_sub_population_attribute_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	/*
	-- check CORE ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local_densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'core'
	into _check_core_ldsities;
	*/
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- check DIVISION ldsities
	with
	w1 as	(
			select (ctv.metadata->'en')->'local densities' as local_densities
			from nfiesta_results.c_target_variable as ctv
			where ctv.id = _target_variable[1]
			)
	,w2 as	(
			select json_array_elements(w1.local_densities) as local_densities from w1 
			)
	select count(w2.*) from w2
	where (w2.local_densities->'ldsity_type')->>'label' = 'division'
	into _check_division_ldsities;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- CORE JSON
	execute
	'
	with
	w1 as	(
			select
					(ctv.metadata->''cs'')->''local_densities'' as local_densities,
					(ctv.metadata->''en'')->''local_densities'' as local_densities_en
			from
					nfiesta_results.c_target_variable as ctv
			where
					ctv.id = $1
			)
	,w2 as	(
			select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
			)
	,w3 as	(
			select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''core''
			)
	,w4 as	(
			select
					row_number() over() as new_id,
					(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
					(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
					(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
					(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
					(w3.local_densities->''ldsity'')->>''label'' as label,
					(w3.local_densities->''ldsity'')->>''description'' as description,	
					w3.local_densities->>''use_negative'' as use_negative,
					(w3.local_densities->''version'')->>''label'' as version_label,
					(w3.local_densities->''version'')->>''description'' as version_description,
					w3.local_densities->''definition_variant'' as definition_variant,
					w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
					w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
			from
					w3
			)
	,w5 as	(
			select
					w4.new_id,
					w4.object_type_label,
					w4.object_type_description,
					w4.object_label,
					w4.object_description,
					w4.label,
					w4.description,
					w4.use_negative,
					case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
					case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
					case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
					case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
					case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
			from
					w4
			)
	,w6 as	(
			select
					w5.new_id,
					json_build_object
						(
						''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
						''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
						''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
						''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
						''definition_variant'',w5.definition_variant,
						''area_domain_restrictions'',w5.area_domain_restrictions,
						''sub_population_restrictions'',w5.sub_population_restrictions,
						''use_negative'',w5.use_negative
						) as  local_densities
			from
					w5
			)
	select
			json_agg(w6.local_densities order by w6.new_id) as local_densities
	from
			w6
	'
	using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
	into _res_core_json;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	-- DIVISION JSON
	if _check_division_ldsities = 0
	then
		_res_division_json := null::json;
	else
		execute
		'
		with
		w1 as	(
				select
						(ctv.metadata->''cs'')->''local_densities'' as local_densities,
						(ctv.metadata->''en'')->''local_densities'' as local_densities_en
				from
						nfiesta_results.c_target_variable as ctv
				where
						ctv.id = $1
				)
		,w2 as	(
				select json_array_elements(w1.local_densities'||_lang_suffix||') as local_densities from w1 
				)
		,w3 as	(
				select * from w2 where (w2.local_densities->''ldsity_type'')->>''label'' = ''division''
				)
		,w4 as	(
				select
						row_number() over() as new_id,
						(w3.local_densities->''ldsity_type'')->>''label'' as object_type_label,
						(w3.local_densities->''ldsity_type'')->>''description'' as object_type_description,				
						(w3.local_densities->''ldsity_object'')->>''label'' as object_label,
						(w3.local_densities->''ldsity_object'')->>''description'' as object_description,						
						(w3.local_densities->''ldsity'')->>''label'' as label,
						(w3.local_densities->''ldsity'')->>''description'' as description,	
						w3.local_densities->>''use_negative'' as use_negative,
						(w3.local_densities->''version'')->>''label'' as version_label,
						(w3.local_densities->''version'')->>''description'' as version_description,
						w3.local_densities->''definition_variant'' as definition_variant,
						w3.local_densities->''area_domain_restrictions'' as area_domain_restrictions,
						w3.local_densities->''sub_population_restrictions'' as sub_population_restrictions
				from
						w3
				)
		,w5 as	(
				select
						w4.new_id,
						w4.object_type_label,
						w4.object_type_description,
						w4.object_label,
						w4.object_description,
						w4.label,
						w4.description,
						w4.use_negative,
						case when (w4.version_label = ''null'' or w4.version_label is null) then $2 else w4.version_label end as version_label,
						case when (w4.version_description = ''null'' or w4.version_description is null) then $3 else w4.version_description end as version_description,
						case when w4.definition_variant::jsonb = ''null''::jsonb then $4 else w4.definition_variant end as definition_variant,
						case when w4.area_domain_restrictions::jsonb = ''null''::jsonb then $5 else w4.area_domain_restrictions end as area_domain_restrictions,
						case when w4.sub_population_restrictions::jsonb = ''null''::jsonb then $5 else w4.sub_population_restrictions end as sub_population_restrictions
				from
						w4
				)
		,w6 as	(
				select
						w5.new_id,
						json_build_object
							(
							''ldsity_type'',json_build_object(''label'',w5.object_type_label,''description'',w5.object_type_description),
							''ldsity_object'',json_build_object(''label'',w5.object_label,''description'',w5.object_description),
							''ldsity'',json_build_object(''label'',w5.label,''description'',w5.description),
							''version'',json_build_object(''label'',w5.version_label,''description'',w5.version_description),
							''definition_variant'',w5.definition_variant,
							''area_domain_restrictions'',w5.area_domain_restrictions,
							''sub_population_restrictions'',w5.sub_population_restrictions,
							''use_negative'',w5.use_negative
							) as  local_densities
				from
						w5
				)
		select
				json_agg(w6.local_densities order by w6.new_id) as local_densities
		from
				w6
		'
		using _target_variable[1], _not_specified_version_label, _not_specified_version_description, _not_specified_defintion_variant, _not_specified_adr_spr
		into _res_division_json;
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	if _check_division_ldsities = 0
	then
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json
		))
		into _res;
	else
		select json_agg(json_build_object
		(
			'indicator',json_build_object('label',_res_indicator_label,'description',_res_indicator_description),
			'state_or_change',json_build_object('label',_res_state_or_change_label,'description',_res_state_or_change_description),
			'unit',json_build_object('label',_res_unit_label,'description',_res_unit_description),
			'area_domain_attribute',_res_area_domain_attribute_json,
			'sub_population_attribute',_res_sub_population_attribute_json,
			'core_contributions',_res_core_json,
			'division_contributions',_res_division_json
		))
		into _res;	
	end if;
	-----------------------------------------------------------------
	-----------------------------------------------------------------
	return _res;
end;
$$
;

comment on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) is
'The function returns metadata (description) for given input ID (that represents target variable for denominator) from t_result_group table.';

grant execute on function nfiesta_results.fn_get_user_metadata_denominator(character varying, integer[]) to public;
-- </function>
--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

insert into nfiesta_results.c_gui_header(header,label_en) values
('region','region'),
('area_domain_category','area domain category'),
('sub_population_category','population category'),
('point_estimate','point estimate'),
('standard_deviation','standard deviation'),
('variation_coeficient','variation coef. %'),
('interval_estimation','interv. estimation');

update nfiesta_results.c_gui_header set label_en = 'area domain classification' where header = 'area_domain_attribute';
update nfiesta_results.c_gui_header set label_en = 'population classification' where header = 'sub_population_attribute';


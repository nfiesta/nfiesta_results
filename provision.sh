# turn off JIT
sudo sed -i "s/#jit = on/jit = off/" /etc/postgresql/12/main/postgresql.conf
sudo sed -i "s/#max_locks_per_transaction = 64/max_locks_per_transaction = 512/" /etc/postgresql/12/main/postgresql.conf
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /etc/postgresql/12/main/postgresql.conf
sudo sed -i "s/all             127.0.0.1\/32/vagrant         172.0.0.0\/8/" /etc/postgresql/12/main/pg_hba.conf
# start postgres
sudo service postgresql start
# create DB user (preferably with same name as Linux user to bypass password provision)
sudo -u postgres psql -c "CREATE USER vagrant SUPERUSER;"
sudo -u postgres psql -c "ALTER USER vagrant WITH PASSWORD 'vagrant';"
sudo -u postgres psql -c "CREATE ROLE adm_nfiesta; CREATE ROLE app_nfiesta; CREATE ROLE app_nfiesta_mng; CREATE ROLE app_joomla; GRANT app_nfiesta TO app_nfiesta_mng;"
# build install extensions
# ===============================nfiesta_results========================
# no need to clone, sources already clonned in current directory (/builds)
sudo make install
# run all automated tests
make installcheck-all
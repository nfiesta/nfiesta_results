--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------


-- get_language_id
select * from nfiesta_results.fn_get_language_id('en-GB'::character varying);
select * from nfiesta_results.fn_get_language_id('cs-CZ'::character varying);


-- get_gui_header
select * from nfiesta_results.fn_get_gui_header('en-GB'::character varying,'indicator'::character varying);
select * from nfiesta_results.fn_get_gui_header('cs-CZ'::character varying,'indicator'::character varying);


-- get_gui_headers
select jsonb_pretty((fn_get_gui_headers)::jsonb) from nfiesta_results.fn_get_gui_headers('cs-CZ'::character varying);
select jsonb_pretty((fn_get_gui_headers)::jsonb) from nfiesta_results.fn_get_gui_headers('en-GB'::character varying);


-- get_user_options_NUMERATOR
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,2) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,2,9) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,2,9,3) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,2,9,3,array[321,322,323,324,1145,1146,1147,1148],true) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,2,9,3,array[321,322,323,324,1145,1146,1147,1148],true,true) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('en-GB'::character varying) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('en-GB'::character varying,2) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('en-GB'::character varying,2,9) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('en-GB'::character varying,2,9,3) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('en-GB'::character varying,2,9,3,array[321,322,323,324,1145,1146,1147,1148],true) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('en-GB'::character varying,2,9,3,array[321,322,323,324,1145,1146,1147,1148],true,true) order by res_label;


-- get_user_options_NUMERATOR_LDSITY_CORE
select * from nfiesta_results.fn_get_user_options_numerator_ldsity_core('cs-CZ'::character varying,array[321,322,323,324,1145,1146,1147,1148]);
select * from nfiesta_results.fn_get_user_options_numerator_ldsity_core('en-GB'::character varying,array[321,322,323,324,1145,1146,1147,1148]);


-- get_user_options_NUMERATOR_AREA_DOMAIN
select * from nfiesta_results.fn_get_user_options_numerator_area_domain('cs-CZ'::character varying,array[321,322,323,324,1145,1146,1147,1148]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_area_domain('cs-CZ'::character varying,array[321,322,323,324,1145,1146,1147,1148],array[0]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_area_domain('en-GB'::character varying,array[321,322,323,324,1145,1146,1147,1148]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_area_domain('en-GB'::character varying,array[321,322,323,324,1145,1146,1147,1148],array[0]) order by res_label;


-- get_user_options_NUMERATOR_SUB_POPULATION
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[321,322,323,324,1145,1146,1147,1148]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[321,322,323,1145,1146,1147],array[1]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[322,323,1146,1147],array[1,50]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[323,1147],array[1,50,55]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[323,1147],array[1,50,55,0]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('en-GB'::character varying,array[321,322,323,324,1145,1146,1147,1148]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('en-GB'::character varying,array[321,322,323,1145,1146,1147],array[1]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('en-GB'::character varying,array[322,323,1146,1147],array[1,50]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('en-GB'::character varying,array[323,1147],array[1,50,55]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('en-GB'::character varying,array[323,1147],array[1,50,55,0]) order by res_label;


-- get_user_options_NUMERATOR_LDSITY_DIVISION
select * from nfiesta_results.fn_get_user_options_numerator_ldsity_division('cs-CZ'::character varying,array[323,1147]);
select * from nfiesta_results.fn_get_user_options_numerator_ldsity_division('en-GB'::character varying,array[323,1147]);


-- get_user_options_NUMERATOR [unit of measurement]
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,2,9,3,array[323,1147],true,true,true) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('en-GB'::character varying,2,9,3,array[323,1147],true,true,true) order by res_label;


-- get_user_options_DENOMINATOR
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_denominator('cs-CZ'::character varying,array[1147]) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_denominator('cs-CZ'::character varying,array[1147],true) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_denominator('en-GB'::character varying,array[1147]) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_denominator('en-GB'::character varying,array[1147],true) order by res_label;


-- get_user_options_DENOMINATOR_LDSITY_CORE
select * from nfiesta_results.fn_get_user_options_denominator_ldsity_core('cs-CZ'::character varying,array[1147]);
select * from nfiesta_results.fn_get_user_options_denominator_ldsity_core('en-GB'::character varying,array[1147]);


-- get_user_options_DENOMINATOR_AREA_DOMAIN
select * from nfiesta_results.fn_get_user_options_denominator_area_domain('cs-CZ'::character varying,array[1147]) order by res_label;
select * from nfiesta_results.fn_get_user_options_denominator_area_domain('cs-CZ'::character varying,array[1147],array[0]) order by res_label;
select * from nfiesta_results.fn_get_user_options_denominator_area_domain('en-GB'::character varying,array[1147]) order by res_label;
select * from nfiesta_results.fn_get_user_options_denominator_area_domain('en-GB'::character varying,array[1147],array[0]) order by res_label;


-- get_user_options_DENOMINATOR_SUB_POPULATION
select * from nfiesta_results.fn_get_user_options_denominator_sub_population('cs-CZ'::character varying,array[1147]) order by res_label;
select * from nfiesta_results.fn_get_user_options_denominator_sub_population('cs-CZ'::character varying,array[1147],array[1]) order by res_label;
select * from nfiesta_results.fn_get_user_options_denominator_sub_population('cs-CZ'::character varying,array[1147],array[1,0]) order by res_label;
select * from nfiesta_results.fn_get_user_options_denominator_sub_population('en-GB'::character varying,array[1147]) order by res_label;
select * from nfiesta_results.fn_get_user_options_denominator_sub_population('en-GB'::character varying,array[1147],array[1]) order by res_label;
select * from nfiesta_results.fn_get_user_options_denominator_sub_population('en-GB'::character varying,array[1147],array[1,0]) order by res_label;


-- get_user_options_DENOMINATOR_LDSITY_DIVISION
select * from nfiesta_results.fn_get_user_options_denominator_ldsity_division('cs-CZ'::character varying,array[1147]);
select * from nfiesta_results.fn_get_user_options_denominator_ldsity_division('en-GB'::character varying,array[1147]);


-- get_user_query
select * from nfiesta_results.fn_get_user_query('cs-CZ'::character varying, array[1147]);
select * from nfiesta_results.fn_get_user_query('en-GB'::character varying, array[1147]);


-- get_user_metadata
select jsonb_pretty(fn_get_user_metadata::jsonb) from nfiesta_results.fn_get_user_metadata('cs-CZ'::character varying, array[1147]);
select jsonb_pretty(fn_get_user_metadata::jsonb) from nfiesta_results.fn_get_user_metadata('en-GB'::character varying, array[1147]);


-- priklad "nadzemni biomasa", kde bude jen jedna karticka u CORE v citateli, ale v ni budou 4 prispevky
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5,2) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5,2,1) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5,2,1,array[854,652,653,654],true) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5,2,1,array[652,653,654,854],true,true) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_ldsity_core('cs-CZ'::character varying,array[652,653,654,854]);
select * from nfiesta_results.fn_get_user_options_numerator_area_domain('cs-CZ'::character varying,array[652,653,654]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_area_domain('cs-CZ'::character varying,array[653,654],array[0]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[653,654]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[653],array[44]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[653],array[44,0]) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5,2,1,array[653],true,true,true) order by res_label;
select * from nfiesta_results.fn_get_user_query('cs-CZ'::character varying, array[653]);
select jsonb_pretty(fn_get_user_metadata::jsonb) from nfiesta_results.fn_get_user_metadata('cs-CZ'::character varying, array[653]);


-- priklad "nadzemni biomasa", kde bude vice karticek u CORE v citateli
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5,1) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5,1,1) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5,1,1,array[444,445,446,447,460,461,462,463,722,721,720,728,727,726,725,724,723],true) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5,1,1,array[720,721,722,723,724,725,726,727,728,444,445,446,447,460,461,462,463],true,true) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_ldsity_core('cs-CZ'::character varying,array[720,721,722,723,724,725,726,727,728,444,445,446,447,460,461,462,463]);
select * from nfiesta_results.fn_get_user_options_numerator_area_domain('cs-CZ'::character varying,array[444,445,446,447]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_area_domain('cs-CZ'::character varying,array[444,445,446,447],array[0]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[444,445,446,447]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[444,446],array[44]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[446],array[44,59]) order by res_label;
select * from nfiesta_results.fn_get_user_options_numerator_sub_population('cs-CZ'::character varying,array[446],array[44,59,0]) order by res_label;
select res_id, res_label, res_id_group from nfiesta_results.fn_get_user_options_numerator('cs-CZ'::character varying,5,1,1,array[446],true,true,true) order by res_label;
select * from nfiesta_results.fn_get_user_query('cs-CZ'::character varying, array[446]);
select jsonb_pretty(fn_get_user_metadata::jsonb) from nfiesta_results.fn_get_user_metadata('cs-CZ'::character varying, array[446]);

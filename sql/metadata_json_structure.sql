/*
Informace pro zobrazeni:

- na webu se bodou zobrazovat elementy label a description:
	- element label predstavuje metadatovy nazev
	- element description predstavuje metadatovy popis
- tam, kde je znak + bude moznost rozbaleni (rozbalenou cast bude tvorit vzdy prislusny element value)

- kdyz se rozbali METADATA, tak pujde o prvni uroven, kde bude zobrazen:
	- label a description pro: topic, period, geographic region, unit of measurement,
	- label numerator s moznosti rozbaleni
	- label denominator s moznosti rozbaleni, pokud tento element v JSONu existuje
	
- denominator ma stejnou strukturu jako numerator, takze dale popisu jen urovne pro numeratora

- kdyz se rozbali Citatel, tak pujde o druhou uroven, kde bude z elementu value zobrazen:
	- label a description pro: indicator, state or change, unit of indicator
	- label area domain attributes s moznosti rozbaleni
	- label sub population attributes s moznosti rozbaleni
	- label local densities main s moznosti rozbaleni
	- label local densities separated s moznosti rozbaleni, pokud tento element v JSONu existuje
	
- treti urovne rozbaleni jsou tedy: area domain attributes, sub population attributes, local densities main a pripadne local densities separated
	- u area domain attributes a sub population attributes se po rozbaleni pod sebou zobrazi descriptions z elementu value
	- u local densities main a local densities separated se po rozbaleni z elementu value zobrazi:
		- label a description pro: name, object, version, use negative (tohle muze byt reseno znakem plus nebo minus pred name)
		- label definition variants s moznosti rozbaleni
		- label are domain restrictions s moznosti rozbaleni
		- label sub population restrictions moznosti rozbaleni
		
- ctvrtou urovni rozbaleni jsou tedy: definition variants, area domain restrictions a sub population restrictions
	- u definition variants se po rozbaleni pod sebou zobrazi descriptions z elementu value
	- u area domain restrictions a sub population restrictions se pod sebou zobrazi radky (zde jsou mozne dve varianty zobrazeni):
		- 1. varianta => label a descriptions pro object a label a description pro restriction
		- 2. varianta => nebo elementy label budou figuovat jako sloupce a pod sebou budou objekty a omezeni
*/

{
	"topic" : {"label" : "Tematicky okruh:", "description" : "Lesni zdroje podle Forest Europe."},
	"period" : {"label" : "Obdobi:", "description" : "Od 1. ledna 2016 do 31. prosince 2020, odpovida NIL2."},
	"geographic region" : {"label" : "Geograficke cleneni:", "description" : "Nomenklatura uzemnich statistickych jednotek 3. uroven � kraj."},
	"unit of measurement" : {"label" : "Jednotka vvystupu:", "description" : "Kubick� metr (metr krychlovy) s kurou."},
	"numerator" :	{
					"label" : "Citatel +",
					"value" :	{
								"indicator" : {"label" : "Indikator:", "description" : "Plocha porostni pudy obnovy."},
								"state or change" : {"label" : "Stav nebo zmena:", "description" : "Stav."},
								"unit of indicator" : {"label" : "Jednotka indikatoru:", "description" : "Hektar."},
								"area domain attributes" : {"label" : "Plosna atributova cleneni +", "value" : [{"description" : "description of area domain attribute 1"}, {"description" : "description of area domain attribute 2"}, {"description" : "description of area domain attribute 3"}]},
								"sub population attributes" : {"label" : "Sub populacni atributova cleneni +", "value" : [{"description" : "description of sub population attribute 1"}, {"description" : "description of sub population attribute 2"}]},
								"local densities main" :	{
															"label" : "Prispevky zakladni +",
															"value" :	[
																		{
																		"name" : {"label" : "Prispevek:", "description" : "Objem jedincu nehroubi"},
																		"object" : {"label" : "Objekt:", "description" : "jedinci nehroubi, vyska od 10 cm, d13 pod 7 cm s k."},
																		"version" : {"label" : "Verze:", "description" : "Bez korekce dat na zaklade nasledujici inventarizace."},
																		"use negative" : {"label" : "Pouziti zaporu:", "description" : "false"},
																		"definition variants" :	{
																								"label" : "Definicni varianty +",
																								"value" :	[
																											{"description" : "description of definition variant 1"},
																											{"description" : "description of definition variant 2"},
																											{"description" : "description of definition variant 3"}
																											]
																								},
																		"area domain restrictions" :	{
																										"label" : "Plosna omezeni +",
																										"value" :	[
																													{
																													"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																													"restriction" : {"label" : "Omezeni:", "description" : "Kategorie pozemku FAO FRA - les."}
																													},
																													{
																													"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																													"restriction" : {"label" : "Omezeni:", "description" : "Pristupna a schudna cast."}
																													}
																													]
																										},
																		"sub population restrictions" : {
																										"label" : "Sub populacni omezeni +",
																										"value" :
																													[
																													{
																													"object" : {"label" : "Objekt:", "description" : "Kmeny jedincu hroubi."},
																													"restriction" : {"label" : "Omezeni:", "description" : "Zive kmeny (stromy) s vycetni tloustkou (d13) dosahujici alespon 7 cm pri mereni s kurou."}
																													}
																													]
																										}
																		}
																		]
															},
								"local densities separated" :	{
																"label" : "Prispevky clenici +",
																"value" :	[
																			{
																			"name" : {"label" : "Prispevek:", "description" : "Objem jedincu nehroubi"},
																			"object" : {"label" : "Objekt:", "description" : "jedinci nehroubi, vyska od 10 cm, d13 pod 7 cm s k."},
																			"version" : {"label" : "Verze:", "description" : "Bez korekce dat na zaklade nasledujici inventarizace."},
																			"use negative" : {"label" : "Pouziti zaporu:", "description" : "false"},
																			"definition variants" :	{
																									"label" : "Definicni varianty +",
																									"value" :	[
																												{"description" : "description of definition variant 1"},
																												{"description" : "description of definition variant 2"},
																												{"description" : "description of definition variant 3"}
																												]
																									},
																			"area domain restrictions" :	{
																											"label" : "Plosna omezeni +",
																											"value" :	[
																														{
																														"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Kategorie pozemku FAO FRA - les."}
																														},
																														{
																														"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Pristupna a schudna cast."}
																														}
																														]
																											},
																			"sub population restrictions" :	{
																											"label" : "Sub populacni omezeni +",
																											"value" :	[
																														{
																														"object" : {"label" : "Objekt:", "description" : "Kmeny jedincu hroubi."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Zive kmeny (stromy) s vycetni tloustkou (d13) dosahujici alespon 7 cm pri mereni s kurou."}
																														}
																														]
																											}
																			},
																			{
																			"name" : {"label" : "Prispevek:", "description" : "Objem jedincu nehroubi"},
																			"object" : {"label" : "Objekt:", "description" : "jedinci nehroubi, vyska od 10 cm, d13 pod 7 cm s k."},
																			"version" : {"label" : "Verze:", "description" : "Bez korekce dat na zaklade nasledujici inventarizace."},
																			"use negative" : {"label" : "Pouziti zaporu:", "description" : "false"},
																			"definition variants" :	{
																									"label" : "Definicni varianty +",
																									"value" :	[
																												{"description" : "description of definition variant 1"},
																												{"description" : "description of definition variant 2"},
																												{"description" : "description of definition variant 3"}
																												]
																									},
																			"area domain restrictions" :	{
																											"label" : "Plosna omezeni +",
																											"value" :	[
																														{
																														"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Kategorie pozemku FAO FRA - les."}
																														},
																														{
																														"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Pristupna a schudna cast."}
																														}
																														]
																											},
																			"sub population restrictions" :	{
																											"label" : "Sub populacni omezeni +",
																											"value" :	[
																														{
																														"object" : {"label" : "Objekt:", "description" : "Kmeny jedincu hroubi."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Zive kmeny (stromy) s vycetni tloustkou (d13) dosahujici alespon 7 cm pri mereni s kurou."}
																														}
																														]
																											}
																			}
																			]
																}
								}
					},
	"denominator" :	{
					"label" : "Jmenovatel +",
					"value" :	{
								"indicator" : {"label" : "Indikator:", "description" : "Plocha porostni pudy obnovy."},
								"state or change" : {"label" : "Stav nebo zmena:", "description" : "Stav."},
								"unit of indicator" : {"label" : "Jednotka indikatoru:", "description" : "Hektar."},
								"area domain attributes" :	{
															"label" : "Plosna atributova cleneni +",
															"value" :	[
																		{"description" : "description of area domain attribute 1"},
																		{"description" : "description of area domain attribute 2"},
																		{"description" : "description of area domain attribute 3"}
																		]
															},
								"sub population attributes" :	{
																"label" : "Sub populacni atributova cleneni +",
																"value" :	[
																			{"description" : "description of sub population attribute 1"},
																			{"description" : "description of sub population attribute 2"}
																			]
																},
								"local densities main" :	{
															"label" : "Prispevky zakladni +",
															"value" :	[
																		{
																		"name" : {"label" : "Prispevek:", "description" : "Objem jedincu nehroubi"},
																		"object" : {"label" : "Objekt:", "description" : "jedinci nehroubi, vyska od 10 cm, d13 pod 7 cm s k."},
																		"version" : {"label" : "Verze:", "description" : "Bez korekce dat na zaklade nasledujici inventarizace."},
																		"use negative" : {"label" : "Pouziti zaporu:", "description" : "false"},
																		"definition variants" :	{
																								"label" : "Definicni varianty +",
																								"value" :	[
																											{"description" : "description of definition variant 1"},
																											{"description" : "description of definition variant 2"},
																											{"description" : "description of definition variant 3"}
																											]
																								},
																		"area domain restrictions" :	{
																										"label" : "Plosna omezeni +",
																										"value" :	[
																													{
																													"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																													"restriction" : {"label" : "Omezeni:", "description" : "Kategorie pozemku FAO FRA - les."}
																													},
																													{
																													"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																													"restriction" : {"label" : "Omezeni:", "description" : "Pristupna a schudna cast."}
																													}
																													]
																										},
																		"sub population restrictions" :	{
																										"label" : "Sub populacni omezeni +",
																										"value" :	[
																													{
																													"object" : {"label" : "Objekt:", "description" : "Kmeny jedincu hroubi."},
																													"restriction" : {"label" : "Omezeni:", "description" : "Zive kmeny (stromy) s vycetni tloustkou (d13) dosahujici alespon 7 cm pri mereni s kurou."}
																													}
																													]
																										}
																		}
																		]
															},
								"local densities separated" :	{
																"label" : "Prispevky clenici +",
																"value" :	[
																			{
																			"name" : {"label" : "Prispevek:", "description" : "Objem jedincu nehroubi"},
																			"object" : {"label" : "Objekt:", "description" : "jedinci nehroubi, vyska od 10 cm, d13 pod 7 cm s k."},
																			"version" : {"label" : "Verze:", "description" : "Bez korekce dat na zaklade nasledujici inventarizace."},
																			"use negative" : {"label" : "Pouziti zaporu:", "description" : "false"},
																			"definition variants" :	{
																									"label" : "Definicni varianty +",
																									"value" :	[
																												{"description" : "description of definition variant 1"},
																												{"description" : "description of definition variant 2"},
																												{"description" : "description of definition variant 3"}
																												]
																									},
																			"area domain restrictions" :	{
																											"label" : "Plosna omezeni +",
																											"value" :	[
																														{
																														"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Kategorie pozemku FAO FRA - les."}
																														},
																														{
																														"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Pristupna a schudna cast."}
																														}
																														]
																											},
																			"sub population restrictions" :	{
																											"label" : "Sub populacni omezeni +",
																											"value" :	[
																														{
																														"object" : {"label" : "Objekt:", "description" : "Kmeny jedincu hroubi."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Zive kmeny (stromy) s vycetni tloustkou (d13) dosahujici alespon 7 cm pri mereni s kurou."}
																														}
																														]
																											}
																			},
																			{
																			"name" : {"label" : "Prispevek:", "description" : "Objem jedincu nehroubi"},
																			"object" : {"label" : "Objekt:", "description" : "jedinci nehroubi, vyska od 10 cm, d13 pod 7 cm s k."},
																			"version" : {"label" : "Verze:", "description" : "Bez korekce dat na zaklade nasledujici inventarizace."},
																			"use negative" : {"label" : "Pouziti zaporu:", "description" : "false"},
																			"definition variants" :	{
																									"label" : "Definicni varianty +",
																									"value" :	[
																												{"description" : "description of definition variant 1"},
																												{"description" : "description of definition variant 2"},
																												{"description" : "description of definition variant 3"}
																												]
																									},
																			"area domain restrictions" :	{
																											"label" : "Plosna omezeni +",
																											"value" :	[
																														{
																														"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Kategorie pozemku FAO FRA - les."}
																														},
																														{
																														"object" : {"label" : "Objekt:", "description" : "Segmenty inventarizacniho kruhu."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Pristupna a schudna cast."}
																														}
																														]
																											},
																			"sub population restrictions" :	{
																											"label" : "Sub populacni omezeni +",
																											"value" :	[
																														{
																														"object" : {"label" : "Objekt:", "description" : "Kmeny jedincu hroubi."},
																														"restriction" : {"label" : "Omezeni:", "description" : "Zive kmeny (stromy) s vycetni tloustkou (d13) dosahujici alespon 7 cm pri mereni s kurou."}
																														}
																														]
																											}
																			}
																			]
																}
								}
					}
}

